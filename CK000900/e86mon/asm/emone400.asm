comment ~
/******************************************************************************
 *                                                                            *
 *     EMONe400.ASM                                                           *
 *                                                                            *
 *     This file contains SC400-specific EMON Startup code.                   *
 *     It MUST be linked first in the link map.                               *
 *                                                                            *
 *     NOTE:  This file is not yet ready to be used (SC400 EMON port          *
 *            is not done and will require further changes.)                  *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
 end comment ~

        title   emone400.asm - 486 Startup Module for EMON

        .486p



_TEXT   segment para public use16 'CODE'

        assume ds:nothing,es:nothing,ss:nothing

extern  CPUIndependentSetup :NEAR       ; Next level of initialization
extern  OriginalCodeSeg: WORD
extern  _PermArray:NEAR                 ; Permanent variable array

;***********************************************************************
;
;    STARTUP()   -- Main program entry point, must be at file offset 0
;
STARTUP proc near public

; Point to our permanent variable record so the binary may be edited

        jmp     short @F
        db      "AMD LPD 01"
        dw      _PermArray
@@:
; In case we jumped here, instead of a real reset, make sure we
; initialize with interrupts off and direction forward.
;
        cli
        cld
HWInitEnd LABEL BYTE

;
; If this is a copy of the monitor which was accidentally left in RAM,
; don't execute it, as not all the variables will be set up properly.

        cmp     OriginalCodeSeg,0
        jz      @F
        retf
@@:

	xor	ax,ax
	mov	ds,ax
	mov	es,ax

; Cache doesn't work unless it thinks the area is cacheable.  Pretend 
; like we know we have DRAM.

        mov     ax,5       ; Disable refresh
        out     22h,ax
        mov     ax,0A600h
        out     22h,ax     ; Arbitrary "enabled" value on bank 0

; Trash all the cache entries, then enable the cache

	invd
        mov     eax, cr0
        and     eax, not(60000000h)
        mov     cr0, eax

; Now the cache is enabled, and empty, and not mapped anywhere.  Force
; it to map down to 0-8191 by reading one word from every cache line.

	mov	di,0
	mov     cx,16
@@:
	mov	ax,ds:[di]
	add	di,16
	and     di,8192-1
	jnz     @B
	loop    @B

; Disable the cache.  This keeps it from shuffling data in and out,
; so now it is just like an 8K static RAM at location 0.

        mov     eax, cr0
        or      eax, 60000000h
        mov     cr0, eax

; Disable the DRAM bank

        mov     ax,0
	out	22h,ax

; Zero out our static RAM

	mov	ax,0
	mov	di,0
	mov	cx,4096
	rep	stosw

; Set up a stack at the top of our cache SRAM

	xor	ax,ax
	mov	ss,ax
	mov	sp,8192

; Enable A20 propagation

	in	al,0EEH

	lea	si,E4_Reg_Initial_Values
	call    OutIndex

	lea	si,Int_And_DMA_Initial_Values
	mov	cx,Num_Int_And_DMA_Initial_Values
	mov	dh,0
@@:
	mov	dl,cs:[si]
	inc	si
	mov	al,cs:[si]
	inc	si
	out	dx,al
	dec	cx
	jnz	@B

	lea	si,SuperIO_Reg_Initial_Values
	call    OutIndex

        mov     bx,8192
        jmp     CPUIndependentSetup
STARTUP  ENDP



Int_And_DMA_Initial_Values:

	; DMA controllers

	db	008h,000h		; Reset
	db	0D0h,000h
	db	040h,00Bh		; Channel 0
	db	0C0h,0D6h		; Channel 4 cascade
	db	041h,00Bh		; Channel 1
	db	041h,0D6h		; Channel 5
	db	042h,00Bh		; Channel 2
	db	042h,0D6h		; Channel 6
	db	043h,00Bh		; Channel 3
	db	043h,0D6h		; Channel 7


	; Primary interrupt controller

	db	020h,11h		; ICW1
	db	021h,08h		; ICW2
	db	021h,04h		; ICW3
	db	021h,01h		; ICW4
	db	021h,0FFh		; Mask

	; Slave interrupt controller

	db	0A0h,11h		; ICW1
	db	0A1h,70h		; ICW2
	db	0A1h,02h		; ICW3
	db	0A1h,01h		; ICW4
	db	0A1h,0FFh		; Mask

Num_Int_And_DMA_Initial_Values EQU ($ - Int_And_DMA_Initial_Values) / 2


E4_Reg_Initial_Values:

	dw	22h,Num_E4_Initial_Values

        db      024h, 11111111b, 002h   ; 2 non-burst wait states
        db      023h, 00110000b, 030h   ; FastROM0, CS_EARLY0

        ;Set DRAM timing as slow as possible

        db      004h, 11011111b, 01011100b

        ;Disable refresh, use 32khz osc for refresh

        db      005h, 01111111b, 000h

	;  Clear all DRAM banks -- sizing algorithm reenables
	;  them one at a time

        db      000h, 11111111b, 000h
        db      001h, 11111111b, 000h
        db      002h, 11111111b, 000h
        db      003h, 11111111b, 000h

        ;If operating at 2.7 volts, add wait state to MMU DRAM hits

;;;     db      014h, 01111111b, 020h

        ;Enable E000 seg for linear decode, disable 3ff0000 boot vector

;;;     db      021h, 11111111b, 009h

        ;Configure romcs1 for 16 bit app rom interface

;;;     db      025h, 01111110b, 004h

        ;slect BL0, SMB pins, iocs16, iochrdy, pirq0+1, aen + tc

        db      038h, 11111111b, 07Fh

        ;VPPx/VCCx for pcmcia slot A+B, LBL2 pin, slct pcmcia slot B signals

;;;     db      039h, 01111111b, 071h    ; For keyboard
        db      039h, 01111111b, 075h    ; For ISA MCS16, SBHE, BALE, PIRQ2,
                                         ; PDRQ1, PDACK1~

        ;Suspend termination configured to support matrix kbd

;;;     db      03Ah, 00000010b, 000h    ; For keyboard
        db      03Ah, 00000010b, 002h    ; For PIRQ7 - PIRQ3

        ;Ena sus/res on falling edge of sus/res pin

        db      050h, 00001111b, 007h

        ;cpu clk:high spd mode=33mhz, low spd mode = 8 mhz

        db      080h, 00011111b, 004h

        ;DMA clock = 4mhz or cpu clk (whichever is slower)

;;;     db      082h, 11111111b, 020h     ; PLLs disabled in suspend mode
        db      082h, 11111111b, 021h     ; PLLs enabled in suspend mode

        ;Enable External Keybd I/F - JSE

        db      0C1h, 11111100b, 08h


        ;Enable PCMCIA controller

        db      0D0h, 00111111b, 002h
;;;     db      0D0h, 00111111b, 032h     ; Echo all cycles to ISA bus

        ;Enable internal UART at 3f8

        db      0D1h, 00001111b, 001h

        ;Route pirq0->IRQ14 (ide support) and pirq1->IRQ1 (kbd support)

        db      0D4h, 11111111b, 01Eh

        ;Route pirq6->IRQ6 (floppy support when matx kbd not used)

        db      0D7h, 11111111b, 006h

        ;Internal UART uses IRQ4

        db      0D8h, 11111111b, 040h

        ;Route DMA0 to IrDA, pdma0 pin to dma2 for external floppy support

        db      0DBh, 11111111b, 043h

        ;Enable 2 slot pcmcia mode (enhanced mode)

        db      0F1h, 11111111b, 001h

Num_E4_Initial_Values EQU ($ - E4_Reg_Initial_Values - 4) / 3

SuperIO_Reg_Initial_Values:

	dw	398h,Num_SuperIO_Initial_Values

        db     000h,  11111111b, 01001010b  ; Enable UART, FDC, IDE
        db     001h,  11001100b, 00000011b  ; Set uart 1 to base 2E8
        db     005h,  11111111b, 00000101b  ; Enable keyboard, disable RTC
        db     00Ah,  11111111b, 80h        ; CS 0 address low
        db     010h,  11111111b, 06h        ; CS 0 address high
        db     00Bh,  10111000b, 90h        ; CS 0 enable
        db     00Ch,  11111111b, 80h        ; CS 1 address low
        db     011h,  11111111b, 00h        ; CS 1 address high
        db     00Dh,  00111000b, 90h        ; CS 1 enable


Num_SuperIO_Initial_Values EQU ($ - SuperIO_Reg_Initial_Values - 4) / 3


;*******************************************
; OutIndex PROC NEAR
;
; Passed:
;    SI = Pointer to index/mask/value table
;
; Returns:
;    Nothing
;
; Destroys:
;    AX, CX, DX, SI, flags
;
;  OutIndex assumes that an 8-bit data port immediately
;  follows an 8-bit index port.  It outputs the index,
;  reads the current data, ANDs it with NOT mask, ORs
;  the result with value AND mask, and then outputs the
;  result to the data port.
;*******************************************

OutIndex PROC NEAR

	mov	dx,cs:[si]
	mov	cx,cs:[si+2]
	add	si,4

        in      al,dx			; "Unlock" sequence required by
        in      al,dx			; SuperIO chip
@@:
        mov     ax, cs:[si]             ; get index into falcon indexed regs
                                        ; and mask to AH
        out     dx,al                  ; Output index
        inc     dx
        in      al,dx
        not     ah                      ; Get save mask
        and     al,ah
        not     ah                      ; Get replace mask
        and     ah,cs:[si+2]
        add     si,3
        or      al,ah
        out     dx,al                   ; Must do twice for SuperIO
        out     dx,al
        dec     dx
        loop    @B
        ret
OutIndex ENDP




;***********************************************************************
; HWInitSize returns the size of the initialization block which
; must be installed at monitor update.

_HWInitSize     PROC NEAR PUBLIC
        lea     ax,HWInitEnd
        ret
_HWInitSize     ENDP

;***********************************************************************
; SegToLin converts a segmented address into a linear address
; It assumes a wrap at 1MB, which is perfectly valid for the 186

SegToLin PROC NEAR PUBLIC
        pop     bx              ; return address
        pop     ax              ; offset
        pop     dx              ; segment
        rol     dx,4
        mov     cx,dx
        and     dx,0Fh
        xor     cx,dx
        add     ax,cx
        adc     dx,0
        and     dx,0Fh          ; Wrap at 1MB
        jmp     bx
SegToLin ENDP

;***********************************************************************
; LinToSeg converts a linear address into a segmented address.
; It assumes a wrap at 1MB, which is perfectly valid for the 186

LinToSeg PROC NEAR PUBLIC
        pop     bx              ; return address
        pop     ax              ; low part
        pop     dx              ; high part
        mov     cx,ax
        and     ax,0Fh
        xor     cx,ax
        and     dx,0Fh
        or      dx,cx
        ror     dx,4
        jmp     bx
LinToSeg ENDP

;***********************************************************************
; RunningFromRAM returns TRUE if we are running out of RAM,
; FALSE otherwise.

_RunningFromRAM PROC NEAR PUBLIC
        mov     ax,cs
        shl     ax,1             ; Top bit to carry
        sbb     ax,ax            ; 0 for RAM, -1 for ROM
        inc     ax               ; 1 for RAM, 0 for ROM
        ret
_RunningFromRAM  ENDP

IRetVec:	iret


_TEXT   ends

end        STARTUP



