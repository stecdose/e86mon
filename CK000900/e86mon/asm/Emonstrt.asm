comment ~
/******************************************************************************
 *                                                                            *
 *     EMONSTRT.ASM                                                           *
 *                                                                            *
 *     This file contains CPU-independent (for an X86) startup                *
 *     code, and routines which would be difficult to do in C.                *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996-1997 Advanced Micro Devices, Inc.                           *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
 end comment ~

        title   emonstrt.asm - Assembly Language Startup Module for EMON

        .186              ; Least common denominator processor

;***********************************************************************
;     P U B L I C   A N D   E X T E R N A L   D E F I N I T I O N S
;***********************************************************************

public DummyStackRec
public OriginalCodeSeg

extern  EMonInit  :NEAR         ; C -level startup code
extern  InitGlobals: NEAR       ; C-level one-time initialization
extern  _IrqEntry :NEAR         ; C -level main entry
extern  SegToLin  :NEAR         ; Converts a segment into a linear address

;***********************************************************************
;                      S T R U C T U R E S
;***********************************************************************

;***********************************************************************
;  DoubleWord structure
;
;  Defines the decomposition of a double word or far pointer into
;  two words.
;***********************************************************************

DoubleWord        STRUCT
        LowW    dw     ?
        HighW   dw     ?
DoubleWord        ENDS

;***********************************************************************
;  StackRec structure
;
;  Defines the order in which our interrupt thunks push registers
;  onto the stack.
;***********************************************************************

StackRec        STRUCT
        _ES     dw      ?            ; Interrupted code's ES, DS
        _DS     dw      ?            
        _Gen    dw      8 dup (?)    ; Interrupted code's general purpose regs
        _IP     dw      ?            ; Interrupted code's IP, CS, and flags
        _CS     dw      ?
        _Flags  dw      ?

        _IType  dw      ?            ; Interrupt type (vector number)
        _SP     dw      ?            ; Original SS:SP, if stack changed
        _SS     dw      ?
StackRec        ENDS
  

;***********************************************************************
;     S E G M E N T   O R D E R I N G   /   G R O U P I N G
;***********************************************************************

;   One of the functions of this file is to define the ordering of
;   the segments in the link.  This is done simply by declaring
;   them in the correct order.

_TEXT      segment para public 'CODE'
_TEXT      ends

_MONINT3   segment byte public 'CODE'
_MONINT3   ends

_ENDTEXT   segment para public 'CONST'
EndCode:
_ENDTEXT   ends

_DATA       segment para public 'DATA'          ; initialized data

StartData	dd	StartData	; Help Makehex figure out where
                                        ; data starts
_DATA       ends

CODE_BSS        segment para public 'BSS' 

;; Make sure we fill all the way to a 16 byte boundary

        db    16 dup (42h)
EndTotalROM:
CODE_BSS        ends

_BSS        segment para public 'BSS' 
EndInitializedData:
_BSS        ends

_STACK      segment para stack 'STACK'
EndStack  EQU $+STKSIZE
_STACK      ends

CGROUP  group   _TEXT, _MONINT3, _ENDTEXT
CGROUP  group   CODE_BSS
DGROUP  group   _STACK, _DATA, _BSS


;***********************************************************************
;          U N I N I T I A L I Z E D   D A T A   S E G M E N T
;***********************************************************************

_BSS        segment

extern BigBufOffset:      WORD
extern BigBufParaOffset:  WORD
extern RestartCodeOffset: WORD

MonInt3LinAddress      DoubleWord <>

_BSS        ends

;***********************************************************************
;                 C O D E   S E G M E N T
;***********************************************************************

_TEXT   segment

        assume ds:DGROUP,es:nothing,ss:nothing

DummyStackRec:

OriginalCodeSeg    dw   0       ; 0 if running from original code seg
                                ; or value of original if moved

;***********************************************************************
;  CheckSum    Near public Pascal proc
;
;  Passed:     Pointer, followed by WORD count
;  Returns:    DX:AX = Checksum
;  Destroys:   All except SI,DI
;***********************************************************************

CheckSum  PROC NEAR PUBLIC
        pop     ax
        pop     cx                          ; Count
        pop     bx                          ; Offset
        pop     es                          ; Segment
        push    ax                          ; Return address

        push    si
        mov     si,bx
        xor     bx,bx
        xor     dx,dx
@@:
        mov     ah,0
        lods    byte ptr es:[si]

        rol     ax,cl
        add     bx,ax
        adc     dx,0
        loop    @B

        mov     ax,bx
        pop     si
        ret
CheckSum  ENDP

;***********************************************************************
;  CurrentDS    Near public Pascal proc
;
;  Passed:     Nothing
;  Returns:    AX = Current code segment
;  Destroys:   AX
;***********************************************************************

CurrentDS  PROC NEAR PUBLIC
        mov     ax,ds
        retn
CurrentDS  ENDP

;***********************************************************************
;  OriginalCS    Near public Pascal proc
;
;  Passed:     Nothing
;  Returns:    AX = Original monitor code segment
;  Destroys:   AX, flags
;
;  The original monitor code segment will be the same as the current
;  CS, unless the monitor has been temporarily relocated to RAM
;  in order to program the flash.
;***********************************************************************

OriginalCS PROC NEAR PUBLIC
        mov     ax,cs:OriginalCodeSeg
        or      ax,ax
        jnz     @F
        mov     ax,cs
@@:
        ret
OriginalCS ENDP

;***********************************************************************
;  MonitorROMSize    Near public Pascal proc
;
;  Passed:     Nothing
;  Returns:    AX = Space the monitor occupies in ROM, including
;                   code, initialized data, and relocation table (if any)
;              BX = Points to EndTotalROM location
;  Destroys:   AX,BX
;***********************************************************************

MonitorROMSize PROC NEAR PUBLIC
        mov     bx, offset CGROUP:EndTotalROM
        mov     ax,cs:[bx]
        inc     ax
        add     ax, bx
        ret
MonitorROMSize ENDP

;***********************************************************************
;  MonitorRAMSize    Near public Pascal proc
;
;  Passed:     Nothing
;  Returns:    AX = Space the monitor occupies in RAM
;  Destroys:   AX,BX
;***********************************************************************

MonitorRAMSize PROC NEAR PUBLIC
        lea     ax,EndStack
        ret
MonitorRAMSize ENDP

;***********************************************************************
;  SetMonInt3LinAddress    Near proc
;
;  Passed:     Nothing
;  Returns:    Nothing
;  Destroys:   AX-DX, Flags
;
; SetMonInt3LinAddress develops the linear address for the monitor's
; interrupt 3 entry point.  This is used to calculate what the value
; of an unexpected interrupt is.
;
;***********************************************************************

SetMonInt3LinAddress PROC NEAR
        push     cs
        push     offset CGROUP:PastMonInt3
        call     SegToLin
        mov     MonInt3LinAddress.LowW,ax
        mov     MonInt3LinAddress.HighW,dx
        ret
SetMonInt3LinAddress ENDP

;***********************************************************************
;  ApplyCodeRelocations    Near proc
;
;  Passed:     AX = Code segment to relocate to
;              DS -> Data segment
;  Returns:    Nothing
;  Destroys:   AX,BX,CX,DX, Flags
;
; ApplyCodeRelocations stores our code segment in every location
; pointed to by the code relocation table.
;
;***********************************************************************

ApplyCodeRelocations PROC NEAR
        
        push    di
        mov     dx,ax                ; Save new CS value to DX
        call    MonitorROMSize
        sub     ax,bx

RelocLoop:
        sub     ax,4
        jbe     FinishedReloc
        add     bx,4

        mov     di,cs:[bx]
        mov     ds:[di],dx
        cmp     word ptr cs:[bx+2],0
        jz      RelocLoop
        mov     ds:[di],ds
        jmp     RelocLoop

FinishedReloc:
        pop     di
        ret
ApplyCodeRelocations ENDP

;***********************************************************************
;  restart    Near public Pascal proc
;
;  Passed:     One word (restart code) on stack
;  Returns:    Doesn't!  Restarts the monitor
;  Destroys:   The world
;
;  restart switches to the "user" stack, which is at the
;  end of BigBuffer, and calls the C language EMonInit
;  with the code that was passed in.  When EMonInit
;  returns, restart sets up in a loop to perform a
;  trace interrupt, which invokes the debugger command line.
;***********************************************************************

restart PROC NEAR PUBLIC
        cli
        cld
        pop     ax              ; Return address
        pop     ax              ; RestartCode
        mov     bx,RestartCodeOffset
        mov     [bx],ax

        ; Make sure we're running on the monitor stack when
        ; we call EMonInit

        push    ds
        pop     ss
        lea     sp,EndStack

        call    EMonInit       ; with 0 for first time, 1 for subsequent

        ; Switch over to our default application stack before
        ; we execute the first int 1 to enter the monitor.

        mov     bx,BigBufOffset
        mov     ax,[bx+2]          ; starting paragraph
        mov     es,ax
        mov     bx,BigBufParaOffset
        add     ax,[bx]
        sub     ax,STKSIZE/16
        mov     ss,ax
        mov     sp,STKSIZE AND 0FFF0h

        sti

        xor     ax,ax
        xor     bx,bx
        xor     cx,cx
        xor     dx,dx
        xor     si,si
        xor     di,di
        xor     bp,bp
        push    es
        pop     ds
@@:
        pushf
        push    cs
        push    OFFSET CGROUP:@B
        jmp     MonitorInt3

restart ENDP

;***********************************************************************
;  CPUIndependentSetup    Near public assembly proc
;
;  Passed:     BX == RAM Size in paragraphs
;  Returns:    Doesn't!  Jumps to restart with 0 on stack
;  Destroys:   The world
;
;  CPU-specific initialization code jumps here at reset.  This
;  code sets up the segment registers and stack pointer,
;  moves the initialized data from ROM to RAM, zeroes the
;  uninitialized data, sets up internal variables, and then
;  jumps to the restart routine.
;***********************************************************************

CPUIndependentSetup PROC NEAR PUBLIC

        assume  ds:nothing

; We are going to copy our initialized data segment from its original
; location to a new location.  The new location is always in RAM.
; The original location may be in ROM or RAM.  The copy is performed
; even if the monitor is running out of RAM, so that if the monitor
; is restarted, we start over with the original variables.

        lea     si,EndCode      ; Data starts on the next paragraph
                                ; boundary after the end of the code.

        lea     cx,EndInitializedData     ; Length of copy

; If the monitor is executing from RAM, we will place the data segment
; right after the code segment.

        mov     dx,cx           ; Size of data in ROM
        add     dx,si           ; + Size of code in ROM
        shr     dx,4            ; Convert from bytes to paragraphs

        mov     ax,cs
        add     ax,dx           ; New data segment = CS + paragraphs

; See if the code is in ROM or RAM by comparing new DS
; to the calculated size of RAM:

        cmp     ax,bx           ; in ROM?
        jb      @F              ; Jump if not

; Code is in ROM.  Allocate data at top of RAM area

        lea     ax,EndStack     ; Total size of code and data
        shr     ax,4
        sub     ax,bx
        neg     ax              ; == RamTop - size(DGROUP)
@@:
; Set up all the segment registers from the new value.
; Fix SP while we are at it

        mov     ds,ax
        mov     es,ax
        mov     ss,ax
        lea     sp,EndStack

        assume  ds:DGROUP

; Move initialized data from ROM to RAM

        xor     di,di
        rep     movs byte ptr es:[di], byte ptr cs:[si]

; Get the size of the BSS space to clear into CX.  Clear all but
; last 100 bytes of stack (to allow tracing of initialization code
; by another copy of the monitor).

        lea     cx,EndStack-100
        sub     cx,di

        xor     ax,ax
        shr     cx,1
        rep     stosw

; Set up BigBuffer pointer and size.  Buffer will start at 400h if
; running from ROM, or after SP if running from RAM.

        mov     ax,40h          ; Assume buffer starts at 400h
        mov     cx,ds           ; and ends at our data segment
        mov     dx,cs
        cmp     cx,dx           ; It's a good assumption if running from ROM
        jb      @F              ; jump if running from ROM (DS < CS)

; Running from RAM

        mov     ax,EndStack     ; Buffer starts at our DS + EndStack/16
        shr     ax,4
        add     ax,cx
        mov     cx,bx           ; Buffer ends at top of RAM
@@:
        inc     ax              ; Remove one paragraph for memory chain
        mov     bx,BigBufOffset
        mov     [bx+2],ax
        sub     cx,ax           ; Ram top - buffer start
        dec     cx              ; Remove one paragraph for memory chain
        mov     bx,BigBufParaOffset
        mov     [bx],cx

        call    SetMonInt3LinAddress
        mov     ax,cs
        call    ApplyCodeRelocations

        call    InitGlobals

        xor     ax,ax
        push    ax
        push    ax
        jmp     restart

CPUIndependentSetup endp

;***********************************************************************
;  ForceMonitorToRAM    Near public Pascal proc
;
;  Passed:     Stack Word = 1 to move to RAM, 0 to move to ROM
;  Returns:    Nothing
;  Destroys:   AX,BX,CX,DX, Flags
;
;  ForceMonitorToRAM will copy the monitor to RAM and update
;  all the interrupt thunks to access the correct code location.
;
;  It will also update the interrupt thunks to "move" back to ROM.
;
; When we move the monitor to RAM, we really should update BigBuffer,
; but the only things which require running out of RAM do not use
; BigBuffer, so we cheat and don't do this right now.
;***********************************************************************

ForceMonitorToRAM PROC NEAR PUBLIC
        pop     dx              ; Return vector
        pop     bx              ; = 1 if RAM desired, 0 if ROM desired

        mov     ax,cs
        sub     ax,8000h        ; set carry if running below 512K
        sbb     ax,ax           ; = 0 if running from ROM, FFFF if RAM
        add     ax,bx
        js      MoveToROM
        jnz     MoveToRAM
JmpDX:
        jmp     dx              ; return to caller

MoveToROM:
        mov     ax,cs:OriginalCodeSeg
        or      ax,ax           ; Do nothing if we didn't start
        jz      JmpDX           ; in ROM

        jmp     FixThunks

MoveToRAM:
        call    MonitorROMSize
        mov     cx,ax                 ; save count for copy operation
        add     ax,32                 ; Leave room for small app stack
        shr     ax, 4                 ; Convert to paragraphs
        mov     bx,BigBufParaOffset
        sub     ax, [bx]              ; Does it fit?
        ja      JmpDX                 ; return without doing anything

        neg     ax
        mov     bx,BigBufOffset
        add     ax,[bx+2]             ; Put at end of buffer

        push    si
        push    di
        cld

        xor     si,si
        mov     es,ax
        xor     di,di
        shr     cx,1
        rep     movs word ptr es:[di],word ptr cs:[si]
        pop     di
        pop     si

        mov     es:OriginalCodeSeg,cs
FixThunks:
        push    ax              ; Push CS:IP to do a FAR ret to ROM
        push    dx
        call    ApplyCodeRelocations
        retf                    ; Far return with new CS
ForceMonitorToRAM endp

;***********************************************************************
;  LockMonitorInRAM    Near public Pascal proc
;
;  Passed:     Nothing
;  Returns:    nothing
;  Destroys:   AX,BX,CX,DX, Flags
;
;  LockMonitorInRAM keeps monitor from moving back to ROM
;
;***********************************************************************

LockMonitorInRAM PROC NEAR PUBLIC

       ; Update OriginalCodeSeg to show we're never going back

        xor     ax,ax
        xchg    ax,cs:OriginalCodeSeg

        ; If we weren't in ROM, we're done.

        or      ax,ax
        jz      LockFinished

        ; We were in ROM.  Update our BigBuffer size to take into
        ; account the fact that we moved.

        mov     dx,cs
        mov     bx,BigBufOffset
        sub     dx,[bx+2]
        dec     dx
        mov     bx,BigBufParaOffset
        mov     [bx],dx

        ; Rummage through the interrupt table, updating anything
        ; which points to our old int 1.

        mov     dx,cs             ; Offset value to dx
        sub     dx,ax
        xor     bx,bx             ; Pointer to es:bx
        mov     es,bx
        mov     bl,2
        mov     cx,100h           ; 256 vectors to do
LockLoop:
        inc     ax
        cmp     ax,es:[bx]
        jnz     @F
        add     es:[bx],dx
@@:
        add     bx,4
        loop    LockLoop

        ; Record the value of our new Int 1 address.

        call    SetMonInt3LinAddress

LockFinished:
        ret

LockMonitorInRAM ENDP


;***********************************************************************
;  _Int3Handler    Near public interrupt proc
;
;  Passed:     Stack set up by interrupt
;  Returns:    Nothing
;  Destroys:   Nothing
;
; This routine invokes the monitor's C code _IRQEntry with a
; stack record.  It will change stacks to our internal stack
; if we are currently running on a different stack.
;
; A standard "thunk" in the data segment is invoked by an int3.
; The reason the thunk is in the data segment is that the ROM code doesn't
; necessarily know where the RAM is, but the RAM always knows where the
; code is.
;
; The thunk pushes the flags, and the CS:IP, and the thunk performs a PUSHA,
; and then pushes DS and ES, before performing a far call to here.  The int 3
; *could* have been invoked by our MonitorInt3, in which case another set
; of flags/cs/ip would have been pushed first.
;***********************************************************************

_Int3Handler    PROC FAR PUBLIC
        pop     ax              ; Don't need to get back to thunk
        pop     bx

; Save pointer to original StackRec in BP and in SI

        mov     bp,sp
        mov     si,sp

; Original SS to BX, DS to AX and ES

        mov     ax,ds
        mov     es,ax
        mov     bx,ss

; Assume SS != DS.  Set up stack at the end of the DGROUP

        mov     di,OFFSET DGROUP:EndStack - (size StackRec)
        cmp     ax,bx
        jnz     @F

; SS == DS, set SP and DI to allow 16 bytes of room on the stack

        sub     sp,16
        mov     di,sp
@@:
        cld

; Move our record down on the stack (SS == DS) or to a stack on
; our DGROUP (SS != DS)

        mov     cx,(size StackRec) / 2
        rep     movs   word ptr es:[di],word ptr ss:[si]

        sub     di,size StackRec        ; Point back to start of StackRec

; Set up new stack (AFTER performing movs)

        mov     ss,ax
        mov     sp,di

        push    bx                      ; Old SS

        push    ds:[di+StackRec._CS]    ; Original CS
        push    ds:[di+StackRec._IP]
        call    SegToLin

        mov     bx,3                    ; Interrupt 3 type
        cmp     ax,MonInt3LinAddress.LowW
        jnz     @F
        cmp     dx,MonInt3LinAddress.HighW
        jnz     @F

        add     bp,6                    ; Adjust for original flags, cs, ip

        call     OriginalCS
        mov     bx,ds:[di+StackRec._CS]
        sub     bx,ax                   ; Interrupt vector code to BX
        dec     bx

        mov     ax,ds:[di+StackRec._IP+6]
        mov     ds:[di+StackRec._IP],ax
        mov     ax,ds:[di+StackRec._CS+6]
        mov     ds:[di+StackRec._CS],ax
        mov     ax,ds:[di+StackRec._Flags+6]
        mov     ds:[di+StackRec._Flags],ax
@@:
        mov     ds:[di+StackRec._IType],bx
        lea     ax,[bp+StackRec._IType]     ; Original End of Stack
        mov     ds:[di+StackRec._SP],ax
        pop     ds:[di+StackRec._SS]

        call    _IrqEntry

        mov     si,sp
        add     si,StackRec._IType-2
        std
        les     di,ss:[si+4]
        sub     di,2
        mov     cx,StackRec._IType/2
        rep     movsw
        cld
        add     di,2
        mov     ax,es
        mov     ss,ax
        mov     sp,di
        pop     es
        pop     ds
        popa
        iret
_Int3Handler    ENDP

;***********************************************************************
;  UnhandledInt   Near public C proc
;
;  Passed:     One stack word containing unhandled interrupt number.
;              This routine must be called by first level C
;              interrupt handler.
;  Returns:    Nothing
;  Destroys:   Nothing
;
;  This routine removes the caller's stack frame, to get back to
;  the thunk stack frame, then fakes up an unhandled interrupt
;  condition.
;***********************************************************************

UnhandledInt   PROC NEAR PUBLIC
        pop     ax              ; Pop off caller's return address
        pop     dx              ; Pop off desired return code
        inc     dx
        leave                   ; Remove caller's stack frame
        pop     ax              ; remove caller's (far) return address
        pop     ax
        push    ss
        pop     es
        mov     si,sp
        sub     sp,6
        mov     di,sp
        cld
        mov     cx,13           ; 13 words down
        rep     movs word ptr es:[di], word ptr es:[si]
        mov     bp,sp
        call    OriginalCS
        add     ax,dx
        mov     ss:[bp+StackRec._CS],ax
        shl     dx,4
        lea     ax,PastMonInt3
        sub     ax,dx
        mov     ss:[bp+StackRec._IP],ax
        push    ax              ; fake a return address
        push    ax
        jmp     _Int3Handler
UnhandledInt        ENDP

;***********************************************************************
;  FarJmp   Near public Pascal proc
;
;  Passed:     One stack dword containing far address to jump to
;  Returns:    Nothing
;  Destroys:   Stack,AX
;
;  This routine removes the caller's stack frame, to get back to
;  the jump address, and then does a far jump.
;***********************************************************************

FarJmp   PROC NEAR PUBLIC
        pop     ax
        retf
FarJmp        ENDP

_TEXT   ends

_MONINT3   segment

;***********************************************************************
;  MonitorInt3    Interrupt public Pascal proc
;
;  Passed:     Nothing
;  Returns:    Nothing
;  Destroys:   Nothing
;
;  MonitorInt3 performs an int 3, and then an IRET.  Unused interrupt
;  vectors should point to the absolute address of MonitorInt3,
;  but with varying segment:offset definitions, so that the debugger
;  can determine which unused interrupt was invoked.
;
;  MonitorInt3 is in a separate code segment simply so that it
;  can have an offset which is high enough in memory to have
;  easy alternate segment:offset addressing without worrying
;  about "wrapping" if the monitor is loaded low.
;***********************************************************************

MonitorInt3 PROC NEAR PUBLIC
        int     3
PastMonInt3 LABEL NEAR
        iret
MonitorInt3 ENDP


_MONINT3   ends

end
