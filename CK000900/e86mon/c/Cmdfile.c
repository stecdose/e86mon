/******************************************************************************
 *                                                                            *
 *     CMDFILE.C                                                              *
 *                                                                            *
 *     This file contains routines to parse and store .HEX files,             *
 *     either into RAM or FLASH.  It supports standard Intel HEX              *
 *     format, and AMD LPD's relocatable extensions.                          *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996-1997 Advanced Micro Devices, Inc.                           *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#define DEF_SIGNATURE

#include "libinfo.h"

//////////////////////////////////////////////////////////////////////////////
//    DownloadInfo structure is used for communication between internal
//    routines
//
typedef struct {
    DWORD LoadSegment;          // Segment record location
    DWORD RecAddr;              // Address of data within record
    PBYTE RecData;
    WORD  RecLen;

    BOOL  ToFlash, RelocFlash;
    LibInfo Exe;
} DownloadInfo, *PDownloadInfo;

//////////////////////////////////////////////////////////////////////////////
//    ParseHex() -- Decode two hex digits into a byte
//
#pragma warning (disable:4701)
STATIC BOOL ParseHex(PBYTE Dest, PBYTE Source)
{
    WORD i, Value, new;

    for (i = 2; i > 0; i--)
    {
        new = tonum(*(Source++));
        if (!assert(new!=0xFFFF,FileMsgNonHex))
            return FALSE;
        Value = (Value << 4) + new;
    }
    *Dest = (BYTE)Value;
    return TRUE;
}
           
//////////////////////////////////////////////////////////////////////////////
//    ConvertRecord() -- Read in a hex line, decode it, and check
//                       the checksum.
//
//    Returns TRUE if it got a line, regardless of the validity of the
//    line.  (If the line is invalid, an error message will be stored.)
//
static BOOL ConvertRecord(BOOL AlreadyInBuffer)
{
    BYTE CheckSum = 0;
    PBYTE Dest;
    PBYTE Source;

    while (AlreadyInBuffer || GetInputLine(SystemMsgNull,FALSE))
    {
        Dest   = EGD.InputLine;
        Source = EGD.InputLine;
        AlreadyInBuffer = FALSE;    // in case we were passed a blank line

        // Ignore blank lines  -- could be line feeds

        if (*Source == 0)
            continue;

        if (((++EGD.FileLineCount) & 7) == 0)
        {
            PostLEDs((BYTE)(EGD.FileLineCount / 8));
            printf(SystemMsgDot);
        }


        if (!assert(*(Source++) == ':',FileMsgNoColon))
            return TRUE;

        while (*Source != 0)
        {
            if (!ParseHex(Dest,Source))
                return TRUE;
            CheckSum = (BYTE)(CheckSum + *(Dest++));
            Source += 2;
        }

        assert((Dest-(PBYTE)EGD.InputLine == EGD.InputLine[0]+5),FileMsgInvalidLength);
        assert((CheckSum == 0), FileMsgChecksum );
        return TRUE;
    }
    return FALSE;
}

//////////////////////////////////////////////////////////////////////////////
//    Flip2(), Flip4() -- Endian conversions
//
STATIC WORD Flip2(PBYTE Source)
{
    return ((WORD)Source[0] << 8) + Source[1];
}

STATIC DWORD Flip4(PBYTE Source)
{
    return ((DWORD)Flip2(Source) << 16) + Flip2(Source+2);
}

//////////////////////////////////////////////////////////////////////////////
//    RelocateAddress() -- Applies a linear, page-aligned offset, to a
//                       segmented address, by shifting up the offset
//                       and adding it to the segment portion of the address.
//
STATIC DWORD RelocateAddress(DWORD Address, DWORD Offset)
{
    return Address + (Offset << 12);
}

//////////////////////////////////////////////////////////////////////////////
//    WriteToMemory() -- uses SetAnyMem to perform range checking
//
static void WriteToMemory(DWORD dest, LPVOID source, DWORD length)
{
    AnyAddress a;
    WORD       chunk;

    a.Flags = 0;
    a.LinearAddress = dest;
    a.MemSpace = 1;
    while (length)
    {
        chunk = 0x8000;
        if (chunk > length)
            chunk = (WORD)length;
        SetAnyMem(&a,source,chunk);
        length -= chunk;
        a.LinearAddress += chunk;
        source = LinToSeg(SegToLin(source)+chunk);
    }
}

//////////////////////////////////////////////////////////////////////////////
//    PrepareExec() -- Sets up registers and PSP area,
//                     relocates program.
//
STATIC BOOL PrepareExec(LPLibInfo Exe, BOOL Relocate)
{
    DWORD SSSP,CSIP;
    WORD  AllocLength;
    DWORD RelocLoc;
    DWORD LinLoadAddress;


    AllocLength = Exe->AllocLength;
    if (!assert(AllocLength <= EGD.BigBufParagraphs, FileMsgExeTooBig))
        return FALSE;

    RelocLoc = Exe->RelocStart;
    LinLoadAddress = Exe->ProgStart;

    if (Relocate)
    {
        DWORD NewLoadAddress;

        RunSystemFromRAM(FALSE);

        dispatch(Dispatch_LoadingFromROM,LinLoadAddress);

        NewLoadAddress = SegToLin(EGD.BigBuffer+0x100);
        WriteToMemory(NewLoadAddress,LinToSeg(LinLoadAddress),
                       RelocLoc-LinLoadAddress);
        LinLoadAddress = NewLoadAddress;
    }

    while (RelocLoc < Exe->ExtraStart)
    {
        LPDWORD offsetPtr  = LinToSeg(RelocLoc);
        LPWORD targetAddress = LinToSeg(LinLoadAddress + *offsetPtr);
        *targetAddress += (WORD)(LinLoadAddress >> 4);
        RelocLoc += 4;
    }

    CSIP = Exe->CSIP;
    if (CSIP == 0xFFFFFFFF)
        return FALSE;

    SSSP = Exe->SSSP;
    if (SSSP != 0xFFFFFFFF)
    {
        if (SSSP)   // Originally .EXE program
            SSSP = RelocateAddress(SSSP,LinLoadAddress);
        else            // Originally .COM program
        {
            AllocLength = EGD.BigBufParagraphs;
            SSSP = (DWORD)EGD.BigBuffer;
            if (AllocLength < 0x1000)
                SSSP += AllocLength << 4;
        }
        EGD.stack->SS = _FP_SEG(SSSP);
        EGD.stack->SP = _FP_OFF(SSSP);
        EGD.stack->_ES = EGD.stack->DS = _FP_SEG(EGD.BigBuffer);
        InitPSP(AllocLength);
    }
    CSIP = RelocateAddress(CSIP,LinLoadAddress);
    EGD.stack->CS = _FP_SEG(CSIP);
    EGD.stack->IP = _FP_OFF(CSIP);
    return TRUE;
}

//////////////////////////////////////////////////////////////////////////////
//    ParseExtendedSegmentRecord() -- handles hex file records containing
//                                    relocatable start record
//
static void ParseExtendedSegmentRecord(PDownloadInfo d)
{
    static ROMCHAR RelocatableRev[] = "AMD LPD ";

    if (!assert((d->RecLen == 28) && (d->Exe.ProgStart == 0) &&
           (emon_memcmp(d->RecData+2,RelocatableRev,8) == 0),FileMsgSegment))
        return;

    d->Exe.ProgStart = SegToLin(EGD.BigBuffer+0x100);
    if (d->RelocFlash)
    {
        d->Exe.ProgStart = FreeFlash(FALSE) + sizeof(d->Exe);
        assert(d->Exe.ProgStart >= GetFlashBase(),FileMsgBadFormat);
    }
    d->LoadSegment += d->Exe.ProgStart;

    d->Exe.AllocLength = Flip2(d->RecData+10) + 0x10;  // Add PSP
    d->Exe.SSSP        = Flip4(d->RecData+12);
    d->Exe.RelocStart  = Flip4(d->RecData+16)+d->Exe.ProgStart;
    d->Exe.ExtraStart  = Flip4(d->RecData+20)+d->Exe.ProgStart;
    d->Exe.NextStruct  = Flip4(d->RecData+24)+d->Exe.ProgStart;


    if (!assert((((BYTE)d->Exe.ProgStart  | (BYTE)d->Exe.RelocStart |
                  (BYTE)d->Exe.ExtraStart | (BYTE)d->Exe.NextStruct)
                     & 0xF) == 0,FileMsgSegment))
        d->LoadSegment = 0;
}

//////////////////////////////////////////////////////////////////////////////
//    ParseSegmentRecord() -- handles hex file records containing
//                                    segment information
//
static void ParseSegmentRecord(PDownloadInfo d)
{
    BOOL InFlash;
    BOOL Relocated;

    d->LoadSegment  = ((DWORD)Flip2(d->RecData) << 4);
    d->LoadSegment += d->Exe.ProgStart;

    if (assert(d->RecAddr == 0, FileMsgSegment) &&
                 (d->RecLen != 2))
        ParseExtendedSegmentRecord(d);

    InFlash   = d->LoadSegment >= GetFlashBase();
    Relocated = d->Exe.ProgStart > 0;

    if (InFlash && !d->ToFlash)
    {
        d->ToFlash    = TRUE;
        RunSystemFromRAM(TRUE);
        d->RelocFlash = Relocated;
        if (Relocated)
            WriteToMemory(FreeFlash(FALSE),&d->Exe,sizeof(d->Exe));
    }

    assert(d->ToFlash || !d->RelocFlash,FileMsgNotReloc);
}

//////////////////////////////////////////////////////////////////////////////
//    ParseStartAddressRecord() -- handles hex file records containing
//                                 program's initial CS:IP address
//
static void ParseStartAddressRecord(PDownloadInfo d)
{
    LPWORD  AppBoot;
    BYTE    Code[5];

    d->Exe.CSIP = Flip4(d->RecData);

    if (d->RelocFlash || !d->ToFlash)
        return;

    AppBoot = LinToSeg(AppBootVector);

    if ((AppBoot[0] & AppBoot[1] & AppBoot[2]) != 0xFFFF)
        return;

    Code[0]  = 0xEA;  // Far jump
    *((LPDWORD)(Code+1)) = d->Exe.CSIP;

    WriteToMemory(AppBootVector,Code,5);
}

//////////////////////////////////////////////////////////////////////////////
//    CommandHexFile() -- download and program flash file or RAM
//
BOOL CommandHexFile(LPCSTR PName)
{
    BOOL EscapeReported = FALSE;
    BOOL Done           = FALSE;

    DownloadInfo d;
    memset(&d,0,sizeof(d));

    d.Exe.CSIP       = 0xFFFFFFFF;
    d.Exe.SSSP       = 0xFFFFFFFF;

    if (PName)
    {
        d.RelocFlash = TRUE;
        emon_memcpy(&d.Exe.Signature,LibSig.Signature,sizeof(d.Exe.Signature));
        emon_strcpy(d.Exe.Name,PName);
    }

    printf(FileMsgDownloadHex);

    EGD.TrashedInputLine = TRUE;

    ConvertRecord(TRUE);

    do {
        d.RecLen  = EGD.InputLine[0];
        d.RecAddr = Flip2((PBYTE)EGD.InputLine+1);
        d.RecData = EGD.InputLine+4;

        switch (EGD.InputLine[3])  // RecType
        {
            // Data records
            case 0:
                WriteToMemory(d.RecAddr+d.LoadSegment,d.RecData,d.RecLen);
                break;

            // EOF record -- treat Reclen special to generate errors
            // on load of older format.

            case 1:
                Done = ((d.RecLen == 0) && (d.RecAddr == 0));
                assert( Done, FileMsgEOF);
                break;

            // Segment or extended start record

            case 2:
                ParseSegmentRecord(&d);
                break;

            case 3:
                if (assert((d.RecLen==4)&&(d.RecAddr==0), FileMsgStart))
                    ParseStartAddressRecord(&d);
                break;

            default: // Error! Unknown record type.
                if (!EscapeReported)
                {
                    printf(FileMsgHowToAbortHex);
                    PrintError(FileMsgUnknownRecord);
                    EscapeReported = TRUE;
                }
                break;
        } // switch
    } while (!Done && ConvertRecord(FALSE));

    PrintCRLF();

    if (!assert(Done,FileMsgUserAbort))
        return FALSE;

    printf(SystemMsgStringCRLF,(LPCSTR)(d.ToFlash
                      ? FlashMsgProgrammedOK
                      : FileMsgDownloadOK));

    return PrepareExec(&d.Exe,d.RelocFlash);

} // LoadHexFile

//////////////////////////////////////////////////////////////////////////////
//    CommandLoad() -- Load a previously stored .EXE into RAM,
//                     or show a list of .EXE files.
//
BOOL CommandLoad(int LoadCount)
{
    LPLibInfo  Exe;
    BOOL       ShowLoads;
    DWORD      LinAddress;

    ShowLoads = FALSE;

    if (LoadCount < 0)
    {
        LoadCount = 0;

        if (ScanEOL(FALSE))
            ShowLoads = TRUE;
        else if (!ScanDecimal(&LinAddress) || !ScanEOL(TRUE))
            return FALSE;
        else
            LoadCount = (int)(WORD)(LinAddress);
    }

    LinAddress = 0;

    for ( ;; )
    {
        if ( (LinAddress = NextLoadedProg(LinAddress,TRUE)) == 0)
        {
            if (!ShowLoads)
                PrintError(FileMsgNoSuchLoad);
            return FALSE;
        }

        Exe = LinToSeg(LinAddress);

        if (ShowLoads)
            printf("  %4d   %a    %s\n",++LoadCount,Exe,&Exe->Name);
        else if ((--LoadCount) <= 0)
            break;
    }

    return PrepareExec(Exe, TRUE);
}
