/******************************************************************************
 *                                                                            *
 *     186SER.C                                                               *
 *                                                                            *
 *     This file contains serial routines for 186 processors.  The            *
 *     bulk of the initialization code is in 186INITS.C.                      *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "emon.h"
#include "186init.h"
#include "isrthunk.h"

// Disable optimization warning
#pragma warning(disable : 4704)

#define INPUT_BUFSIZE 256    // MUST BE POWER OF 2!!!!!!!
#define INPUT_HIGHWATER 80
#define INPUT_LOWWATER  40

#define OUTPUT_BUFSIZE  16    // MUST BE POWER OF 2!!!!!!!


static SerPort SysSprt;

static BYTE InputBuffer[INPUT_BUFSIZE];
static BYTE OutputBuffer[OUTPUT_BUFSIZE];
static volatile WORD InputPutPtr;
static          WORD InputGetPtr;
static          WORD OutputPutPtr;
static volatile WORD OutputGetPtr;
static          BOOL XOffSent;
static          BOOL NeedXOff;
static          BOOL ReceivedXOff;

WORD   UARTStatus;
WORD   BaudDivisor;

static WORD far SerialIO(SerialAction action,WORD handle);

typedef WORD (far * Proc)(SerialAction, WORD);

#pragma pack (1)
struct {
    BYTE MovAxCs[2];
    BYTE FarJmp;
    Proc JumpTarget;
       } SerialThunk = {{0x8C, 0xC8}, 0xEA, SerialIO};
#pragma pack ( )
        

/////////////////////////////////////////////////////////////////////////
// IrqSerialEntry() is the console driver's interrupt routine.  It
// handles all transmission/reception and XON/XOFF flow control.
//
#pragma warning (disable:4701)           // Data used -- may not be init
void far cdecl IrqSerialEntry(void)
{
    WORD Status;
    BYTE   Data;
    WORD BufRoom;
    WORD  Ctrl;

    Status = (WORD)_inpw(SysSprt.StatAddr);
    if ((Status & SysSprt.RxStatMask) != 0)
        Data = (BYTE)_inpw(SysSprt.RxAddr);

    //  Only clear the status register when an error occurs to 
    //  miminize the effect of the EM sport erratum on the application     
    if (Status & SysSprt.ErrStatMask)
        OutToPCBReg(SysSprt.StatAddr, 0);

    Ctrl = (WORD)_inpw(SysSprt.CtlAddr);
    OutToPCBReg(SysSprt.CtlAddr,SysSprt.NoIntCtlMask);
    OutToPCBReg( SysSprt.EOIAddr, SysSprt.IntType); // end-of-interrupt

    BufRoom = (InputGetPtr - InputPutPtr-1) & (INPUT_BUFSIZE-1);

    UARTStatus |= Status;

    if ((Status & SysSprt.RxStatMask) != 0)
        switch (Data)
        {
        case ASCII_XOFF:
            ReceivedXOff = TRUE;
            break;

        case ASCII_XON:
            ReceivedXOff = FALSE;
            Ctrl |= SysSprt.TxCtlMask;
            break;

        default:
            if (BufRoom == 0)
            {
                UARTStatus |= 0x8000;
                break;
            }
            InputBuffer[InputPutPtr++] = Data;
            InputPutPtr &= (INPUT_BUFSIZE-1);

            if (BufRoom < INPUT_BUFSIZE - INPUT_HIGHWATER)
                if ((NeedXOff = !XOffSent) != 0)
                    Ctrl |= SysSprt.TxCtlMask;
            break;
    }

    if (((Ctrl & SysSprt.TxCtlMask) != 0) &&
         ((Status & SysSprt.TxStatMask) != 0))
        if (NeedXOff)
        {
            OutToPCBReg( SysSprt.TxAddr, ASCII_XOFF);
            NeedXOff = FALSE;
            XOffSent = TRUE;
        }
        else if (XOffSent && (BufRoom > INPUT_BUFSIZE - INPUT_LOWWATER))
        {
            OutToPCBReg( SysSprt.TxAddr, ASCII_XON );
            XOffSent = FALSE;
        }
        else if ((OutputPutPtr != OutputGetPtr) && !ReceivedXOff)
        {
            OutToPCBReg( SysSprt.TxAddr, OutputBuffer[OutputGetPtr++] );
            OutputGetPtr &= (OUTPUT_BUFSIZE-1);
        }
        else
            Ctrl &= ~SysSprt.TxCtlMask;

    OutToPCBReg(SysSprt.CtlAddr, Ctrl);

    if (Status & SysSprt.BreakStatMask)
        UnhandledInt(257);
}


/////////////////////////////////////////////////////////////////////////
// SerialIO() is the runtime entry into this module.
//
static WORD far SerialIO(SerialAction action, WORD handle)
{
    BOOL Done;
    WORD result;
    WORD NextPtr;

    if (handle != 0)
        return 0xFFFF;

    _asm push ds;
    _asm mov  ds,ax;

    switch (action >> 8)
    {
        case SerialPutch >> 8:   // putch

// putch() buffers a character for transmission out the serial port
//     The main complication is that it allows the monitor to reenter
//     itself on a break.  Otherwise, NextPtr could be initialized
//     once per routine, and interrupts would not have to be disabled.
//
            do {
                _disable();
                Done = (NextPtr = (OutputPutPtr+1) & (OUTPUT_BUFSIZE-1))
                                        != OutputGetPtr;
                if (Done)
                {
                    OutputBuffer[OutputPutPtr] = (BYTE)action;
                    OutputPutPtr = NextPtr;
                }
                _enable();
                OutToPCBReg(SysSprt.CtlAddr,
                                 SysSprt.RxCtlMask | SysSprt.TxCtlMask);
            } while (!Done);

            result = 0;
            break;

        case SerialTryToGetch >> 8:  // Getch

           // The only reason we disable interrupts is because this
           // routine could be reentered (if a BREAK is received).
           // Disabling interrupts allows us to make sure that we
           // get the character that we thought we saw in the buffer.

           result = 0xFFFF;

           _disable();
           if (InputGetPtr != InputPutPtr)
           {
               result = InputBuffer[InputGetPtr];
               InputGetPtr = (InputGetPtr+1) & (INPUT_BUFSIZE-1);
           }
           _enable();

           if (XOffSent && 
                (((InputGetPtr - InputPutPtr-1) & (INPUT_BUFSIZE-1))
                           > INPUT_BUFSIZE - INPUT_LOWWATER))
                OutToPCBReg(SysSprt.CtlAddr,
                         SysSprt.RxCtlMask | SysSprt.TxCtlMask);
            break;

        case SerialFlushOutput >> 8: // Flush output
            while ((OutputPutPtr != OutputGetPtr) ||
                 ((_inpw(SysSprt.CtlAddr) & SysSprt.TxCtlMask) != 0))
            {
            }
            break;

        case SerialFlushInput >> 8: // Flush input
            InputGetPtr = InputPutPtr;
            break;

        case SerialKbhit >> 8:        // Input data available
            result = (WORD)(InputGetPtr != InputPutPtr);
            break;

        default:
            result = 0xFFFF;
            break;
    }

    _asm pop ds;
    return result;
}

/////////////////////////////////////////////////////////////////////////
// InitSPRT() initializes a serial port, optionally enabling interrupts
// so it can be used as the main console I/O port.
//
void InitSPRT(SerPortPtr Sprt, BOOL EnableInterrupts)
{
    ISRTHUNK(IrqSerialEntry);

    // Mask off the interrupts at the interrupt controller,
    // then disable interrupts at the serial port,
    // then insure that the in-service bit is reset

    OutToPCBReg(Sprt->CtlAddr, Sprt->DisabledCtlMask);
    OutToPCBReg( Sprt->EOIAddr, Sprt->IntType);     // end-of-interrupt
    OutToPCBReg(Sprt->IMaskAddr, Sprt->IntsDisabled);

       // Set the baud rate and clear the input status, and
       // reroute the interrupt vector to the dummy handler

    OutToPCBReg(Sprt->BaudAddr, BaudDivisor);
    _inpw(Sprt->RxAddr);
    OutToPCBReg(Sprt->StatAddr, 0);
    SetDummyIntVector(Sprt->IntType);

    // If this is not our console port, we are finished

    if (!EnableInterrupts)
        return;

    // Clear state of interrupt variables

    InputPutPtr = 0;
    InputGetPtr = 0;
    OutputPutPtr = 0;
    OutputGetPtr = 0;
    XOffSent = TRUE;
    NeedXOff = FALSE;
    UARTStatus = 0;
    ReceivedXOff  = FALSE;

    // Copy the data into SysSprt for faster access during interrupts

    SysSprt = *Sprt;

    // Set up our interrupt vector to point to our "thunk", and
    // our "thunk" to point to the ISR, and enable interrupts
    // at the interrupt controller and then at the serial port.

    SetISRVector(SysSprt.IntType,&Thunk_IrqSerialEntry);
    OutToPCBReg(SysSprt.IMaskAddr, SysSprt.IntsEnabled);
    OutToPCBReg(SysSprt.CtlAddr, SysSprt.RxCtlMask);

} // InitSPRT()
