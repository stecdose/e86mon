/******************************************************************************
 *                                                                            *
 *     PRINTF.C                                                               *
 *                                                                            *
 *     This file contains most of the console output functions of the         *
 *     monitor.  The most-used one is the ubiquitous printf, which            *
 *     is a scaled-down version.  There are also error printing               *
 *     and character repeating routines.                                      *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "emon.h"

#pragma warning(disable:4213)

static WRITECHAR writechar;

static void cdecl iprintf(LPCSTR format, ...);    // Internal printf

/////////////////////////////////////////////////////////////////////////
// ShowUserSomethingsHappening() prints out periods, and updates the
// LEDs, approximately 3 times/second.
//
void ShowUserSomethingsHappening(void)
{
    static BYTE PrevTick;

    BYTE TickCount = (BYTE)(EGD.Milliseconds >> 6);

    PostLEDs(TickCount);
    if (((TickCount - PrevTick) & 7) == 7)
    {
        PrevTick = TickCount;
        printf(".");
    }
}

/////////////////////////////////////////////////////////////////////////
// WriteError is used by PrintError to store a string in the error buffer
//
static void WriteError(BYTE ch)
{
    if (EGD.ErrorStored < EGD.SmallBufSize-1)
        EGD.SmallBuffer[EGD.ErrorStored++] = ch;
}

/////////////////////////////////////////////////////////////////////////
// FillSpaces() prints out a given number of spaces or zeroes
//
static void FillSpaces(int Size, BOOL DoIt, BOOL FillZero)
{
    if (DoIt)
        while (Size-- >0)
            writechar((char) (FillZero ? '0' : ' '));
}

/////////////////////////////////////////////////////////////////////////
// HexDigit() returns the correct hexadecimal digit for a number
//
static char HexDigit(DWORD value, char UpperLower)
{
    if (value < 10)
        return (char)(value + '0');

    return (char)(value-10 + 'A' + UpperLower - 'X');
}

/////////////////////////////////////////////////////////////////////////
// PrintAddress prints a standard (segment:offset) or long address.
// It is an internal routine used by printf.
//
static void PrintAddress(LPWORD param, BOOL Long, WORD StringIndex)
{
    static ROMCHAR string0[] = "%04X:%04lX";
    static ROMCHAR string1[] = "%04x:%04lx";
    static ROMCHAR string2[] = "%01X/%07lX";
    static ROMCHAR string3[] = "%01x/%07lx";

    static PROMCHAR ROM strings[] = {string0,string1,string2,string3};

    FPANYADDRESS a;

    DWORD LinearAddress = param[0];
    WORD  MemSpace      = param[1];
    WORD  SegValue;

    if (Long)
    {
        a = (FPANYADDRESS)*((LPDWORD)param);
        LinearAddress = a->LinearAddress;
        MemSpace      = a->MemSpace;
        SegValue    = _FP_SEG(a->SegAddress);

        if ((a->Flags & ANY_DISP_SEG) == 0)
            StringIndex += 2;
        else
        {
            MemSpace = ((WORD)(LinearAddress >> 4) - SegValue);
            MemSpace &= 0xF000;
            MemSpace +=  SegValue;
            LinearAddress -= ((DWORD)MemSpace << 4);
        }
    }
    iprintf(strings[StringIndex],MemSpace,LinearAddress);
}

/////////////////////////////////////////////////////////////////////////
// Minimal printf implementation.  Supports:
//  % [-] [0] [<length>] [l] [s|c|x|i|d|u]
//
//  -     == Left justify
//  0     == Fill field with zeroes
// length == Minimum field width
//  l     == Doubleword for x|i|d|u
//  s     == string pointer
//  c     == single character
//  x     == hex number
//  i,d   == signed decimal number
//  u     == unsigned number
//  a     == address (segment:offset)
//  la    == long address (AnyAddress type)
//
void vpprintf(WRITECHAR outroutine, LPCSTR far * ptrptr)
{
    static ROMCHAR Percent[]      = "%";
    static ROMCHAR PercentQuery[] = "%???";
    static ROMCHAR NullMsg[]      = "";

    LPCSTR ptr     = *ptrptr;
    LPWORD param   = (LPWORD)(ptrptr+1);
    BOOL ZeroFill;
    int  MinWidth;
    BOOL Long;
    enum {ParseIncomplete, ParseString, ParseNum, ParsePercent,
                   ParseAddress, ParseNonsense} ParseType;
    BOOL LeftJustify;
    LPSTR sPtr;
    WORD NumBase;
    BOOL Signed;
    char ch;
    char NumString[16];          // Holds largest number

    writechar = outroutine;

    for (;;)
    {
        switch (ch = *(ptr++))
        {
            case 0:
               return;
            case '%':
                ZeroFill = FALSE;
                MinWidth = 0;
                Long = FALSE;
                LeftJustify = FALSE;
                sPtr = 0;
                NumBase = 10;
                Signed = FALSE;
                ParseType = ParseIncomplete;
                while (ParseType == ParseIncomplete)
                    switch (tolower(ch = *(ptr++)))
                    {
                        case 0:
                            ptr--;
                            ParseType = ParseNonsense;
                            break;
                        case '-':
                            LeftJustify = TRUE;
                            break;
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            MinWidth = MinWidth*10 + ch - '0';
                            if (MinWidth == 0)
                                ZeroFill = TRUE;
                            break;
                        case 'l':
                            Long = TRUE;
                            break;
                        case 'c':
                            sPtr = (LPSTR)(param++);
                            sPtr[1] = 0;
                            ParseType = ParseString;
                            break;
                        case 's':
                            sPtr = MK_FP(param[1],param[0]);
                            param += 2;
                            ParseType = ParseString;
                            break;
                        case 'x':
                            NumBase = 16;
                            ParseType = ParseNum;
                            break;
                        case 'd':
                        case 'i':
                            Signed = TRUE;
                        case 'u':
                            ParseType = ParseNum;
                            break;
                        case 'a':
                            ParseType = ParseAddress;
                            break;
                        case '%':
                            ParseType = ParsePercent;
                            break;
                        default:
                            ParseType = ParseNonsense;
                            break;
                    }
                switch (ParseType)
                {
                    case ParsePercent:
                        sPtr = (LPSTR)Percent;
                        break;
                    case ParseNonsense:
                        sPtr = (LPSTR)PercentQuery;
                        break;

                    case ParseAddress:
                        MinWidth -= 9;
                        FillSpaces(MinWidth,!LeftJustify,ZeroFill);
                        PrintAddress(param,Long,(WORD)(ch=='a'));
                        sPtr = (LPSTR)NullMsg;
                        param += 2;
                        break;

                    case ParseNum:
                    {
                        DWORD value;
                        if (Long)
                            value = *(((LPDWORD)param)++);
                        else
                            value = *(((LPWORD)param)++);
                        if (Signed)
                        {
                            Signed = (value >= (0x8000UL << (16*Long)));
                            if (Signed)
                            {
                                value = 0-value;
                                if (!Long)
                                    value &= 0x7FFF;
                            }
                        }

                        sPtr = &NumString[sizeof(NumString)];
                        *(--sPtr) = 0;
                        do {
                            *(--sPtr) = HexDigit(value % NumBase,ch);
                            value /= NumBase;
                        } while (value > 0);
                        if (Signed)
                            *(--sPtr) = '-';
                    }
                }
                MinWidth -= emon_strlen(sPtr);
                FillSpaces(MinWidth,!LeftJustify,ZeroFill);
                while (*sPtr != 0)
                    writechar(*(sPtr++));
                FillSpaces(MinWidth,LeftJustify,FALSE);
                break;

            default:
                writechar(ch);
                break;
        }
    }
}

/////////////////////////////////////////////////////////////////////////
//   iprintf - Internal printf for recursion, doesn't alter writechar
//
static void cdecl iprintf(LPCSTR format, ...)
{
    vpprintf(writechar,&format);
}

/////////////////////////////////////////////////////////////////////////
// iprinterror() prints a string to the internal error buffer, if
// we have not already stored one.
//
BOOL iprinterror(LPCSTR * format)
{
    if (EGD.ErrorStored == 0)
    {
        vpprintf(WriteError,format);
        if (EGD.FileLineCount)
            iprintf(FileMsgAtLineNumber,EGD.FileLineCount);
    }
    return FALSE;
}

/////////////////////////////////////////////////////////////////////////
// OutputErrorString outputs the error string if it has been stored.
//
void OutputErrorString(void)
{
    if (EGD.ErrorStored)
    {
        EGD.SmallBuffer[EGD.ErrorStored] = 0;
        printf((LPCSTR)EGD.SmallBuffer);
        EGD.ErrorStored = 0;
    }
}

/////////////////////////////////////////////////////////////////////////
// ScanError() indicates to the user an error occurred in scanning
// the input.  It places a caret at the offending line location.
//
BOOL Base_ScanError( LPCSTR CauseMsg )
{
    int SpaceCount = EGD.PromptLength+EGD.ErrorIndex;

    if (EGD.IgnoreError || (EGD.ErrorStored != 0))
        return FALSE;

    writechar = WriteError;
    FillSpaces (SpaceCount,SpaceCount>0,FALSE);
    iprintf(ConsoleMsgParseErr,CauseMsg);

    return FALSE;

} // ScanError()
