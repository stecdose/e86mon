/******************************************************************************
 *                                                                            *
 *     FLASH.C                                                                *
 *                                                                            *
 *     This file contains data and routines which allow programming           *
 *     of most common AMD flash devices.  The code should be executed         *
 *     from RAM, rather than the ROM it is programming.  There are            *
 *     seven entry points to this module:                                     *
 *                                                                            *
 *     FindFlash()    -- Attempts to identify the flash device                *
 *     ShowFlashParameters() -- Shows information about device                *
 *                                                                            *
 *     EraseFlash()   -- Erases an area of the flash                          *
 *     ProgramFlash() -- Programs bytes into the flash                        *
 *     UpdateMonitor() -- Updates the boot monitor from one running           *
 *                        at the base of the flash.                           *
 *     GetFlashBase()   -- Returns linear base address of device              *
 *     GetFlashBootBase() -- Returns linear address of boot code              *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "emon.h"
#include "am186msr.h" // so much for abstraction

// Flash device programming information.  This program works on
// 8-bit devices, in X8 and X16 configurations.  Internally,
// all operations are treated as if they are 16 bit, with 8
// bit data replicated into the high order bits for comparison
// purposes.

#define AUTOSELECT_CMD 0x9090
#define PROGRAM_CMD    0xA0A0
#define ERASE_CMD      0x8080
#define READ_MODE      0xF0F0
#define ERASE_CHIP     0x1010
#define ERASE_SECTOR   0x3030
#define DONE_STATUS    0x8080
#define TOGGLE_STATUS  0x4040
#define TIMEOUT_STATUS 0x2020
#define ERASE_TIMER    0x0808

// Shorthand information for megabyte/kilobyte

#define MB_8           0x800000L
#define MB_4           0x400000L
#define MB_2           0x200000L
#define MB_1           0x100000L
#define KB_512         0x080000L
#define KB_256         0x040000L
#define KB_128         0x020000L
#define KB_64          0x010000L
#define KB_32          0x008000L
#define KB_16          0x004000L
#define KB_8           0x002000L
#define KB_4           0x001000L
#define KB_2           0x000800L
#define KB_1           0x000400L

// Special sector sizes.  Boot block devices all have a nominal sector
// size of 64K, except that the first or last sector is subdivided
// into smaller sectors.  The first sector in a "B" device is divided
// into 16,8,8,32, and the last sector in a "T" device:  32,8,8,16.

#define BOOT_BOTTOM   0              // Boot block at bottom or top
#define BOOT_TOP      1

#define SMALLEST_SECTOR  KB_8         // Smallest sector on any device

//////////////////////////////////////////////////////////////////////////

typedef struct {
    WORD  FlashID;                // ID that the device reports
    PROMCHAR DevName;             // Human-readable device name
    WORD DevSize;                 // Size of device in KB.
    WORD SectorSize;              // Sector size in KB
    WORD BootBlockSize;           // Size of boot block we leave free.
               } DevTypeStruct;


DevTypeStruct ROM DevTypes[] =
{
      // code    Name       size in K   sector size boot size
      { 0x0000, FlashMsgUnknown,  0,       0,         0},
      { 0x2020, FlashMsg29F010,   128,    16,         32},
      { 0xA4A4, FlashMsg29F040,   512,    64,         64}, 
      { 0xD5D5, FlashMsg29F080,   1024,   64,         64},  
      { 0xADAD, FlashMsg29F016,   2048,   64,         64},
      { 0x22DF, FlashMsg29F100B,  128,    BOOT_BOTTOM,64},
      { 0x22D9, FlashMsg29F100T,  128,    BOOT_TOP,   32},
      { 0x2257, FlashMsg29F200B,  256,    BOOT_BOTTOM,64},
      { 0x2251, FlashMsg29F200T,  256,    BOOT_TOP,   32},
      { 0x22AB, FlashMsg29F400B,  512,    BOOT_BOTTOM,64},
      { 0x2223, FlashMsg29F400T,  512,    BOOT_TOP,   32},
      { 0x22BA, FlashMsg29LV400B, 512,    BOOT_BOTTOM,64},
      { 0x22B9, FlashMsg29LV400T, 512,    BOOT_TOP,   32},
      { 0x2258, FlashMsg29F800B,  1024,   BOOT_BOTTOM,64},
      { 0x22D6, FlashMsg29F800T,  512,    BOOT_TOP,   32}, // (Non-Banking Aware))
      { 0x0000, FlashMsgUnknown,  0,      0,          0}
};

STATIC  WORD  AddressAdjust = 0;   // 0 or 2 
STATIC  WORD  FlashIndex = 0;
STATIC  WORD  DevWidth;            // 1 or 2 bytes
STATIC DWORD  FlashBase;           // Base address of part;
STATIC DWORD  FlashBootBase;       // Base address of boot area
STATIC WORD   FlashDepth;

// List of sector offsets into device (in KB) for the Transient Program
// Area, and for the boot area.

STATIC WORD  TPASectors[33];
STATIC WORD  BootSectors[6];
STATIC PWORD LastBootSector;
STATIC PWORD LastAppSector;

#define SECTOR_SENTINEL 0xFFFF    // End of sector list

//////////////////////////////////////////////////////////////////////////
// DeviceIdentified() sets error and returns FALSE if device not identified.
//
STATIC BOOL DeviceIdentified(void)
{
    return assert(FlashIndex != 0, FlashMsgNoID);
}

//////////////////////////////////////////////////////////////////////////
// GetFlashBase() returns the base address of the flash device
//
DWORD GetFlashBase(void)
{
    return FlashBase;
}

//////////////////////////////////////////////////////////////////////////
// GetFlashBootBase() returns the base address of the boot area in the flash
//
DWORD GetFlashBootBase(void)
{
    if (FlashIndex == 0)
        return 0xF8000;
    return FlashBootBase;
}

//////////////////////////////////////////////////////////////////////////
// GetFlashSize() returns the size of the flash device in kilobytes
//
WORD GetFlashSize(void)
{
    if (!DeviceIdentified())
        return FALSE;
    return FlashDepth*DevWidth;

}

//////////////////////////////////////////////////////////////////////////
// WriteMem() writes a byte or word to memory in an atomic operation.
//
STATIC void WriteMem(LPVOID Adr, WORD Data)
{
    switch (DevWidth)
    {
        case 1: *((BYTE far *)Adr) = (BYTE)Data;
                break;
        case 2: *((WORD far *)Adr) = (WORD)Data;
                break;
    }
}

//////////////////////////////////////////////////////////////////////////
// SetMem() uses WriteMem() to write a byte or word to any arbitrary
// memory space in an atomic operation.
//
STATIC void SetMem(DWORD Address, WORD Data)
{
    WriteMem(LinToSeg(Address),Data);
}


//////////////////////////////////////////////////////////////////////////
// ReadMem() reads a byte or word from memory in an atomic operation.
//
STATIC WORD ReadMem(LPVOID Adr)
{
    switch (DevWidth)
    {
        case 1: return *((BYTE far *)Adr);
        case 2: return *((WORD far *)Adr);
    }
    return 0;
}

//////////////////////////////////////////////////////////////////////////
// GetMem() uses ReadMem() to read a byte or word from any arbitrary
// memory space in an atomic operation.
//
STATIC WORD GetMem(DWORD Address)
{
    return ReadMem(LinToSeg(Address));
}

//////////////////////////////////////////////////////////////////////////
// Replicate() 8 bits into 16 if necessary
//
STATIC WORD Replicate(WORD Data)
{
    switch (DevWidth)
    {
        case 1: Data &= 0xFF;
                Data |= (Data << 8);
                break;
    }
    return Data;
}

//////////////////////////////////////////////////////////////////////////
//  GetWORD() Retrieves data from memory,
//           and replicates it as necessary to 16 bits
//
STATIC WORD GetWORD(DWORD Address)
{
    return Replicate(GetMem(Address));
}

//////////////////////////////////////////////////////////////////////////
// FlashUnlock() sends a flash device the "unlock" sequence.
//
STATIC void FlashUnlock(void)
{
    SetMem(FlashBase+(DevWidth+AddressAdjust)*0x5555L,0xAAAA);
    SetMem(FlashBase+(DevWidth+AddressAdjust)*0x2AAAL,0x5555);
}

//////////////////////////////////////////////////////////////////////////
// FlashCmd() sends a flash device a command.
//
STATIC void FlashCmd(WORD Command)
{
    FlashUnlock();
    SetMem(FlashBase+(DevWidth+AddressAdjust)*0x5555L,Command);
}

//////////////////////////////////////////////////////////////////////////
// Given a sector offset and nominal sector size, NextSector() returns 
// the offset of the next sector.  The only complication involves boot
// block devices.
//
STATIC WORD NextSector(WORD ThisSector, WORD SectorSize, WORD DevSize)
{
    switch (SectorSize)
    {
        case BOOT_BOTTOM:
            switch (ThisSector)
            {
                case 0:    SectorSize = 16;
                           break;
                case 16:
                case 24:   SectorSize = 8;
                           break;
                case 32:   SectorSize = 32;
                           break;
                default:   SectorSize = 64;
            }
            break;
            
        case BOOT_TOP:
            switch (DevSize - ThisSector)
            {
                case 16:   SectorSize = 16;
                           break;
                case 24:
                case 32:   SectorSize = 8;
                           break;
                case 64:   SectorSize = 32;
                           break;
                default:   SectorSize = 64;
            }
            break;
    }
    return ThisSector + SectorSize;
}
            
//////////////////////////////////////////////////////////////////////////
// FillSectorList constructs a list of sectors.
//
STATIC void FillSectorList(WORD CurSector,PWORD Ptr, PWORD * last,
                        DWORD memtop, WORD SectorSize, WORD DevSize)
{

    do {
        *(Ptr++)  = CurSector;
        CurSector = NextSector(CurSector,SectorSize,DevSize);
    } while (CurSector*KB_1 + FlashBase <= memtop);

    *Ptr = SECTOR_SENTINEL;
    *last  = Ptr-2;
}

//////////////////////////////////////////////////////////////////////////
// SetSectorMap() sets up a map of sectors for the device and returns
//
STATIC void SetSectorMap(void)
{
    WORD SectorSize =    DevTypes[FlashIndex].SectorSize;
    WORD DevSize =       DevTypes[FlashIndex].DevSize;

    // Get the size of the boot record

    FlashBootBase  =    DevTypes[FlashIndex].BootBlockSize;

    // If it's not a boot block part, and it's 2 bytes wide, it must
    // be composed of 2 separate devices.  In this case, the effective
    // device, sector, and boot sizes are doubled.
    
   
    if ((SectorSize > 8) && (DevWidth > 1))
    {
        DevSize *= 2;
        SectorSize *= 2;
        if (SectorSize > FlashBootBase)
            FlashBootBase = SectorSize;
    }

    FlashDepth = DevSize/DevWidth;

    // Set flash boot base

    FlashBootBase = MB_1 - (FlashBootBase * KB_1);

    // Fill in the TPASectors list, then the BootSectors list

    FillSectorList(0,(PWORD)TPASectors,&LastAppSector,FlashBootBase,
                                         SectorSize,DevSize);
    FillSectorList(LastAppSector[1],(PWORD)BootSectors,&LastBootSector,MB_1,
                                         SectorSize,DevSize);

}
    
//////////////////////////////////////////////////////////////////////////
// ListSectors() shows a list of all the sectors in the boot or the
// TPA list.
//
STATIC void ListSectors(LPWORD List, LPWORD secNum)
{
    WORD i = 0;
    while (List[1] != SECTOR_SENTINEL)
    {
        printf(FlashMsgSectorList,(*secNum)++,
                  (WORD)((FlashBase + (*(List++) * KB_1)) >> 4) );
        if (List[1] != SECTOR_SENTINEL)
            printf(SystemMsgCommaSpace);
        if (++i >= 4)
        {
            i = 0;
            printf("\n               ");
        }
    }
}

//////////////////////////////////////////////////////////////////////////
// ShowFlashParameters() shows all the information about the flash device.
//
BOOL ShowFlashParameters(void)
{
    WORD secNum = 0;

    if (!DeviceIdentified())
        return FALSE;
    printf(FlashMsgFlashParam1,
                (LPCSTR)DevTypes[FlashIndex].DevName,
                FlashDepth*DevWidth,FlashDepth,DevWidth*8);
    ListSectors(TPASectors,&secNum);
    printf(FlashMsgFlashParam2);
    ListSectors(BootSectors,&secNum);
    PrintCRLF();
    PrintCRLF();
    return TRUE;
}

//////////////////////////////////////////////////////////////////////////
// NextBase() selects a new FlashBase for attempting to determine the
// flash parameters.  It doubles the assumed flash size, which is
// subtracted from 1MB to get the base address.
//
STATIC DWORD NextBase(void)
{
    return  MB_1 - (MB_1 - FlashBase) * 2;
}


//////////////////////////////////////////////////////////////////////////
// FindFlash() attempts to ID the highest flash device in memory
//
BOOL FindFlash(void)
{
    if (!SystemInRAM())
        return PrintError(FlashMsgMustRunFromRAM);
    
    for (FlashBase=MB_1-KB_128;  FlashBase>=KB_512; FlashBase = NextBase())
    {
        for (DevWidth = 2; DevWidth >= 0; DevWidth--)
        {
        
            DWORD Address = FlashBase;
            WORD  IDValue;
            WORD  Manufacturer;
            
            // When running from the a device that can be byte or word wide
            // such as the Am29F800-- the bit mode we have to adjust 
            // the address for the flash commands; therefore we iterate 
            // one more time checking for the 16/8bit wide flash
            if (DevWidth == 0)
            {    
                if (AddressAdjust == 0 )
                {
                    DevWidth = 2;
                    AddressAdjust =1;
                    continue;
                }
                else
                {
                    continue;
                }
            }

            // So that we can tell when we found a flash part, find
            // an even address that doesn't read 1, or an odd address
            // that DOES read 1.  This way, we can tell a change when
            // we issue the autoselect command.

            while (GetMem(Address) & 0xFF == 0x01)
            {
                if (GetMem(Address+(DevWidth+AddressAdjust)) & 0xFF == 0x01)
                    break;
                if ((Address += KB_4) >= MB_1)
                    break;
            }                   

            if (Address >= MB_1)
                break;

            FlashCmd(AUTOSELECT_CMD);
            Manufacturer = GetWORD(Address);
            IDValue = GetWORD(Address+(DevWidth+AddressAdjust));
            SetMem(FlashBase,READ_MODE);

            if ( (Manufacturer != 0x0101) &&
                 ( (Manufacturer != 0x0001) || ((IDValue & 0xFF00) != 0x2200)) )
                continue;  // Not AMD device

            for (FlashIndex = 1; DevTypes[FlashIndex].FlashID != 0; FlashIndex++)
                if (IDValue == Replicate(DevTypes[FlashIndex].FlashID))
                {
                    //We've got a positive ID on the device.  Do all the
                    // required housekeeping.
                    
                    DWORD TotalDevSize = DevTypes[FlashIndex].DevSize * KB_1;
                    if ((IDValue & 0xFF00) != 0x2200)
                        TotalDevSize *= DevWidth;

                    // Find the real beginning of the flash.  Some devices
                    // ignore A16..An when programming, so we might have
                    // the starting address wrong.

                    while ((FlashBase > KB_512) &&
                        ((DWORD)(MB_1 - FlashBase) < TotalDevSize))
                    {
                        WORD NewManufacturer, NewIDValue;
                        Address = NextBase();

                        FlashCmd(AUTOSELECT_CMD);
                        NewManufacturer = GetWORD(Address);
                        NewIDValue = GetWORD(Address+(DevWidth+AddressAdjust));
                        SetMem(FlashBase,READ_MODE);

                        if ((NewManufacturer == Manufacturer) &&
                            (NewIDValue == IDValue))
                            FlashBase = Address;
                        else
                            break;
                   }

                    // Set up information about the flash sectors, and
                    // show the user information, if required.

                    SetSectorMap();

                    if (DevWidth == 1)
                        MainMsgPrompt[3] = '8';    // 88, not 86
                    return TRUE;
                }
        }
    }  
    FlashIndex = 0;
    return DeviceIdentified();
}  // FindFlash

//////////////////////////////////////////////////////////////////////////
// FlashStatus() is called at the end of every sector erase or byte program
// operation.  It waits until the operation has completed successfully or
// has generated an error, and returns TRUE if successful.
//
STATIC BOOL FlashStatus(LPVOID ptr, BOOL Erasing, WORD Data)
{
    WORD cur,prev, temp;
    static WORD LoopCount = 0;

    cur = ReadMem(ptr);
    do {
        if (LoopCount++ > 1000)
        {
            LoopCount = 0;
            if (Erasing || EGD.SpinLEDsWhileProgramming)
                ShowUserSomethingsHappening();
        }
        prev = cur;
        cur = ReadMem(ptr);
        temp = (prev^cur) & TOGGLE_STATUS;
    } while( (temp != 0) &&                     // Toggling and
              ((temp & (prev+prev)) == 0)  );   // not timed-out

    if (Erasing)
        return (Replicate(cur) & DONE_STATUS) == DONE_STATUS;
    else
        return (Data == ReadMem(ptr));
}

//////////////////////////////////////////////////////////////////////////
// ValidateErase() validates that a block of memory is erased.
//
STATIC BOOL ValidateErase(DWORD Address, WORD Size)
{
    // LinToSeg returns a pointer with a 0 offset.

    LPWORD Ptr = LinToSeg(Address);
    Ptr += (Size/2);

    while (((WORD)(DWORD)(--Ptr) > 0) && (*Ptr == 0xFFFF)) ;

    return *Ptr == 0xFFFF;
}

//////////////////////////////////////////////////////////////////////////
// EraseSector() erases a single sector, given its starting/ending addresses
//
STATIC BOOL EraseSector(LPWORD SectorInfo, BOOL CheckProtect)
{
    DWORD Address    = FlashBase + SectorInfo[0] * KB_1;
    DWORD EndAddress = FlashBase + SectorInfo[1] * KB_1;
    DWORD ProtectAddress = GetProtectFlash();
    DWORD EndProtect     = FlashBase + *LastAppSector * KB_1;
    BOOL Result;

    if ( (SectorInfo[0] == SECTOR_SENTINEL) ||
         (SectorInfo[1] == SECTOR_SENTINEL) )
        return PrintError(FlashMsgUnexpectedSentinel);

    printf(FlashMsgErasing,Address);

    if (CheckProtect && ProtectAddress
        && (EndAddress > ProtectAddress) && (Address < EndProtect) )
    {
        printf(FlashMsgProtected);
        return TRUE;
    }
        

    // Avoid erasing it if we don't have to

    while (ValidateErase(EndAddress -= SMALLEST_SECTOR, SMALLEST_SECTOR))
        if (EndAddress <= Address)
            return PrintCRLF();

    // Send the erase command

    FlashCmd(ERASE_CMD);
    FlashUnlock();
    SetMem(Address,ERASE_SECTOR);

    // Wait for something to happen

    Result = FlashStatus(LinToSeg(Address),TRUE,0);

    // Force the device back to read mode

    SetMem(FlashBase,READ_MODE);

    if (Result)
        return PrintCRLF();

    return PrintError(FlashMsgEraseFailed);
}

//////////////////////////////////////////////////////////////////////////
// EraseFlash() erases a region of the flash, either the TPA or the monitor.
//
BOOL EraseFlash( WORD WhichSectors )
{
    BOOL   CheckProtect = FALSE;
    LPWORD First = TPASectors;
    LPWORD Last  = LastAppSector;

    if (!DeviceIdentified() || !SystemInRAM())
        return PrintError(FlashMsgMustRunFromRAM);

    if (WhichSectors == 0xFFFF)  // Program boot sectors
    {
        if (((DWORD)OriginalCS() <<4) == FlashBootBase)
            return PrintError(FlashMsgBadErase);
        First = BootSectors;
        Last  = LastBootSector-1;
    }
    else
    {
        if ( (((DWORD)OriginalCS() <<4) >= FlashBase) &&
             (((DWORD)OriginalCS() <<4) <  FlashBootBase) )
            return PrintError(FlashMsgBadErase);

        if (WhichSectors == 0xA)     // 'A' is for 'ALL'
            CheckProtect = TRUE;
        else
            if (WhichSectors > (WORD)(Last-First))
                return PrintError(FlashMsgSectorRange);
            else
            {
                First += WhichSectors;
                Last = First;
            }
    }

    while (First <= Last)
        if (!EraseSector(First++, CheckProtect))
            return FALSE;

    return TRUE;
} //EraseFlash()

//////////////////////////////////////////////////////////////////////////
// ProgramOneFlashUnit() programs at the native size of the flash.  It is
// up to the caller to align the data, and make it the right size.
//
STATIC BOOL ProgramOneFlashUnit(DWORD Dest, WORD Data)
{
    LPVOID Ptr = LinToSeg(Dest);
    WORD OldData = ReadMem(Ptr);

    if (DevWidth == 1)
        Data &= 0xff;

    if (Data == OldData)
        return TRUE;

    // Can't program bits from low to high.  Do a check on this.

    if (Replicate(Data & ~OldData) != 0)
        return PrintError(FlashMsgByteNotFF);

    // Do it and wait for the results.

    FlashCmd(PROGRAM_CMD);
    WriteMem(Ptr,Data);
    if (!FlashStatus(Ptr,FALSE,Data)) {
        printf("Ptr: %lx, Data: %x, mem: %x\n",
            Ptr, Data, ReadMem(Ptr));
        return PrintError(FlashMsgProgramFailed);
    }
    return TRUE;
}

//////////////////////////////////////////////////////////////////////////
// ProgramOneByte() programs one byte into the flash.  If the flash
// is programmed a word at a time, it will fetch the current value
// in the byte not being programmed so it doesn't disturb it.
//
STATIC BOOL ProgramOneByte(DWORD Dest, BYTE NewByte)
{
    WORD Index;
    WORD Data;

    Index = (WORD)Dest & (DevWidth-1);
    Dest -= Index;
    Data = GetMem(Dest);

    ((LPBYTE)(&Data))[Index] = NewByte;

    return ProgramOneFlashUnit(Dest,Data);
}

//////////////////////////////////////////////////////////////////////////
// ProgramFlash() programs a string of bytes into the flash memory
//
BOOL ProgramFlash(DWORD Dest, LPBYTE Source, WORD DataSize)
{
    if (!DeviceIdentified() || !SystemInRAM())
        return PrintError(FlashMsgMustRunFromRAM);

    while (DataSize)
    {
        if (((Dest&(DevWidth-1)) != 0) || (DataSize < DevWidth) )
        {
            if (!ProgramOneByte(Dest,*Source))
                return FALSE;
            Source++;
            Dest++;
            DataSize--;
            continue;
        }
        
        while (DataSize >= DevWidth)
        {
            if (!ProgramOneFlashUnit(Dest,*((LPWORD)Source)) )
                return FALSE;
    
            Source   += DevWidth;
            Dest     += DevWidth;
            DataSize -= DevWidth;
        }
    }

    return TRUE;
}

//////////////////////////////////////////////////////////////////////////
// CheckSumMonitor() -- Used by ReFlashulate to determine if flash
// switch actually was moved by the user.
//
DWORD CheckSumMonitor(void)
{
    DWORD Start = FlashBootBase;
    DWORD Value = 0;

    do {
        Value += CheckSum(LinToSeg(Start),KB_32);
        Start += KB_32;
    } while (Start < 0x100000);
    return Value;
}
    
        
//////////////////////////////////////////////////////////////////////////
// Reflashulate() --  Copies from one ROM to another
//
BOOL Reflashulate(void)
{


    DWORD OldSum = CheckSumMonitor();
    WORD  OldIndex = FlashIndex;
    BYTE  Boot[5];

    LockMonitorInRAM();
    InitPSP(0);
    EGD.MonitorMoved = TRUE;

    if (!MigratePermVars(FALSE,TRUE))
        return FALSE;


    // Ask the user to change over
    do {
        if (!GetYNResponse(FlashMsgMigratePrompt,TRUE))
            return FALSE;

        // The 186CC can boot from an 8 bit device,
        // but we may want to replace a 16-bit flash
        // so give the user the option when applicable
        // Actually we should test this case if FindFlash
        // fails.  To-do item.
        //
        if (IS186CC && (_inpw(CS_UMCS) & UMCS_USIZ))
        {
            if (GetYNResponse(FlashMsgReplace16bit,TRUE))
            {
                outpw(CS_UMCS, inpw(CS_UMCS) & ~UMCS_USIZ);
            }
        }

        if (!FindFlash())
        {

#ifndef	ER

            if (IS186CC && (_inpw(CS_UMCS) & UMCS_USIZ))
            {
                WORD wTemp;
                wTemp = (WORD) inpw(CS_UMCS);
                outpw(CS_UMCS, wTemp & ~UMCS_USIZ);
                if (!FindFlash())
                {
                    outpw(CS_UMCS, wTemp);
                    return FALSE;
                }
            }   
            else

#endif
                return FALSE;
        }
    } while ((OldIndex == FlashIndex) &&
        (OldSum == CheckSumMonitor()));

    if (!EraseSector(LastBootSector,FALSE) || !EraseFlash(0xFFFF))
        return FALSE;

    printf(FlashMsgCopyingMonitor);

    EGD.SpinLEDsWhileProgramming = TRUE;

    Boot[0] = 0xEA;
    *((LPDWORD)(Boot+1)) = (DWORD)LinToSeg(FlashBootBase);

    if (!ProgramFlash(FlashBootBase,MK_FP(CurrentCS(),0),
                                       MonitorROMSize()) || 
        !ProgramFlash(0xFFFF0,Boot,5))
        return FALSE;

    printf(FlashMsgMonitorUpdated);


    return TRUE;
}

//////////////////////////////////////////////////////////////////////////
// BOOL UpdateInProgress -- returns TRUE if monitor is not
// finished updating.
//
BOOL UpdateInProgress(void)
{
    return (*(LPWORD)LinToSeg(0xFFFF0) == 0x05EB);
}

//////////////////////////////////////////////////////////////////////////
// UpdateMonitor() Updates the boot monitor from the one
//                         running 32K below the main one
//
//    Because the system is vulnerable during update (the flash
//    is usually soldered down), several steps are taken to try
//    to insure that the update process is relatively foolproof:
//
//      (1) The update process must be running from a monitor which
//          has been installed 32K below the boot base, with the
//          application boot vector pointing to it.  This is so,
//          in case we lose power or get a reset, we have a
//          new monitor in the flash, not just in RAM.
//
//      (2) The new monitor is checked to make sure it fits.
//
//      (3) The user is prompted for confirmation of his intent.
//
//      (4) In the critical stage of the update, the last sector
//          is erased (on some devices this is the only sector),
//          and a very small reset vector is installed which points
//          to the new monitor in its original location.
//
//      (5) Remaining boot sectors (if any) are erased.
//      (6) The new monitor is copied from its original location
//          to the boot location.
//
//      (7) In the final step, the reset vector is patched to point
//          to the new location.
//
//      During steps 5 and 6, if the user were to press reset or
//      if the power were to go off, recovery is possible.  An
//      error during step 4 is probably irrecoverable.
//
//      If the user aborted during step 5 or 6, the update would
//      be continued when the monitor is initialized after the
//      next reset.
//
void UpdateMonitor(BOOL FinishOldUpdate)
{
    #pragma pack(1)
    struct {
        BYTE FarJmpToUpdatingMonitor;
        WORD MustBeZeroA;
        WORD UpdatingCS;
        BYTE NearJmpAroundFarJmp;
        BYTE NearJmpTarget;
        BYTE FarJmpToFinalMonitor;
        WORD MustBeZeroB;
        WORD FinalCS;
        BYTE FarJmpToHWInit;
        WORD HWInitOffset;
        WORD HWInitSeg;
    } BootTemplate = {0xEA,0,0,0xEB,5,0xEA,0,0,0xEA,256-16-5,0xFFF0};
    #pragma pack()

    PROMCHAR  ErrMsg;
    DWORD  OriginalBase;
    LPBYTE AppBootPtr;

    if (FinishOldUpdate && !UpdateInProgress())
        return;

    RunSystemFromRAM(TRUE);

    if (!FindFlash())
        return;

    OriginalBase = ((DWORD)OriginalCS()) << 4;

    if (OriginalBase == FlashBootBase)
    {
        if (!FinishOldUpdate)
            Reflashulate();
        else
        {
            BootTemplate.NearJmpTarget = 0;
            if (ProgramFlash(0xFFFF1,&BootTemplate.NearJmpTarget,1) &&
                EraseSector(LastAppSector,FALSE))
                printf(FlashMsgMonitorUpdated);
        }
        return;
    }

    AppBootPtr = LinToSeg(AppBootVector);

    BootTemplate.UpdatingCS = OriginalCS();
    BootTemplate.FinalCS = (WORD)(FlashBootBase >> 4);
    BootTemplate.HWInitOffset -= HWInitSize();

    if (HWInitSize() > 256 - 32)
        ErrMsg = FlashMsgNewInitTooBig;
    else if (MonitorROMSize() > MB_1 - 512 - FlashBootBase)
        ErrMsg = FlashMsgNewMonTooBig;
    else if (OriginalBase != FlashBootBase - KB_32)
        ErrMsg = FlashMsgExecFromUpdate;
    else if ( (*((LPWORD)(AppBootPtr+3)) != OriginalCS()) ||
              (*((LPWORD)(AppBootPtr+1)) != 0) || (AppBootPtr[0] != 0xEA))
        ErrMsg = FlashMsgBadAppVector;
    else
        ErrMsg = 0;

    if (ErrMsg > 0)
    {
        printf(FlashMsgCannotUpdate,(LPCSTR)ErrMsg);
        return;
    }

    if (FinishOldUpdate)
        printf(FlashMsgUpdateContinuation);
    else
    {
        if (!MigratePermVars(TRUE,FALSE))
            return;

        // Give the user one last chance to wimp out

        if (!GetYNResponse(FlashMsgUpdatePrompt,TRUE))
            return;

        if (!MigratePermVars(FALSE,FALSE))
            return;

        printf(FlashMsgReplacingBootBlock);
    }

    EGD.SpinLEDsWhileProgramming = TRUE;

////////  **** CRITICAL SECTION ****   DO OR DIE!!!!!!!!!

    while (!(FinishOldUpdate || EraseSector(LastBootSector,FALSE)) ||
           !ProgramFlash(0xFFFF0-5,(LPBYTE)&BootTemplate,
                                    sizeof(BootTemplate)) ||
           !ProgramFlash(0xFFFF0-5-HWInitSize(),LinToSeg(OriginalBase),
                               HWInitSize()))
    {
        if (FinishOldUpdate)
        {
            FinishOldUpdate = FALSE;   // Allow erase to happen
            continue;
        }
        OutputErrorString();
        if (!GetYNResponse(FlashMsgRetryProgram,TRUE))
            break;
    }

////////  End super-critical section.  Now only mildly critical.

    if (!EraseFlash(0xFFFF))
        return;

    printf(FlashMsgCopyingMonitor);

    if (!ProgramFlash(FlashBootBase,LinToSeg(OriginalBase),
                                       MonitorROMSize()))
        return;

    printf(PermMsgRebooting);
    FlushTransmitter();

    FarJmp(LinToSeg(FlashBootBase));
   
   
    return;
}
