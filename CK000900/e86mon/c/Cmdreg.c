/******************************************************************************
 *                                                                            *
 *     CMDREG.C                                                               *
 *                                                                            *
 *     This file contains all the code which handles the 'R' (register)       *
 *     command.  All registers and flags may be examined and changed.         *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "emon.h"

static ROMCHAR RegNames[] = "AXBXCXDXSPBPSIDIDSESSSCSIP";
static ROMCHAR FlagNames[] = "NVOVUPDNDIEIPLNGNZZRNAACPOPENCCY";
static ROMWORD FlagMap[] = {11,10,9,7,6,4,2,0};
static ROMWORD RegMap[]  = {
                             (WORD)(DWORD)&DummyStackRec.AX,
                             (WORD)(DWORD)&DummyStackRec.BX,
                             (WORD)(DWORD)&DummyStackRec.CX,
                             (WORD)(DWORD)&DummyStackRec.DX,
                             (WORD)(DWORD)&DummyStackRec.SP,
                             (WORD)(DWORD)&DummyStackRec.BP,
                             (WORD)(DWORD)&DummyStackRec.SI,
                             (WORD)(DWORD)&DummyStackRec.DI,
                             (WORD)(DWORD)&DummyStackRec.DS,
                             (WORD)(DWORD)&DummyStackRec._ES,
                             (WORD)(DWORD)&DummyStackRec.SS,
                             (WORD)(DWORD)&DummyStackRec.CS,
                             (WORD)(DWORD)&DummyStackRec.IP
                           };

static ROMWORD NumRegs = sizeof(RegMap)/sizeof(WORD);
static ROMWORD NumFlags = sizeof(FlagMap)/sizeof(WORD);

//////////////////////////////////////////////////////////////////////
// RegIndex() returns a register index, given a string offset
//
LPWORD RegIndex(WORD StrucOffset)
{
    return (LPWORD)
      ((LPBYTE)EGD.stack + RegMap[StrucOffset] - (WORD)&DummyStackRec);
}

//////////////////////////////////////////////////////////////////////
// ShowReg() shows a given register and its current value.
//
static void ShowReg(WORD RegNum, char Delimiter)
{
    static ROMCHAR MyString[] = "%c%c=%04X%c ";
    printf(MyString,RegNames[RegNum*2],RegNames[RegNum*2+1],
             *RegIndex(RegNum), Delimiter);
}

//////////////////////////////////////////////////////////////////////
// ShowFlags() shows current settings for all the flags
//
static void ShowFlags(void)
{
    static ROMCHAR MyString[] = " %c%c";
    WORD i;
    for (i=0;i<NumFlags;i++)
    {
        WORD j = i*4;
        if ((EGD.stack->Flags & (1 <<FlagMap[i])) != 0)
            j += 2;
        printf(MyString,FlagNames[j],FlagNames[j+1]);
    }
}

//////////////////////////////////////////////////////////////////////
// PrintRegisters() uses ShowRegister() and ShowFlags() to dump
// all the registers out to the display.
//
void PrintRegisters(void)
{
    WORD i;

    for (i=0;i<NumRegs;i++)
    {
        ShowReg(i,' ');
        if (i==7)
            PrintCRLF();
    }
    ShowFlags();
    PrintCRLF();
}

//////////////////////////////////////////////////////////////////////
// ShowRegNames() is used if the user is confused, to show him
// all the register names.
//
void ShowRegNames(BOOL ScanErr)
{
    WORD i;
    char Str[65];
    char * ptr = Str;

    for (i=0;i<NumRegs;i++)
    {
        *ptr++ = RegNames[i*2];
        *ptr++ = RegNames[i*2+1];
        *ptr++ = ',';
        *ptr++ = ' ';
    }
    *(ptr-2) = 0;
    if (ScanErr)
        ScanError(Str);
    else
        printf(RegisterMsgRegNames,(LPCSTR)Str);
}

//////////////////////////////////////////////////////////////////////
// ShowFlagNames() is used if the user is confused, to show him
// all the flag names.
//
void ShowFlagNames(BOOL ScanErr)
{
    WORD i;
    char Str[65];
    char * ptr = Str;

    for (i=0;i<NumFlags;i++)
    {
        *ptr++ = FlagNames[i*4];
        *ptr++ = FlagNames[i*4+1];
        *ptr++ = '/';
        *ptr++ = FlagNames[i*4+2];
        *ptr++ = FlagNames[i*4+3];
        *ptr++ = ',';
        *ptr++ = ' ';
    }
    *(ptr-2) = 0;
    if (ScanErr)
        ScanError(Str);
    else
        printf(RegisterMsgFlagNames,(LPCSTR)Str);
}

//////////////////////////////////////////////////////////////////////
// FindTwoChars() attempts to match up the next two characters in
// the input stream with a register or flag name.
//
static WORD FindTwoChars(LPCSTR string)
{
    WORD i = 0;
    char ch1 = toupper(ReadInputLine(FALSE));
    char ch2 = toupper(ReadInputLine(FALSE));

    if (ch2 == 0)
        return 0xFFFF;

    while ((string[i] != 0) && ((string[i] != ch1) || (string[i+1] != ch2)))
        i += 2;

    return i/2;
}
    
//////////////////////////////////////////////////////////////////////
// CommandRegister() is the main routine.  It examines the command
// line to determine what to do.
//
void CommandRegister(BOOL JustPrint)
{
    WORD i;
    WORD value;


    if (JustPrint || ScanEOL(FALSE))
    {
        PrintRegisters();
        return;
    }

    if (!ScanOption(SCAN_SPACES|SCAN_COMMAS,'f'))
    {
        i = FindTwoChars(RegNames);
        if (i >= NumRegs)
        {
            ScanError(RegisterMsgRegOrFlagName);
            OutputErrorString();
            ShowRegNames(FALSE);
            ShowFlagNames(FALSE);
            return;
        }
        if (ScanEOL(FALSE))
        {
            ShowReg(i, ':');
            GetInputLine(SystemMsgNull,TRUE);
            if (ScanEOL(FALSE))
                return;
        }
        ScanDelimiters(SCAN_SPACES|SCAN_COMMAS|SCAN_EQUALS);
        if (ScanWord(&value) && ScanEOL(TRUE))
            *RegIndex(i) = value;
        return;
    }

    if (ScanEOL(FALSE))
    {
        ShowFlags();
        GetInputLine(SystemMsgColonSpaceSpace,TRUE);
        if (ScanEOL(FALSE))
            return;
    }
    i = FindTwoChars(FlagNames);
    if (i >= NumFlags*2)
    {
        ShowFlagNames(TRUE);
        return;
    }
    if (!ScanEOL(TRUE))
        return;

    value = (WORD)(1 << FlagMap[i/2]);
    EGD.stack->Flags &= ~value;
    if ((value&1) == 0)
        EGD.stack->Flags |= value;
}

//////////////////////////////////////////////////////////////////////
// GetRegValue returns the number of digits associated with a register
// name, and returns the value of the register.  0 digits if no
// match.
//
WORD GetRegValue(LPDWORD d)
{
    WORD OldIndex = EGD.LineIndex;
    WORD i = FindTwoChars(RegNames);
    if (i >= NumRegs)
    {
        EGD.LineIndex = OldIndex;
        return 0;
    }
    *d = *RegIndex(i);
    return 4;
}
