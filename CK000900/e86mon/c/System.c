/******************************************************************************
 *                                                                            *
 *     SYSTEM.C                                                               *
 *                                                                            *
 *     This file contains the C initialization code, as well as               *                                                                            *
 *     miscellaneous variables and routines which don't really fit            *
 *     anywhere else, primarily data conversion routines,                     *
 *     and routines to set the interrupt vectors.                             *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "libinfo.h"
#include "isrthunk.h"

// Disable optimization warning
#pragma warning(disable : 4704)

////////////////////////////////////////////////////////////////////////
// Convert a linear address to a segmented one, with as small an
// offset as possible.  Implementation is in EMSTART.ASM
//
//LPVOID LinToSeg(DWORD Linear)
//{
//    return (LPVOID) (((Linear << 12) & 0xffff0000L) + (Linear & 0x000f));
//}

////////////////////////////////////////////////////////////////////////
// Convert a segmented address to a linear one.  Make sure to wrap at
// 1MB.  Implementation is in EMSTART.ASM
//
//DWORD  SegToLin(LPVOID Segmented)
//{
//    return ( ((DWORD)Segmented & 0xFFFF) +
//             (((DWORD)Segmented & 0xFFFF0000L) >> 12) ) & 0xFFFFFL;
//}

////////////////////////////////////////////////////////////////////////
// SetISRVector() associates an interrupt vector with a procedure.
//
void SetISRVector(WORD iNum, ISRThunkData _far * Address)
{
    *(LPVOID far *)(DWORD)(iNum*4) = Address;
}

void SetDummyIntVector(WORD index)
{
    SetISRVector(index,MK_FP(OriginalCS()+index+1,
                             (WORD)MonitorInt3-16-index*16));
}

////////////////////////////////////////////////////////////////////////
// FlushAndRestart() finishes sending out serial data, then restarts.
//
void FlushAndRestart(WORD code)
{
    FlushTransmitter();
    restart(code);
}

///////////////////////////////////////////////////////////////////////
//    Int20Handler() -- Old-style DOS terminate
//    This function also provides the address of EmonData if called properly
//
void __far cdecl Int20Handler( STACKREC stack )
{
    static ROMCHAR TestMe[] = "EMONDATA";
    if (emon_memcmp(TestMe,MK_FP(stack.CS,stack.IP),8) != 0)
        restart(RESTART_EXIT_PROG);

    stack.IP += 8;
    stack.DX = _FP_SEG(EmonData);
    stack.AX = _FP_OFF(EmonData);
}

////////////////////////////////////////////////////////////////////////
// InitVectTable() is called once at system initialization time, and
// initializes all the interrupt vectors to default values.
//
// Interrupts which we do not handle are pointed at MonitorInt3
// (in EMSTART.ASM), but with unique segment/offset values, so
// that it is easy to determine which interrupt occurred.
//
void InitVectTable( void )
{
    ISRTHUNK(Int3Handler);
    ISRTHUNK(Int20Handler);

   WORD index;

   // Go through the table and install a dummy handler
   for (index = 0; index < 0x100; index++)
       SetDummyIntVector(index);

    SetISRVector(0x03,&Thunk_Int3Handler);
    SetISRVector(0x20,&Thunk_Int20Handler);
}

//////////////////////////////////////////////////////////////////////////
//  RamInit() initializes RAM-resident code
//
void RamInit(RamOp op)
{
    LPRamLink link;

    for (link = EmonGlobalData.RamHead; link != 0; link=link->next)
    {
        RamFunc func = link->func;
        WORD funcDS  = _FP_SEG(func);
        _asm push ds
        _asm mov  ds,funcDS
        func(op);
        _asm pop ds
    }
}

//////////////////////////////////////////////////////////////////////////
//  EMonInit() is the monitor's C startup code.  The assembly startup
//  calls this routine with interrupts disabled, then executes an
//  'int 3' to force entry to the debugger.
//
void EMonInit( void )
{
    BOOL InitSerial   = (EGD.RestartCode <= RESTART_RE_AUTOBAUD);
    BOOL Reinitialize = (EGD.RestartCode <= RESTART_EXIT_PROG);
    BOOL DoLinkLibraries = Reinitialize;

    // These variables are in the main loop

    extern BOOL DoAutoRun;
    extern LPBYTE BPAddress;

    // set up everything for entry into the monitor

    BPAddress = 0L;                       // disable breakpoint

    if (Reinitialize)
    {
        RamInit(RAM_STOP);
        DisableAllInterrupts();
        InitVectTable();                    // install interrupt vectors
    }

    if (EGD.RestartCode == RESTART_TOTAL)
    {
        ForceMonitorToRAM(TRUE);
        FindFlash();              // Figure out app boot location
        InitPermVars();
    }

    ForceMonitorToRAM(FALSE);

    if (Reinitialize)
    {
        InitSystem(InitSerial);
        ClearLibraries();
    }

    if (InitSerial)
    {
        LPDWORD AppBoot;

        AppBoot = LinToSeg(AppBootVector);

        do {
            DoAutoRun = DoLinkLibraries = !AutoBaud(FALSE);
        } while (DoAutoRun && UpdateInProgress());

        if ( DoAutoRun && (*AppBoot != 0xFFFFFFFF))
        {
            DispatchInstall func = (LPVOID)AppBoot;
            DoAutoRun = FALSE;
            func(0);
        }
    }

    if (Reinitialize)
    {
        EnableAllInterrupts();
        if (DoLinkLibraries)
		{
            LinkLibraries();
        	if (EGD.RestartCode != RESTART_EXIT_PROG)
            	InitPSP(0);
		}

        // Initialize external console code

        RamInit(RAM_START);
    }
} // EMonInit()
