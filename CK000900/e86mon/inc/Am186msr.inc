;*****************************************************************************
; Copyright 1995 Advanced Micro Devices, Inc.
;
; This software is the property of Advanced Micro Devices, Inc  (AMD)  which
; specifically  grants the user the right to modify, use and distribute this
; software provided this notice is not removed or altered.  All other rights
; are reserved by AMD.
;
; AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS
; SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL
; DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR
; USE OF THIS SOFTWARE.
;
; So that all may benefit from your experience, please report  any  problems
; or  suggestions about this software to:
;
; Advanced Micro Devices, Inc.
; Embedded Processor Division
; Mail Stop 590
; 5900 E. Ben White Blvd.
; Austin, TX 78741
; (800) 222-9323 
;****************************************************************************/
;*****************************************************************************
;
; am186er.ah
;
; Header file for defines specific to processor and board.
;
; Created: 12/95 MT
;
; Modified:
; Date    Initials    Comment
;
;
;****************************************************************************/
; =====================================================================
; INTERNAL REGISTER OFFSETS

; Miscellaneous control registers
;
    OFFS_RELC_REG      equ 0feh         ;  peripheral control block relocation
    OFFS_RI_REG        equ 0fch			;  refesh interval register
    OFFS_BISTEA_REG    equ 0fah			;  Build in self test error address
    OFFS_BISTSC_REG    equ 0f8h			;  build in self test status/control
    OFFS_RCFG_REG      equ 0f6h         ;  reset configuration register
    OFFS_PRL_REG       equ 0f4h         ;  Processor release level register
    OFFS_PDCN_REG      equ 0f0h         ;  Power-save control register
;
; Refresh control unit registers
;
    OFFS_DRAM_ENAB     equ 0e4h         ;  Enable RCU register
    OFFS_DRAM_CLK      equ 0e2h         ;  Clock prescaler register
    OFFS_DRAM_MEM      equ 0e0h         ;  Memory partition register
;
; DMA 1 registers
;
    OFFS_DMA1_CTL      equ 0dah         ;  DMA1 control register
    OFFS_DMA1_CNT      equ 0d8h         ;  DMA1 transfer count register
    OFFS_DMA1_DSH      equ 0d6h         ;  DMA1 destination addr high register
    OFFS_DMA1_DES      equ 0d4h         ;  DMA1 destination addr low register
    OFFS_DMA1_SRH      equ 0d2h         ;  DMA1 source addr high register
    OFFS_DMA1_SRC      equ 0d0h         ;  DMA1 source addr low register
;
; DMA 0 registers
;
    OFFS_DMA0_CTL      equ 0cah         ;  DMA0 control register
    OFFS_DMA0_CNT      equ 0c8h         ;  DMA0 transfer count register
    OFFS_DMA0_DSH      equ 0c6h         ;  DMA0 destination addr high register
    OFFS_DMA0_DES      equ 0c4h         ;  DMA0 destination addr low register
    OFFS_DMA0_SRH      equ 0c2h         ;  DMA0 source addr high register
    OFFS_DMA0_SRC      equ 0c0h         ;  DMA0 source addr low register
;
; Chip select registers
;
    OFFS_CS_MPCS       equ 0a8h         ;  PCS# and MCS# auxiliary register
    OFFS_CS_MMCS       equ 0a6h         ;  Midrange memory chip select reg.
    OFFS_CS_PACS       equ 0a4h         ;  Peripheral chip select register 
    OFFS_CS_LMCS       equ 0a2h         ;  lmsc register
    OFFS_CS_UMCS       equ 0a0h         ;  umcs register
    OFFS_CS_IMCS       equ 0ach         ;  Internal Memory Chip Select reg.
;
;
    OFFS_SPRT0_BDV     equ 088h         ;  serial port bauddiv reg
    OFFS_SPRT0_RX      equ 086h         ;  serial port receive reg
    OFFS_SPRT0_TX      equ 084h         ;  serial port transmit reg
    OFFS_SPRT0_STAT    equ 082h         ;  serial port status reg
    OFFS_SPRT0_CTL     equ 080h         ;  serial port control reg
    OFFS_SPRT_BDV      equ 088h         ;  serial port bauddiv reg 
    OFFS_SPRT_RX       equ 086h         ;  serial port receive reg
    OFFS_SPRT_TX       equ 084h         ;  serial port transmit reg
    OFFS_SPRT_STAT     equ 082h         ;  serial port status reg
    OFFS_SPRT_CTL      equ 080h         ;  serial port control reg
;
; PIO registers
;
    OFFS_PIO_DATA1     equ 07ah         ;  PIO data 1 register
    OFFS_PIO_DIR1      equ 078h         ;  PIO direction 1 register
    OFFS_PIO_MODE1     equ 076h         ;  PIO mode 1 register
    OFFS_PIO_DATA0     equ 074h         ;  PIO data registers
    OFFS_PIO_DIR0      equ 072h         ;  PIO direction register
    OFFS_PIO_MODE0     equ 070h         ;  PIO mode 0 register
;
; Timer 2 control registers
;
    OFFS_TMR2_CTL      equ 066h         ;  Timer 2 mode/control register
    OFFS_TMR2_MAXA     equ 062h         ;  Timer 2 maxcount compare A register
    OFFS_TMR2_CNT      equ 060h         ;  Timer 2 count register
;
; Timer 1 control registers
;
    OFFS_TMR1_CTL      equ 05eh         ;  Timer 1 mode/control register
    OFFS_TMR1_MAXB     equ 05ch         ;  Timer 1 maxcount compare B register
    OFFS_TMR1_MAXA     equ 05ah         ;  Timer 1 maxcount compare A register
    OFFS_TMR1_CNT      equ 058h         ;  Timer 1 count register
;
; Timer 0 control registers
;
    OFFS_TMR0_CTL      equ 056h         ;  Timer 0 mode/control register
    OFFS_TMR0_MAXB     equ 054h         ;  Timer 0 maxcount compare B register
    OFFS_TMR0_MAXA     equ 052h         ;  Timer 0 maxcount compare A register
    OFFS_TMR0_CNT      equ 050h         ;  Timer 0 count register
;
; Interrupt registers
;
    OFFS_INT_SPRT0     equ 044h         ;  sprt 0 interrupt ctrl reg
    OFFS_INT_SPRT      equ 044h         ;  serial port interrupt control reg
    OFFS_INT_WDOG      equ 042h         ;  Watchdog timer interrupt control
;
    OFFS_INT_INT4      equ 040h         ;  INT4 control register
    OFFS_INT_INT3      equ 03eh         ;  INT3 control register
    OFFS_INT_INT2      equ 03ch         ;  INT2 control register
    OFFS_INT_INT1      equ 03ah         ;  INT1 control register
    OFFS_INT_INT0      equ 038h         ;  INT0 control register

    OFFS_INT_DMA1      equ 036h         ;  DMA1 interrupt control register
    OFFS_INT_DMA0      equ 034h         ;  DMA0 interrupt control register

    OFFS_INT_TMR       equ 032h         ;  Timer interrupt control register

    OFFS_INT_STAT      equ 030h         ;  Interrupt status register
    OFFS_INT_IREQ      equ 02eh         ;  Interrupt request register
    OFFS_INT_INSV      equ 02ch         ;  In-service register
    OFFS_INT_PMSK      equ 02ah         ;  Priority mask register
    OFFS_INT_MASK      equ 028h         ;  Interrupt mask register
    OFFS_INT_PSTT      equ 026h         ;  Poll status register
    OFFS_INT_POLL      equ 024h         ;  Poll register
    OFFS_INT_EOI       equ 022h         ;  End-of-interrupt register
    OFFS_INT_VEC       equ 020h         ;  Interrupt vector register
;
; Synchronous serial port register
;
    OFFS_SS_RX         equ 018h         ;  Synchronous serial receive register
    OFFS_SS_TX0        equ 016h         ;  Synchronous serial transmit 0 reg.
    OFFS_SS_TX1        equ 014h         ;  Synchronous serial transmit 1 reg.
    OFFS_SS_CTL        equ 012h         ;  Synchronous serial control register
    OFFS_SS_STAT       equ 010h         ;  Synchronous serial status register


;
; ======================================================================
; STANDARD EXTERNAL-MEMORY SEGMENT AND OFFSET MAP

    CTL_OFF            equ 0ff00h        ;  Ctl reg offset for peripherals


; Miscellaneous control registers
;
    RELC_REG           equ (CTL_OFF+OFFS_RELC_REG)
    RI_REG             equ (CTL_OFF+OFFS_RI_REG)
    BISTEA_REG         equ (CTL_OFF+OFFS_BISTEA_REG)
    BISTSC_REG         equ (CTL_OFF+OFFS_BISTSC_REG)
    RCFG_REG           equ (CTL_OFF+OFFS_RCFG_REG)
    PRL_REG            equ (CTL_OFF+OFFS_PRL_REG)
    PDCN_REG           equ (CTL_OFF+OFFS_PDCN_REG)
;
; Refresh control unit registers
;
    DRAM_ENAB          equ (CTL_OFF+OFFS_DRAM_ENAB)
    DRAM_CLK           equ (CTL_OFF+OFFS_DRAM_CLK)
    DRAM_MEM           equ (CTL_OFF+OFFS_DRAM_MEM)
;
; DMA 1 registers
;
    DMA1_CTL           equ (CTL_OFF+OFFS_DMA1_CTL)
    DMA1_CNT           equ (CTL_OFF+OFFS_DMA1_CNT)
    DMA1_DSH           equ (CTL_OFF+OFFS_DMA1_DSH)
    DMA1_DES           equ (CTL_OFF+OFFS_DMA1_DES)
    DMA1_SRH           equ (CTL_OFF+OFFS_DMA1_SRH)
    DMA1_SRC           equ (CTL_OFF+OFFS_DMA1_SRC)
;
; DMA 0 registers
;
    DMA0_CTL           equ (CTL_OFF+OFFS_DMA0_CTL)
    DMA0_CNT           equ (CTL_OFF+OFFS_DMA0_CNT)
    DMA0_DSH           equ (CTL_OFF+OFFS_DMA0_DSH)
    DMA0_DES           equ (CTL_OFF+OFFS_DMA0_DES)
    DMA0_SRH           equ (CTL_OFF+OFFS_DMA0_SRH)
    DMA0_SRC           equ (CTL_OFF+OFFS_DMA0_SRC)
;
; Chip select registers
;
    CS_MPCS            equ (CTL_OFF+OFFS_CS_MPCS)
    CS_MMCS            equ (CTL_OFF+OFFS_CS_MMCS)
    CS_PACS            equ (CTL_OFF+OFFS_CS_PACS)
    CS_LMCS            equ (CTL_OFF+OFFS_CS_LMCS)
    CS_UMCS            equ (CTL_OFF+OFFS_CS_UMCS)
    CS_IMCS            equ (CTL_OFF+OFFS_CS_IMCS)
;
; Asynchronous serial port  registers
;
    SPRT0_BDV          equ (CTL_OFF+OFFS_SPRT0_BDV)
    SPRT0_RX           equ (CTL_OFF+OFFS_SPRT0_RX)
    SPRT0_TX           equ (CTL_OFF+OFFS_SPRT0_TX)
    SPRT0_STAT         equ (CTL_OFF+OFFS_SPRT0_STAT)
    SPRT0_CTL          equ (CTL_OFF+OFFS_SPRT0_CTL)
    SPRT_BDV           equ (CTL_OFF+OFFS_SPRT_BDV)
    SPRT_RX            equ (CTL_OFF+OFFS_SPRT_RX)
    SPRT_TX            equ (CTL_OFF+OFFS_SPRT_TX)
    SPRT_STAT          equ (CTL_OFF+OFFS_SPRT_STAT)
    SPRT_CTL           equ (CTL_OFF+OFFS_SPRT_CTL)
;
; PIO registers
;
    PIO_DATA1          equ (CTL_OFF+OFFS_PIO_DATA1)
    PIO_DIR1           equ (CTL_OFF+OFFS_PIO_DIR1)
    PIO_MODE1          equ (CTL_OFF+OFFS_PIO_MODE1)
    PIO_DATA0          equ (CTL_OFF+OFFS_PIO_DATA0)
    PIO_DIR0           equ (CTL_OFF+OFFS_PIO_DIR0)
    PIO_MODE0          equ (CTL_OFF+OFFS_PIO_MODE0)
;
; Timer 2 control registers
;
    TMR2_CTL           equ (CTL_OFF+OFFS_TMR2_CTL)
    TMR2_MAXA          equ (CTL_OFF+OFFS_TMR2_MAXA)
    TMR2_CNT           equ (CTL_OFF+OFFS_TMR2_CNT)
;
; Timer 1 control registers
;
    TMR1_CTL           equ (CTL_OFF+OFFS_TMR1_CTL)
    TMR1_MAXB          equ (CTL_OFF+OFFS_TMR1_MAXB)
    TMR1_MAXA          equ (CTL_OFF+OFFS_TMR1_MAXA)
    TMR1_CNT           equ (CTL_OFF+OFFS_TMR1_CNT)
;
; Timer 0 control registers
;
    TMR0_CTL           equ (CTL_OFF+OFFS_TMR0_CTL)
    TMR0_MAXB          equ (CTL_OFF+OFFS_TMR0_MAXB)
    TMR0_MAXA          equ (CTL_OFF+OFFS_TMR0_MAXA)
    TMR0_CNT           equ (CTL_OFF+OFFS_TMR0_CNT)
;
; Interrupt registers
;
    INT_SPRT0          equ (CTL_OFF+OFFS_INT_SPRT0)
    INT_SPRT           equ (CTL_OFF+OFFS_INT_SPRT)
    INT_WDOG           equ (CTL_OFF+OFFS_INT_WDOG)
;
    INT_INT4           equ (CTL_OFF+OFFS_INT_INT4)
    INT_INT3           equ (CTL_OFF+OFFS_INT_INT3)
    INT_INT2           equ (CTL_OFF+OFFS_INT_INT2)
    INT_INT1           equ (CTL_OFF+OFFS_INT_INT1)
    INT_INT0           equ (CTL_OFF+OFFS_INT_INT0)

    INT_DMA1           equ (CTL_OFF+OFFS_INT_DMA1)
    INT_DMA0           equ (CTL_OFF+OFFS_INT_DMA0)

    INT_TMR            equ (CTL_OFF+OFFS_INT_TMR)

    INT_STAT           equ (CTL_OFF+OFFS_INT_STAT)
    INT_IREQ           equ (CTL_OFF+OFFS_INT_IREQ)
    INT_INSV           equ (CTL_OFF+OFFS_INT_INSV)
    INT_PMSK           equ (CTL_OFF+OFFS_INT_PMSK)
    INT_MASK           equ (CTL_OFF+OFFS_INT_MASK)
    INT_PSTT           equ (CTL_OFF+OFFS_INT_PSTT)
    INT_POLL           equ (CTL_OFF+OFFS_INT_POLL)
    INT_EOI            equ (CTL_OFF+OFFS_INT_EOI)
    INT_VEC            equ (CTL_OFF+OFFS_INT_VEC)
;
; Synchronous serial port register
;
    SS_RX              equ (CTL_OFF+OFFS_SS_RX)
    SS_TX0             equ (CTL_OFF+OFFS_SS_TX0)
    SS_TX1             equ (CTL_OFF+OFFS_SS_TX1)
    SS_CTL             equ (CTL_OFF+OFFS_SS_CTL)
    SS_STAT            equ (CTL_OFF+OFFS_SS_STAT)

; =======================================================================
; Internal Register field definitions

; RELOCATION REGISTER

    REL_REG_SLAVE      equ 04000h         ;  slave interrupt controls
    REL_REG_MEMSPACE   equ 01000h         ;  PCB in memory space

; --------------------------------------------------------------
; Processor release level (PRL_REG - offset 0xf4)

    PRL_REG_REVA8      equ 02100h          ;   Am188ER 
    PRL_REG_REVA6      equ 02000h          ;   Am186ER 


; --------------------------------------------------------------
; Power-save control register (PDCN_REG - offset 0xf0)

    PDCN_REG_PSEN      equ 08000h          ;   Power-save enable bit
    PDCN_REG_CBF       equ 00800h          ;   CLKOUTB Output Frequency
    PDCN_REG_CBD       equ 00400h          ;   CLKOUTB Drive Disable
    PDCN_REG_CAF       equ 00200h          ;   CLKOUTA Output Frequency
    PDCN_REG_CAD       equ 00100h          ;   CLKOUTA Drive Disable
    PDCN_REG_CDIV1     equ 00000h          ;   clock div factor = 1
    PDCN_REG_CDIV2     equ 00001h          ;   clock div factor = 2
    PDCN_REG_CDIV4     equ 00002h          ;   clock div factor = 4
    PDCN_REG_CDIV8     equ 00003h          ;   clock div factor = 8
    PDCN_REG_CDIV16    equ 00004h          ;   clock div factor = 16
    PDCN_REG_CDIV32    equ 00005h          ;   clock div factor = 32
    PDCN_REG_CDIV64    equ 00006h          ;   clock div factor = 64
    PDCN_REG_CDIV128   equ 00007h          ;   clock div factor = 128
;
; RAM refresh control
;
    DRAM_ENABLE        equ 08000h          ;   enable bit for DRAM
    INT_INSV_SPRT0     equ 00400h          ;   serial port 0x
    INT_INSV_SPRT1     equ 00200h          ;   serial port 1
    INT_INSV_INT4      equ 00100h          ;   external int 4
    INT_INSV_INT3      equ 00080h          ;   external int 3
    INT_INSV_INT2      equ 00040h          ;   external int 2
    INT_INSV_INT1      equ 00020h          ;   external int 1
    INT_INSV_INT0      equ 00010h          ;   external int 0x
    INT_INSV_DMA1      equ 00008h          ;   DMA1
    INT_INSV_DMA0      equ 00004h          ;   DMA0
    INT_INSV_TMR       equ 00001h          ;   all of the timers (except WDOG)

; -------------------------------------------------------------
; INTERRUPT REQUEST REGISTER BITS (REQST - 0x2e)
    INT_REQ_SPRT0      equ INT_INSV_SP0
    INT_REQ_SPRT1      equ INT_INSV_SP1
    INT_REQ_INT4       equ INT_INSV_INT4
    INT_REQ_INT3       equ INT_INSV_INT3
    INT_REQ_INT2       equ INT_INSV_INT2
    INT_REQ_INT1       equ INT_INSV_INT1
    INT_REQ_INT0       equ INT_INSV_INT0
    INT_REQ_DMA1       equ INT_INSV_DMA1
    INT_REQ_DMA0       equ INT_INSV_DMA0
    INT_REQ_TMR        equ INT_INSV_TMR
;
; -------------------------------------------------------------
; PIO PINS
    PIO0_TMRI1         equ 00001h    ;  PIO 0
    PIO0_TMRO1         equ 00002h    ;  PIO 1
    PIO0_PCS6          equ 00004h    ;  PIO 2
    PIO0_PCS5          equ 00008h    ;  PIO 3
    PIO0_DTR           equ 00010h    ;  PIO 4
    PIO0_DEN           equ 00020h    ;  PIO 5
    PIO0_SRDY          equ 00040h    ;  PIO 6
    PIO0_A17           equ 00080h    ;  PIO 7
    PIO0_A18           equ 00100h    ;  PIO 8
    PIO0_A19           equ 00200h    ;  PIO 9
    PIO0_TMRO0         equ 00400h    ;  PIO 10
    PIO0_TMRI0         equ 00800h    ;  PIO 11
    PIO0_DRQ0          equ 01000h    ;  PIO 12
    PIO0_DRQ1          equ 02000h    ;  PIO 13
    PIO0_MCS0          equ 04000h    ;  PIO 14
    PIO0_MCS1          equ 08000h    ;  PIO 15

    PIO1_PCS0          equ 00001h    ;  PIO 16
    PIO1_PCS1          equ 00002h    ;  PIO 17
    PIO1_PCS2          equ 00004h    ;  PIO 18
    PIO1_PCS3          equ 00008h    ;  PIO 19
    PIO1_SCLK          equ 00010h	;  PIO 20
    PIO1_SDATA         equ 00020h	;  PIO 21
    PIO1_SDEN0         equ 00040h    ;  PIO 22
    PIO1_SDEN1         equ 00080h    ;  PIO 23
    PIO1_MCS2          equ 00100h    ;  PIO 24
    PIO1_MCS3          equ 00200h    ;  PIO 25
    PIO1_UZI           equ 00400h    ;  PIO 26
    PIO1_TX            equ 00800h    ;  PIO 27
    PIO1_RX            equ 01000h    ;  PIO 28
    PIO1_S6            equ 02000h    ;  PIO 29
    PIO1_INT4          equ 04000h    ;  PIO 30
    PIO1_INT2          equ 08000h    ;  PIO 31
;
;
; Asynchronous serial port control register (SPRT_CTL)
;
    SPRT_CTL_TXIE      equ 00800h   ;   enable transmit intrpt
    SPRT_CTL_RXIE      equ 00400h   ;   enable receive intrpt
    SPRT_CTL_LOOP      equ 00200h   ;   enable loop-back mode
    SPRT_CTL_BRK       equ 00100h   ;   send break or *break
    SPRT_CTL_BRKHIGH   equ 00080h   ;   break value is high
    SPRT_CTL_BRKLOW    equ 00000h   ;   break value is low
    SPRT_CTL_NOPARITY  equ 00000h   ;   no parity bit
    SPRT_CTL_EVEN      equ 00060h   ;   even parity
    SPRT_CTL_ODD       equ 00040h   ;   odd parity
    SPRT_CTL_8BITS     equ 00010h   ;   char lengt is 8 bits
    SPRT_CTL_7BITS     equ 00000h   ;   char lengt is 7 bits
    SPRT_CTL_2STOP     equ 00008h   ;   two stop bits
    SPRT_CTL_1STOP     equ 00000h   ;   one stop bit
    SPRT_CTL_TX        equ 00004h   ;   enable transmitter
    SPRT_CTL_RSIE      equ 00002h   ;   enable Rx error intrpts
    SPRT_CTL_RX        equ 00001h   ;   enable receiver
;
; Asynchronous serial port status register (SPRT_STAT)
;
    SPRT_STAT_TEMT     equ 00040h   ;   transmitter is empty
    SPRT_STAT_THRE     equ 00020h   ;   Tx holding reg. empty
    SPRT_STAT_RDR      equ 00010h   ;   receive char ready
    SPRT_STAT_BRK      equ 00008h   ;   break received
    SPRT_STAT_FRAME    equ 00004h   ;   framing error detected
    SPRT_STAT_PARITY   equ 00002h   ;   parity error detected
    SPRT_STAT_OVERFLOW equ 00001h   ;   receive overflow
    SPRT_STAT_ERROR    equ SPRT_STAT_BRK+SPRT_STAT_FRAM+SPRT_STAT_PARITY+SPRT_STAT_OVERFLOW

; -----------------------------------------------------------------------
; DMA Control Registers (1&2 - 0xda, 0xca)

    DMA_DEST_MEM       equ 08000h   ;  DMA dest. 1=memory, 0=I/O space
    DMA_DEST_DEC       equ 04000h   ;  decrement DMA dest addr
    DMA_DEST_INC       equ 02000h   ;  increment DMA dest addr
    DMA_SRC_MEM        equ 01000h   ;  DMA source 1=memory, 0=I/O space
    DMA_SRC_DEC        equ 00800h   ;  decrement DMA src addr
    DMA_SRC_INC        equ 00400h   ;  increment DMA src addr
    DMA_TC             equ 00200h   ;  DMA uses terminal count
    DMA_INT            equ 00100h   ;  intrpt on terminal count
    DMA_SYN1           equ 00080h   ;  1=Src. Sync.2=Dest. Sync.3=Undef.
    DMA_SYN0           equ 00040h   ;  SYN1:0 0x = Unsynchronized
    DMA_P              equ 00020h   ;  if set, channel=hi prio.,else low
    DMA_IDRQ           equ 00010h   ;  if set, use timer 2
    DMA_CHG            equ 00004h   ;  set before DMA_STRT can be set
    DMA_STRT           equ 00002h   ;  if set, DMA channel is "armed"
    DMA_WORD           equ 00001h   ;  if set, word transfers, else byte
    DMA_BYTE           equ 00000h   ;  Byte transfers
; some common combinations
    DMA_S_MEM_INC      equ DMA_SRC_MEM+DMA_SRC_INC
    DMA_D_MEM_INC      equ DMA_DEST_MEM+DMA_DEST_INC
    DMA_S_D_MEM_INC    equ DMA_S_MEM_INC+DMA_D_MEM_INC
    DMA_ARM            equ DMA_CHG+DMA_STRT

    DMA_DEST_CONST     equ 00000h   ;   DMA destination addr is constant
    DMA_SRC_IO         equ 00000h   ;   DMA source is in I/O space
    DMA_SRC_CONST      equ 00000h   ;   DMA source addr is constant
    DMA_DEST_IO        equ 00000h   ;   DMA destination is in I/O space
    DMA_UNSYNC         equ 00000h   ;   unsynchronized transfers
    DMA_SRCSYNC        equ 000c0h   ;   source synchronized transfers
    DMA_DESTSYNC       equ 00080h   ;   destination synchronized transfers
    DMA_HIGHPRI        equ 00040h   ;   channel is high priority
    DMA_LOWPRI         equ 00000h   ;   channel is low priority
; 
; -------------------------------------------------------------
; CHIP-SELECT REGISTERS

; Fields used by more than one of the chip select registers
    CS_WAIT0           equ 00000h   ;   zero wait states
    CS_WAIT1           equ 00001h   ;   one wait state
    CS_WAIT2           equ 00002h   ;   two wait states
    CS_WAIT3           equ 00003h   ;   three wait states
    CS_IGXRDY          equ 00004h   ;   ignore external ready
; The following wait states can only be used for PCS3-0 - set in
; the PACS register (CS_PACS).
;
    CS_MPCS_WAIT5      equ 00008h   ;   five wait states
    CS_MPCS_WAIT7      equ 00009h   ;   seven wait states
    CS_MPCS_WAIT9      equ 0000ah   ;   nine wait states
    CS_MPCS_WAIT15     equ 0000bh   ;   fifteen wait states
; The following bits are only on LCS (CS_LMCS) and UCS (CS_UMCS)
    CS_DISADDR         equ 00080h   ;   disable AD addr output
	CS_DRAM_ENABLE     equ 00040h   ;   enable DRAM on 186ED
;
; UMCS (upper memory) register
    CS_UMCS_64K        equ 07000h   ;   UCS is 64K
    CS_UMCS_128K       equ 06000h   ;   UCS is 128K
    CS_UMCS_256K       equ 04000h   ;   UCS is 256K
    CS_UMCS_512K       equ 00000h   ;   UCS is 512K
	CS_UMCS_RESERVED   equ 08038h	;   Reserved set to 1
;
; LMCS (lower memory) register
    CS_LMCS_PSRAM      equ 00040h   ;  turn off PSRAM when subtracted out
    CS_LMCS_64K        equ 00000h   ;   LCS is 64K  (0 - 0ffffh)
    CS_LMCS_128K       equ 01000h   ;   LCS is 128K (0 - 1ffffh)
    CS_LMCS_256K       equ 03000h   ;   LCS is 256K (0 - 3ffffh)
    CS_LMCS_512K       equ 07000h   ;   LCS is 512K (0 - 7ffffh)
    CS_LMCS_PSEN       equ 00040h   ;   enable PSRAM support
;
; IMCS (internal memory) register
	CS_IMCS_SHOW_READ  equ 00400h	;  show read enable
	CS_IMCS_RAM_ENABLE equ 00200h	;  ram enable
    CS_IMCS_RESERVED   equ 000ffh	;  reserved set to 1
;
; MPCS register  bits
;
    CS_MPCS_EX         equ 00080h
    CS_MPCS_MS         equ 00040h
    CS_MPCS_EXWT       equ 00008h   ;   EXTENDED PCS WAIT STATES
    CS_MPCS_8K         equ 00100h
    CS_MPCS_16K        equ 00200h
    CS_MPCS_32K        equ 00400h
    CS_MPCS_64K        equ 00800h
    CS_MPCS_128K       equ 01000h
    CS_MPCS_256K       equ 02000h
    CS_MPCS_512K       equ 04000h
    CS_MPCS_PCS_ADDR   equ 00000h   ;   PCS6-5 are addr lines

;
; MMCS register bits
;
    CS_MMCS_8K         equ 00200h
    CS_MMCS_16K        equ 00400h
    CS_MMCS_32K        equ 00800h
    CS_MMCS_64K        equ 01000h
    CS_MMCS_96K        equ 01800h
    CS_MMCS_128K       equ 02000h
    CS_MMCS_256K       equ 04000h
    CS_MMCS_512K       equ 08000h
    CS_MMCS_704K       equ 0b000h 
    CS_MMCS_896K       equ 0e000h

;
; -------------------------------------------------------------
; Timer register (1&2 - 0x56, 0x5e)
    TMR_ENABLE         equ 08000h
    TMR_INH            equ 04000h
    TMR_INT            equ 02000h   ;   generate intrpt rest
    TMR_RIU            equ 01000h   ;   ax count reached flag
    TMR_MC             equ 00020h   ;   max count reached flag
    TMR_RTG            equ 00010h   ;   retrigger bit
    TMR_2PRES          equ 00008h   ;   timer 2 is a prescaler
    TMR_EXT            equ 00004h   ;   use external timer
    TMR_ALT            equ 00002h
    TMR_CONT           equ 00001h   ;   continuous mode
    TMR_START          equ TMR_ENABLE+TMR_INH     ;   start timer
; These fields apply to master mode interrupt control registers
; Priorities also apply to interrupt priority register (INT_PMSK)
    INT_PRI0           equ 00000h   ;   highest priority
    INT_PRI1           equ 00001h   ;   priority = 1
    INT_PRI2           equ 00002h   ;   priority = 2
    INT_PRI3           equ 00003h   ;   priority = 3
    INT_PRI4           equ 00004h   ;   priority = 4
    INT_PRI5           equ 00005h   ;   priority = 5
    INT_PRI6           equ 00006h   ;   priority = 6
    INT_PRI7           equ 00007h   ;   lowest priority
    INT_DISABLE        equ 00008h   ;   disable interrupt
    INT_ENABLE         equ 00000h   ;   enable interrupt
    INT_LEVEL          equ 00010h   ;   level triggered mode
    INT_EDGE           equ 00000h   ;   edge triggered mode

; These two fields apply only to INT_INT0 and INT_INT1

    INT_CASCADE        equ 00020h   ;   cascade mode enable
    INT_SFNM           equ 00040h   ;   specl fully nested mode
; Interrupt Status Register fields (INT_STAT)
    INT_DHLT           equ 08000h   ;   halt DMA activity
    INT_STAT_TMR2      equ 00004h   ;   TMR2 has intrpt pending
    INT_STAT_TMR1      equ 00002h   ;   TMR1 has intrpt pending
    INT_STAT_TMR0      equ 00001h   ;   TMR0 has intrpt pending
;
; These fields apply to the interrupt request register (INT_IREQ),
; the interrupt in-service register (INT_INSV), and the interrupt
; mask register (INT_MASK)
    INT_SPT0           equ 00400h   ;   serial port
    INT_WATCHDOG       equ 00200h   ;   watchdog timer
    INT_I4             equ 00100h   ;   INT4
    INT_I3             equ 00080h   ;   INT3
    INT_I2             equ 00040h   ;   INT2
    INT_I1             equ 00020h   ;   INT1
    INT_I0             equ 00010h   ;   INT0
    INT_D1             equ 00008h   ;   DMA1
    INT_D0             equ 00004h   ;   DMA0
    INT_TIMER          equ 00001h   ;   any timer (see INT_STAT)
; EOI register fields
    EOI_NONSPEC        equ 08000h   ;   non-specific EOI
; Interrupt poll and poll status register fields
    INT_POLL_IREQ      equ 08000h   ;   interrupt pending flag
; Synchronous  serial port status register fields (SS_STAT)
    SS_STAT_ERR        equ 00004h   ;   error flag
    SS_STAT_COMPLETE   equ 00002h   ;   transaction complete
    SS_STAT_BUSY       equ 00001h   ;   SS port busy flag
; Synchronous  serial port control register fields (SS_CTL)
    SS_CTL_CLK2        equ 00000h   ;   SS clck = 1/2 proc clck
    SS_CTL_CLK4        equ 00010h   ;   SS clck = 1/4 proc clck
    SS_CTL_CLK8        equ 00020h   ;   SS clck = 1/8 proc clck
    SS_CTL_CLK16       equ 00030h   ;   SS clck = 1/16proc clck
	SS_CTL_DEN0        equ 00001h   ;   SDEN0 Enable
	SS_CTL_DEN1        equ 00002h   ;   SDEN0 Enable
;
; ======================================================================
; Interrupt types
; These are the values to write to the EOI register
    EOITYPE_TMR0       equ 008h
    EOITYPE_TMR1       equ EOITYPE_TMR0
    EOITYPE_TMR2       equ EOITYPE_TMR0
    EOITYPE_DMA0       equ 00ah
    EOITYPE_DMA1       equ 00bh
    EOITYPE_INT0       equ 00ch
    EOITYPE_INT1       equ 00dh
    EOITYPE_INT2       equ 00eh
    EOITYPE_INT3       equ 00fh
    EOITYPE_INT4       equ 010h
    EOITYPE_INT5       equ EOITYPE_DMA0
    EOITYPE_INT6       equ EOITYPE_DMA1
    EOITYPE_SPRT       equ 014h
    EOITYPE_SPRT0      equ 014h
    EOITYPE_WDOG       equ 011h
    EOITYPE_NONSPEC    equ 08000h

    ITYPE_DIV          equ 00h             ;   Divide error
    ITYPE_TRACE        equ 01h             ;   trace trap
    ITYPE_NMI          equ 02h             ;   non-maskable interrupt
    ITYPE_BREAK        equ 03h             ;   breakpoint
    ITYPE_OVERFLOW     equ 04h             ;   overflow
    ITYPE_BOUNDS       equ 05h             ;   bound
    ITYPE_ILLOP        equ 06h             ;   illegal opcode
    ITYPE_ESC          equ 07h             ;   ESC
    ITYPE_TMR0         equ EOITYPE_TMR0    ;   Timer 0
    ITYPE_TMR1         equ 012h            ;   Timer 1
    ITYPE_TMR2         equ 013h            ;   Timer 2
    ITYPE_DMA0         equ EOITYPE_DMA0    ;   DMA 0
    ITYPE_DMA1         equ EOITYPE_DMA1    ;   DMA 1
    ITYPE_INT0         equ EOITYPE_INT0    ;   INT0
    ITYPE_INT1         equ EOITYPE_INT1    ;   INT1
    ITYPE_INT2         equ EOITYPE_INT2    ;   INT2
    ITYPE_INT3         equ EOITYPE_INT3    ;   INT3
    ITYPE_INT4         equ EOITYPE_INT4    ;   INT4
    ITYPE_SPRT         equ EOITYPE_SPRT    ;   Serial Port 0
    ITYPE_SPRT0        equ EOITYPE_SPRT0   ;   Serial Port 0
    ITYPE_WDOG         equ EOITYPE_WDOG    ;   Watchdog timer
    ITYPE_SPRT         equ EOITYPE_SPRT    ;   Asynchronous serial port
;
; -----------------------------------------------------------------------
; Baud rate constants
    BAUD300at20        equ 2082
    BAUD300at25        equ 2603
    BAUD300at33        equ 3471
    BAUD300at40        equ 4165

    BAUD600at20        equ 1040
    BAUD600at25        equ 1301
    BAUD600at33        equ 1735
    BAUD600at40        equ 2082

    BAUD1200at20       equ 519
    BAUD1200at25       equ 650
    BAUD1200at33       equ 867
    BAUD1200at40       equ 1040

    BAUD2400at20       equ 259
    BAUD2400at25       equ 324
    BAUD2400at33       equ 433
    BAUD2400at40       equ 519

    BAUD4800at20       equ 129
    BAUD4800at25       equ 161
    BAUD4800at33       equ 216
    BAUD4800at40       equ 259

    BAUD9600at20       equ 64
    BAUD9600at25       equ 80
    BAUD9600at33       equ 107
    BAUD9600at40       equ 129

    BAUD14400at20      equ 42
    BAUD14400at25      equ 53
    BAUD14400at33      equ 71
    BAUD14400at40      equ 85

    BAUD19200at20      equ 31
    BAUD19200at25      equ 39
    BAUD19200at33      equ 53
    BAUD19200at40      equ 64

;
; -----------------------------------------------------------------------
; ES-specific watch dog register values

WDOG_REG_ES EQU 0FFE6h
WDOG_WR1_ES EQU 03333h
WDOG_WR2_ES EQU 0cccch

;
; -----------------------------------------------------------------------
; CC-specific register values


PIOMODE0_CC		EQU	0FFC0h
PIODIR0_CC		EQU	0FFC2h
PIODATA0_CC		EQU	0FFC4h
PIOSET0_CC		EQU 0FFC6h
PIOCLEAR0_CC	EQU	0FFC8h

PIOMODE1_CC		EQU	0FFCAh
PIODIR1_CC		EQU	0FFCCh
PIODATA1_CC		EQU	0FFCEh
PIOSET1_CC		EQU 0FFD0h
PIOCLEAR1_CC	EQU	0FFD2h

PIOMODE2_CC		EQU	0FFD4h
PIODIR2_CC		EQU	0FFD6h
PIODATA2_CC		EQU	0FFD8h
PIOSET2_CC		EQU 0FFDAh
PIOCLEAR2_CC	EQU	0FFDCh

PIO0_MCS0_CC    EQU 00010h
PIO0_MCS3_CC	EQU	00020h

WDTCON_CC		EQU 0FFE0h
CDRAM_CC		EQU 0FFAAh
EDRAM_CC		EQU 0FFACh
