/******************************************************************************
 *                                                                            *
 *     EMON.H                                                                 *
 *                                                                            *
 *     This file contains definitions for variables and functions             *
 *     within EMON which are used in one file and defined in another.         *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#ifndef _emon_h_
#define _emon_h_

#include "emondata.h"
#include <string.h>
#include "message.h"

extern EmonInternals EmonGlobalData;

#define   EGD        EmonGlobalData

typedef void (* WRITECHAR)(BYTE);


extern STACKREC ROM DummyStackRec;


int      __cdecl _inp(unsigned);
unsigned __cdecl _inpw(unsigned);
int      __cdecl _outp(unsigned, int);
unsigned __cdecl _outpw(unsigned, unsigned);

// Procedural address conversions:
//      LinToSeg -- Linear to segmented address
//      SegToLin -- Segmented address to linear address
//      GetFullPtr -- Converts WORD ptr to full DWORD, for TEXT or DATA seg

LPVOID LinToSeg(DWORD Linear);
DWORD  SegToLin(LPVOID Segmented);
LPVOID GetFullPtr(WORD ptr);

//
// Global Function Prototypes
//
DWORD FreeFlash(BOOL recalculate);
DWORD   CheckSum(LPVOID Source, WORD Count);
void    ForceMonitorToRAM(BOOL DoIt);
BOOL    RunSystemFromRAM(BOOL DoIt);
void    LockMonitorInRAM(void);
void    restart(WORD value);                // from emstart.asm
void    FlushAndRestart(WORD value);
WORD    CurrentDS(void);
WORD    OriginalCS(void);
WORD    MonitorROMSize(void);
WORD    MonitorRAMSize(void);
WORD    HWInitSize(void);
BOOL    RunningFromRAM(void);
BOOL    SystemInRAM(void);
void    MonitorInt3(void);
void    cdecl  Int3Handler(void);
void    UnhandledInt(WORD Cause);
void    FarJmp(LPVOID Address);

BOOL    GetAnyMem(FPANYADDRESS a, LPVOID Buffer, WORD TransferSize);
BOOL    SetAnyMem(FPANYADDRESS a, LPVOID Buffer, WORD TransferSize);

//
//    OutToPCBReg() -- write to PCB register in I/O Space
//    This function does "out  dx, al".  For PCB registers,
//    which are always accessed internally as 16 bits, this
//    is the same thing as an "out  dx, ax" except that it
//    results in one less bus cycle on the Am188Ex processors.
//
#pragma warning(disable:4505)    // Don't warn if we don't use this

__inline void OutToPCBReg( WORD usAddress, WORD usValue )
{
#if 1
    _outp(usAddress, usValue);            // MS compiler always loads AX
#else
    _outpw(usAddress, usValue);
#endif
}

//
//    function prototypes
//
void SetDummyIntVector(WORD index);

//
// Line-oriented input variables.
//
extern char _near MainMsgPrompt[];

//
// Higher level output functions in printf.c
//
void        vpprintf(WRITECHAR proc, LPCSTR far * ptrptr);
BOOL        iprinterror(LPCSTR * format);
void        OutputErrorString(void);
void        ShowUserSomethingsHappening(void);

// AppBootVector contains a far jump to application code.

#define AppBootVector (GetFlashBootBase()-16)

// FindFlash() returns TRUE if it can locate a flash device.  In this
// case, it will have set all the internal parameters for the device.

BOOL FindFlash(void);

// ShowFlashParameters shows the user the flash information.  It returns
// FALSE if FindFlash has not been called or did not find a device.

BOOL ShowFlashParameters(void);

// EraseFlash erases according to the given parameter:
// 0-7 -- given sector, assuming it's not in the boot block
// 0Ah -- all non-boot block sectors
// 0FFFFh -- all boot block sectors

BOOL EraseFlash( WORD WhichSectors );

// ProgramFlash places a string of bytes into the flash memory

BOOL ProgramFlash(DWORD Dest, LPBYTE Source, WORD DataSize);

// GetFlashBase returns the base address of the flash device

DWORD GetFlashBase(void);

// GetFlashBootBase returns the base address of the boot area in the flash

DWORD GetFlashBootBase(void);

// GetFlashSize returns the size of the flash device in K

WORD GetFlashSize(void);

WORD GetRegValue(LPDWORD d);

BOOL UpdateInProgress(void);
void UpdateMonitor(BOOL OnlyIfInterrupted);

// This command is in CMDREG.C

void CommandRegister(BOOL JustPrint);

// These commands are in CMDMEM.C

void CommandCompare(void);
void CommandDump(void);
void CommandExamine(void);
void CommandFill(void);
void CommandMove(void);
void CommandSearch(void);

// This command is in CMDFILE.C
// Returns true if a load record found and CS/IP updated.

BOOL CommandHexFile(LPCSTR PName);
BOOL CommandLoad(int LoadCount);

// These commands in PermVars.C

void CommandPermVar(void);
void InitPermVars(void);
BOOL MigratePermVars(BOOL TestOnly, BOOL Reflashulate);

void InitSystem(BOOL FirstTime);
BOOL AutoBaud(BOOL ReInitialize);

void DisableAllInterrupts(void);
void EnableAllInterrupts(void);

DWORD GetProtectFlash(void);
DWORD GetAutoRun(void);

#endif
