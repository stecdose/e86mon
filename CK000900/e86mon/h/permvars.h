/******************************************************************************
 *                                                                            *
 *     PERMVARS.H                                                             *
 *                                                                            *
 *     This file contains structure definitions which allow separation        *
 *     of permanent variable code (in PERMVARS.C) from the permanent          *
 *     variable values (in CPU-specific modules).                             *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#define DISPLAY_NONE    0
#define DISPLAY_DECIMAL 1
#define DISPLAY_DWORD   2
#define DISPLAY_WORD    3

/*********************************************************************
  Permanent variables actually have two defaults, because the
  'Ptr' points to initialized data, which is burned into ROM.
  When a monitor is being updated, however, we want to minimize
  the window where the monitor is vulnerable to reset or power
  problems.  So in preparation for updating from an 'upgrade'
  ROM copy to the 'boot' ROM copy, we want to fix up the upgrading
  copy's permanent variables to match the current user selections.

  This is why we have the 'upgrade' value.
 *********************************************************************/

// Previous versions of E86MON had a single segment for both code and data,
// so the offset for any data was from the start of the file.  In order to
// facilitate changing this while still allowing backward compatibility,
// we add a separate RAM copy of the variable.

typedef struct {
    DWORD    RamValue;
    BYTE     DisplayType;
    BYTE     AllowChange;
}  RamPermVar;

// For legacy compatibility, the first element in PermVar _must_
// be the Value.  This structure stays in ROM and may be referenced
// from other programs.

typedef struct {
    DWORD               RomValue;            // MUST BE FIRST!  (Legacy)
    RamPermVar near *   RamCopy;
}  ROM RomPermVar;

typedef RomPermVar ROM * ROMPVAR;

// This structure should not be changed, or the array of permanent
// variables will not be backward-compatible with old monitors.
// This is important for migrating permanent variables forward
// on monitor upgrades.

typedef struct {
    PROMCHAR Name;                 // Text name of the permanent variable
    ROMPVAR  Ptr;                  // Pointer to the internal variable
    DWORD    Upgrade;              // Starts at FFFFFFFF, gets updated
                                   // during upgrade process.
} PermVarArray, far * LPPERM;

extern PermVarArray ROM PermArray[];

WORD GetPermByName(LPSTR String);
