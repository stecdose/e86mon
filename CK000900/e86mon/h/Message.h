/******************************************************************************
 *                                                                            *
 *     MESSAGE.H                                                              *
 *                                                                            *
 *     This file contains declarations for all messages used in EMON.         *
 *     Also, when included by USENGLIS.C, this file will instantiate          *
 *     the messages.  For other languages, copy this file into a              *
 *     language-specific C file, and then modify it.  This way,               *
 *     no other module should need to be modified or recompiled.              *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#ifndef  _message_h_
#define  _message_h_

#ifdef _usenglish_c_
#define BIGCHAR  const char _based( _segname ("BIG_CHAR" ))

    #define BIGMSG(module,name,string) \
        BIGCHAR module##Msg##name##[] = string
    #define MSG(module,name,string) \
        ROMCHAR module##Msg##name##[] = string
    #define ERRMSG(module,name,string) \
        ROMCHAR module##Msg##name##[] = #module " error: " string
#else
    #define BIGMSG MSG
    #define MSG(module,name,string) \
                 extern ROMCHAR module##Msg##name##[]
    #define ERRMSG(module,name,string)  MSG(module,name,module)
#endif

MSG(System,Name,           "E86MON");
MSG(System,Cancel,         " %2X ##\n");
MSG(System,StringCRLF,     "%s\n");
MSG(System,CharCRLF,       "%c\n");
MSG(System,Space,          " ");
MSG(System,2Spaces,        "  ");
MSG(System,3Spaces,        "   ");
MSG(System,CommaSpace,     ", ");
MSG(System,ColonSpaceSpace,":  ");
MSG(System,Dot,            ".");
MSG(System,Null,           "");
ERRMSG(System,MonReentered,   "Cannot modify flash within second monitor\n"
                 "             invocation (G FFFF0 to restart monitor)");

MSG(Console,YN,            "%s  (Y/N):  ");
MSG(Console,ParseErr,      "^  -- Error: expected %s.\n");
MSG(Console,EndOfLine,     "end of line");
MSG(Console,String,        "ASCII string");
MSG(Console,Decimal,       "decimal number");
MSG(Console,Byte,          "BYTE (1 or 2 hex digits)");
MSG(Console,Word,          "WORD (1-4 hex digits)");
MSG(Console,DWord,         "DWORD (1-8 hex digits)");
MSG(Console,Address,       "address (seg:offset or 5 hex digits)");
MSG(Console,RangeEnd,      "<address> or L <length>");
MSG(Console,RangeErr,      "second address > first, and in same memory space");
MSG(Console,List,          "list of bytes and/or quoted string");
MSG(Console,PlusMinus,     "plus or minus");
MSG(Console,NumReg,        "number or register");
MSG(Console,ReturnedFromBreak, "\n   (returning from monitor BREAK)\n");

MSG(Register,RegOrFlagName,"register name or 'f' followed by flag setting");
MSG(Register,RegNames,     "Valid register names are: %s.\n");
MSG(Register,FlagNames,    "Valid flag settings are: %s.\n");

MSG(Flash,Unknown,         "Unknown");
MSG(Flash,29F010,          "29F010");
MSG(Flash,29F040,          "29F040");
MSG(Flash,29F080,          "29F080");
MSG(Flash,29F016,          "29F016");
MSG(Flash,29F100B,         "29F100B");
MSG(Flash,29F100T,         "29F100T");
MSG(Flash,29F200B,         "29F200B");
MSG(Flash,29F200T,         "29F200T");
MSG(Flash,29F400B,         "29F400B");
MSG(Flash,29F400T,         "29F400T");
MSG(Flash,29LV400B,        "29LV400B");
MSG(Flash,29LV400T,        "29LV400T");
MSG(Flash,29F800B,         "29F800B");
MSG(Flash,29F800T,         "29F800T");

MSG(Flash,FlashParam1,     "\nFlash device:  %s -- %uK bytes "\
                                           "organized as %uK X %u"\
                           "\nApp sectors:   ");
MSG(Flash,FlashParam2,     "\nBoot sectors:  ");
MSG(Flash,SectorList,      "%d at %X");

MSG(Flash,ProgrammedOK,    "Device programmed successfully");
MSG(Flash,Erasing,         "Erasing flash sector at %lX...");
MSG(Flash,Protected,       "  Protected! (not erased by XA)\n");
MSG(Flash,UpdatePrompt,    "Are you sure?  (replace boot monitor)");
MSG(Flash,MigratePrompt,   "\nE86MON has been relocated to RAM.\n\n"\
                           "To resurrect a flash, switch to the\n"\
                           " bad flash and press 'Y'\n\n"\
                           "If this is not what you want to do,\n"\
                           "press 'N' or reset the demo board...");
MSG(Flash,Replace16bit,    "The monitor is running from an 8-bit flash.\n"\
                           "Do you want to replace a 16-bit device");
MSG(Flash,ReplacingBootBlock,  "Replacing Boot Block now (DO NOT DISTURB!)\n");
MSG(Flash,RetryProgram,    "\nAttempt Retry on Boot install?");
MSG(Flash,UpdateContinuation, "\nContinuing boot monitor update...\n");
MSG(Flash,CopyingMonitor,  "Now copying monitor to boot area...");
MSG(Flash,MonitorUpdated,  "\nMonitor updated!\n");

ERRMSG(Flash,CannotUpdate,     "Cannot update monitor -- %s\n");
MSG(Flash,NewInitTooBig,       "new hardware init code too large");
MSG(Flash,NewMonTooBig,        "new monitor too large");
MSG(Flash,ExecFromUpdate,      "must update from app flash area");
MSG(Flash,BadAppVector,        "app boot vector must point to new monitor");

ERRMSG(Flash,MustRunFromRAM,   "Cannot modify flash while executing from it\n");
ERRMSG(Flash,NoID,             "Cannot identify device");
ERRMSG(Flash,UnexpectedSentinel,          "Unexpected Sentinel");
ERRMSG(Flash,EraseFailed,  "Erase operation failed");
ERRMSG(Flash,ProgramFailed,"Program operation failed");
ERRMSG(Flash,ByteNotFF,    "Byte already programmed (erase first)");
ERRMSG(Flash,SectorRange,  "Invalid sector number");
ERRMSG(Flash,BadErase,     "Cannot erase the area you are running from!");

MSG(File,DummyAlert,       "Begin hex file transfer (using XON/XOFF) now...\n");
MSG(File,DownloadHex,      "Transferring hex file (Press Esc to abort)...");
MSG(File,HowToAbortHex,    "\nError encountered!  Press escape to continue.\n");
MSG(File,AtLineNumber,     " at file line number %u.");
MSG(File,DownloadOK,       "File download successful");

ERRMSG(File,ExeTooBig,     ".EXE file does not fit in memory"); 
ERRMSG(File,UserAbort,     "User aborted download");
ERRMSG(File,Checksum,      "Invalid checksum");
ERRMSG(File,NotReloc,      "'w' command requires relocatable file");
ERRMSG(File,NoColon,       "Record does not start with colon");
ERRMSG(File,NonHex,        "Non-hex character in record");
ERRMSG(File,EOF,           "Invalid EOF record");
ERRMSG(File,Segment,       "Invalid Segment or start record");
ERRMSG(File,Start,         "Invalid start location record");
ERRMSG(File,UnknownRecord, "Unknown record type");
ERRMSG(File,InvalidLength, "Record Length Incorrect");
ERRMSG(File,NoSuchLoad,    "Program not found in flash");
ERRMSG(File,BadFormat,     "Flash not formatted correctly (use XA to erase)");

#define MAKESTR2(what) #what
#define MAKESTR(what) MAKESTR2(what)

MSG(Main,Help,
"\nE86 Boot Monitor -- Version " MAKESTR(VER_STRING) "\n" \
"                        Copyright (C) 1994-1998 AMD, Austin, Texas, USA\n"\
     "\nHelp not available -- use LL command to load extension with help.\n\n");
MSG(Main,TooDeep,       "\nBreaks nested too deeply -- returning to previous level\n");
MSG(Main,AtBP,          "\nStopped at breakpoint\n");
MSG(Main,Invalid,       "command -- type '?' for help");
MSG(Main,Show1,  "\n   Module  Code Segment/Length   Data Segment/Length");
MSG(Main,Library,          "\n  %-15s%04X    %04X          %04X    %04X");
MSG(Main,Show2,         "\n\nFree data paragraphs: %04x"\
                          "\nCurrent system time:  %lu.%03u\n");

MSG(Main,TestingAutobaud, "Change your terminal to desired baudrate,"\
                          " then press 'a'.\n\r\r\r");


MSG(Main,MonitorMoved,  "Monitor locked into RAM\n");
MSG(Main,EmbeddedInt3,  "Broke at embedded int 3\n");
MSG(Main,UnhandledInt,  "Unhandled interrupt #%Xh\n");
MSG(Main,BreakReceived,\
            "\n\nBREAK received on serial port -- monitor entered.\n"\
            "    (Type G <enter> to continue with program)\n");
MSG(Main,Welcome,       "\n\nWelcome to AMD's EMon 186! "\
                           "     (? <Enter> for help)\n\n");
MSG(Main,ProgramTerminated,\
                  "\nProgram terminated with result code %02X\n\n");
MSG(Main,RamOverwritten,\
            "\nNote -- the flash operation used (overwrote) the RAM.\n\n");
MSG(Main,NoProgramLoaded, "\nNo program loaded\n\n");

MSG(Main,LAddress3Space, "%la   ");
MSG(Main,LAddressCRLF,   "%la\n");
MSG(Main,HexByteSpace,  "%02x ");
MSG(Main,LAddressHexByte,"%la  %02x  ");
MSG(Main,HexByteCRLF,   "  %02x\n");
MSG(Main,HexWordCRLF,   "  %04x\n");
MSG(Main,LAddrByteByteLAddr,"%lA  %02X  %02X  %lA\n");
MSG(Main,HexMath1,      "%lX+%lX=%lX, %lX-%lX=%lX, %lX*%lX=%lX\n");
MSG(Main,HexMath2,      "%lX/%lX=%lX, %lX%%%lX=%lX\n");
MSG(Main,MemLAddrRange,  "\nMemory region out of range: %la %x");

MSG(Perm,VarDecimal,         "    %-15s= %lu\n");
MSG(Perm,VarHexDword,        "    %-15s= %08lx\n");
MSG(Perm,VarHexWord,         "    %-15s= %04lx\n");
MSG(Perm,CantChange,         "This variable cannot be changed on this system.\n");
MSG(Perm,TestFirst,          "You can make your system unbootable if you\n"\
                             " program invalid values into the flash.\n\n"\
                             "Would you like to test by rebooting with\n"\
                             " this value before you make it permanent?");
MSG(Perm,Rebooting,          "\nRebooting... (press 'A' to autobaud again)\n");
MSG(Perm,AreYouSure,         "\nMake the permanent value of '%s' = %lu?");
MSG(Perm,AreYouSureW,        "\nMake the permanent value of '%s' = %04lx?");
MSG(Perm,AreYouSureDW,       "\nMake the permanent value of '%s' = %08lx?");
MSG(Perm,OutOfRoom,          "Out of room in permanent variable area\n");
MSG(Perm,VarUpdated,         "\nPermanent variable successfully updated.\n\n");
ERRMSG(Perm,VarNotFound,        "'%s' is not a valid permanent variable\n");
ERRMSG(Perm,MustBeBootMonitor,  "You must be running from the boot monitor\n"\
                                 "       to update permanent variables.\n");
ERRMSG(Perm,CurrentMismatch,    "The current value of '%s'\n"\
                                 "       does not match its boot value.\n");
ERRMSG(Perm,CannotFixDefault,   "The default value of '%s' cannot be updated.\n");


#endif
