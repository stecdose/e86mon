/******************************************************************************
 *                                                                            *
 *     AM186MSR.H                                                             *
 *                                                                            *
 *     This file contains hardware-specific declarations for                  *
 *     186EM, 186ER, and 186ES parts.                                         *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#ifndef _AM186ER_H_
#define _AM186ER_H_
// =====================================================================
// INTERNAL REGISTER OFFSETS

// Miscellaneous control registers
//
#define OFFS_RELC_REG      0xfe         // peripheral control block relocation
#define OFFS_RI_REG        0xfc         // refesh interval register
#define OFFS_BISTEA_REG    0xfa         // Build in self test error address
#define OFFS_BISTSC_REG    0xf8         // build in self test status/control
#define OFFS_RCFG_REG      0xf6         // reset configuration register
#define OFFS_PRL_REG       0xf4         // Processor release level register
#define OFFS_AUXC_REG      0xf2         // auxiliary cfg register (ES)
#define OFFS_PDCN_REG      0xf0         // Power-save control register
#define OFFS_WDOG_REG      0xe6         // watchdog timer register on ES
//
// Refresh control unit registers
//
#define OFFS_DRAM_ENAB     0xe4         // Enable RCU register
#define OFFS_DRAM_CLK      0xe2         // Clock prescaler register
#define OFFS_DRAM_MEM      0xe0         // Memory partition register
//
// DMA 1 registers
//
#define OFFS_DMA1_CTL      0xda         // DMA1 control register
#define OFFS_DMA1_CNT      0xd8         // DMA1 transfer count register
#define OFFS_DMA1_DSH      0xd6         // DMA1 destination addr high register
#define OFFS_DMA1_DES      0xd4         // DMA1 destination addr low register
#define OFFS_DMA1_SRH      0xd2         // DMA1 source addr high register
#define OFFS_DMA1_SRC      0xd0         // DMA1 source addr low register
//
// DMA 0 registers
//
#define OFFS_DMA0_CTL      0xca         // DMA0 control register
#define OFFS_DMA0_CNT      0xc8         // DMA0 transfer count register
#define OFFS_DMA0_DSH      0xc6         // DMA0 destination addr high register
#define OFFS_DMA0_DES      0xc4         // DMA0 destination addr low register
#define OFFS_DMA0_SRH      0xc2         // DMA0 source addr high register
#define OFFS_DMA0_SRC      0xc0         // DMA0 source addr low register
//
// Chip select registers
//
#define OFFS_CS_MPCS       0xa8         // PCS# and MCS# auxiliary register
#define OFFS_CS_MMCS       0xa6         // Midrange memory chip select reg.
#define OFFS_CS_PACS       0xa4         // Peripheral chip select register 
#define OFFS_CS_LMCS       0xa2         // lmsc register
#define OFFS_CS_UMCS       0xa0         // umcs register
#define OFFS_CS_IMCS       0xac         // Internal Memory Chip Select reg.
//
// ES Serial port 0 registers
// EM/ER Asynchronous serial port registers
//
#define OFFS_SPRT0_BDV     0x88         // serial port bauddiv reg
#define OFFS_SPRT0_RX      0x86         // serial port receive reg
#define OFFS_SPRT0_TX      0x84         // serial port transmit reg
#define OFFS_SPRT0_STAT    0x82         // serial port status reg
#define OFFS_SPRT0_CTL     0x80         // serial port control reg
#define OFFS_SPRT_BDV      0x88          // serial port bauddiv reg 
#define OFFS_SPRT_RX       0x86          // serial port receive reg
#define OFFS_SPRT_TX       0x84          // serial port transmit reg
#define OFFS_SPRT_STAT     0x82          // serial port status reg
#define OFFS_SPRT_CTL      0x80          // serial port control reg
//
// PIO registers
//
#define OFFS_PIO_DATA1     0x7a         // PIO data 1 register
#define OFFS_PIO_DIR1      0x78         // PIO direction 1 register
#define OFFS_PIO_MODE1     0x76         // PIO mode 1 register
#define OFFS_PIO_DATA0     0x74         // PIO data registers
#define OFFS_PIO_DIR0      0x72         // PIO direction register
#define OFFS_PIO_MODE0     0x70         // PIO mode 0 register
//
// Timer 2 control registers
//
#define OFFS_TMR2_CTL      0x66         // Timer 2 mode/control register
#define OFFS_TMR2_MAXA     0x62         // Timer 2 maxcount compare A register
#define OFFS_TMR2_CNT      0x60         // Timer 2 count register
//
// Timer 1 control registers
//
#define OFFS_TMR1_CTL      0x5e         // Timer 1 mode/control register
#define OFFS_TMR1_MAXB     0x5c         // Timer 1 maxcount compare B register
#define OFFS_TMR1_MAXA     0x5a         // Timer 1 maxcount compare A register
#define OFFS_TMR1_CNT      0x58         // Timer 1 count register
//
// Timer 0 control registers
//
#define OFFS_TMR0_CTL      0x56         // Timer 0 mode/control register
#define OFFS_TMR0_MAXB     0x54         // Timer 0 maxcount compare B register
#define OFFS_TMR0_MAXA     0x52         // Timer 0 maxcount compare A register
#define OFFS_TMR0_CNT      0x50         // Timer 0 count register
//
// Interrupt registers
//
#define OFFS_INT_SPRT0     0x44         // sprt 0 interrupt ctrl reg
#define OFFS_INT_SPRT1     0x42         // sprt 0 interrupt ctrl reg
#define OFFS_INT_SPRT      0x44         // serial port interrupt control reg
#define OFFS_INT_WDOG_EM   0x42         // Watchdog timer interrupt control
//
#define OFFS_INT_INT4      0x40         // INT4 control register
#define OFFS_INT_INT3      0x3e         // INT3 control register
#define OFFS_INT_INT2      0x3c         // INT2 control register
#define OFFS_INT_INT1      0x3a         // INT1 control register
#define OFFS_INT_INT0      0x38         // INT0 control register

#define OFFS_INT_DMA1      0x36         // DMA1 interrupt control register
#define OFFS_INT_DMA0      0x34         // DMA0 interrupt control register
#define OFFS_INT_INT6      OFFS_INT_DMA1 // ES only
#define OFFS_INT_INT5      OFFS_INT_DMA0 // ES only

#define OFFS_INT_TMR       0x32         // Timer interrupt control register

#define OFFS_INT_STAT      0x30         // Interrupt status register
#define OFFS_INT_IREQ      0x2e         // Interrupt request register
#define OFFS_INT_INSV      0x2c         // In-service register
#define OFFS_INT_PMSK      0x2a         // Priority mask register
#define OFFS_INT_MASK      0x28         // Interrupt mask register
#define OFFS_INT_PSTT      0x26         // Poll status register
#define OFFS_INT_POLL      0x24         // Poll register
#define OFFS_INT_EOI       0x22         // End-of-interrupt register
#define OFFS_INT_VEC       0x20         // Interrupt vector register
//
// ES Serial port 1 register
// EM/ER Synchronous serial port register
//
#define OFFS_SPRT1_BDV     0x18         // serial port bauddiv reg
#define OFFS_SPRT1_RX      0x16         // serial port receive reg
#define OFFS_SPRT1_TX      0x14         // serial port transmit reg
#define OFFS_SPRT1_STAT    0x12         // serial port status reg
#define OFFS_SPRT1_CTL     0x10         // serial port control reg

#define OFFS_SS_RX         0x18         // Synchronous serial receive register
#define OFFS_SS_TX0        0x16         // Synchronous serial transmit 0 reg.
#define OFFS_SS_TX1        0x14         // Synchronous serial transmit 1 reg.
#define OFFS_SS_CTL        0x12         // Synchronous serial control register
#define OFFS_SS_STAT       0x10         // Synchronous serial status register


//
// ======================================================================
// STANDARD EXTERNAL-MEMORY SEGMENT AND OFFSET MAP

#define CTL_OFF            0xff00        // Ctl reg offset for peripherals
#define DEF_RLC_OFF        0x00ff        // (ES) 0ffh is default A20-A28


// Miscellaneous control registers
//
#define RELC_REG           (CTL_OFF+OFFS_RELC_REG)
#define RI_REG             (CTL_OFF+OFFS_RI_REG)
#define BISTEA_REG         (CTL_OFF+OFFS_BISTEA_REG)
#define BISTSC_REG         (CTL_OFF+OFFS_BISTSC_REG)
#define RCFG_REG           (CTL_OFF+OFFS_RCFG_REG)
#define PRL_REG            (CTL_OFF+OFFS_PRL_REG)
#define AUXC_REG           (CTL_OFF+OFFS_AUXC_REG)  // ES only
#define PDCN_REG           (CTL_OFF+OFFS_PDCN_REG)
#define WDOG_REG           (CTL_OFF+OFFS_WDOG_REG)  // ES only
//
// Refresh control unit registers
//
#define DRAM_ENAB          (CTL_OFF+OFFS_DRAM_ENAB)
#define DRAM_CLK           (CTL_OFF+OFFS_DRAM_CLK)
#define DRAM_MEM           (CTL_OFF+OFFS_DRAM_MEM)
//
// DMA 1 registers
//
#define DMA1_CTL           (CTL_OFF+OFFS_DMA1_CTL)
#define DMA1_CNT           (CTL_OFF+OFFS_DMA1_CNT)
#define DMA1_DSH           (CTL_OFF+OFFS_DMA1_DSH)
#define DMA1_DES           (CTL_OFF+OFFS_DMA1_DES)
#define DMA1_SRH           (CTL_OFF+OFFS_DMA1_SRH)
#define DMA1_SRC           (CTL_OFF+OFFS_DMA1_SRC)
//
// DMA 0 registers
//
#define DMA0_CTL           (CTL_OFF+OFFS_DMA0_CTL)
#define DMA0_CNT           (CTL_OFF+OFFS_DMA0_CNT)
#define DMA0_DSH           (CTL_OFF+OFFS_DMA0_DSH)
#define DMA0_DES           (CTL_OFF+OFFS_DMA0_DES)
#define DMA0_SRH           (CTL_OFF+OFFS_DMA0_SRH)
#define DMA0_SRC           (CTL_OFF+OFFS_DMA0_SRC)
//
// Chip select registers
//
#define CS_MPCS            (CTL_OFF+OFFS_CS_MPCS)
#define CS_MMCS            (CTL_OFF+OFFS_CS_MMCS)
#define CS_PACS            (CTL_OFF+OFFS_CS_PACS)
#define CS_LMCS            (CTL_OFF+OFFS_CS_LMCS)
#define CS_UMCS            (CTL_OFF+OFFS_CS_UMCS)
#define CS_IMCS            (CTL_OFF+OFFS_CS_IMCS)
//
// ES Serial port 0 registers
// EM/ER Asynchronous serial port  registers
//
#define SPRT0_BDV          (CTL_OFF+OFFS_SPRT0_BDV)
#define SPRT0_RX           (CTL_OFF+OFFS_SPRT0_RX)
#define SPRT0_TX           (CTL_OFF+OFFS_SPRT0_TX)
#define SPRT0_STAT         (CTL_OFF+OFFS_SPRT0_STAT)
#define SPRT0_CTL          (CTL_OFF+OFFS_SPRT0_CTL)
#define SPRT_BDV           (CTL_OFF+OFFS_SPRT_BDV)
#define SPRT_RX            (CTL_OFF+OFFS_SPRT_RX)
#define SPRT_TX            (CTL_OFF+OFFS_SPRT_TX)
#define SPRT_STAT          (CTL_OFF+OFFS_SPRT_STAT)
#define SPRT_CTL           (CTL_OFF+OFFS_SPRT_CTL)
//
// PIO registers
//
#define PIO_DATA1          (CTL_OFF+OFFS_PIO_DATA1)
#define PIO_DIR1           (CTL_OFF+OFFS_PIO_DIR1)
#define PIO_MODE1          (CTL_OFF+OFFS_PIO_MODE1)
#define PIO_DATA0          (CTL_OFF+OFFS_PIO_DATA0)
#define PIO_DIR0           (CTL_OFF+OFFS_PIO_DIR0)
#define PIO_MODE0          (CTL_OFF+OFFS_PIO_MODE0)
//
// Timer 2 control registers
//
#define TMR2_CTL           (CTL_OFF+OFFS_TMR2_CTL)
#define TMR2_MAXA          (CTL_OFF+OFFS_TMR2_MAXA)
#define TMR2_CNT           (CTL_OFF+OFFS_TMR2_CNT)
//
// Timer 1 control registers
//
#define TMR1_CTL           (CTL_OFF+OFFS_TMR1_CTL)
#define TMR1_MAXB          (CTL_OFF+OFFS_TMR1_MAXB)
#define TMR1_MAXA          (CTL_OFF+OFFS_TMR1_MAXA)
#define TMR1_CNT           (CTL_OFF+OFFS_TMR1_CNT)
//
// Timer 0 control registers
//
#define TMR0_CTL           (CTL_OFF+OFFS_TMR0_CTL)
#define TMR0_MAXB          (CTL_OFF+OFFS_TMR0_MAXB)
#define TMR0_MAXA          (CTL_OFF+OFFS_TMR0_MAXA)
#define TMR0_CNT           (CTL_OFF+OFFS_TMR0_CNT)
//
// Interrupt registers
//
#define INT_SPRT0          (CTL_OFF+OFFS_INT_SPRT0)
#define INT_SPRT1          (CTL_OFF+OFFS_INT_SPRT1)  //ES
#define INT_SPRT           (CTL_OFF+OFFS_INT_SPRT)
#define INT_WDOG_EM        (CTL_OFF+OFFS_INT_WDOG_EM)
//
#define INT_INT4           (CTL_OFF+OFFS_INT_INT4)
#define INT_INT3           (CTL_OFF+OFFS_INT_INT3)
#define INT_INT2           (CTL_OFF+OFFS_INT_INT2)
#define INT_INT1           (CTL_OFF+OFFS_INT_INT1)
#define INT_INT0           (CTL_OFF+OFFS_INT_INT0)

#define INT_DMA1           (CTL_OFF+OFFS_INT_DMA1)
#define INT_DMA0           (CTL_OFF+OFFS_INT_DMA0)
#define INT_INT6           (CTL_OFF+OFFS_INT_INT6)
#define INT_INT5           (CTL_OFF+OFFS_INT_INT5)

#define INT_TMR            (CTL_OFF+OFFS_INT_TMR)

#define INT_STAT           (CTL_OFF+OFFS_INT_STAT)
#define INT_IREQ           (CTL_OFF+OFFS_INT_IREQ)
#define INT_INSV           (CTL_OFF+OFFS_INT_INSV)
#define INT_PMSK           (CTL_OFF+OFFS_INT_PMSK)
#define INT_MASK           (CTL_OFF+OFFS_INT_MASK)
#define INT_PSTT           (CTL_OFF+OFFS_INT_PSTT)
#define INT_POLL           (CTL_OFF+OFFS_INT_POLL)
#define INT_EOI            (CTL_OFF+OFFS_INT_EOI)
#define INT_VEC            (CTL_OFF+OFFS_INT_VEC)
//
// ES Serial port 1 register
// EM/ER Synchronous serial port register
//
#define SPRT1_BDV          (CTL_OFF+OFFS_SPRT1_BDV)
#define SPRT1_RX           (CTL_OFF+OFFS_SPRT1_RX)
#define SPRT1_TX           (CTL_OFF+OFFS_SPRT1_TX)
#define SPRT1_STAT         (CTL_OFF+OFFS_SPRT1_STAT)
#define SPRT1_CTL          (CTL_OFF+OFFS_SPRT1_CTL)

#define SS_RX              (CTL_OFF+OFFS_SS_RX)
#define SS_TX0             (CTL_OFF+OFFS_SS_TX0)
#define SS_TX1             (CTL_OFF+OFFS_SS_TX1)
#define SS_CTL             (CTL_OFF+OFFS_SS_CTL)
#define SS_STAT            (CTL_OFF+OFFS_SS_STAT)

// =======================================================================
// Internal Register field definitions

// RELOCATION REGISTER

#define REL_REG_SLAVE      0x4000         // slave interrupt controls
#define REL_REG_MEMSPACE   0x1000         // PCB in memory space

// --------------------------------------------------------------
// Processor release level (PRL_REG - offset 0xf4)

#define PRL_REG_ES_REVA    0x1000          //  Am188ES
#define PRL_REG_ER_REVA8   0x2100          //  Am188ER 
#define PRL_REG_ER_REVA6   0x2000          //  Am186ER 
#define PRL_REG_EM_REVC    0x0100          //  Am186EM 
#define PRL_REG_EM_REVD    0x0200          //  Am186EM 
#define PRL_REG_EM_REVE    0x0300          //  Am186EM 
#define PRL_REG_EM_REVF    0x0400          //  Am186EM 
#define PRL_REG_EM_REVG    0x0500          //  Am186EM 


// --------------------------------------------------------------
// Power-save control register (PDCN_REG or SYSCON - offset 0xf0)

#define PDCN_REG_PSEN      0x8000          //  Power-save enable bit
#define PDCN_REG_MCSBIT    0x4000          //  Power-save enable bit
#define PDCN_REG_DSDEN     0x2000          //  Power-save enable bit
#define PDCN_REG_PWD       0x1000          //  Power-save enable bit
#define PDCN_REG_CBF       0x0800          //  CLKOUTB Output Frequency
#define PDCN_REG_CBD       0x0400          //  CLKOUTB Drive Disable
#define PDCN_REG_CAF       0x0200          //  CLKOUTA Output Frequency
#define PDCN_REG_CAD       0x0100          //  CLKOUTA Drive Disable
#define PDCN_REG_CDIV1     0x0000          //  clock div factor = 1
#define PDCN_REG_CDIV2     0x0001          //  clock div factor = 2
#define PDCN_REG_CDIV4     0x0002          //  clock div factor = 4
#define PDCN_REG_CDIV8     0x0003          //  clock div factor = 8
#define PDCN_REG_CDIV16    0x0004          //  clock div factor = 16
#define PDCN_REG_CDIV32    0x0005          //  clock div factor = 32
#define PDCN_REG_CDIV64    0x0006          //  clock div factor = 64
#define PDCN_REG_CDIV128   0x0007          //  clock div factor = 128
//
// RAM refresh control
//
#define DRAM_ENABLE        0x8000          //  enable bit for DRAM
//
// --------------------------------------------------------------
// INTERRUPT IN-SERVICE REGISTER BITS (INSERV - 0x2c)
//
#define INT_INSV_SPRT0     0x0400          //  serial port 0x
#define INT_INSV_SPRT1     0x0200          //  serial port 1
#define INT_INSV_INT4      0x0100          //  external int 4
#define INT_INSV_INT3      0x0080          //  external int 3
#define INT_INSV_INT2      0x0040          //  external int 2
#define INT_INSV_INT1      0x0020          //  external int 1
#define INT_INSV_INT0      0x0010          //  external int 0x
#define INT_INSV_DMA1      0x0008          //  DMA1
#define INT_INSV_INT6      0x0008          //  ES INT6 same as DMA1
#define INT_INSV_DMA0      0x0004          //  DMA0
#define INT_INSV_INT5      0x0004          //  ES INT5 same as DMA0
#define INT_INSV_TMR       0x0001          //  all of the timers (except WDOG)

// -------------------------------------------------------------
// INTERRUPT REQUEST REGISTER BITS (REQST - 0x2e)
#define INT_REQ_SPRT0      INT_INSV_SP0
#define INT_REQ_SPRT1      INT_INSV_SP1
#define INT_REQ_INT4       INT_INSV_INT4
#define INT_REQ_INT3       INT_INSV_INT3
#define INT_REQ_INT2       INT_INSV_INT2
#define INT_REQ_INT1       INT_INSV_INT1
#define INT_REQ_INT0       INT_INSV_INT0
#define INT_REQ_DMA1       INT_INSV_DMA1
#define INT_REQ_INT6       INT_INSV_INT6
#define INT_REQ_DMA0       INT_INSV_DMA0
#define INT_REQ_INT5       INT_INSV_INT5
#define INT_REQ_TMR        INT_INSV_TMR
//
// -------------------------------------------------------------
// PIO PINS
#define PIO0_TMRI1         0x0001    // PIO 0
#define PIO0_TMRO1         0x0002    // PIO 1
#define PIO0_PCS6          0x0004    // PIO 2
#define PIO0_PCS5          0x0008    // PIO 3
#define PIO0_DTR           0x0010    // PIO 4
#define PIO0_DEN           0x0020    // PIO 5
#define PIO0_SRDY          0x0040    // PIO 6
#define PIO0_A17           0x0080    // PIO 7
#define PIO0_A18           0x0100    // PIO 8
#define PIO0_A19           0x0200    // PIO 9
#define PIO0_TMRO0         0x0400    // PIO 10
#define PIO0_TMRI0         0x0800    // PIO 11
#define PIO0_DRQ0          0x1000    // PIO 12
#define PIO0_DRQ1          0x2000    // PIO 13
#define PIO0_MCS0          0x4000    // PIO 14
#define PIO0_MCS1          0x8000    // PIO 15

#define PIO1_PCS0          0x0001    // PIO 16
#define PIO1_PCS1          0x0002    // PIO 17
#define PIO1_PCS2          0x0004    // PIO 18
#define PIO1_PCS3          0x0008    // PIO 19
#define PIO1_RTS0          0x0010    // PIO 20 on ES
#define PIO1_CTS0          0x0020    // PIO 21 on ES
#define PIO1_SCLK          0x0010    // PIO 20 on EM/ER
#define PIO1_SDATA         0x0020    // PIO 21 on EM/ER
#define PIO1_TX0           0x0040    // PIO 22 on ES
#define PIO1_RX0           0x0080    // PIO 23 on ES
#define PIO1_SDEN0         0x0040    // PIO 22 on EM/ER
#define PIO1_SDEN1         0x0080    // PIO 23 on EM/ER
#define PIO1_MCS2          0x0100    // PIO 24
#define PIO1_MCS3          0x0200    // PIO 25
#define PIO1_UZI           0x0400    // PIO 26
#define PIO1_TX            0x0800    // PIO 27
#define PIO1_RX            0x1000    // PIO 28
#define PIO1_TX1           0x0800    // PIO 27
#define PIO1_RX1           0x1000    // PIO 28
#define PIO1_S6            0x2000    // PIO 29
#define PIO1_INT4          0x4000    // PIO 30
#define PIO1_INT2          0x8000    // PIO 31
//
//
// Asynchronous serial port control register (SPRT_CTL)
//
//  -------------------------------------------------------------
//  SERIAL PORT CONTROL REGISTERS (1&2 - 0x80, 0x10)
//  DMA bits -- ES ONLY
#define SPRT_CTL_RD0_TD1  0x2000    //  Receive DMA0, Transmit DMA1
#define SPRT_CTL_RD1_TD0  0x4000    //  Receive DMA1, Transmit DMA0
#define SPRT_CTL_RD0_TOFF 0x8000    //  Receive DMA0, Transmit off
#define SPRT_CTL_RD1_TOFF 0xA000    //  Receive DMA1, Transmit off
#define SPRT_CTL_ROFF_TD0 0xC000    //  Receive off, Transmit DMA0
#define SPRT_CTL_ROFF_TD1 0xE000    //  Receive off, Transmit DMA1
//
// Other serial port bits -- EM/ER only
//
#define SPRT_CTL_TXIE_EM      0x0800   //  enable transmit intrpt
#define SPRT_CTL_RXIE_EM      0x0400   //  enable receive intrpt
#define SPRT_CTL_LOOP_EM      0x0200   //  enable loop-back mode
#define SPRT_CTL_BRK_EM       0x0100   //  send break or *break
#define SPRT_CTL_BRKHIGH_EM   0x0080   //  break value is high
#define SPRT_CTL_BRKLOW_EM    0x0000   //  break value is low
#define SPRT_CTL_NOPARITY_EM  0x0000   //  no parity bit
#define SPRT_CTL_EVEN_EM      0x0060   //  even parity
#define SPRT_CTL_ODD_EM       0x0040   //  odd parity
#define SPRT_CTL_8BITS_EM     0x0010   //  char lengt is 8 bits
#define SPRT_CTL_7BITS_EM     0x0000   //  char lengt is 7 bits
#define SPRT_CTL_2STOP_EM     0x0008   //  two stop bits
#define SPRT_CTL_1STOP_EM     0x0000   //  one stop bit
#define SPRT_CTL_TX_EM        0x0004   //  enable transmitter
#define SPRT_CTL_RSIE_EM      0x0002   //  enable Rx error intrpts
#define SPRT_CTL_RX_EM        0x0001   //  enable receiver
//
//
// Other serial port bits -- ES Only
//
#define SPRT_CTL_RSIE_ES      0x1000   //  enable Rx status interrupts
#define SPRT_CTL_BRK_ES       0x0800   //  send break
#define SPRT_CTL_TB8_ES       0x0400   //  Transmit data bit 8
#define SPRT_CTL_HS_ES        0x0200   //  hardware handshake enable
#define SPRT_CTL_TXIE_ES      0x0100   //  enable transmit interrupt
#define SPRT_CTL_RXIE_ES      0x0080   //  enable receive interrupt
#define SPRT_CTL_TMODE_ES     0x0040   //  enable transmitter
#define SPRT_CTL_RMODE_ES     0x0020   //  enable receiver 
#define SPRT_CTL_EVN_ES       0x0010   //  even parity
#define SPRT_CTL_PE_ES        0x0008   //  enable parity checking
#define SPRT_CTL_MODE1_ES     0x0001   //  Async. mode A
#define SPRT_CTL_MODE2_ES     0x0002   //  Async. address recog. mode
#define SPRT_CTL_MODE3_ES     0x0003   //  Async. mode B
#define SPRT_CTL_MODE4_ES     0x0004   //  Async. mode C
//
// Asynchronous serial port status register (SPRT_STAT) EM/ER
//
#define SPRT_STAT_TEMT_EM     0x0040   //  transmitter is empty
#define SPRT_STAT_THRE_EM     0x0020   //  Tx holding reg. empty
#define SPRT_STAT_RDR_EM      0x0010   //  receive char ready
#define SPRT_STAT_BRK_EM      0x0008   //  break received
#define SPRT_STAT_FRAME_EM    0x0004   //  framing error detected
#define SPRT_STAT_PARITY_EM   0x0002   //  parity error detected
#define SPRT_STAT_OVERFLOW_EM 0x0001   //  receive overflow
//
// Asynchronous serial port status register (SPRT_STAT) ES only
//
#define SPRT_STAT_BRK1_ES     0x0400  //  Long break detected
#define SPRT_STAT_BRK0_ES     0x0200  //  Short break detected
#define SPRT_STAT_RB8_ES      0x0100  //  Receive data bit 9
#define SPRT_STAT_RDR_ES      0x0080  //  Receive data ready
#define SPRT_STAT_THRE_ES     0x0040  //  Tx holding reg. empty
#define SPRT_STAT_FRAME_ES    0x0020  //  Framing error detected
#define SPRT_STAT_OVERFLOW_ES 0x0010  //  Overrun error detected
#define SPRT_STAT_PARITY_ES   0x0008  //  Parity error detected
#define SPRT_STAT_TEMT_ES     0x0004  //  transmitter is empty
#define SPRT_STAT_HS0_ES      0x0002  //  *CTS signal asserted
//
//  all Sbreaks must set this bit
#define SPRT_STAT_BRK_ES    SPRT_STAT_BRK0_ES


// -----------------------------------------------------------------------
// DMA Control Registers (1&2 - 0xda, 0xca)

#define DMA_DEST_MEM       0x8000   // DMA dest. 1=memory, 0=I/O space
#define DMA_DEST_DEC       0x4000   // decrement DMA dest addr
#define DMA_DEST_INC       0x2000   // increment DMA dest addr
#define DMA_SRC_MEM        0x1000   // DMA source 1=memory, 0=I/O space
#define DMA_SRC_DEC        0x0800   // decrement DMA src addr
#define DMA_SRC_INC        0x0400   // increment DMA src addr
#define DMA_TC             0x0200   // DMA uses terminal count
#define DMA_INT            0x0100   // intrpt on terminal count
#define DMA_SYN1           0x0080   // 1=Src. Sync.2=Dest. Sync.3=Undef.
#define DMA_SYN0           0x0040   // SYN1:0 0x = Unsynchronized
#define DMA_P              0x0020   // if set, channel=hi prio.,else low
#define DMA_IDRQ           0x0010   // if set, use timer 2
#define DMA_EXT            0x0008   // (ES only) if set, DRQ used for ext. int.
#define DMA_CHG            0x0004   // set before DMA_STRT can be set
#define DMA_STRT           0x0002   // if set, DMA channel is "armed"
#define DMA_WORD           0x0001   // if set, word transfers, else byte
#define DMA_BYTE           0x0000   // Byte transfers
// some common combinations
#define DMA_S_MEM_INC      DMA_SRC_MEM+DMA_SRC_INC
#define DMA_D_MEM_INC      DMA_DEST_MEM+DMA_DEST_INC
#define DMA_S_D_MEM_INC    DMA_S_MEM_INC+DMA_D_MEM_INC
#define DMA_ARM            DMA_CHG+DMA_STRT

#define DMA_DEST_CONST     0x0000   //  DMA destination addr is constant
#define DMA_SRC_IO         0x0000   //  DMA source is in I/O space
#define DMA_SRC_CONST      0x0000   //  DMA source addr is constant
#define DMA_DEST_IO        0x0000   //  DMA destination is in I/O space
#define DMA_UNSYNC         0x0000   //  unsynchronized transfers
#define DMA_SRCSYNC        0x00c0   //  source synchronized transfers
#define DMA_DESTSYNC       0x0080   //  destination synchronized transfers
#define DMA_HIGHPRI        0x0040   //  channel is high priority
#define DMA_LOWPRI         0x0000   //  channel is low priority
// 
// -------------------------------------------------------------
// CHIP-SELECT REGISTERS

// Fields used by more than one of the chip select registers
#define CS_WAIT0           0x0000   //  zero wait states
#define CS_WAIT1           0x0001   //  one wait state
#define CS_WAIT2           0x0002   //  two wait states
#define CS_WAIT3           0x0003   //  three wait states
#define CS_IGXRDY          0x0004   //  ignore external ready
// The following wait states can only be used for PCS3-0 - set in
// the PACS register (CS_PACS).
//
#define CS_MPCS_WAIT5      0x0008   //  five wait states
#define CS_MPCS_WAIT7      0x0009   //  seven wait states
#define CS_MPCS_WAIT9      0x000a   //  nine wait states
#define CS_MPCS_WAIT15     0x000b   //  fifteen wait states

// The following bit is only on LCS (CS_LMCS) and UCS (CS_UMCS)
#define CS_DISADDR         0x0080   //  disable AD addr output
//
// UMCS (upper memory) register
#define CS_UMCS_64K        0x7000   //  UCS is 64K
#define CS_UMCS_128K       0x6000   //  UCS is 128K
#define CS_UMCS_256K       0x4000   //  UCS is 256K
#define CS_UMCS_512K       0x0000   //  UCS is 512K
//
// LMCS (lower memory) register
#define CS_LMCS_PSRAM      0x0040   // turn off PSRAM when subtracted out
#define CS_LMCS_64K        0x0000   //  LCS is 64K  (0 - 0ffffh)
#define CS_LMCS_128K       0x1000   //  LCS is 128K (0 - 1ffffh)
#define CS_LMCS_256K       0x3000   //  LCS is 256K (0 - 3ffffh)
#define CS_LMCS_512K       0x7000   //  LCS is 512K (0 - 7ffffh)
#define CS_LMCS_PSEN       0x0040   //  enable PSRAM support

// IMCS (internal memory) register	(Am186ER)
#define CS_IMCS_SHOW_READ  0x0040   //  show read enable
#define CS_IMCS_RAM_ENABLE 0x0200	//  internal ram enable
#define CS_IMCS_RESERVED   0x00ff	//  reserved bits with a 1 value

// MPCS specific register bits
#define CS_MPCS_EX         0x0080
#define CS_MPCS_MS         0x0040
#define CS_MPCS_EXWT       0x0008   //  EXTENDED PCS WAIT STATES
#define CS_MPCS_8K         0x0100
#define CS_MPCS_16K        0x0200
#define CS_MPCS_32K        0x0400
#define CS_MPCS_64K        0x0800
#define CS_MPCS_128K       0x1000
#define CS_MPCS_256K       0x2000
#define CS_MPCS_512K       0x4000
#define CS_MPCS_PCS_ADDR   0x0000   //  PCS6-5 are addr lines
#define CS_MPCS_RESERVED   0x8038   // reserved bits with a 1 value 

// PACS specific register bits
#define CS_PACS_RESERVED   0x0070	// reserved bits with a 1 value
				
//
// -------------------------------------------------------------
// Timer register (1&2 - 0x56, 0x5e)
#define TMR_ENABLE         0x8000
#define TMR_INH            0x4000
#define TMR_INT            0x2000   //  generate intrpt rest
#define TMR_RIU            0x1000   //  ax count reached flag
#define TMR_MC             0x0020   //  max count reached flag
#define TMR_RTG            0x0010   //  retrigger bit
#define TMR_2PRES          0x0008   //  timer 2 is a prescaler
#define TMR_EXT            0x0004   //  use external timer
#define TMR_ALT            0x0002
#define TMR_CONT           0x0001   //  continuous mode
#define TMR_START          TMR_ENABLE+TMR_INH     //  start timer
// These fields apply to master mode interrupt control registers
// Priorities also apply to interrupt priority register (INT_PMSK)
#define INT_PRI0           0x0000   //  highest priority
#define INT_PRI1           0x0001   //  priority = 1
#define INT_PRI2           0x0002   //  priority = 2
#define INT_PRI3           0x0003   //  priority = 3
#define INT_PRI4           0x0004   //  priority = 4
#define INT_PRI5           0x0005   //  priority = 5
#define INT_PRI6           0x0006   //  priority = 6
#define INT_PRI7           0x0007   //  lowest priority
#define INT_DISABLE        0x0008   //  disable interrupt
#define INT_ENABLE         0x0000   //  enable interrupt
#define INT_LEVEL          0x0010   //  level triggered mode
#define INT_EDGE           0x0000   //  edge triggered mode

// These two fields apply only to INT_INT0 and INT_INT1

#define INT_CASCADE        0x0020   //  cascade mode enable
#define INT_SFNM           0x0040   //  specl fully nested mode
// Interrupt Status Register fields (INT_STAT)
#define INT_DHLT           0x8000   //  halt DMA activity
#define INT_STAT_TMR2      0x0004   //  TMR2 has intrpt pending
#define INT_STAT_TMR1      0x0002   //  TMR1 has intrpt pending
#define INT_STAT_TMR0      0x0001   //  TMR0 has intrpt pending
//
// These fields apply to the interrupt request register (INT_IREQ),
// the interrupt in-service register (INT_INSV), and the interrupt
// mask register (INT_MASK)
#define INT_SPT0           0x0400   //  serial port
#define INT_WATCHDOG       0x0200   //  watchdog timer (EM/ER)
#define INT_SPT1           0x0200   //  serial port 1 (ES)
#define INT_I4             0x0100   //  INT4
#define INT_I3             0x0080   //  INT3
#define INT_I2             0x0040   //  INT2
#define INT_I1             0x0020   //  INT1
#define INT_I0             0x0010   //  INT0
#define INT_D1             0x0008   //  DMA1
#define INT_D0             0x0004   //  DMA0
#define INT_I6             INT_D1   //  DMA1
#define INT_I5             INT_D0   //  DMA0
#define INT_TIMER          0x0001   //  any timer (see INT_STAT)
// EOI register fields
#define EOI_NONSPEC        0x8000   //  non-specific EOI
// Interrupt poll and poll status register fields
#define INT_POLL_IREQ      0x8000   //  interrupt pending flag
// Synchronous  serial port status register fields (SS_STAT)
#define SS_STAT_ERR        0x0004   //  error flag
#define SS_STAT_COMPLETE   0x0002   //  transaction complete
#define SS_STAT_BUSY       0x0001   //  SS port busy flag
// Synchronous  serial port control register fields (SS_CTL)
#define SS_CTL_CLK2        0x0000   //  SS clck = 1/2 proc clck
#define SS_CTL_CLK4        0x0002   //  SS clck = 1/4 proc clck
#define SS_CTL_CLK8        0x0004   //  SS clck = 1/8 proc clck
#define SS_CTL_CLK16       0x0006   //  SS clck = 1/16proc clck
//
//  -------------------------------------------------------------
//   Auxiliary Configuration Register (offset 0xf2) -- ES ONLY
//  
//   Serial port hardware handshaking bits

#define AUXC_SPRT1_HS1     0x0040   //  1 = ERR, 0 CTS flow control
#define AUXC_SPRT1_HS0     0x0020   //  1 = RTS, 0 RTR flow control
#define AUXC_SPRT0_HS1     0x0010   //  1 = ERR, 0 CTS flow control
#define AUXC_SPRT0_HS0     0x0008   //  1 = RTS, 0 RTR flow control
#define AUXC_SPRT1_ERR     0x0040   //  SPRT1 ERR flow control
#define AUXC_SPRT1_RTS     0x0020   //  SPRT1 RTS flow control
#define AUXC_SPRT0_ERR     0x0010   //  SPRT0 ERR flow control
#define AUXC_SPRT0_RTS     0x0008   //  SPRT0 ERR flow control
#define AUXC_SPRT1_CTS     0x0000   //  SPRT1 CTS flow control
#define AUXC_SPRT1_RTR     0x0000   //  SPRT1 RTR flow control
#define AUXC_SPRT0_CTS     0x0000   //  SPRT0 CTS flow control
#define AUXC_SPRT0_RTR     0x0000   //  SPRT0 RTR flow control

// Static bus sizing configuration bits
#define AUXC_LSIZ          0x0004   //  set to 1 for 8-bit LCS size
#define AUXC_MSIZ          0x0002   //  set to 1 for 8-bit non LCS/non UCS mem size
#define AUXC_IOSZ          0x0001   //  set to 1 for 8-bit IO size
//
//  -------------------------------------------------------------
//  Watchdog timer register defined (0xe6)  -- ES ONLY
#define WDOG_ENA_ES           0x8000
#define WDOG_INT_ES           0x4000
#define WDOG_RSTF_ES          0x2000   
#define WDOG_NMIF_ES          0x1000   
#define WDOG_TMODE_ES         0x0800
#define WDOG_MULT_10X_ES      0x0001
#define WDOG_MULT_20X_ES      0x0002
#define WDOG_MULT_21X_ES      0x0004
#define WDOG_MULT_22X_ES      0x0008
#define WDOG_MULT_23X_ES      0x0010
#define WDOG_MULT_24X_ES      0x0020
#define WDOG_MULT_25X_ES      0x0040
#define WDOG_MULT_26X_ES      0x0080

//  Keys for watchdog timer
#define WDOG_CLR1_ES          0xaaaa
#define WDOG_CLR2_ES          0x5555
#define WDOG_WR1_ES           0x3333
#define WDOG_WR2_ES           0xcccc
//
// ======================================================================
// Interrupt types
// These are the values to write to the EOI register
#define EOITYPE_TMR0       0x08
#define EOITYPE_TMR1       EOITYPE_TMR0
#define EOITYPE_TMR2       EOITYPE_TMR0
#define EOITYPE_DMA0       0x0a
#define EOITYPE_DMA1       0x0b
#define EOITYPE_INT0       0x0c
#define EOITYPE_INT1       0x0d
#define EOITYPE_INT2       0x0e
#define EOITYPE_INT3       0x0f
#define EOITYPE_INT4       0x10
#define EOITYPE_INT5       EOITYPE_DMA0
#define EOITYPE_INT6       EOITYPE_DMA1
#define EOITYPE_SPRT       0x14
#define EOITYPE_SPRT0      0x14
#define EOITYPE_WDOG_EM    0x11            // Watchdog timer on EM/ER
#define EOITYPE_SPRT1      0x11            // Serial port 1 on ES
#define EOITYPE_NONSPEC    0x8000

#define ITYPE_DIV          0x0             //  Divide error
#define ITYPE_TRACE        0x1             //  trace trap
#define ITYPE_NMI          0x2             //  non-maskable interrupt
#define ITYPE_BREAK        0x3             //  breakpoint
#define ITYPE_OVERFLOW     0x4             //  overflow
#define ITYPE_BOUNDS       0x5             //  bound
#define ITYPE_ILLOP        0x6             //  illegal opcode
#define ITYPE_ESC          0x7             //  ESC
#define ITYPE_TMR0         EOITYPE_TMR0    //  Timer 0
#define ITYPE_TMR1         0x12            //  Timer 1
#define ITYPE_TMR2         0x13            //  Timer 2
#define ITYPE_DMA0         EOITYPE_DMA0    //  DMA 0
#define ITYPE_DMA1         EOITYPE_DMA1    //  DMA 1
#define ITYPE_INT0         EOITYPE_INT0    //  INT0
#define ITYPE_INT1         EOITYPE_INT1    //  INT1
#define ITYPE_INT2         EOITYPE_INT2    //  INT2
#define ITYPE_INT3         EOITYPE_INT3    //  INT3
#define ITYPE_INT4         EOITYPE_INT4    //  INT4
#define ITYPE_INT5         EOITYPE_INT5    //  INT5 -- ES only
#define ITYPE_INT6         EOITYPE_INT6    //  INT6 -- ES only
#define ITYPE_SPRT         EOITYPE_SPRT    //  Serial Port 0
#define ITYPE_SPRT0        EOITYPE_SPRT0   //  Serial Port 0
#define ITYPE_SPRT1        EOITYPE_SPRT1   //  Serial Port 1 on ES
#define ITYPE_WDOG_EM      EOITYPE_WDOG_EM //  Watchdog timer on EM/ER
#define ITYPE_WDOG_ES      ITYPE_NMI       //  Watchdog timer on ES

//
// 186CC defines
//
#define CTL_OFF_CC	       0xfc00

#define TIP_LEDS		   0x0305
#define TIP_LEDS_H         0305h 
#define TIP_HEX			   0x0306

#define TIP_LEDS_TIMER	   0x80
#define TIP_LEDS_SPORT	   0x40

#define UMCS_USIZ          0x0020          // Upper Memory Data Bus Size

#define MPCS_IOSIZ_CC	   0x0010		   // IO Space Data Bus Size

#define RESCON_CC		   0xffde		   // Reset Configuration
#define CDRAM_CC		   0xffaa		   // DRAM Refresh Clock Prescaler
#define T2CMPA_CC          0xff54          // Timer 2 Maxcount Compare A
#define T2CNT_CC           0xff52          // Timer 2 Count
#define T2CON_CC           0xff50          // Timer 2 Mode/Control 
#define T1CMPA_CC          0xff4c          // Timer 1 Maxcount Compare A TMR1_MAXA_CC
#define T1CNT_CC           0xff4a          // Timer 1 Count TMR1_CNT_CC
#define T1CON_CC           0xff48          // Timer 1 Mode/Control  TMR1_CTL_CC

#define CH0CON_CC     (CTL_OFF_CC + 0x300) // Interrupt Channel 0 Control (Timers)
#define CH3CON_CC     (CTL_OFF_CC + 0x306) // Interrupt Channel 3 Control (Hight-Speed UART)
#define CH11CON_CC    (CTL_OFF_CC + 0x316) // Interrupt Channel 11 Control (UART)
#define CHXCON_PR_7_CC     0x0037          // Interrupt Channel Priority 7 (Lowest)
#define CHXCON_MSK_OFF_CC  0x003f          // Interrupt Channel Priority 7 (Lowest)
#define CHXCON_SRC_INT_CC  0x0030          // Interrupt Channel internal source (Masked)
#define CHXCON_MSK         0x0008          // Interrupt channel masked

#define SYSCON_CC		   		0xfff0
#define SYSCON_ITF4_UART_CC 	0x0200
#define SYSCON_ITF4_MASK_CC 	0x0300

#define IMASK_CC           0xff26          // Interrupt Mask 
#define EOI_CC             0xff20          // End of Interrupt
#define EOI_T1_CC          0x09            // Timer 1 EOI Type
#define EOI_HSUART_CC      0x0d            // High-Speed UART EOI Type
#define EOI_UART_CC        0x1b            // UART EOI Type

#define PIOMODE0_CC	       0xffc0          // PIO Mode 0
#define PIODIR0_CC	       0xffc2          // PIO Direction 0
#define PIODATA0_CC	       0xffc4          // PIO Data 0
#define PIOSET0_CC	       0xffc6          // PIO Set 0
#define PIOCLEAR0_CC       0xffc8          // PIO Clear 0
#define PIOMODE1_CC	       0xffca          // PIO Mode 1
#define PIODIR1_CC	       0xffcc          // PIO Direction 1
#define PIODATA1_CC	       0xffce          // PIO Data 1
#define PIOSET1_CC	       0xffd0          // PIO Set 1
#define PIOCLEAR1_CC       0xffd2          // PIO Clear 1
#define PIOMODE2_CC        0xffd4          // PIO Mode 2
#define PIODIR2_CC		   0xffd6          // PIO Direction 2
#define PIODATA2_CC		   0xffd8          // PIO Data 2
#define PIOSET2_CC		   0xffda          // PIO Set 2
#define PIOCLEAR2_CC	   0xffdc          // PIO Clear 2

#define PIO1_UTX_CC        0x0010          
#define PIO1_URX_CC        0x0400
#define PIO1_HSURX_CC      0x0001

#define SPCON_CC      (CTL_OFF_CC + 0x280) // Serial Port Control 0
#define SPCON1_CC     (CTL_OFF_CC + 0x282) // Serial Port Control 1
#define SPSTAT_CC     (CTL_OFF_CC + 0x284) // Serial Port Status
#define SPIMSK_CC     (CTL_OFF_CC + 0x286) // Serial Port Interrupt Mask
#define SPTXD_CC      (CTL_OFF_CC + 0x288) // Serial Port Transmit Data
#define SPRXD_CC      (CTL_OFF_CC + 0x28a) // Serial Port Receive Data
#define SPREXP_CC     (CTL_OFF_CC + 0x28c) // Serial Port Receive Data Peek
#define SPBDV_CC      (CTL_OFF_CC + 0x28e) // Serial Port Baud Rate Divisor

/////////////////////////////////////////////////////////////////////
#define ITYPE_HSUART         EOITYPE_INT4    //  INT4

// here are the register definitions
#define   HSPM2_CC        (CTL_OFF_CC + 0x274)   // High Speed Serial Port Character Match 2 Register
#define   HSPM1_CC        (CTL_OFF_CC + 0x272)   // High Speed Serial Port Character Match 1 Register
#define   HSPM0_CC        (CTL_OFF_CC + 0x270)   // High Speed Serial Port Character Match 0 Register
#define   HSPBDV_CC       (CTL_OFF_CC + 0x26e)   // High Speed Serial Port Baud Rate Divisor Register
#define   HSPRXDP_CC      (CTL_OFF_CC + 0x26c)   // High Speed Serial Port Receive Data Peek Register
#define   HSPRXD_CC       (CTL_OFF_CC + 0x26a)   // High Speed Serial Port Receive Data Register
#define   HSPTXD_CC       (CTL_OFF_CC + 0x268)   // High Speed Serial Port Transmit Data Register
#define   HSPIMSK_CC      (CTL_OFF_CC + 0x266)   // High Speed Serial Port Interrupt Mask Register
#define   HSPSTAT_CC      (CTL_OFF_CC + 0x264)   // High Speed Serial Port Status Register
#define   HSPCON1_CC      (CTL_OFF_CC + 0x262)   // High Speed Serial Port Control 1 Register
#define   HSPCON0_CC      (CTL_OFF_CC + 0x260)   // High Speed Serial Port Control 0 Register

//
// High Speed Serial Port Receive Data Peek (HSPRXDP - offset 026C hex) Bits
//
#define   HSPRXDP_RDR            HSPRXD_RDR                 // Receive Data Ready
#define   HSPRXDP_THRE           HSPRXD_THRE                // Transmit Holding Register Empty
#define   HSPRXDP_FER            HSPRXD_FER                 // Framing Error
#define   HSPRXDP_OER            HSPRXD_OER                 // Overrun Error
#define   HSPRXDP_PER            HSPRXD_PER                 // Parity Error
#define   HSPRXDP_MATCH          HSPRXD_MATCH               // Match
#define   HSPRXDP_BRK            HSPRXD_BRK                 // Break
#define   HSPRXDP_AB             HSPRXD_AB                  // Address Bit

//
// High Speed Serial Port Receive Data (HSPRXD - offset 026A hex) Bits
//
#define   HSPRXD_RDR             0x8000                     // Receive Data Ready
#define   HSPRXD_THRE            0x4000                     // Transmit Holding Register Empty
#define   HSPRXD_FER             0x2000                     // Framing Error
#define   HSPRXD_OER             0x1000                     // Overrun Error
#define   HSPRXD_PER             0x0800                     // Parity Error
#define   HSPRXD_MATCH           0x0400                     // Match
#define   HSPRXD_BRK             0x0200                     // Break
#define   HSPRXD_AB              0x0100                     // Address Bit

//
// High Speed Serial Port Transmit Data (HSPTXD - offset 0268 hex) Bits
//
#define   HSPTXD_AB              0x0100                     // Address Bit

//
// High Speed Serial Port Interrupt Mask (HSPIMSK - offset 0266 hex) Bits
//
#define   HSPIMSK_RTHRSH         0x8000                     // Receive FIFO Threshold Interrupt Enable
#define   HSPIMSK_TTHRSH         0x4000                     // Transmit FIFO Threshold Interrupt Enable
#define   HSPIMSK_OERIM          0x1000                     // Overrun Error Detected Immediate Enable
#define   HSPIMSK_MATCH          0x0400                     // Character Match Interrupt Enable
#define   HSPIMSK_BRK            0x0200                     // Break Interrupt Enable
#define   HSPIMSK_AB             0x0100                     // Address Bit Interrupt Enable
#define   HSPIMSK_RDR            0x0080                     // Receive Data Ready Interrupt Enable
#define   HSPIMSK_THRE           0x0040                     // Tranmit Holding Register Empty Interrupt Enable
#define   HSPIMSK_FER            0x0020                     // Framing Error Interrupt Enable
#define   HSPIMSK_OER            0x0010                     // Overrun Error Interrupt Enable
#define   HSPIMSK_PER            0x0008                     // Parity Error Interrupt
#define   HSPIMSK_TEMT           0x0004                     // Transmitter Empty Interrupt Enable
#define   HSPIMSK_IDLED          0x0002                     // Receive Line Idle Detected Interrupt Enable
#define   HSPIMSK_IDLE           0x0001                     // Receive Line Idle Interrupt Enable

//
// High Speed Serial Port Status (HSPSTAT - offset 0264 hex) Bits
//
#define   HSPSTAT_RTHRSH         0x8000                     // Receive FIFO Threshold Reached
#define   HSPSTAT_TTHRSH         0x4000                     // Transmit FIFO Threshold Reached
#define   HSPSTAT_OERIM          0x1000                     // Overrun Error Detected Immediate
#define   HSPSTAT_MATCH          0x0400                     // Address Match Detected
#define   HSPSTAT_BRK_CC         0x0200                     // Break Detected
#define   HSPSTAT_AB             0x0100                     // Address Bit Detected
#define   HSPSTAT_RDR_CC         0x0080                     // Recevie Data Ready
#define   HSPSTAT_THRE_CC        0x0040                     // Transmit Holding Register Empty
#define   HSPSTAT_FER_CC         0x0020                     // Framing Error Detected
#define   HSPSTAT_OER_CC         0x0010                     // Overrun Error Detected
#define   HSPSTAT_PER_CC         0x0008                     // Parity Error Detected
#define   HSPSTAT_TEMT           0x0004                     // Transmitter Empty
#define   HSPSTAT_IDLED          0x0002                     // Receive Line Idle Detected
#define   HSPSTAT_IDLE           0x0001                     // Receive Line Idle

#define SPSTAT_BRK_CC      0x0200          // Break Detected
#define SPSTAT_RDR_CC      0x0080          // Recevie Data Ready
#define SPSTAT_THRE_CC     0x0040          // Transmit Holding Register Empty
#define SPSTAT_FER_CC      0x0020          // Framing Error Detected
#define SPSTAT_OER_CC      0x0010          // Overrun Error Detected
#define SPSTAT_PER_CC      0x0008          // Parity Error Detected
//
// High Speed Serial Port Control 1 (HSPCON1 - offset 0262 hex) Bits
//
#define   HSPCON1_TFEN           0x8000                     // Transmit FIFO Enable
#define   HSPCON1_RFEN           0x4000                     // Receive FIFO Enable
#define   HSPCON1_TFLUSH         0x2000                     // Transmit FIFO Flush
#define   HSPCON1_RFLUSH         0x1000                     // Receive FIFO Flush
#define   HSPCON1_ABAUD          0x0800                     // Auto Baud Enable
#define   HSPCON1_MEN            0x0080                     // Character Match Enable
#define   HSPCON1_MAB2           0x0040                     // Match Address Bit 2
#define   HSPCON1_MAB1           0x0020                     // Match Address Bit 1
#define   HSPCON1_MAB0           0x0010                     // Match Address Bit 0
#define   HSPCON1_BRKVAL         0x0008                     // Break Value
#define   HSPCON1_EXDWR          0x0004                     // Extended Write
#define   HSPCON1_EXDRD          0x0002                     // Extended Read
#define   HSPCON1_XTRN           0x0001                     // External Clock

//
// High Speed Serial Port Control 0 (HSPCON0 - offset 0260 hex) Bits
//
#define   HSPCON0_RSIE           0x1000                     // Receive Status Interrupt Enable
#define   HSPCON0_BRK            0x0800                     // Send Break
#define   HSPCON0_AB             0x0400                     // Address bit
#define   HSPCON0_FC             0x0200                     // Flow Control Enable
#define   HSPCON0_TXIE_CC        0x0100                     // Transmit Interrupt Enable
#define   HSPCON0_RXIE_CC        0x0080                     // Receive Data Interrupt Enable
#define   HSPCON0_TMODE_CC       0x0040                     // Transmitter Enable
#define   HSPCON0_RMODE_CC       0x0020                     // Receiver Enable
#define   HSPCON0_EVN            0x0010                     // Even Parity
#define   HSPCON0_PEN            0x0008                     // Parity Enable
#define   HSPCON0_ABEN           0x0004                     // Address Bit Enable
#define   HSPCON0_D7             0x0002                     // Data 7 Bits
#define   HSPCON0_STP2           0x0001                     // Stop Bit Length

#define SPCON0_TXIE_CC     0x0100          // Transmit Interrupt Enable
#define SPCON0_RXIE_CC     0x0080          // Receive Data Interrupt Enable
#define SPCON0_TMODE_CC    0x0040          // Transmitter Enable
#define SPCON0_RMODE_CC    0x0020          // Receiver Enable

#define IS186CC ((_inpw(PRL_REG) & 0xf000)==0x4000)

#define IS_TIP_ATTACHED ((_inpw( RESCON_CC ) & 0x0080))

#endif
