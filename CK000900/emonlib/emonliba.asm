comment ~
/******************************************************************************
 *                                                                            *
 *     EMONLIBA.ASM                                                           *
 *                                                                            *
 *     This file contains startup code for an E86Mon library file.            *
 *     It MUST be first in the link, or E86Mon will not recognize             *
 *     the file as a library file.                                            *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1997 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
 end comment ~

        title   emonliba.asm - Startup Module for EMON Installable Library

        .186


;***********************************************************************
;     P U B L I C   A N D   E X T E R N A L   D E F I N I T I O N S
;***********************************************************************

extern  LibInit    :NEAR         ; Pascal format main entry code
public EndCode
public EndData
public FirstFunc
public FirstRamLink

;***********************************************************************
;     S E G M E N T   O R D E R I N G   /   G R O U P I N G
;***********************************************************************

;   One of the functions of this file is to define the ordering of
;   the segments in the link.  This is done simply by declaring
;   them in the correct order.

_TEXT      segment para public 'CODE'
_TEXT      ends

END_TEXT segment para public 'CODE'
EndCode:
END_TEXT ends

RELOCATE_TO_RAM segment para public 'CODE'
RELOCATE_TO_RAM ends

CONST      segment word readonly public 'CONST'
CONST       ends

_DATA       segment word public 'DATA'          ; initialized data
_DATA       ends

DISPATCH_FUNCTIONS segment word public 'DATA'
FirstFunc:
DISPATCH_FUNCTIONS ends

END_DISPATCH segment word public 'DATA'
    dd 0
StartData dd StartData  ; Help MAKEHEX figure out where data starts.
END_DISPATCH ends

RAM_LINKS segment word public 'DATA'
FirstRamLink:
RAM_LINKS ends

END_RAMLINKS segment word public 'DATA'
    dw 0FFFFh
END_RAMLINKS ends

CODE_BSS        segment para public 'BSS' 

;; Make sure we fill all the way to a 16 byte boundary

        db    16 dup (42h)
EndTotalROM:
CODE_BSS        ends

_BSS        segment word public 'BSS' 
EndInitializedData:
_BSS        ends

_STACK      segment para stack 'STACK'
EndData:
_STACK      ends

DGROUP  group   _STACK, _DATA, CONST, _BSS, DISPATCH_FUNCTIONS, END_DISPATCH
DGROUP  group   RELOCATE_TO_RAM, RAM_LINKS, END_RAMLINKS

CGROUP  group   _TEXT, END_TEXT, CODE_BSS

;***********************************************************************
;                 C O D E   S E G M E N T
;***********************************************************************

_TEXT   segment para public 'CODE'

        assume ds:nothing,es:nothing,ss:nothing


;***********************************************************************
;  STARTUP     proc far C public Main program entry point
;
;  Passed:     Pointer on stack
;  Returns:    Nothing
;  Destroys:   All except SI,DI
;
;  STARTUP jumps straight to LibInit to perform library initialization.
;  LibInit is in emonlibc.c
;***********************************************************************

STARTUP proc far public

; The startup code MUST have this format, or it will not be
; recognized as a library by E86Mon.

        jmp     short @F
        db      "E86Mon Lib Extension 1"   ; Required library signature
@@:

; Go get EmonData pointer

        int      20h
        db       "EMONDATA"

; See if it's the same as what we were passed

        push     bp
        mov      bp,sp
        cmp      ss:[bp+6],ax
        jnz      @F
        cmp      ss:[bp+8],dx
@@:
        pop      bp
        push     dx
        push     ax

        jnz      NotSame

; If it's the same, we're being CALLED as part of E86Mon initialization.
; Perform our initialization and return to E86Mon.

        call     LibInit
        retf

; If it's not the same, we're being TESTED as a .EXE program, running
; out of RAM.  Perform our initialization and execute an int 3 to get
; back to E86Mon.

NotSame:
        call     LibInit
@@:
        int      3
        jmp      @B
Startup ENDP


;***********************************************************************
;  NewDS       Near public Pascal proc
;
;  Passed:     Segment location in RAM where DS has been allocated
;  Returns:    DS points to newly allocated data segment
;  Destroys:   All except SI,DI
;
;  NewDS copies initialized data from ROM to RAM, then zeroes out
;  the BSS area of the newly allocated data segment.
;***********************************************************************

;  NewDS copies our initialized data from ROM to RAM

NewDS PROC NEAR PUBLIC
        pop     dx                   ; Get return address off stack
        pop     es                   ; Get new data segment into ES
        push    di                   ; Save SI and DI (compiler expects this)
        push    si
        push    cs                   ; We will copy from CS
        pop     ds
        lea     si,EndCode           ; Source location to SI
        xor     di,di                ; Destination location to DI
        lea     cx,EndInitializedData ; Size to move into CX
        lea     bx,EndData
        sub     bx,cx                ; Size to zero out into BX

; Copy the initialized data

        cld
        shr     cx,1
        rep     movsw
        adc     cx,cx
        rep     movsb

; Zero out the uninitialized data

        xor     ax,ax
        mov     cx,bx
        shr     cx,1
        rep     stosw
        adc     cx,cx
        rep     stosb

; Data segment is set up now

        push    es
        pop     ds

; Perform data relocations

        mov     bx, offset CGROUP:EndTotalROM
        mov     ax,cs:[bx]
        inc     ax

RelocLoop:
        sub     ax,4
        jbe     FinishedReloc
        add     bx,4

        mov     di,cs:[bx]
        mov     ds:[di],cs
        cmp     word ptr cs:[bx+2],0
        jz      RelocLoop
        mov     ds:[di],ds
        jmp     RelocLoop

FinishedReloc:

; Finish up and get out

        pop     si
        pop     di
        jmp     dx
NewDS   ENDP

_TEXT   ends

end        STARTUP
