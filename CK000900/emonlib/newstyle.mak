!include ..\version.mak

all:
    @del emonlink.lib
    @copy elibfunc.c near_lib.c 
    @copy elibfunc.c far_lib.c 
    @copy elibfunc.c ram_lib.c 
    @
    @$(AMD_LPD_CL)cl /nologo /c @<< 
@ /I..\include /f- /ACw /W4 /Oiscglo
@ /Gs /G1 /Zd /Zl /GA /Gc
@ /D VER_NUM=$(VER_NUM_C) /D VER_STRING=$(VER_STRING)
@ emonlibc.c
<<
    @$(AMD_LPD_CL)lib /NOLOGO /BATCH emonlink +emonlibc;
    @$(AMD_LPD_CL)cl /nologo /c @<< 
@ /I..\include /f- /ACw /W4 /Oiscglo
@ /Gs /G1 /Zd /Zl /GA /Gc /Gy /D MAKE_ALL
@ /D VER_NUM=$(VER_NUM_C) /D VER_STRING=$(VER_STRING) /D CODE_LOC=Far_
@ far_lib.c
<<
    @$(AMD_LPD_CL)lib /NOLOGO /BATCH emonlink +far_lib;
    @$(AMD_LPD_CL)cl /nologo /c @<< 
@ /I..\include /f- /ACw /W4 /Oiscglo
@ /Gs /G1 /Zd /Zl /GA /Gc /Gy /D MAKE_ALL
@ /D VER_NUM=$(VER_NUM_C) /D VER_STRING=$(VER_STRING) /D CODE_LOC=Near_
@ near_lib.c
<<
    @$(AMD_LPD_CL)lib /NOLOGO /BATCH emonlink +near_lib;
    @$(AMD_LPD_CL)cl /nologo /c @<< 
@ /I..\include /f- /ACw /W4 /Oiscglo
@ /Gs /G1 /Zd /Zl /GA /Gc /Gy /D MAKE_ALL
@ /D VER_NUM=$(VER_NUM_C) /D VER_STRING=$(VER_STRING) /D CODE_LOC=Ram_
@ ram_lib.c
<<
    @$(AMD_LPD_CL)lib /NOLOGO /BATCH emonlink +ram_lib;
    @del emonlink.bak
    @del *.obj
    @del near_lib.c
    @del far_lib.c
    @del ram_lib.c
    @set ml= /c /Sa /DSTKSIZE=1024 /W3 /Zi /Iinc
	@$(AMD_LPD_ML)ml  /nologo emonliba.asm

clean:
    del *.lib
    del *.obj
