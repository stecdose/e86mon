/******************************************************************************
 *                                                                            *
 *     ELIBFUNC.C                                                             *
 *                                                                            *
 *     This file contains the implementation for standard 386mon library      *
 *     functions.                                                             *
 *                                                                            *
 *     These functions can be called from within the monitor, or from         *
 *     within monitor extensions or DOS-style application programs.           *
 *                                                                            *
 *     This file is compiled multiple times to generate Near_, Far_,          *
 *     and Ram_ versions of all the functions.                                *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1997 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#include "interlib.h"

// Disable optimization warning
#pragma warning(disable : 4704)


//////////////////////////////////////////////////////////////////////////////
//    dispatch() -- Tries to call an external routine to perform a function
//
#if defined(MAKE_ALL) || defined( MAKE_dispatch )

dispatch_hdr(CODE_LOC)
{
    LPDispatchLink     start = EmonData->LibHead;
    DispatchResult     r = Dispatch_NotExclusive;

    while (start)
    {
        DispatchFunc far * funcptr = start->func;
        DispatchFunc func;
        while ((func=*funcptr) != 0)
        {
            WORD NewDS = start->dataseg;
            _asm push ds;
            _asm mov  ds,NewDS;
            r = func((LPDispatchStruct)&op);
            _asm pop ds;
            if (r != Dispatch_NotExclusive)
                return r;
            funcptr++;
        }
        start = start->next;
    }
    return r; 
}
#endif

//////////////////////////////////////////////////////////////////////////////
//    InitEmonData -- get a pointer to EMON data
//
#if defined(MAKE_ALL) || defined( MAKE_InitEmonData )

FPEmonInternals EmonData;

InitEmonData_hdr(CODE_LOC)
{
    _asm {
        int     20h
        _emit  'E'
        _emit  'M'
        _emit  'O'
        _emit  'N'
        _emit  'D'
        _emit  'A'
        _emit  'T'
        _emit  'A'
        mov    word ptr EmonData,ax
        mov    word ptr EmonData+2,dx
         }

    return (EmonData->StrucSize == sizeof(*EmonData)) &&
           (EmonData->Version == VER_NUM);
}
#endif

//////////////////////////////////////////////////////////////////////////////
//    CurrentCS() -- Return current code segment
//
#if defined(MAKE_ALL) || defined( MAKE_CurrentCS )

#pragma warning(disable : 4035)

CurrentCS_hdr(CODE_LOC)
{
    _asm  mov  ax,cs
}
#endif

//////////////////////////////////////////////////////////////////////////////
//    printf() -- External or internal printf function
//
#if defined(MAKE_ALL) || defined( MAKE_printf )
printf_hdr(CODE_LOC)
{
    dispatch(Dispatch_Printf,&format);
}
#endif

/////////////////////////////////////////////////////////////////////////
// FindItem() is passed two strings:  a unique (e.g. LONG) module
// identifier, and an item identifier.  It returns the address
// of the item if a module "claims" the find, or 0 if no such
// item.
//
#if defined(MAKE_ALL) || defined( MAKE_FindItem )
FindItem_hdr(CODE_LOC)
{
    LPVOID itemLoc;

    InitEmonData();
    if (dispatch(Dispatch_FindItem,Module,Item,(LPVOID)&itemLoc)
        != Dispatch_NotExclusive)
        return itemLoc;
    return 0;

}
#endif

/////////////////////////////////////////////////////////////////////////
// BreakVector() returns the address of the monitor's break handler
//
#if defined(MAKE_ALL) || defined( MAKE_BreakVector )
BreakVector_hdr(CODE_LOC)
{
    DWORD MonitorInt3 = EmonData->UnhandledIntVector;
    return MK_FP(_FP_SEG(MonitorInt3)+258,_FP_OFF(MonitorInt3)-16*258);
}
#endif

///////////////////////////////////////////////////////////////////////////
// GetCurrentTime() gets the current time in milliseconds
//
#if defined(MAKE_ALL) || defined( MAKE_GetCurrentTime )
GetCurrentTime_hdr(CODE_LOC)
{
    dispatch(Dispatch_GetTime,Milliseconds);
}
#endif

///////////////////////////////////////////////////////////////////////////
//    PostLEDs() -- light the LEDs with the supplied values
//
#if defined(MAKE_ALL) || defined( MAKE_PostLEDs )
PostLEDs_hdr(CODE_LOC)
{
    dispatch(Dispatch_PostLEDs,LEDMask);
}
#endif


///////////////////////////////////////////////////////////////////////////
//    InquireLEDs() -- Get the current LED values
//
#if defined(MAKE_ALL) || defined( MAKE_InquireLEDs )
InquireLEDs_hdr(CODE_LOC)
{
    return (BYTE)dispatch(Dispatch_InquireLEDs);
}
#endif

//////////////////////////////////////////////////////////////////////////
//   GetYNResponse() - Gets a Y or N response from the user.
//
#if defined(MAKE_ALL) || defined( MAKE_GetYNResponse )
GetYNResponse_hdr(CODE_LOC)
{
    return dispatch(Dispatch_GetYN,Prompt,LeadingCRLF);
}
#endif

//////////////////////////////////////////////////////////////////////////
//    GetInputLine() -- Scan in a line from the user and place it
//                      into InputLine.
//
#if defined(MAKE_ALL) || defined( MAKE_GetInputLine )
GetInputLine_hdr(CODE_LOC)
{
    return dispatch(Dispatch_GetInputLine,Prompt,echo);
}
#endif
////////////////////////////////////////////////////////////////////////////
//    ScanDelimiters -- munch on spaces and commas
//
#if defined(MAKE_ALL) || defined( MAKE_ScanDelimiters )
ScanDelimiters_hdr(CODE_LOC)
{
    dispatch(Dispatch_ScanDelimiters,Which);
}
#endif

/////////////////////////////////////////////////////////////////////////
// ScanError() indicates to the user an error occurred in scanning
// the input.  It places a caret at the offending line location.
//
#if defined(MAKE_ALL) || defined( MAKE_ScanError )
ScanError_hdr(CODE_LOC)
{
    return dispatch(Dispatch_ScanError,CauseMsg);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanOption() searches for an option character
//
#if defined(MAKE_ALL) || defined( MAKE_ScanOption )
ScanOption_hdr(CODE_LOC)
{
    return dispatch(Dispatch_ScanOption,Delimiters,name);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanEOL() searches for the end of the line
//
#if defined(MAKE_ALL) || defined( MAKE_ScanEOL )
ScanEOL_hdr(CODE_LOC)
{
    return dispatch(Dispatch_ScanEOL,ErrorIfNot);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanString() assembles a string
//
#if defined(MAKE_ALL) || defined( MAKE_ScanString )
ScanString_hdr(CODE_LOC)
{
    return dispatch(Dispatch_ScanString,s,len);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanHexDigits() assembles an arbitrary number of digits into a number
//
#if defined(MAKE_ALL) || defined( MAKE_ScanHexDigits )
ScanHexDigits_hdr(CODE_LOC)
{
    return (WORD)dispatch(Dispatch_ScanHexDigits,result,base);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanByte() uses ScanHexDigits() to retrieve a single byte
//
#if defined(MAKE_ALL) || defined( MAKE_ScanByte )
ScanByte_hdr(CODE_LOC)
{
    return dispatch(Dispatch_ScanByte,b);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanWord() uses ScanHexDigits() to retrieve a word
//
#if defined(MAKE_ALL) || defined( MAKE_ScanWord )
ScanWord_hdr(CODE_LOC)
{
    return dispatch(Dispatch_ScanWord,w);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanDWord() uses ScanHexDigits() to retrieve a double word
//
#if defined(MAKE_ALL) || defined( MAKE_ScanDWord )
ScanDWord_hdr(CODE_LOC)
{
    return dispatch(Dispatch_ScanDWord,r);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanDecimal() uses ScanHexDigits() to retrieve a decimal number
//
#if defined(MAKE_ALL) || defined( MAKE_ScanDecimal )
ScanDecimal_hdr(CODE_LOC)
{
    return dispatch(Dispatch_ScanDecimal,r);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanAddress() scans for an address.
//
#if defined(MAKE_ALL) || defined( MAKE_ScanAddress )
ScanAddress_hdr(CODE_LOC)
{
    return dispatch(Dispatch_ScanAddress,Flags,clientAddress);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanRange() looks for a range in either "address address"
//    or "address L length" format.
//
#if defined(MAKE_ALL) || defined( MAKE_ScanRange )
ScanRange_hdr(CODE_LOC)
{
    return dispatch(Dispatch_ScanRange,Flags,a,l);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanList() assembles a list consisting of hex numbers and/or
//      quoted strings.
//
#if defined(MAKE_ALL) || defined( MAKE_ScanList )
ScanList_hdr(CODE_LOC)
{
    return dispatch(Dispatch_ScanList,NumBytes,EmptyOK);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    InitPSP() initializes a loaded program's program segment prefix.
//
#if defined(MAKE_ALL) || defined( MAKE_InitPSP )
InitPSP_hdr(CODE_LOC)
{
    dispatch(Dispatch_InitPSP,MinProgramParagraphs);
}
#endif

///////////////////////////////////////////////////////////////////////
// RAMSafe() returns the size of "safe" RAM.  If the RAM appears
// to have been trashed by the monitor, this is 0.
//
#if defined(MAKE_ALL) || defined( MAKE_RAMSafe )
RAMSafe_hdr(CODE_LOC)
{
    return (WORD)dispatch(Dispatch_RAMSafe);
}
#endif

//////////////////////////////////////////////////////////////////////////
//    SampleInputLine() -- just return the next non-delimiter character
//
#if defined(MAKE_ALL) || defined( MAKE_SampleInputLine )
SampleInputLine_hdr(CODE_LOC)
{
    BYTE ch = EmonData->InputLine[EmonData->LineIndex];

    if (lowercase)
        ch = tolower(ch);
    return ch;
}
#endif

//////////////////////////////////////////////////////////////////////////
//    ReadInputLine() -- read the next character from input line
//
#if defined(MAKE_ALL) || defined( MAKE_ReadInputLine )
ReadInputLine_hdr(CODE_LOC)
{
    BYTE ch;

    ch = SampleInputLine(lowercase);
    if (ch != 0)
        EmonData->LineIndex++;
    return ch;
}
#endif // ReadInputLine()

////////////////////////////////////////////////////////////////////////
// tolower() Lowercases a character
//
#if defined(MAKE_ALL) || defined( MAKE_tolower )
tolower_hdr(CODE_LOC)
{
    ch &= 0x7F;
    if ( (ch >= 'A') && (ch <= 'Z'))
        ch += 'a' - 'A';
    return ch;
}
#endif

////////////////////////////////////////////////////////////////////////
// toupper() Uppercases a character
//
#if defined(MAKE_ALL) || defined( MAKE_toupper )
toupper_hdr(CODE_LOC)
{
    ch &= 0x7F;
    if ( (ch >= 'a') && (ch <= 'z'))
        ch -= 'a' - 'A';
    return ch;
}
#endif

////////////////////////////////////////////////////////////////////////
// tonum() Converts a character to a number
//
#if defined(MAKE_ALL) || defined( MAKE_tonum )
tonum_hdr(CODE_LOC)
{
        if ( (ch >= '0') && (ch <= '9') )
            return (WORD)ch - (WORD)'0';
        else if ( (ch >= 'a') && (ch <= 'f') )
            return (WORD)ch - (WORD)'a' + 10;
        else if ( (ch >= 'A') && (ch <= 'F') )
            return (WORD)ch - (WORD)'A' + 10;
        else
            return 0xFFFF;
}
#endif
/////////////////////////////////////////////////////////////////////////
// PrintCRLF() prints a CRLF, and then returns
// TRUE  to show no error occurred.
//
#if defined(MAKE_ALL) || defined( MAKE_PrintCRLF )
PrintCRLF_hdr(CODE_LOC)
{
static ROMCHAR lf[] = "\n";
    printf(lf);
    return TRUE;
}
#endif

/////////////////////////////////////////////////////////////////////////
// PrintError() prints a string to the internal error buffer, if
// we have not already stored one.
//
#if defined(MAKE_ALL) || defined( MAKE_PrintError )
PrintError_hdr(CODE_LOC)
{
    return dispatch(Dispatch_PrintError,&format);
}
#endif

/////////////////////////////////////////////////////////////////////////
// assert saves a string to the internal error buffer,
// if and only if it is an error, and is the first error.
//
#if defined(MAKE_ALL) || defined( MAKE_assert )
assert_hdr(CODE_LOC)
{
    if (!ok)
        dispatch(Dispatch_PrintError,&format);

    return (EmonData->ErrorStored == 0);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanSegOffset scans for an address in the lower megabyte and
//    returns it in seg:offset format.
//
#if defined(MAKE_ALL) || defined( MAKE_ScanSegOffset )
ScanSegOffset_hdr(CODE_LOC)
{
    AnyAddress a;

    if (!ScanAddress(SCAN_1MEG,&a))
        return FALSE;

    *sa = a.SegAddress;
    return TRUE;
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanRangeAddr acquires a range and another address.
//
#if defined(MAKE_ALL) || defined( MAKE_ScanRangeAddress )
ScanRangeAddress_hdr(CODE_LOC)
{
    return ScanRange(Flags,a1,l) && ScanAddress(Flags,a2) && ScanEOL(TRUE);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    ScanRangeList acquires a range and a list
//
#if defined(MAKE_ALL) || defined( MAKE_ScanRangeList )
ScanRangeList_hdr(CODE_LOC)
{
    return ScanRange(Flags,a1,l) && ScanList(NumBytes,FALSE);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    putch() sends one character to the display
//
#if defined(MAKE_ALL) || defined( MAKE_putch )
putch_hdr(CODE_LOC)
{
    EmonData->LineUnfinished = TRUE;

    if (ch == '\n')
    {
        EmonData->LineUnfinished = FALSE;
        if ((EmonData->RawIO&1) == 0)
            EmonData->SerialIO(SerialPutch+'\r',0);
    }

    EmonData->SerialIO(SerialPutch+ch,0);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    getch() gets one character from the keyboard
//
#if defined(MAKE_ALL) || defined( MAKE_getch )
getch_hdr(CODE_LOC)
{
    WORD result;

    do {
        result = EmonData->SerialIO(SerialTryToGetch,0);
    } while (result == 0xFFFF);
    return (BYTE)result;
}
#endif

////////////////////////////////////////////////////////////////////////////
//    kbhit() returns TRUE if there is at least one character ready
//    to be received via getch().
//
#if defined(MAKE_ALL) || defined( MAKE_kbhit )
kbhit_hdr(CODE_LOC)
{
    return EmonData->SerialIO(SerialKbhit,0);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    FlushTransmitter() waits until all waiting characters have been sent
//
#if defined(MAKE_ALL) || defined( MAKE_FlushTransmitter )
FlushTransmitter_hdr(CODE_LOC)
{
    EmonData->SerialIO(SerialFlushOutput,0);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    FlushReceiver() clears the getch() receive buffer.
//
#if defined(MAKE_ALL) || defined( MAKE_FlushReceiver )
FlushReceiver_hdr(CODE_LOC)
{
    EmonData->SerialIO(SerialFlushInput,0);
}
#endif

////////////////////////////////////////////////////////////////////////////
//    EMON has special versions of string routines for these reasons:
//       (1) intrinsics should be turned on so EI/DI and I/O work fine.
//       (2) String intrinsics (inlining) is wasteful
//       (3) There are bugs associated with string intrinsics and optimization
//
#if defined(MAKE_ALL) || defined( MAKE_strlen )
emon_strlen_hdr(CODE_LOC)
{
    return _fstrlen(str);
}
#endif

#if defined(MAKE_ALL) || defined( MAKE_strcmp )
emon_strcmp_hdr(CODE_LOC)
{
    return _fstrcmp(s1,s2);
}
#endif

#if defined(MAKE_ALL) || defined( MAKE_strcpy )
emon_strcpy_hdr(CODE_LOC)
{
    _fstrcpy(dst,src);
}
#endif

#if defined(MAKE_ALL) || defined( MAKE_memcmp )
emon_memcmp_hdr(CODE_LOC)
{
    return _fmemcmp(s1,s2,len);
}
#endif

#if defined(MAKE_ALL) || defined( MAKE_memcpy )
emon_memcpy_hdr(CODE_LOC)
{
    _fmemcpy(dst,src,len);
}
#endif
