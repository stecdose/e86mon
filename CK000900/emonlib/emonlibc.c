/******************************************************************************
 *                                                                            *
 *     EMONLIBC.C                                                             *
 *                                                                            *
 *     This file contains code to automatically link E86Mon extensions        *
 *     into the monitor chain.  This is compiled and placed into the          *
 *     emonlink.lib library.                                                  *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1997 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#include "interlib.h"

// Disable optimization warning
#pragma warning(disable : 4704)

// NewDS is in emonliba.asm.  It copies from our ROM DS to the
// RAM DS, and applies code relocations.

void pascal NewDS(WORD ds);

// FirstFunc and FirstRamLink are defined in emonliba.asm.  When
// C modules use the DISPATCH or RAMLINK macros, they create entries
// in these arrays (they are in their own data segments).

extern DispatchFunc near FirstFunc[];
extern RamLink      near FirstRamLink[];

// EndData and EndCode are in emonliba.asm

extern WORD near EndData;
extern WORD _based(_segname("_CODE")) EndCode;

// This is the standard ROM-based linkage which allows E86Mon to
// dispatch to us, and also to report our presence.  These are
// linked in priority order by this module.

static DispatchLink MyNode =  { 0, FirstFunc, LibName, 0,
                      (WORD)(DWORD)&EndCode,0,
                      (WORD)(DWORD)&EndData,0};

static BOOL FirstTime = TRUE;   // Have we been called before?

// EmonData contains a pointer to E86Mon's internal structure after
// it is properly set up.

FPEmonInternals EmonData;

///////////////////////////////////////////////////////////////////////
// GetDataSegment() attempts to find our old data segment (we may be
// reinitializing).  Failing that, it attempts to allocate new data
// for us.

WORD GetDataSegment(FPEmonInternals internals)
{
    WORD i;
    WORD MyDS;
    WORD DSSize;
    FPLibEntry lib;

    // Try to find old data segment.

    for (i=0; i < internals->MaxLibEntry; i++)
    {
        lib = internals->LibEntries + i;
        if ((lib->codelength == (WORD)&EndCode) &&
            (lib->datalength == (WORD)&EndData) &&
            (lib->codeseg    == CurrentCS()))
        {
            MyDS = lib->dataseg;
            _asm mov ds,MyDS
            return MyDS;
        }
    }

    // Calculate new number of paragraphs required, and return
    // if not enough available.

    DSSize = ((WORD)&EndData) >> 4;
    if (DSSize > internals->BigBufParagraphs)
        return 0;

    // Allocate new data segment, copy ROM data to it, zero out BSS,
    // apply relocations, and make everything point to the new segment.

    for (i=0; i < internals->MaxLibEntry; i++)
    {
        lib = internals->LibEntries + i;
        if (lib->codelength != 0)
            continue;

        // NOTE: Always use one paragraph AFTER BigBuffer+BigBufParagraphs

        internals->BigBufParagraphs -= DSSize;
        MyDS = _FP_SEG(internals->BigBuffer)
                             + internals->BigBufParagraphs + 1;

        NewDS(MyDS);

        // Set this up so we can find ourselves later.

        lib->dataseg    = MyDS;
        lib->codeseg    = CurrentCS();
        lib->datalength = (WORD)&EndData;
        lib->codelength = (WORD)&EndCode;
        
        EmonData = internals;
        return MyDS;
    }
    return 0;
}

///////////////////////////////////////////////////////////////////////
// InstallMyNode() installs our node in the E86Mon ROM chain at
// the proper priority level, and calls all the ROM dispatchers
// with an Install message if it is the first time.

void InstallMyNode(WORD MyDS)
{
    LPDispatchLink far * head;

    MyNode.priority   = LibPriority;
    MyNode.dataseg    = MyDS;
    MyNode.codeseg    = CurrentCS();

    head = &EmonData->LibHead;
    while ((*head != 0) && ( (*head)->priority < MyNode.priority))
        head = &((*head)->next);

    MyNode.next  = *head;
    *head        = &MyNode;

    if (FirstTime)
    {
        WORD s = Dispatch_Install;
        DispatchFunc far * func = FirstFunc;
        while (*func != 0)
        {
            (*func)((LPDispatchStruct)&s);
            func++;
        }
    }
}

///////////////////////////////////////////////////////////////////////
// InstallRamLinks installs any RAM dispatchers which are in this
// extension.  These dispatchers are created with the RAMLINK macro.
//

void InstallRamLinks(void)
{
    LPRamLink InstallThis = FirstRamLink;

    if (!FirstTime)
        return;

    for (InstallThis = FirstRamLink; InstallThis->next == 0; InstallThis++)
    {
        LPRamLink far * head = &EmonData->RamHead;

        while ((*head != 0) && ( (*head)->priority < InstallThis->priority))
            head = &((*head)->next);

        InstallThis->next  = *head;
        *head        = InstallThis;
        InstallThis->func(RAM_INSTALL);
    }
}

///////////////////////////////////////////////////////////////////////
// LibInit is called WITH INTERRUPTS OFF whenever E86Mon restarts.
// It links this library into the chain if the version information
// is correct.

void near pascal LibInit(FPEmonInternals internals)
{
    WORD MyDS;

    if ( (internals == 0) ||
         (internals->StrucSize != sizeof(*internals)) ||
         (internals->Version != VER_NUM) ||
         ( (MyDS = GetDataSegment(internals)) == 0) )
        return;

    InstallMyNode(MyDS);
    InstallRamLinks();

    FirstTime = FALSE;
}
