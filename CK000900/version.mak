REV_INT   = 3
REV_FRAC  = 40
BETA_NUM  = 3

VER         =   $(REV_INT)$(REV_FRAC)

VER_NUM_C   = 0x$(VER)
VER_NUM_ASM =   $(VER)h

VER_STRING = "$(REV_INT).$(REV_FRAC)  1998/10/20"
