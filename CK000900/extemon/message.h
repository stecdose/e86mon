/******************************************************************************
 *                                                                            *
 *     MESSAGE.H                                                              *
 *                                                                            *
 *     This file contains declarations for all messages used in EMON.         *
 *     Also, when included by USENGLIS.C, this file will instantiate          *
 *     the messages.  For other languages, copy this file into a              *
 *     language-specific C file, and then modify it.  This way,               *
 *     no other module should need to be modified or recompiled.              *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#ifndef  _message_h_
#define  _message_h_

#ifdef _usenglish_c_
    #define MSG(module,name,string) \
        ROMCHAR module##Msg##name##[] = string
    #define ERRMSG(module,name,string) \
        ROMCHAR module##Msg##name##[] = #module " error: " string
#else
    #define MSG(module,name,string) \
                 extern ROMCHAR module##Msg##name##[]
    #define ERRMSG(module,name,string)  MSG(module,name,module)
#endif

MSG(System,Null,           "");
MSG(Dos,MemBlock,          "Memory block at %A, size %04X, owner %04X\n");
MSG(Dos,MemTrashed,        "Memory chain not initialized.\n");
ERRMSG(Dos,InsufficientRAM,"Insufficient RAM for program's requirements\n");
MSG(Main,NoParameters,"Use the 'N' command to set command line parameters");

#define MAKESTR2(what) #what
#define MAKESTR(what) MAKESTR2(what)

MSG(Main,Help,
"\nE86 Boot Monitor -- Version " MAKESTR(VER_STRING) "\n" \
"                        Copyright (C) 1994-1998 AMD, Austin, Texas, USA\n"\
"Commands:\n"\
"  B addr        Set breakpoint     M range address       Move memory\n"\
"  C range addr  Compare memory     N [arglist]           Name .exe arguments\n"\
"  D [range]     Dump memory        O[W] word byte|word   Output[word]\n"\
"  E addr [list] Enter memory       P [varname decimal]   Boot Parameters\n"\
"  F range list  Fill memory        R [reg name or '?']   Display/alter regs\n"\
"  G [[=]addr]   Go to address      S range list          Search for string\n"\
"  H [word word] Hex Math or help   T [=address] [word]   Trace 'n' steps\n"\
"  I             Info about system  W [mnemonic name]     Write .EXE to flash\n"\
"  I[W] word     Input[word]        X sectnum|A           Exterminate flash\n"\
"  J             Autobaud again     Z                     Upgrade monitor\n"\
"  L[G][decimal] Load RAM from ROM  :                     Begins download\n"\
"  LL            Load Library       ;                     Comment\n"\
                                                                     "\n"\
"byte == 1-2 hex digits\n"\
"word == 1-4 hex digits\n"\
"addr == word:word or 5 hex digits for absolute address\n"\
"range == addr addr  OR  addr L length\n"\
"list  == list of hex bytes or quoted characters\n"\
"decimal == 1-9 decimal digits\n"\
                                                                     "\n"
);

#endif
