
#include "interlib.h"
#include "message.h"

// Disable optimization warning
#pragma warning(disable : 4704)

///////////////////////////////////////////////////////////////////////
//        ExtendedInfo -- IX command
//
void ExtendedInfo(void)
{
printf("\n\n"
"EmonData structure at %A, size = %X bytes, version = %X, beta = %X\n",
    EmonData,EmonData->StrucSize,EmonData->Version,EmonData->BetaCode);
printf("InputLine at %X, size = %X bytes, Small buffer at %X, size = %X bytes\n",
       EmonData->InputLine,EmonData->InputLineSize,EmonData->SmallBuffer,EmonData->SmallBufSize);
printf("Big buffer at %A, size = %X paragraphs\n",
       EmonData->BigBuffer,EmonData->BigBufParagraphs);
printf("First library chain entry at %A, First RAM chain entry at %A\n",
       EmonData->LibHead,EmonData->RamHead);
printf("Library data array at %X, size = %X entries\n",
       EmonData->LibEntries,EmonData->MaxLibEntry);
printf("Serial IO routine at %A, RawIO=%X\n",
       EmonData->SerialIO,EmonData->RawIO);
printf("LineIndex = %X, ErrorIndex = %X, PromptLength = %X, ErrorStored = %X\n",
        EmonData->LineIndex,EmonData->ErrorIndex,EmonData->PromptLength,EmonData->ErrorStored);
printf("IgnoreError = %X, FileLineCount = %X, LineUnfinished = %X\n",
        EmonData->IgnoreError,EmonData->FileLineCount,EmonData->LineUnfinished);
printf("RestartCode = %X, ExitCode = %X, Milliseconds = %ld\n",
        EmonData->RestartCode,EmonData->ExitCode,EmonData->Milliseconds);
printf("Stack at %A, TrashedInputLine = %X, MonitorExited = %X, NestLevel = %X\n",
        EmonData->stack,EmonData->TrashedInputLine,EmonData->MonitorExited,EmonData->NestLevel);
printf("SpinLeds = %X, MonitorMoved = %X, UnhandledInt at %A\n\n",
        EmonData->SpinLEDsWhileProgramming,EmonData->MonitorMoved,EmonData->UnhandledIntVector);
}

///////////////////////////////////////////////////////////////////////
//        CommandName();     Parameter list
//
void CommandName(void)
{
    WORD i = 129;
    ScanDelimiters(SCAN_SPACES);       // Strip leading spaces

    if (!ScanEOL(FALSE))
        EmonData->BigBuffer[i++] = ' ';          // Start off with exactly one space

    while ((EmonData->BigBuffer[i++] = ReadInputLine(FALSE)) != 0);

    EmonData->BigBuffer[i-1] = ASCII_CR;
    EmonData->BigBuffer[128] = (BYTE)(i-130);
}

///////////////////////////////////////////////////////////////////////
// The DISPATCH macro is what tells EMONLINK.LIB to link us into
// E86MON.  This particular instantiation handles a few commands.
//
DISPATCH(ExtEmon)
{
    case Dispatch_Restart:
        if (EmonData->RestartCode != RESTART_EXIT_PROG)
        {
            // Explain how to get parameters

            emon_strcpy((LPSTR)EmonData->InputLine,MainMsgNoParameters);
            EmonData->LineIndex=0;
            CommandName();
            break;
        }

    case Dispatch_ExecCmd:
        EmonData->LineIndex = 0;
        EmonData->FileLineCount = 0;
        EmonData->ErrorStored   = 0;
        ScanDelimiters(SCAN_SPACES);
        switch (ReadInputLine(TRUE))
        {
            case 'n':
                CommandName();
                return Dispatch_IDidIt;

            case 'i':
                if (ScanOption(0,'x') && ScanEOL(FALSE))
                {
                    ExtendedInfo();
                    return Dispatch_IDidIt;
                }
                break;

            case 'h':
            case '?':
                printf (MainMsgHelp);
                return Dispatch_IDidIt;

            case 'u':
                printf("\n\nDisassembler not supported yet.\n");
                return Dispatch_IDidIt;
            case 'a':
//                if (ScanOption(0,'t') && ScanEOL(FALSE))
                if (ScanOption(0,'t'))
                {
					printf("\nOK\n");
                    return Dispatch_IDidIt;
                }
                printf("\n\nAssembler not supported yet.\n");
                return Dispatch_IDidIt;
            default:
                break;
        }
        break;

  default:
        break;
}
END_DISPATCH
