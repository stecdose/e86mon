
// leds.c
//
// This program relies on the E86MON monitor (ver 3.x) to perform
// processor/pio/serial port initialization and to provide serial i/o support. 
// Serial i/o is accomplished via INT 21 DOS service calls to E86MON monitor.
// For more information on E86MON's DOS service support, please refer to the 
// E86MON information the the README.TXT file , or the E86MON source code 
// itself. 
//
                                                                
#include <conio.h>   
#include <dos.h>

#define PIO_DIR0         0xff72  // PIO direction 0 PCB Register location in IO space

typedef unsigned char    BYTE;
typedef unsigned short   USHORT;

//=============================================================
//
//    ReadChar() -- wait for a character to be received and 
//                  return it
//
BYTE ReadChar( void )
{
   char bChar;
   bChar = (char) _bdos( 0x01, 0, 0);	// Call Dos Service 01h
                                        // Read character with echo
   return bChar;
}
  
//=============================================================
//
//  PostMessage() -- prints the string text message terminated 
//                   with the $ character; calls E86MON's DOS service 
//                   function 09h display string  
//
void PostMessage( char * pszMessage )
{
	_bdos (0x9, (int) pszMessage, 0);	// Call DOS Service 09h
										// Display string
}

//=============================================================
//
//    PostLEDS() -- light the LEDs with the supplied values
//                  This routine assumes that the E86MON monitor has
//                  already configured the respective bits in the 
//                  PIO Mode 0 and PIO Data 0 registers with a value
//                  of 1.  The demo board led is lit by writting a 
//                  0 bit value to the respective bit in the PIO 
//                  Direction 0 register (changing the PIO from an  
//                  input to an output).
//			 
PostLEDS( BYTE bLEDMask )
{
   USHORT usValue;

    bLEDMask = ~bLEDMask;
    usValue = ((USHORT)bLEDMask & 0xc0)<< 8 |(bLEDMask &0x3f);
	_outpw(PIO_DIR0,usValue); 
}


//=============================================================
//

void main(void) 
 {
   USHORT num;
   BYTE   str[3];
   
   PostMessage("Enter a number 0-f, send break signal to abort: $");
   num = ReadChar();

   str[0] = '\r';
   str[1] = '\n';
   str[2] = '$';
   PostMessage(str);

   if (num >= 'A' && num <= 'Z' ) 
     num += 0x20;
   if ( num > 0x60 )
     num -= 87;
   num = num & 0x0f;
   PostLEDS((BYTE) num);
 }
