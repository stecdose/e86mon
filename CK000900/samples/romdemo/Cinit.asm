	page	60, 132
	name	cinit
	title	Microsoft C/C++ 8.0 C/C++ Initializers

;
; Paradigm LOCATE Embedded System Startup Code for MSC/C++ 8.0, Version 5.11
; Copyright (C) 1995 Paradigm Systems.  All rights reserved.
;
; *********************************************************************
; Permission to modify and distribute object files based on this
; source code is granted to licensed users of Paradigm LOCATE.
;
; Under no circumstances is this source code to be re-distributed
; without the express permission of Paradigm Systems.
; *********************************************************************
;
; Requires MASM 6.1 or later to assemble.
; 	masm /mx /D__MDL__ cinit.asm
; 	ml /c /Cx /D__MDL__ cinit.asm
;
; where
; 	__MDL__ is one of the following:
;
; 	__S__	Small memory model
; 	__C__	Compact memory model
; 	__M__	Medium memory model
; 	__L__	Large memory model
; 	__H__	Huge memory model
;
; If no memory model is specified, the small memory model is
; assumed and a warning is issued.
;
	
	INCLUDE	startup.inc		; Macros/assembly language definitions
	INCLUDE	msc80.inc		; MSC/C++ 8.0 specific definitions
	SHOW	<Paradigm LOCATE Microsoft C/C++ 8.0 C/C++ Initializers>

	subttl	Segment ordering/alignment section
	page
;
; Segment and group declarations.  The order of these declarations is
; used to control the order of segments in the .EXE file since the
; Microsoft linker copies segments in the same order they are encountered
; in the object files.
;

DefSeg	_text,    word,  public,  CODE,	       <>
DefSeg	xipb,     word,  public,  DATA,        DGROUP
DefSeg	xip,      word,  public,  DATA,        DGROUP	; Paradigm initializers
DefSeg	xipe,     word,  public,  DATA,        DGROUP
DefSeg	xib,      word,  public,  DATA,        DGROUP
DefSeg	xi,       word,  public,  DATA,        DGROUP	; Near initializers
DefSeg	xie,      word,  public,  DATA,        DGROUP
DefSeg	xifb,     word,  public,  DATA,        DGROUP
DefSeg	xif,      word,  public,  DATA,        DGROUP	; Far initializers
DefSeg	xife,     word,  public,  DATA,        DGROUP
DefSeg	xifcb,    word,  public,  DATA,        DGROUP
DefSeg	xifu,     word,  public,  DATA,        DGROUP	; Far C++ user constructors
DefSeg	xifl,     word,  public,  DATA,        DGROUP	; Far C++ library constructors
DefSeg	xifm,     word,  public,  DATA,        DGROUP	; Far C++ MS constructors
DefSeg	xifce,    word,  public,  DATA,        DGROUP

IFDEF	FARDATA
IF	(FARDATA GT 0)
	SHOW	<Microsoft C/C++ FAR_DATA/FAR_BSS support enabled>
;
; These segments are only used when the application contains far
; initialized data structures or uses huge data.
;

DefSeg	_bfd,     para,  public,  FAR_DATA,      <>	; Start FAR_DATA class
DefSeg	_efd,     para,  public,  ENDFAR_DATA,   <>	; End FAR_DATA class
DefSeg	_brfd,    para,  public,  ROMFARDATA,    <>	; Start of FAR_DATA copy
DefSeg	_erfd,    para,  public,  ENDROMFARDATA, <>	; End of FAR_DATA copy

;
; These segments define the FAR_BSS class.  This is where far uninitialzied data
; is placed.
;

DefSeg	_bfb,     para,  public,  FAR_BSS,       <>	; Start FAR_BSS class
DefSeg	_efb,     para,  public,  ENDFAR_BSS,    <>	; End FAR_BSS class
ENDIF	; FARDATA GT 0
ENDIF	; FARDATA

	page
;
; Functions defined external to this module.
;

IFDEF	COMPRESSED
ExtProc	_DecompressClass		; Paradigm LOCATE class decompression helper
	SHOW	<Including Paradigm LOCATE class decompression support>
ENDIF	; COMPRESSED

IF	@CodeSize EQ MM_NEAR
ExtProc	_aNahdiff, near			; Specify the huge pointer difference
diffProc	equ	_aNahdiff	; function name
ELSE
ExtProc	_aFahdiff, far			; Specify the huge pointer difference
diffProc	equ	_aFahdiff	; function name
ENDIF	; MM_NEAR
	
_text	segment
	assume	cs:_text

BegProc	_cinit

IFDEF	FARDATA
IF	(FARDATA GT 0)
	push	ds			; Save the important registers
	push	si
	push	di

IFDEF	COMPRESSED
;
; Set up the addresses and perform the decompression
;
	mov	ax, _bfd		; Push the destination address
	push	ax
	mov	ax, offset bfdata
	push	ax

	mov	ax, _brfd		; Push the source address
	push	ax
	mov	ax, offset brfdata
	push	ax

	call	_DecompressClass	; Decompress the class
	add	sp, 8			; Clean up the stack and return
ELSE
;
; Copy the FAR_DATA class.  This class can span multiple segments so
; the copying from ROM to RAM must be iterated as many times as
; necessary if the class is larger than 64KB in size.  We start by
; making a normalized pointer to the FAR_DATA class in RAM.  To save
; time and space, we use a run-time library helper function to compute
; difference between the huge pointers.
;
	mov	cx, _bfd		; Get the class base address
	mov	bx, offset bfdata
	mov	dx, _efd		; Get the class end address
	mov	ax, offset efdata
	push	cx			; Push the begin address pointer
	push	bx
	push	dx			; Push the end address pointer
	push	ax
	call	diffProc		; Compute the difference
	mov	bx, _bfd		; Get the class base address
	mov	es, bx
	mov	di, offset bfdata	; ES:DI destination address

	mov	bx, _brfd		; Get the class base address
	mov	ds, bx
	mov	si, offset brfdata	; DS:SI ROM far data source address

	or	dx, dx			; Test if less than a segment
	jz	not64K			; Skip if less than 64K

@@:
	mov	cx, 08000h		; Move 64KB
	rep	movsw			; Copy a full segment
	mov	cx, ds			; Get the source segment
	add	cx, 1000h		; Adjust for the 64K just transferred
	mov	ds, cx			; Save it back
	mov	cx, es			; Get the destination segment
	add	cx, 1000h		; Adjust for the 64K just transferred
	mov	es, cx			; Save it back
	dec	dx			; Bump the count
	jnz	@B			; Repeat for all complete segments

not64K:
	mov	cx, ax			; Get the remaining count
	jcxz	@F			; Skip the copy if zero
	shr	cx, 1			; Convert from bytes to words
	rep	movsw			; Copy the remaining data
@@:
ENDIF	; COMPRESSED

;
; Zero out the FAR_BSS area.  This is more complicated since the
; FAR_BSS class can span multiple segments, exceeding 64K in length.
;
	mov	cx, _bfb		; Get the class base address
	mov	bx, offset bfbss
	mov	dx, _efb		; Get the class end address
	mov	ax, offset efbss
	push	cx			; Push the begin pointer
	push	bx
	push	dx			; Push the end pointer
	push	ax
	call	diffProc		; Compute the difference
	mov	bx, ax			; DX:BX is the length
	mov	ax, _bfb		; Get the class base address
	mov	es, ax
	mov	di, offset bfbss	; ES:DI destination address

	xor	ax, ax			; Use a zero fill pattern
	or	dx, dx			; Test if less than a segment
	jz	$$64			; Skip if less than 64K

@@:
	mov	cx, 08000h		; Clear a full 64KB segment
	rep	stosw			; Clear the segment
	mov	cx, es			; Get the destination segment
	add	cx, 1000h		; Adjust for the 64K just transferred
	mov	es, cx			; Save it back
	dec	dx			; Bump the count
	jnz	@B			; Repeat for all complete segments

$$64:
	mov	cx, bx			; Get the remaining count
	jcxz	@F			; Skip the copy if zero
	shr	cx, 1			; Convert from bytes to words
	rep	stosw			; Clear the segment
@@:

	pop	di			; Clean up and exit
	pop	si
	pop	ds
ENDIF	; FARDATA GT 0
ENDIF	; FARDATA

;
; Call any Paradigm initializers linked in.  These must be executed
; first to guarantee that everything is initialized for the MSC/C++
; initializers.
;
	mov	si, offset DGROUP:xipbegin
	mov	di, offset DGROUP:xipend
	call	farinitterm

;
; Call any far initializers linked in
;
	mov	si, offset DGROUP:xifbegin
	mov	di, offset DGROUP:xifend
	call	farinitterm

;
; Call any near initializers linked in
;
	mov	si, offset DGROUP:xibegin
	mov	di, offset DGROUP:xiend
	call	initterm

;
; Call any far C++ constructors linked in
;
	mov	si, offset DGROUP:xifcbegin
	mov	di, offset DGROUP:xifcend
	call	farinitterm

	ret
EndProc	_cinit

	page
;
; This code installs the initializers/constructors
;

IF	@CodeSize GE MM_FAR		; Far code models
farinitterm	proc	near
ENDIF	;	MM_NEAR

initterm	proc	near
	cmp	si, di			; Test if complete
	jae	alldone

IF	@CodeSize EQ MM_NEAR		; Near code models
	sub	di, 2
	mov	cx, [di]		; Get the initializer address
	jcxz	initterm		; Skip the null procedures
	call	cx			; Make the call
ELSE					; Far data models
	sub	di, 4
	mov	ax, [di]		; Check for a null pointer
	or	ax, [di+2]
	jz	initterm		; Skip any null procedures
	call	dptr [di]		; Call the initializer
ENDIF	;	MM_NEAR
	jmp	initterm		; Keep looping till done
alldone:
	ret
initterm	endp
IF	@CodeSize GE MM_FAR		; Far code models
farinitterm	endp
ENDIF	;	MM_NEAR


IF	@CodeSize EQ MM_NEAR
;
; Only needed in the near code models since the default version will
; use far initializers automatically.
;
farinitterm	proc	near
	cmp	si, di			; Test if complete
	jae	falldone

	sub	di, 4
	mov	ax, [di]		; Check for null pointers
	or	ax, [di+2]
	jz	farinitterm		; Skip any null procedures
	call	dptr [di]		; Call the initializer
	jmp	farinitterm		; Keep looping till done
falldone:
	ret
farinitterm	endp
ENDIF	;	MM_NEAR

_text	ends

	page
IFDEF	FARDATA
IF	(FARDATA GT 0)
;
; These are the four segments we defined above to support the FAR_DATA class.
; Here we open each segment and define a label that we can take the address of
; for the purposes of finding the length of class FAR_DATA, and the starting
; addresses of the classes FAR_DATA and ROMFARDATA.
;

_bfd	segment
bfdata	label	byte			; Mark the start of class FAR_DATA
_bfd	ends
_efd	segment
efdata	label	byte
	db	16 dup (?)		; Force the next segment to a new paragraph
_efd	ends

_brfd	segment
brfdata	label	byte			; Mark the start of class ROMFARDATA
_brfd	ends
_erfd	segment
	db	16 dup (?)		; Force the next segment to a new paragraph
_erfd	ends

;
; These are the two segments we defined above to support the FAR_BSS class.
; Here we open each segment and define a label that we can take the address of
; for the purposes of finding the length and starting address of the FAR_BSS
; class.
;

_bfb	segment
bfbss	label	byte			; Mark the start of class FAR_BSS
_bfb	ends
_efb	segment
efbss	label	byte
	db	16 dup (?)		; Force the next segment to a new paragraph
_efb	ends

ENDIF	; FARDATA GT 0
ENDIF	; FARDATA

;
; Define the contents of the initializer segments.  The main purpose is to
; define symbols which can be referenced to compute the number of initializers
; linked in at run-time.
;

xipb	segment				; Start of Paradigm initializers
xipbegin label	byte
xipb	ends
xipe	segment				; End of Paradigm initializers
xipend	label	byte
xipe	ends
xib	segment				; Start of near initializers
xibegin	label	byte
xib	ends
xie	segment				; End of near initializers
xiend	label	byte
xie	ends
xifb	segment				; Start of far initializers
xifbegin label	byte
xifb	ends
xife	segment				; End of far initializers
xifend	label	byte
xife	ends
xifcb	segment				; Start of far C++ constructors
xifcbegin label	byte
xifcb	ends
xifce	segment				; End of far C++ constructors
xifcend	label	byte
xifce	ends

	end
