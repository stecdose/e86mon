	.xlist
;
; Assembly language definitions for Microsoft C/C++, Version 5.11
; Copyright (C) 1995 Paradigm Systems.  All rights reserved.
;
; *********************************************************************
; Permission to modify and distribute object files based on this
; source code is granted to licensed users of Paradigm LOCATE.
;
; Under no circumstances is this source code to be re-distributed
; without the express permission of Paradigm Systems.
; *********************************************************************
;
; This include file helps set the memory model variables and defines
; data structures used by Microsoft C/C++ assembly language modules.
;

;
; This macro is used to display comments during assembly
;
SHOW	macro	msg
IFDEF	??Version			; Check if TASM is doing the work
NOWARN	PDC				; Disable pass 1 warnings for TASM
ENDIF	; ??Version
IF1
	%out	msg
ENDIF	; IF1
IFDEF	??Version
WARN	PDC				; Turn warnings back on
ENDIF	; ??Version
	endm

IFDEF	??Version			; Check for TASM
NOWARN	RES				; Turn off reserved words warnings
ENDIF	; ??Version

;
; Determine the memory module begin used and define values which can
; be tested later.  This section appears after the default segment
; declarations to avoid any problems with changing the default alignment
; characteristics.
;
IFDEF		__S__
	@CodeSize = 0
	@DataSize = 0
	pptr	equ	near ptr
	SHOW	<Assembling for the small memory model>
ELSEIFDEF	__C__
	@CodeSize = 0
	@DataSize = 1
	pptr	equ	near ptr
	SHOW	<Assembling for the compact memory model>
ELSEIFDEF	__M__
	@CodeSize = 1
	@DataSize = 0
	pptr	equ	far ptr
	SHOW	<Assembling for the medium memory model>
ELSEIFDEF	__L__
	@CodeSize = 1
	@DataSize = 1
	pptr	equ	far ptr
	SHOW	<Assembling for the large memory model>
ELSEIFDEF	__H__
	@CodeSize = 2
	@DataSize = 2
	pptr	equ	far ptr
	SHOW	<Assembling for the huge memory model>
ELSE	
	@CodeSize = 0
	@DataSize = 0
	pptr	equ	near ptr
	SHOW	<No model specified - Assembling for the small memory model>
ENDIF	; IFDEF


; --- Microsoft C/C++ Extensions ---

;
; These definitions allow access to the data structures used by the
; Microsoft C/C++ heap run-time library routines.
;

_mcb	struc
	id	db	?		; MCB type
	owner	dw	?		; MCB owner
	thesize	dw	?		; MCB block size
		db	11 dup (?)	; Padding to paragraph length
_mcb	ends


_heap_seg_desc	struc
	checksum	dw	?	; checksum area
	flags		dw	?	; flags word
	segsize 	dw	?	; size of segment
	start		dw	?	; offset of first heap entry
	rover		dw	?	; rover offset
	last		dw	?	; offset to end-of-heap marker
	nextseg 	dd	?	; far pointer to next _heap_seg_desc
	prevseg 	dd	?	; far pointer to previous _heap_seg_desc
_heap_seg_desc	ends


;
; _heap_seg_desc.flags bit offsets
;

_HEAP_MODIFY	equ	01h		; heap segment size can be modified
_HEAP_FREE	equ	02h		; heap segment may be freed up to OS
_HEAP_NEAR	equ	04h		; heap segment is part of the near heap
_HEAP_BASED	equ	08h		; heap segment is part of the based heap


;
; General Use Heap Constants
;

_HEAP_END	equ	0FFFEh		; End-of-heap flag

;
; DOS file handles
;

_NFILE	 	equ	20		; maximum # files per process

FOPEN		equ	01H		; file handle open
FEOFLAG		equ	02H		; end of file has been encountered
FCRLF		equ	04H		; CR-LF across read buffer (in text mode)
FPIPE		equ	08H		; file handle refers to a pipe
FRDONLY 	equ	10H		; file handle associated with read only file
FAPPEND 	equ	20H		; file handle opened O_APPEND
FDEV		equ	40H		; file handle refers to device
FTEXT		equ	80H		; file handle is in text mode

	.list
