#include "stdio.h"
#include "stdlib.h"

#define MAX_INNER_ITERATIONS 32000     // or Dhrystone dies
#define MAX_OUTER_ITERATIONS 320L      // or we overflow

typedef unsigned long DWORD;
typedef unsigned short WORD;
typedef unsigned char  BYTE;

typedef WORD BOOL;
#define TRUE   1
#define FALSE  0

static int  Outer_Iterations = 0;
static WORD Inner_Iterations = MAX_INNER_ITERATIONS;

static DWORD Total_Iterations;
static DWORD Start_Time;

// Disable assembly language warning
#pragma warning(disable : 4704)

DWORD GetTime(void)
{
    BYTE Hour, Minutes, Seconds, Hundredths;
    DWORD Total;

    _asm {
        mov     ah,0x2C
        int     0x21
        mov     Hour,ch
        mov     Minutes,cl
        mov     Seconds,dh
        mov     Hundredths,dl
   }

    Total = Hour;
    Total *= 60;
    Total += Minutes;
    Total *= 60;
    Total += Seconds;
    Total *= 100;
    Total += Hundredths;
    return Total;
}

void SetupEverything(void)
{
    char  string[100];
    WORD  index;
    BOOL  Done = FALSE;

    while (!Done)
    {
        printf("\r\nPlease enter number of iterations to run (0 to exit) :");
        scanf("%s",string);
        if (string[0] == 0)
            continue;
        index = 0;
        Total_Iterations = 0;
        Done = TRUE;
        while (string[index] != 0)
            if ((string[index] < '0') || (string[index] > '9')
                      || (Total_Iterations > (1L * MAX_INNER_ITERATIONS)
                                                  *MAX_INNER_ITERATIONS/100))
            {
                Done = FALSE;
                break;
            }
            else
            {
                Total_Iterations *= 10;
                Total_Iterations += string[index++] - '0';
            }
    }

    if (Total_Iterations == 0)
        exit(0);

    Outer_Iterations = (WORD)(Total_Iterations / MAX_INNER_ITERATIONS);
    Inner_Iterations = (WORD)(Total_Iterations % MAX_INNER_ITERATIONS);

    printf("\nDhrystone Benchmark, Version 2.1 (Language: C)\n\n"
           "Compiler: MS-Visual C++ [16-bit] v1.52\n\n"
           "Iterations: %lu\n\n",Total_Iterations); 
}


void timer_stop(void)
{
    DWORD Total_Time;
    DWORD Dhrystones;

    Total_Time = GetTime() - Start_Time;
    if (Total_Time == 0)
        Total_Time = 1;

    Dhrystones = (Total_Iterations*100)/Total_Time;


    printf("Elapsed time:              %lu.%02lu seconds\n"
           "Iterations:                %lu\n"
           "Dhrystone 2.1 value:       %lu\n"
           "VAX mips (Dhrystone/1757): %lu.%02lu\n",
           Total_Time/100,
           Total_Time%100,
           Total_Iterations,
           Dhrystones,
           Dhrystones/1757,
           ((Dhrystones*100)/1757) % 100
           );
}
int timer_start(void)
{
    static BOOL First_Time = TRUE;
    static BOOL Print_Results = FALSE;
    WORD   Result;

    if (First_Time)
    {
        if (Print_Results)
            timer_stop();
        else
            Print_Results = TRUE;

        First_Time = FALSE;
        SetupEverything();
        Start_Time = GetTime();
    }

    while (Outer_Iterations-- > 0)
        return MAX_INNER_ITERATIONS;

    Result = Inner_Iterations;
    First_Time = TRUE;

    return Result;
}
