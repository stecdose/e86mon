//
//  $Id: Testmon.c 1.1 1997/01/29 01:05:26 bblack Exp $
//  $Author: bblack $
//  $Locker: $
//
/******************************************************************************
 * Copyright 1995, 1996 Advanced Micro Devices, Inc.
 *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which
 * specifically  grants the user the right to modify, use and distribute this
 * software provided this notice is not removed or altered.  All other rights
 * are reserved by AMD.
 *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR
 * USE OF THIS SOFTWARE.
 *
 * So that all may benefit from your experience, please report  any  problems
 * or  suggestions about this software to:
 *
 * Advanced Micro Devices, Inc.
 * Logic Products Division
 * Mail Stop 590
 * 5204 E. Ben White Blvd.
 * Austin, TX 78741
 *****************************************************************************/
 
 /******************************************************************************
 *
 * tesmon.c
 *
 * Test application for 186 Monitor Dos .exe support
 *
 * Created: 12/96 RBB
 *
 * Modified:
 *
 *****************************************************************************/

#include <stdio.h>
#include <dos.h>
                                         
extern void ShowMemory(void);

/* Function 00h - Terminate process */
int test_00()
{
	// This function requires CS = PSP which is not true in this .exe
	printf("Testing 00h Terminate process: \n");
	_bdos( 0x00, 0, 0);
	return 1;           
}                                         
                                         
/* Function 01h - Character input with echo */
int test_01()
{
	char cTest;
	printf("Testing 01h character input with echo - please type a character:");
	cTest = (char) _bdos( 0x01, 0, 0);
	printf(" Got '%c'.\n",cTest);
	return 1;           
}

/* Function 02h - Character echo */
int test_02()
{
	char cTest = '*';
	printf("Testing 02h character output - character is an asterisk: ");
	_bdos( 0x02, (int)cTest, 0);
	printf("\n");
	return 1;			
}

/* Function 06h - Direct console I/O */
int test_06()
{
	char cTest = '*';
	printf("Testing 06h console output - character is an asterisk: ");
	_bdos( 0x06, (int)cTest, 0);
	printf("\n");
	printf("Testing 06h console input (no echo) - please type a character >");
	while ((_bdos (0x0b, 0,0) & 0xff) == 0);	// wait for a character from the keyboard
	cTest = _bdos( 0x06, (int)0xff, 0);
	printf("< Got '%c'.\n",cTest);
	return 1;			
}

/* Function 07h - Unfiltered Character input without echo */
int test_07()
{
	char cTest;
	printf("Testing 07h character input without echo - please type a character >");
	cTest = (char) _bdos( 0x07, 0, 0);
	printf("< Got '%c'.\n",cTest);
	return 1;           
}

/* Function 08h - Unfiltered Character input without echo */
int test_08()
{
	char cTest;
	printf("Testing 08h character input without echo - please type a character >");
	cTest = (char) _bdos( 0x08, 0, 0);
	printf("< Got '%c'.\n",cTest);
	return 1;           
}

/* Function 09h - display string */
int test_09()
{
	char str[] = "Test passed.\r\n$";
   /* Offset of string must be in DX, segment in DS. AL is not needed,
    * so 0 is used.    */   
    printf("Testing 09h display string: ");
    _bdos( 0x09, (int)str, 0 );
    return 1;
}
 
/* Function 0ah - Buffered keyboard input  */
int test_0a()
{
	char cBuff[82];
	union _REGS inregs, outregs;        
	struct _SREGS segregs;

	cBuff[0] = 80;
	cBuff[1] = 0;
	printf("Testing 0ah buffered keyboard input - enter a string and hit c/r: ");
	inregs.h.ah = 0x0a; 			/* DOS Buffered keyboard input function */
    inregs.x.dx = (int)cBuff ;		// offset of buffer
	segregs.ds = (_segment)cBuff;	// equivilant to current DS for near data
	_intdosx( &inregs, &outregs, &segregs );
	
	cBuff[cBuff[1]+2] = 0;  		// null terminate string
    printf("\n\t\t'%s' %d characters read. Test complete.\n",(cBuff+2), cBuff[1] );
	return 1;  
}                                         

/* Function 0bh - check keyboard status */
int test_0b()
{
	unsigned int	nCharReady =0;
	nCharReady = _bdos(0x0b, 0,0) & 0xff; 			// value passed in al 
	
	if (nCharReady == 0xff)
		return -1;					// don't expect a character to be ready 
	
	printf("Testing 0bh keyboard status - please hit a key >");
	while (nCharReady == 0)
	{
		nCharReady = _bdos (0x0b, 0,0) & 0xff;
	}
	(char) _bdos( 0x01, 0, 0);		// get character input with echo 

	printf("< Test complete.\n"); 
	return 1;
}
             
/* Function 0ch - Flush  */
int test_0c()
{
	char cTest;
	printf("Testing 0ch flush functionality - type a character to buffer ->");
	while ((_bdos (0x0b, 0,0) & 0xff) == 0);	// wait for a character from the keyboard
	printf("<\nTesting 0ch flush functionality - now type a different character :");
	cTest = (char) _bdos( 0x0c, 0, 0x01);
	printf(" Got '%c'.\n",cTest);
	return 1;           
}


/* Function 25h - Set interrupt vector  */
int test_25()
{
	union _REGS inregs, outregs;        
	struct _SREGS segregs;
	static int 		nRestoreFlag =0;
	static int		nOrigOffset =0;
	static int		nOrigSegment =0;
	
	printf("Testing 25h set interrupt vector: ");
 	
 	if (! nRestoreFlag )
 	{
		nRestoreFlag ++;;
		inregs.h.ah = 0x35;           /* DOS get interrupt vector function: */
	    inregs.h.al = 0;              /* get current divide by zero handler */
		_intdosx( &inregs, &outregs, &segregs );
		
		nOrigSegment = segregs.es;    /* save these values to restore later */
		nOrigOffset = outregs.x.bx;
 		segregs.ds = 0xcafe;
 	 	inregs.x.dx = 0xbeef;
		printf("Modifying divide by 0 handler... ");
 	}
 	else
 	{
 		segregs.ds =  nOrigSegment;
 		inregs.x.dx = nOrigOffset;
		printf("Restoring divide by 0 handler... ");
    }
	inregs.h.ah = 0x25;           /* DOS set interrupt vector function: */
    inregs.h.al = 0;

 	_intdosx( &inregs, &outregs, &segregs );
    
	printf("done.\n");    
	return 1;  
}                                         

/* Function 2ah - Get date  */
int test_2a()
{
	union _REGS inregs, outregs;
	printf("Testing 2ah get date: ");
	inregs.h.ah = 0x2a;           /* DOS Get Date function */
	_intdos( &inregs, &outregs );
	printf( "Date: %d/%d/%d\n", outregs.h.dh, outregs.h.dl, outregs.x.cx );
	return 1;
}                                         
             
/* Function 2ch - Get time  */
int test_2c()
{
	union _REGS inregs, outregs;        
	printf("Testing 2ah get time: ");
	inregs.h.ah = 0x2c;           /* DOS Get Time function: */
	_intdos( &inregs, &outregs );
	printf( "Time: %d:%d:%d.%d\n",outregs.h.ch, outregs.h.cl, outregs.h.dh, outregs.h.dl);
	return 1;	
}                                  

/* Function 2dh - Set time  */
int test_2d()
{
	union _REGS inregs, outregs;        
	printf("Testing 2dh set time: ");
	inregs.h.ah = 0x2d;           /* DOS Set Time function: */
   	/* try valid date */
	inregs.h.ch = 23; 		// hour       (0-23)
	inregs.h.cl = 59;       // minute     (0-59)
	inregs.h.dh = 59;       // second     (0-59)
	inregs.h.dl = 99;       // hundreths  (0-99)
	_intdos( &inregs, &outregs );
   	if ( outregs.h.al == 0xff )
   	{
   		printf("\n\t\tFAILED to set valid time.\n");
   		return -1;
   	}
  	/* try invalid date */
	inregs.h.ch = 23;
	inregs.h.cl = 60;   	// invalid minutes
	inregs.h.dh = 59;
	inregs.h.dl = 99;
   	_intdos( &inregs, &outregs );
   	if ( outregs.h.al == 00 )
   	{
   		printf("\n\t\tFAILED to flag error with bogus time.\n");
   		return -1; 
   	}
   	printf("Test complete.\n");
   	return 1;
}

/* Function 30h - Get DOS version  */
int test_30()
{
	union _REGS inregs, outregs;        
	printf("Testing 30h DOS version: ");
	inregs.h.ah = 0x30;           /* DOS Get MS-DOS version number */
    inregs.h.al = 0;
	_intdos( &inregs, &outregs );
    printf("Ver %d.%d OEM %d\n", outregs.h.al,outregs.h.ah, outregs.h.bh);
    return 1;
}

/* Function 35h - Get interrupt vector  */
int test_35()
{
	union _REGS inregs, outregs;        
	struct _SREGS segregs;
	printf("Testing 35h get interrupt vector: ");
	inregs.h.ah = 0x35;           /* DOS get interrupt vector function: */
    inregs.h.al = 0;
    inregs.x.bx = 0;	// standard output handle
	_intdosx( &inregs, &outregs, &segregs );
    printf("divide by 0 handler location %04x:%04x\n", segregs.es, outregs.x.bx);  
	return 1;  
}                                         

/* Function 3fh - Read file or device  */
int test_3f()
{
	char cBuff[81];
	union _REGS inregs, outregs;        
	struct _SREGS segregs;

	printf("Testing 3Fh Read file or device - enter a string and hit c/r: ");
	inregs.h.ah = 0x3f; /* DOS read file or device function: */
    inregs.x.bx = 0;	// standard input handle
    inregs.x.cx = 40;	// max number of bytes to read
    inregs.x.dx = (int)cBuff ;	// offset of buffer
	segregs.ds = (_segment)cBuff;		// equivilant to current DS for near data
	_intdosx( &inregs, &outregs, &segregs );
    if (outregs.x.cflag != 0)
    {
    	printf("\n\t\tFAILED.  Error 0x%x\n", outregs.x.ax);
    	return -1;
    }
	cBuff[outregs.x.ax -2] =0;
    printf("\t\t'%s' %d characters read. Test complete.\n",cBuff, outregs.x.ax );
	return 1;  
}                                         

/* Function 40h - Write file or device */ 
int test_40()
{
	char cBuff[] = "Test passed.\r\n";
	union _REGS inregs, outregs;        
	struct _SREGS segregs;

	printf("Testing 40h Write file or device: ");
	inregs.h.ah = 0x40; /* DOS read file or device function: */
    inregs.x.bx = 1;	// standard output handle
    inregs.x.cx = 14;	// number of bytes to write
    inregs.x.dx = (int)cBuff ;	// offset of buffer
	segregs.ds = (_segment)cBuff;		// equivilant to current DS for near data
	_intdosx( &inregs, &outregs, &segregs );
    if (outregs.x.cflag != 0)
    {
    	printf("\n\t\tFAILED.  Error 0x%x\n", outregs.x.ax);
    	return -1;
    }
	return 1;  
}                                         

/* Function 44h - IOCTL  subfunction 00h get device information */
int test_44_00()
{
	union _REGS inregs, outregs;        
	printf("Testing 44h-00 IOCTL get device information: ");
	inregs.h.ah = 0x44;           /* DOS Set Time function: */
    inregs.h.al = 0;
    inregs.x.bx = 1;	// standard output handle
	_intdos( &inregs, &outregs );
    printf("STDOUT information 0x%04x.\n", outregs.x.dx);  
	return 1;  
}                                         

/* Function 48h - Allocate memory block */
int test_48(int nMemSize, int nExtraPages)
{
	union _REGS inregs, outregs;        
	printf("Testing 48h allocate memory: ");

	printf("%dKB block...",nMemSize);
	inregs.h.ah = 0x48;           /* DOS allocate memory function: */
    inregs.x.bx = (64 * nMemSize) + nExtraPages;
	_intdos( &inregs, &outregs );
	if (outregs.x.cflag !=0)
	{
		printf("\n\t\tFAILED. Error 0x%x paragraphs avail: 0x%x\n", outregs.x.ax, outregs.x.bx);
		return 0;
	}
	else
	{
		printf("done.  Base seg 0x%x\n",outregs.x.ax);
	}

    return outregs.x.ax;
}                                         

/* Function 49h - Free memory block */ 
int test_49(int nMemSeg)
{
	union _REGS inregs, outregs;        
	struct _SREGS segregs;
	if (nMemSeg == 0)
		return 0;
	printf("Testing 49h release memory: ");

	printf("block seg 0x%x...",nMemSeg);
	inregs.h.ah = 0x49;           /* DOS release memory function: */
    segregs.es = nMemSeg;         /* passed memory segment address */
	_intdosx( &inregs, &outregs, &segregs );
	if (outregs.x.cflag !=0)
	{
		printf("\n\t\tFAILED. Error 0x%x\n", outregs.x.ax);
		return -1;
	}
	printf("done!\n");
    return 1;
}                                         

/* Function 4ah - Resize memory block */ 
int test_4a(int nMemSeg, int nMemSize)
{
	union _REGS inregs, outregs;        
	struct _SREGS segregs;

	if (nMemSeg == 0)
		return 0;
	printf("Testing 4ah resize memory: ");

	printf("block segment 0x%x to %d K...",nMemSeg, nMemSize);
	inregs.h.ah = 0x4a;           /* DOS allocate memory function: */
    inregs.x.bx = 64 * nMemSize;
    segregs.es = nMemSeg;
	_intdosx( &inregs, &outregs, &segregs );
	if (outregs.x.cflag !=0)
	{
		printf("\n\t\tFAILED. Error 0x%x paragraphs avail: 0x%x\n", outregs.x.ax, outregs.x.bx);
		return outregs.x.bx;
	}
	printf("done!\n");
    return 1;

}                                         

/* Function 4ch - Terminate process */ 
int test_4c()
{
	printf("Testing 4ch Terminate process w/ return code 0x55.\n");
	_bdos( 0x4c, 0, 0x55);
	return 1;           
}                                         

void main(int argc, char *argv[])
{
	int i,nMemSeg1, nMemSeg2, nMemSeg3;
	
	printf("\nTesting INT 21 DOS calls.\n");
	printf("=========================\n");

	printf("argc = %d:\n ", argc);
	for (i=0; i< argc; ++i)
		printf("\targv[%d] = %s\n", i, argv[i]);
	printf("\n");

//	test_00();   	// Terminate process
 	test_01();
 	test_02();
 	test_06();
 	test_07();
 	test_08();
 	test_09();
 	test_0a();
 	test_0b();
 	test_0c();
 	test_35(); 	// report current int vector
 	test_25();      // change vector
 	test_35();      // report vector
 	test_25();      // restore vector
 	test_35();      // report vector
 	test_2a();
 	test_2c();
// 	test_2d();		
 	test_30();
 	test_3f();
 	test_40();
 	test_44_00();
 	nMemSeg1 = test_48(4,0);// allocate memory Block 1
 	test_4a(nMemSeg1,3);    // resize memory Block 1 smaller
 	test_4a(nMemSeg1,3);    // resize memory Block 1 same
 	test_4a(nMemSeg1,5);    // resize  memory Block 1 larger
 	nMemSeg2 = test_48(0,0);// allocate memory Block 2 of size 0
 	test_4a(nMemSeg2,3);    // resize  memory Block 2 larger
 	nMemSeg3 = test_48(1,0);// allocate memory Block 3
	test_49(nMemSeg2);      // free memory Block 2
 	test_4a(nMemSeg1,8);    // resize memory Block 1 larger (is Block #2's space avaiable?)
 	test_49(nMemSeg1);      // free memory Block 1 Join two free memory blocks? 
 	nMemSeg1 = test_48(4,1);// allocate memory Block 1 Where does new memory block go?
 	test_4a(nMemSeg3,0);    // resize memory Block 3 to size 0
 	test_49(nMemSeg3);      // free 	memory Block 3
 	test_49(nMemSeg1);      // free 	memory Block 1
	printf("Testing unsupported interrupt 21.  Type 'G' at the E86MON prompt to continue...\n");
	_bdos( 0xbe, 0, 0xef);	// unsupported interrupt
 	test_4c();		// Terminate w/ return value
 	
	printf("\n=========================\n");
 	printf("End of test.\n");
 	
// 	exit(1);	// Should perform 4c termination   
}                                                   
