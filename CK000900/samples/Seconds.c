#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

typedef unsigned long DWORD;
typedef unsigned short WORD;
typedef unsigned char  BYTE;

typedef WORD BOOL;
#define TRUE   1
#define FALSE  0

// Disable assembly language warning
#pragma warning(disable : 4704)

DWORD GetTime(void)
{
    BYTE Hour, Minutes, Seconds, Hundredths;
    DWORD Total;

    _asm {
        mov     ah,0x2C
        int     0x21
        mov     Hour,ch
        mov     Minutes,cl
        mov     Seconds,dh
        mov     Hundredths,dl
   }

    Total = Hour;
    Total *= 60;
    Total += Minutes;
    Total *= 60;
    Total += Seconds;
    Total *= 100;
    Total += Hundredths;
    return Total;
}

void main(void)
{
    DWORD lastTime,thisTime;
    WORD  seconds = 0;

    lastTime = GetTime();
    while (!_kbhit())
    {
        thisTime = GetTime();
        if (thisTime - lastTime < 100)
            continue;
        seconds++;
        lastTime += 100;
         printf("\r%u",seconds);
     }
     getch();
     printf("\n\n");
 }
