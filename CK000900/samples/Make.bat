call ..\setenv

rem Set compiler options

set cl=/nologo /f- /Gs /G1 /W3 /Oa /Ob2 /Oc /Oe /Og /Oi /Ol /On /Oo

rem Compile and link

%amd_lpd_cl%cl  amddhry.c dhry_1.c dhry_2.c
..\out\makehex amddhry
move amddhry.hex ..\out
%amd_lpd_cl%cl  seconds.c
..\out\makehex seconds
move seconds.hex ..\out

%amd_lpd_cl%cl /Od /D "-DOS" testmon.c
..\out\makehex testmon
move testmon.hex ..\out

rem We don't have the tools to make the ROM demo, so we'll just copy it

copy romdemo\*.hex ..\out

move *.exe ..\out
del *.obj

set cl=
