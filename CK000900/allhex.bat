cd out
echo ; > all.hex
echo ; Downloading upgrade monitor >> all.hex
echo ; >> all.hex
type EMON340U.HEX >> all.hex
echo ; >> all.hex
echo ; Downloading monitor extensions >> all.hex
echo ; >> all.hex
type EXTEM340.HEX >> all.hex
echo ; >> all.hex
echo ; Downloading dual CRT sample >> all.hex
echo ; >> all.hex
type DUALC340.HEX >> all.hex
echo ; >> all.hex
echo ; Downloading FINDEE sample >> all.hex
echo ; >> all.hex
type FINDEE.HEX >> all.hex
echo w testmon >> all.hex
type TESTMON.HEX >> all.hex
echo w amddhry >> all.hex
type AMDDHRY.HEX >> all.hex
echo w finder >> all.hex
type FINDER.HEX >> all.hex
echo w seconds >> all.hex
type SECONDS.HEX >> all.hex
cd ..
