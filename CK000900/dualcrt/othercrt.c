/******************************************************************************
 *                                                                            *
 *     OTHERCRT.C                                                             *
 *                                                                            *
 *     This file gives an example extension which connects both               *
 *     serial ports on an ES or ER.  It is called by DUALCRT.C                *
 *                                                                            *
 *     Most of the code in here was stolen straight from 186SER.C             *
 *     and 186INITS.C in the main monitor.                                    *
 *                                                                            *
 *     The only different things are the detection of the inactive            *
 *     serial port (by examining interrupt enable masks), and the             *
 *     break handling (which is partly done in the thunk).                    *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/


#define CODE_LOC Ram_    /* Tell libraries we want RAM versions */

#include "emon.h"
#include "am186msr.h"
#include "186init.h"

#pragma warning(disable:4701)    // Local vars referenced before init

static SerPort ES_Serial[2] = {
      {SPRT1_CTL,SPRT1_STAT,SPRT1_TX,SPRT1_RX,
       SPRT1_BDV,INT_SPRT1,INT_EOI,ITYPE_SPRT1, IMASK_ON, IMASK_OFF,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES | SPRT_CTL_RMODE_ES,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES | SPRT_CTL_RMODE_ES |
           SPRT_CTL_RXIE_ES,
       SPRT_CTL_TXIE_ES,
       SPRT_STAT_RDR_ES,
       SPRT_STAT_THRE_ES,
       SPRT_STAT_BRK1_ES,
       SPRT_STAT_FRAME_ES | SPRT_STAT_PARITY_ES | SPRT_STAT_OVERFLOW_ES | SPRT_STAT_BRK1_ES,
       16,0,
       (WORD)~(PIO1_TX0 | PIO1_RX0 | PIO1_TX1 | PIO1_RX1),
       PIO1_RX0 | PIO1_RX1
      },
      {SPRT0_CTL,SPRT0_STAT,SPRT0_TX,SPRT0_RX,
       SPRT0_BDV,INT_SPRT0,INT_EOI,ITYPE_SPRT0, IMASK_ON, IMASK_OFF,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES | SPRT_CTL_RMODE_ES,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES | SPRT_CTL_RMODE_ES |
           SPRT_CTL_RXIE_ES,
       SPRT_CTL_TXIE_ES,
       SPRT_STAT_RDR_ES,
       SPRT_STAT_THRE_ES,
       SPRT_STAT_BRK1_ES,
       SPRT_STAT_FRAME_ES | SPRT_STAT_PARITY_ES | SPRT_STAT_OVERFLOW_ES | SPRT_STAT_BRK1_ES,
       16,0,
       (WORD)~(PIO1_TX0 | PIO1_RX0 | PIO1_TX1 | PIO1_RX1),
       PIO1_RX0
      }
};


// Disable optimization warning
#pragma warning(disable : 4704)

#define INPUT_BUFSIZE 256    // MUST BE POWER OF 2!!!!!!!
#define INPUT_HIGHWATER 80
#define INPUT_LOWWATER  40

#define OUTPUT_BUFSIZE  16    // MUST BE POWER OF 2!!!!!!!


static SerPort SysSprt;

static BYTE InputBuffer[INPUT_BUFSIZE];
static BYTE OutputBuffer[OUTPUT_BUFSIZE];
static volatile WORD InputPutPtr;
static          WORD InputGetPtr;
static          WORD OutputPutPtr;
static volatile WORD OutputGetPtr;
static          BOOL XOffSent;
static          BOOL NeedXOff;
static          BOOL ReceivedXOff;

WORD   UARTStatus;

static WORD far SerialIO(WORD action);

typedef WORD (far * Proc)(WORD);


/////////////////////////////////////////////////////////////////////////
// IrqSerialEntry() is the console driver's interrupt routine.  It
// handles all transmission/reception and XON/XOFF flow control.
//
BOOL far cdecl IrqSerialEntry(void)
{
    WORD Status;
    BYTE   Data;
    WORD BufRoom;
    WORD  Ctrl;

    Status = (WORD)_inpw(SysSprt.StatAddr);
    if ((Status & SysSprt.RxStatMask) != 0)
        Data = (BYTE)_inpw(SysSprt.RxAddr);

    //  Only clear the status register when an error occurs to 
    //  miminize the effect of the EM sport erratum on the application     
    if (Status & SysSprt.ErrStatMask)
        OutToPCBReg(SysSprt.StatAddr, 0);

    Ctrl = (WORD)_inpw(SysSprt.CtlAddr);
    OutToPCBReg(SysSprt.CtlAddr,SysSprt.NoIntCtlMask);
    OutToPCBReg( SysSprt.EOIAddr, SysSprt.IntType); // end-of-interrupt

    BufRoom = (InputGetPtr - InputPutPtr-1) & (INPUT_BUFSIZE-1);

    UARTStatus |= Status;

    if ((Status & SysSprt.RxStatMask) != 0)
        switch (Data)
        {
        case ASCII_XOFF:
            ReceivedXOff = TRUE;
            break;

        case ASCII_XON:
            ReceivedXOff = FALSE;
            Ctrl |= SysSprt.TxCtlMask;
            break;

        default:
            if (BufRoom == 0)
            {
                UARTStatus |= 0x8000;
                break;
            }
            InputBuffer[InputPutPtr++] = Data;
            InputPutPtr &= (INPUT_BUFSIZE-1);

            if (BufRoom < INPUT_BUFSIZE - INPUT_HIGHWATER)
                if ((NeedXOff = !XOffSent) != 0)
                    Ctrl |= SysSprt.TxCtlMask;
            break;
    }

    if (((Ctrl & SysSprt.TxCtlMask) != 0) &&
         ((Status & SysSprt.TxStatMask) != 0))
        if (NeedXOff)
        {
            OutToPCBReg( SysSprt.TxAddr, ASCII_XOFF);
            NeedXOff = FALSE;
            XOffSent = TRUE;
        }
        else if (XOffSent && (BufRoom > INPUT_BUFSIZE - INPUT_LOWWATER))
        {
            OutToPCBReg( SysSprt.TxAddr, ASCII_XON );
            XOffSent = FALSE;
        }
        else if ((OutputPutPtr != OutputGetPtr) && !ReceivedXOff)
        {
            OutToPCBReg( SysSprt.TxAddr, OutputBuffer[OutputGetPtr++] );
            OutputGetPtr &= (OUTPUT_BUFSIZE-1);
        }
        else
            Ctrl &= ~SysSprt.TxCtlMask;

    OutToPCBReg(SysSprt.CtlAddr, Ctrl);

    return (Status & SysSprt.BreakStatMask) == 0;
}

/////////////////////////////////////////////////////////////////////////
// InitSPRT() initializes a serial port, optionally enabling interrupts
// so it can be used as the main console I/O port.
//
void OtherInitSPRT(void)
{
    SerPortPtr Sprt, Other;

// Define a thunk which calls IrqSerialEntry, and then either
// returns from the interrupt or jumps to the BreakVector, depending
// on the return value from IrqSerialEntry.

#pragma pack(1)
static struct {
    BYTE PushA;
    BYTE PushDS;
    BYTE PushES;
    BYTE PushCS;
    BYTE PopDS1;
    BYTE FarCall;
    LPVOID CallTarget;
    BYTE OrAxAx[2];

    BYTE PopES;
    BYTE PopDS2;
    BYTE PopA;
    BYTE Jz[2];
    BYTE IRet;
    BYTE FarJmp;
    LPVOID JmpTarget;
               } Thunk_IrqSerialEntry= 
        {0x60,0x1E,0x06,0x0E,0x1F,0x9A, IrqSerialEntry, {0x09,0xC0},
          0x07,0x1F,0x61,{0x74,0x01}, 0xCF, 0xEA, 0};
#pragma pack()



    // Figure out which port is the default port, and take the other one.

    Sprt = ES_Serial;
    Other = ES_Serial+1;

    if ((_inpw(Sprt->IMaskAddr) & (Sprt->IntsDisabled & ~Sprt->IntsEnabled)) == 0)
    {
      Sprt = ES_Serial+1;
      Other = ES_Serial;
    }

    // Copy the data into SysSprt for faster access during interrupts

    if ((_inpw(Sprt->IMaskAddr) & (Sprt->IntsDisabled & ~Sprt->IntsEnabled)) != 0)
        SysSprt = *Sprt;

    // If both ports were already allocated, just return.

    if (SysSprt.CtlAddr == 0)
        return;

    // Mask off the interrupts at the interrupt controller,
    // then disable interrupts at the serial port,
    // then insure that the in-service bit is reset

    OutToPCBReg(SysSprt.CtlAddr, SysSprt.DisabledCtlMask);
    OutToPCBReg( SysSprt.EOIAddr, SysSprt.IntType);     // end-of-interrupt
    OutToPCBReg(SysSprt.IMaskAddr, SysSprt.IntsDisabled);

       // Set the baud rate and clear the input status, and
       // reroute the interrupt vector to the dummy handler

    OutToPCBReg(SysSprt.BaudAddr, (WORD)_inpw(Other->BaudAddr));
    _inpw(SysSprt.RxAddr);
    OutToPCBReg(SysSprt.StatAddr, 0);

    // Clear state of interrupt variables

    InputPutPtr = 0;
    InputGetPtr = 0;
    OutputPutPtr = 0;
    OutputGetPtr = 0;
    XOffSent = TRUE;
    NeedXOff = FALSE;
    UARTStatus = 0;
    ReceivedXOff  = FALSE;

    // Set up our interrupt vector to point to our "thunk"

    *(LPVOID far *)(DWORD)(SysSprt.IntType*4) = &Thunk_IrqSerialEntry;

    // Set up our "FALSE" target state to vector to the
    // monitor break code.

    Thunk_IrqSerialEntry.JmpTarget = BreakVector();

    // Enable interrupts at the interrupt controller
    // and then at the serial port.
    OutToPCBReg(SysSprt.IMaskAddr, SysSprt.IntsEnabled);
    OutToPCBReg(SysSprt.CtlAddr, SysSprt.RxCtlMask);

} // InitSPRT()

/////////////////////////////////////////////////////////////////////////
// SerialIO() is the runtime entry into this module.
//
WORD near OtherSerialIO(SerialAction action, WORD handle)
{
    BOOL Done;
    WORD result;
    WORD NextPtr;

    if (handle != 0)
        return 0xFFFF;

    switch (action >> 8)
    {
        case SerialPutch >> 8:

// putch() buffers a character for transmission out the serial port
//     The main complication is that it allows the monitor to reenter
//     itself on a break.  Otherwise, NextPtr could be initialized
//     once per routine, and interrupts would not have to be disabled.
//
            do {
                _disable();
                Done = (NextPtr = (OutputPutPtr+1) & (OUTPUT_BUFSIZE-1))
                                        != OutputGetPtr;
                if (Done)
                {
                    OutputBuffer[OutputPutPtr] = (BYTE)action;
                    OutputPutPtr = NextPtr;
                }
                _enable();
                OutToPCBReg(SysSprt.CtlAddr,
                                 SysSprt.RxCtlMask | SysSprt.TxCtlMask);
            } while (!Done);

            result = 0;
            break;

        case SerialTryToGetch >> 8:

           // The only reason we disable interrupts is because this
           // routine could be reentered (if a BREAK is received).
           // Disabling interrupts allows us to make sure that we
           // get the character that we thought we saw in the buffer.

           result = 0xFFFF;

           _disable();
           if (InputGetPtr != InputPutPtr)
           {
               result = InputBuffer[InputGetPtr];
               InputGetPtr = (InputGetPtr+1) & (INPUT_BUFSIZE-1);
           }
           _enable();

           if (XOffSent && 
                (((InputGetPtr - InputPutPtr-1) & (INPUT_BUFSIZE-1))
                           > INPUT_BUFSIZE - INPUT_LOWWATER))
                OutToPCBReg(SysSprt.CtlAddr,
                         SysSprt.RxCtlMask | SysSprt.TxCtlMask);
            break;

        case SerialFlushOutput >> 8:
            while ((OutputPutPtr != OutputGetPtr) ||
                 ((_inpw(SysSprt.CtlAddr) & SysSprt.TxCtlMask) != 0))
            {
            }
            break;

        case SerialFlushInput >> 8:
            InputGetPtr = InputPutPtr;
            break;

        case SerialKbhit >> 8:
            result = (WORD)(InputGetPtr != InputPutPtr);
            break;

        default:
            result = 0xFFFF;
            break;
    }

    return result;
}
