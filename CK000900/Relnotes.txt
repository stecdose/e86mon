

                E 8 6 M O N   R E L E A S E   N O T E S
------------------------------------------------------------------------------

Current Bugs Reported:
-----------------------------


------------------------------------------------------------------------------

E86MON Revision history:

3.40 
----------------------------
 - Support for starting monitor in SRAM if available is added for ER.
 - Support for 186CC for booting from 8 bit flash such as that on
	the Test Interface Port (TIP) and updating a 16 bit flash
	on the demo board was added.
 - A bug where modem at commands set to the monitor were interpretted
	as E86MON command line arguments was fixed.
 - The extension help file was modified to include help for 'LL' and ';'
	commands.
 - E86MON is now too large to run on the ER, so a stripped down version of
	E86MON has been compiled to run on ER boards.  These files have
	E_ER in their names.

3.22 beta 2 (INTERNAL RELEASE ONLY)
----------------------------
  - Made common error message handling routines, removing a lot of
    redundancy.
        - Used the 128 byte buffer allocated for new version of cmdmem
        - Reworked printf to be able to use arbitrary primitive putch
        - Error messages now stored into buffer where they are generated,
          printed from buffer when it's time to report the error.
  - Soon we'll be able to use GetMem/SetMem for cmdfile.c and permvars.c
  - SC400.TXT added for porting notes for that platform


3.30
----------------------------

Major upgrade.  Entire monitor changed.  See readme.330 for details.
  

3.21 (Shipped on second production of ED demo boards)
----------------------------

  - Fixed cosmetic error with permanent variable update message.
  - Added copyright message.
  - Enhanced 'I' command reporting.
  - Added 'G' option to 'L' command.
  - Added ReFlashulator(tm) support to Z command.
  - Added 'monitorport' permanent variable to set default serial port
  - Continued code reorganization to better support future ports of E86mon.
  - Made update error messages more meaningful
  - Updated Paradigm sample configuration file.
  - Updated EDITMON.EXE and permanent variable code to make 0 be
    a place-holder variable number
  - Updated README.TXT to show new commands and permanent variables
  - Added RELNOTES.TXT to release
  

3.20  (Distributed at specialist conference)
-----------------------------
  - Fixed serial port driver to minimize EM serial port erratum.
  - Changed default PIO configuration on ED to disable hardware handshake
    behavior on serial port 1.
  - Fixed an error with the ':' command with RAM located hex files.
  - Fixed error where blank lines in a hex file caused infinite loop.
  - Added support for Net186 (configures chip select and led pios)
  - Fixed problem with monitor forgetting that an application has been
    downloaded -  complains no program loaded.
  - Fixed 3.16 bug that caused the dhrystone example to fail on ER
  - Modified the 'W' command, and added the 'L' command to work with
    it.  Now works on ER as well as on other parts.
  - Added 'autorun' permanent variable to run loaded .EXEs at startup
  - Reorganized code to better support future ports of E86mon.
  - Fixed a bug in the 'T' trace command.
  - Fixed compiler switches for SS != DS, which might fix several bugs.
  - Did not update README.TXT


3.16 -- ESC East internal tradeshow release
-----------------------------
  - Enhanced the MAKEHEX.EXE utility to handle arbitrarily large .exe files.
  - Added the 'W' Write .EXE to flash command, for all but ER (RAM constraints)
  - Broke Dhrystone exeution on ER.
  - Monitor sometimes forgets that programs have been loaded.

3.15 -- First stable ED-enhanced release
-----------------------------
  -Fixed a bug with running the RAM version of the monitor on ER parts.
  -Fixed a bug in the software flow control where an XOFF was not being sent.
  
3.14
-----------------------------
  -Fixed a bug introduced with 3.13 which broke monitor operation on EM parts.
  -Added 186ED support.

3.13
-----------------------------
  -Permanant variables REFRESH and WAITSTATES were added for ED support.
  -Undocumented LED value of 54321 was added to support internal verif boards.
  -Monitor will boot from DRAM or SRAM (in case no DRAM) with 186ED
  -A few corrections and additions were made to the am186er.inc file.  
  -The leds.cfg file was added (rom demo example), it was previously omitted. 
  -testmon.c was added as an example of DOS (int 21) calls.
  -GetInputLine was simplified and printf.c optimization was removed.
  -Broke monitor operation on EM parts

3.11 (Shipped on ER demo disk & HHKeychain FLASH)
-----------------------------
  -Bug with sending Xoff without sending a Xon was fixed.
  -However, XOffs would not be sent unless transmitter was already on


3.10 (Shipped on ER flashes)
-----------------------------
  -First 3.xx external release
  -If an Xoff was ever sent, no Xon would ever follow


------------------------------------------------------------------------------

Potential future E86MON enhancement:
-----------------------------
 - Int 1A handler for Borland C (debugging option???)
 - FPU handler to no-op fpu instructions
 - Enhance the monitor to be be FLASH banking aware.
 - Standardize Am186 header and include files to provide common 
   naming across verification, sysapps, and customer support.

 - Add call-ins/call-outs to be able to add additional functionality
   in separate modules easily.  Call-ins include:
       Lock down monitor to RAM
       Program/erase flash (except monitor boot area)
       printf
       Start up loaded relocatable program from flash
       TSR equivalent -- make E86MON give up space
   Call-outs include:
       Ability to add commands
       Ability to redirect character I/O
   Call-ins/call-outs are to be added via int 2Dh

 - Add DOS timer tick support
 - Add keyboard line editing commands (arrow keys)
 - Add assembly 'A' and disassembly 'U' command functionality
 - Add support for SC400/SC410 

