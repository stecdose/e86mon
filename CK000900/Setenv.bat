@echo off
@rem
@rem  Customize this file to point to your MSVC and MASM directories.
@rem
@rem  These directories are used from the viewpoint of the subdirectories
@rem  under AMDLPD
@rem
@set amd_lpd_cl=C:\projects\mstools\
@set amd_lpd_ml=C:\projects\mstools\
@set include=C:\projects\mstools\
@set lib=C:\projects\mstools\
@rem
@set amd_lpd_cl=C:\msvc\bin\
@set amd_lpd_ml=C:\masm611\bin\
@set include=C:\msvc\include\
@set lib=C:\msvc\lib\
