
EMON 3.40 (EMON) README.TXT file

Sections you can search for in this file:

WELCOME                         -- Hi, there, y'all, from AMD EPD in Texas
INSTALLING                      -- Unzipping EMON340.ZIP to your system
QUICK START                     -- For the terminally anxious
DETAILED STARTUP INFORMATION    -- For the terminally cautious
EMON SYNTAX                    -- These are in the manual supplied in PDF format
EMON COMMANDS                  -- These are in the manual supplied in PDF format
EMON EXTENSIONS                 -- Linked to EMON to provide DOS Emulation
DOWNLOADING HEX FILES           -- Into ROM or RAM
DOWNLOADING EXE FILES           -- Convert them first using MAKEHEX
DOS EMULATION ENVIRONMENT       -- Will my program work?
UPGRADING PREVIOUS VERSIONS     -- To EMON version 3.40
UTILITIES INCLUDED WITH EMON    -- MakeHex and EditMon
TERMINAL PROGRAMS               -- Information about various programs
EMON ERROR MESSAGES             -- It did what I said, not what I meant

---------------------------------------------------------------------------

WELCOME

Welcome to EMON, version 3.40!

EMON is the control program for AMD's line of SD186/188
demonstration boards for the following processors:

Am186EM/Am188EM  -- Enhanced high-performance 186/188 with serial port
                    and general purpose I/O pins.
Am186ES/Am188ES  -- Enhanced high-performance 186/188 with two serial
                    ports and general purpose I/O pins.
Am186ED/Am188ED  -- Enhanced high-performance 186/188 with two serial
                    ports, a DRAM controller, and general purpose I/O pins.
Am186CC		 -- Enhanced high-performance 186 with two serial
                    ports, a DRAM controller, and general purpose I/O pins.

WARNING!!!!!!!!!!!
This particular version of EMON is larger than 32k and will not work on 
the Am186ER/Am188ER because of the 32k or SRAM available on these processors.

EMON is a basic monitor.  It supports download of executable
images to ROM or RAM, and rudimentary debugging.

For serious development, AMD recommends searching our Fusion
Catalog and/or Fusion CD for compilers, linker/locators,
real-time operating systems, complete DOS emulation, source
level debuggers, and other powerful tools that are provided by
our Fusion Partners.

For developers just getting started, EMON running on
an SD186/SD188 demonstration board provides a powerful tool to
allow quick prototyping and benchmarking of simple algorithms,
before a major investment is made in x86 development tools. 
The minimal DOS emulator allows the developer to download and
run small .EXE files, that were developed and tested using
standard compilers on a PC running DOS. Full source is
provided as a teaching and demonstration aid.

---------------------------------------------------------------------------

INSTALLING

Create a directory to install EMON, give it a name such as
EMON340.  Use Winzip or PKzip to unzip the EMON340.ZIP file
into the directory you created.  When done, you should have a directory
structure created at the location you unzipped to that includes 
directories such as OUT, EMON,...  The EMON hex files that you can
download to your board if desired are located in the OUT directory.

Note, that the version of EMON on your demo board may be less than
3.40, please see the relnotes text file to see the errata fixes and
improvements implemented in EMON 3.40. You may want to upgrade.

---------------------------------------------------------------------------

QUICK START FOR EMON VERSION 3

NOTE!  THIS DESCRIPTION APPLIES TO BOARDS OPERATING AT FACTORY
       DEFAULT SETTINGS.  SEE "DETAILED STARTUP INFORMATION"
       BELOW IF THIS PROCEDURE DOES NOT WORK FOR YOU.

Set up your PC's terminal program for 19200 bits/second, at 8
bits per character, with no parity and one stop bit.  Set the
terminal program's flow control to X-On/X-Off.  Connect the
supplied serial cable from the PC to the demonstration board.

When the SD186/188 board is reset, the LEDs will flash, one at a
time, in an oscillating pattern.  This first pattern will last for
three seconds (two full up/down sweeps), at which time EMON will
display its signon screen to the terminal and change the LED
pattern.  Previous versions simply stop updating the LED display;
version 3.11 and above change the display to one of 8 alternating
patterns which show the monitor is waiting for keyboard input.

At this point, you can press '?' followed by <ENTER> for
EMON's help screen.

If the version number shown at the top of the help screen is
less than 3.40, we strongly recommend you upgrade your monitor
using the files supplied on this diskette so that the features
and operation of your monitor match the descriptions in
this README file.  The section titled "UPGRADING PREVIOUS
VERSIONS" gives full information on this process.

---------------------------------------------------------------------------

DETAILED STARTUP INFORMATION

(EMON versions prior to 3.0 required you to press BREAK, rather
than an "a".  If you are running such a version, see the section
titled "UPGRADING PREVIOUS VERSIONS".)

When reset is pressed or power is supplied, the initial oscillating
LED pattern indicates that EMON is waiting for an "a" character
to be received from the terminal.  If it receives an "a", it automatically adjusts to the baud rate of the "a" and display the
EMON welcome message and prompt.  If it receives any character
other than an "a" it restarts the three-second LED oscillation,
and let the user try again to press an "a".

If you do not press an "a" during the initial oscillating
LED pattern (nominally three seconds), EMON's next action depends
on whether you have installed a startup program or any
extensions in the ROM ornot. If the startup vector at 
F7FF:0000 has been filled in with a far jump, then EMON 
uses the startup vector to jump to the
your program.  If the startup vector has not been filled in, and
you have used the "W" command to store a DOS .EXE program in
the Flash memory and the "P autorun" command to mark it for running at
startup time, then that DOS program will be executed.  Otherwise,
EMON displays the welcome message and prompt, but must assume
the baud rate.  If the baud rate does not match that of the
terminal, you will see nothing or garbled characters.  (See
the "DOWNLOADING HEX FILES" and "DOWNLOADING EXE FILES" sections for
information about installing user programs.)

At the factory, the baud rate is set to 19200.  You can change
this default using the "P" command.  See the EMON COMMANDS
section in this file for details.

The automatic baud rate detection is very useful in the following
circumstances:

  -- If a user program is installed, but you wish to invoke
     the monitor instead.
  -- If the programmed baud rate does not match the terminal baud
     rate.
  -- If the programmed CPU speed does not match the actual CPU speed.
     (The bit clock is divided down from the CPU clock.)
  -- If you don't want to wait 3 seconds for the monitor to
     boot.

The automatic baud rate detection is designed to detect baud rates
from 2400 to 115200, but how well it works depends on the CPU type
and speed.  On 188 boards, it does not reliably detect 115200,
so if you wish to use this baud rate on a 188 board, you should
use the "P" command to set the baud rate.  The algorithm
may also fail at higher baud rates if you run the CPU at slower
frequencies than the default 40MHz.

---------------------------------------------------------------------------

E86MON EXTENSIONS

To provide DOS emulation, extensions need to be linked into EMON by
using the 'LL' command from the command line or by not pressing 'a'
to autobaud when the monitor starts (it finds the extension in
Flash memory, and loads it automatically, and assumes a baudrate based on what
is set in the permanent variable pool).  The extensions reside in Flash
memory at location E000:0000 (default) and do not run out of RAM.  The 
extensions, when loaded, allow users to run code that uses common
INT21h calls, which are described in the DOS EMULATION SUPPORT
portion of this readme.  This extension is named extemon.hex on the
floppy supplied in this kit.

---------------------------------------------------------------------------

DOWNLOADING HEX FILES

There are four example hex files you can download in the \OUT
subdirectory on the floppy:

    AMDDHRY.HEX    -- A RAM-based DHRYSTONE benchmark
    SECONDS.HEX    -- A RAM-based seconds counter
    TESTMON.HEX    -- A RAM-based minimal DOS emulator test
    LEDS.HEX       -- A ROM-based demonstration

Source for these files is under the \SAMPLES subdirectory.

EMON supports the downloading of Intel extended hex files
into RAM or Flash ROM.  

A file being downloaded to RAM for execution should be
located between 410h and the start of the monitor data at the
end of the RAM; a file being downloaded to ROM for
execution should be located between the start of the ROM and
F8000h.  The monitor "I" command shows the size and
location of the free RAM and also shows information about the size
and location of the flash ROM.

It is impermissible for the file to have some sections download
to RAM and others download to ROM because EMON relocates
itself to RAM (destroying any loaded RAM program) to
burn the Flash memory when it is downloading to ROM.  EMON
reports a range error on the download of such a file.

If you are downloading into ROM, you should first make sure
the target download area is empty by using the "X" command to
erase the Flash memory sectors.  Unless you are storing multiple
programs into Flash memory, the easiest way to do this is to use
"XA" to erase all the application sectors.

There is no specific command to download hex files, simply
start transferring with your terminal program in ASCII or
Raw ASCII mode.  EMON echos the first record as
it receives it, but when it parses it and determines that it
is a hex file record, it switches into a file transfer
mode.  The type 1 EOF record at the end of the file
switches back to command mode.

If an error is encountered during the download, an error
message is printed, and EMON stays in download
mode until it receives an Escape character (1Bh), at which
time it prints a more detailed error message and then
returns to command mode.

If you are downloading to Flash memory, and the hex file contains
a type 3 start address record, and the application code
vector at F7FF0 has not been programmed, then EMON
automatically programs a far jump to the start address into
this vector.  On subsequent reset sequences, if you do
not press "a" in the first three seconds to force entry to
EMON, EMON notices that there is a valid far jump at
F7FF0, and vectors there to automatically start up
your application program.

---------------------------------------------------------------------------

DOWNLOADING EXE FILES

EMON can download and run DOS executable files (after being
converted to hex files using makehex), enabling customers
to use affordable, readily available, and familiar PC-based compilers
and assemblers to develop initial test and benchmarking code.  EMON
provides a minimal subset of DOS int 21h functionality (when 
the dos extension is loaded), which is fully described 
in the next section, "DOS EMULATION ENVIRONMENT".
Most compilers are capable of generating .EXE files which work
within this environment, as long as do not use library
functions that require file-based I/O.

Unlike some prior versions, EMON version 3 does not support direct
downloading of .EXE files.  Instead, it supports AMD EPD extensions
to the Intel hex file format, and a supplied conversion program 
converts .EXE files into this extended hex file format.  There are
several reasons for this change:
     (1) Unlike hex files, exe files do not have error checking
     (2) Some terminal programs, e.g. HyperTerm which comes with
         Windows 95, will not transmit binary data unchanged.
     (3) The added overhead of transferring a hex file is mitigated
         by the fact that EMON version 3 allows baud rates up
         to 115200.
     (4) The relocatable hex file can be stored to Flash memory (using
         the "W" command) and later moved to RAM and executed
         (using the "L" command).

To convert your EXE file into a HEX file, use the MAKEHEX utility
supplied on this diskette in the OUT subdirectory.  For example,
to convert EXEC.EXE into EXEC.HEX, simply type MAKEHEX EXEC
(assuming MAKEHEX.EXE is in your path).

When you have converted your EXE file, simply download it to
EMON as described in the previous section.  When it is
downloaded, you can set parameters for the program (if it
expects a command line) with the "N" command, and then
start execution with the "G" command.

Alternatively, use the "W" command before you start downloading
the file, to program it into Flash memory.  Since Flash
memory is nonvolatile, the program can then be run multiple times,
even after power has been cycled.

---------------------------------------------------------------------------

DOS EMULATION ENVIRONMENT

EMON supplies a minimal DOS emulation environment for running
converted EXE files (when the DOS emulation extension is loaded).
When an AMD-extended hex file (see previous
section) is downloaded, a Program Segment Prefix (PSP) is generated 
for it with most of the fields which programs examine filled in, and 
the program is relocated into RAM.  One difference from normal DOS
environments is that the default near heap is very small, especially on
186ER demonstration boards.  Use the far heap if this is a problem (e.g, _fmalloc).

EMON also supports DOS emulation for ROM images.  When a ROM
image makes its first int 21h call, EMON enables
interrupts for the serial port and timer (which would not have
been enabled if EMON jumped straight to the application
vector at reset), and is then ready to provide services.

The following DOS interrupts are supported:

Int 20h              -- Terminate program execution
Int 21h, service 00h -- Terminate program execution
Int 21h, service 01h -- Character input with echo
Int 21h, service 02h -- Character output
Int 21h, service 03h -- Aux input (reads LED code)
Int 21h, service 04h -- Aux output (writes LED code)
Int 21h, service 06h -- Direct console I/O
Int 21h, service 07h -- Raw input
Int 21h, service 08h -- Raw input
Int 21h, service 09h -- Display string terminated with '$' 
Int 21h, service 0Ah -- Buffered console input
Int 21h, service 0Bh -- Check Keyboard Status
Int 21h, service 0Ch -- Flush and execute keyboard function
Int 21h, service 25h -- Set interrupt vector
Int 21h, service 2Ah -- Get date (returns dummy date)
Int 21h, service 2Ch -- Get time
Int 21h, service 2Dh -- Set time
Int 21h, service 30h -- Get version (returns 2.10)
Int 21h, service 35h -- Get interrupt vector
Int 21h, service 3Fh -- File read (only supported on console)
Int 21h, service 40h -- File write (only supported on console)
Int 21h, service 44h -- Get device information (only supported on console)
                           Subfunction 0 -- Get device data
Int 21h, service 48h -- Allocate memory
Int 21h, service 49h -- Free memory
Int 21h, service 4Ah -- Resize memory
Int 21h, service 4Ch -- Terminate process with return code

If an int 21h call is not supported, EMON is entered when
the int 21h is invoked.  At this point, an examination of the
registers quickly determines which code it is, and in many
cases, simply using the "G" command to resume execution will
suffice.

---------------------------------------------------------------------------

UPGRADING PREVIOUS VERSIONS

Files used to upgrade EMON are in the \OUT directory on the floppy:

    EMON340U.HEX  -- is the main upgrade file
    EMON340R.HEX  -- is a RAM-based version of the monitor
    EMON340B.HEX  -- is a "bootable" (suitable for programming with
                     a PROM programmer) version of the monitor

The exact commands used to upgrade EMON depend on which version you
are currently running.  Use the "H" or "?" command to determine
this.

Upgrading version 3.32 or later:

(1) Use the 'XA' command to erase the application Flash memory sectors.
(2) Download EMON340U.HEX, the upgrade file, to the board.  It
    is not necessary to type any command to do this -- the new
    EMON automatically recognizes a file download when it sees
    the colon which starts the file.
(3) Use the 'G' command to go to the new monitor, which is
    running out of user Flash ROM space.  This will automatically
    go to the correct address.
(4) Press 'a' to establish communication with the new monitor.
    You are now running out of the application ROM based copy
    of the monitor.
(5) Type 'Z' <enter> to initiate the upgrade.  You will be
    asked if this is really what you want to do.  Answer 'Y'
    to perform the upgrade, but do not do this if your power
    is not stable, or if little children are near the reset
    button.  If the upgrade is aborted before it finishes,
    you may need to send your board back to AMD to have the
    Flash memory reprogrammed.
(6) Your monitor is now upgraded, but you are still running
    out of the application ROM copy of the monitor.  To run
    out of the new boot copy of the monitor, either press
    the reset button, or type G FFFF0 to go to the reset vector,
    then press 'a' within three seconds to establish communication
    with the boot copy of the monitor.
(7) You can now use the 'XA' command to remove the application
    copy of the monitor, and then download any desired hex
    file to application ROM.

---------------------------------------------------------------------------

UTILITIES INCLUDED WITH EMON

Two DOS utility programs are included with EMON, both in C source
form and as .EXE files.

MAKEHEX is a program that takes a DOS .EXE, .COM, or .BIN file,
and creates a .HEX file suitable for downloading to EMON.  There
are two ways to use MAKEHEX:

  MAKEHEX FOO   -- This takes FOO.COM, FOO.BIN, or FOO.EXE,
                   and creates a RELOCATABLE hex file.  EMON
                   loads this file straight to RAM, relocates
                   it, and prepares it for running.  Or the "W" command
                   accepts this file and stores it to Flash memory for
                   subsequent copy to RAM.

  MAKEHEX FOO <segment>  -- If a segment is given, the file should
                   not have any relocation items in it.  MAKEHEX
                   creates a HEX file that is located at the
                   given segment.  This option is used to build
                   EMON itself.

EDITMON is a program that allows editing of the EMON permanent
variables inside the .EXE file before the monitor is converted
using MAKEHEX.  This is useful for generating a file that
is going to be burned using a PROM programmer (e.g. EMON320B.HEX).

The make of the monitor for the Hamilton-Hallmark Amber reference
design uses EDITMON in order to set the CPU frequency and 
to set the HHKEY permanent variable.

---------------------------------------------------------------------------

TERMINAL PROGRAMS  (Miscellaneous information)

Most Windows programs send a break by using Ctrl-Break.

DOS PROCOMM PLUS sends a break by using Alt-B.

The HyperTerm program that comes with Windows 95 works with
EMON, but other commercially-available programs work better.

HyperTerm has the following problems:

    If the UART FIFO is enabled, priority is given
    to receive, making it difficult to abort a long dump done
    in error.  Shutting off the UART FIFO helps this dramatically.

    The only way to abort a file transmission seems to be to
    Disconnect and Reconnect.  Unfortunately, when you do this,
    HyperTerm forgets it has the file open, and you have
    to exit and restart HyperTerm to close the file.

    For transmitting HEX files, the baud rate seems to be
    immaterial to HyperTerm.  Unless your computer is exceptionally
    fast, transfers at 115200 bps operate no faster than at 19200.

    HyperTerm also cannot send a raw ASCII file without either
    stripping or adding linefeeds, but this limitation does not
    affect the current version of EMON.

---------------------------------------------------------------------------

EMON ERROR MESSAGES

"Cannot update monitor -- new hardware init code too large"
    The size of the new hardware initialization area
    (in EMON_186.ASM) is larger than 224 bytes.

"Cannot update monitor -- new monitor too large"
    The new monitor will not fit into the space provided.

"Cannot update monitor -- must update from app flash area"
    The new monitor is not executing from the correct
    location (F0000).  Use MAKEHEX to reload at correct location.

"Cannot update monitor -- app boot vector must point to new monitor"
    The application boot pointer does not point to the new
    monitor.  Use the XA command to clear the application boot
    vector, and download the new monitor again to cause the
    application boot vector to be filled in properly.


"Cannot identify device"
"Unexpected Sentinel"
"Cannot modify flash while executing from it"
    These messages should not be seen during normal operation.

"Erase operation failed"
"Verify operation failed"
"Program operation failed"
    Your Flash memory device may require replacement.

"Byte already programmed (erase first)"
    Use the "X" command to clear the Flash memory before programming.

"Invalid sector number"
    Use the "I" command to determine the number of sectors in
    the device.  The XA command will only erase application
    sectors.

"Cannot erase the area you are running from!"
    You usually encounter this command if you have loaded
    a copy of EMON into the application area and are running
    from it.  Reset or type G FFFF0, and press "a" within three
    seconds to execute from the monitor in the boot location.

".EXE file does not fit in memory"
    This message is easy to get on the ER demo boards.  Try
    to make your program smaller.

"Record does not start with colon at file line number x"
"Invalid checksum at file line number x"
"Non-hex character in record at file line number x"
"Invalid EOF record at file line number x"
"Invalid Segment record at file line number x"
"Invalid start location record at file line number x"
"Unknown record type at file line number x"
"Record Length Incorrect at file line number x"
    These messages indicate a problem with your .HEX file,
    or that line noise was encountered during the download.
    Some processor variants, especially early EM systems,
    are susceptible to minute baud rate variations between
    the demo board and the terminal.  If you suspect this
    is the case, set your terminal program for two stop bits.

"Program region out of range at file line number x"
    This message indicates that the HEX file does not lie
    completely within the application RAM region or completely
    within the application ROM region.

"User aborted download at file line number x"
    An ASCII control character (e.g. escape) was received
    during the download.

"Note -- the flash operation used (overwrote) the RAM."
    This is simply an informational message indicating that
    the contents of the RAM may no longer be what you expect.

"No program loaded"
    The default CS:IP that are set up when EMON is entered,
    and whenever a program terminates, point to code which will
    produce this message when executed.

"You must be running from the boot monitor to update permanent variables."
    You are running from a RAM copy or application ROM copy of the monitor.
    Press reset, or type G FFFF0, and press "a" within three seconds to run
    from the boot monitor.

"The current value of 'permvar' does not match its boot value."
    You tested a permanent variable change, and then tried to update
    the boot monitor.  Set all permanent variables the way you want
    them (from the boot monitor) before running an updating monitor
    from the application ROM space.

"The default value of 'permvar' cannot be updated."
    You updated the boot monitor, ran it, changed a permanent variable,
    and are now trying to update it again.  Use the "XA" command and
    then download the new monitor again before you update the boot
    monitor.

"WRITE command requires relocatable file"
    After entering the "W" command, you downloaded a hex file which was
    not created from a .EXE using the supplied MAKEHEX utility.

"Cannot load from flash"
    The parameter you gave to the "L" command did not match any
    image loaded in the Flash memory.

