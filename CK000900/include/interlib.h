/******************************************************************************
 *                                                                            *
 *     INTERLIB.H                                                             *
 *                                                                            *
 *     This file contains definitions which are used by the ROM library       *
 *     dispatcher.  Internal and external libraries and programs can          *
 *     call these functions by linking with EMONLINK.LIB.                     *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#ifndef _interlib_h_
#define _interlib_h_

#include "emondata.h"

//////////////////////////////////////////////////////////////////////////////
//    DispatchResult is what a dispatch routine in a library extension
//    may return for execution.
//
typedef enum {
          Dispatch_NotExclusive     = -1,
          Dispatch_IDidIt           =  0,
          Dispatch_ReturnToDebuggee =  1
             } DispatchResult;

//////////////////////////////////////////////////////////////////////////////
//    DispatchOp is an enumeration which is passed to the chain
//    of dispatch routines.  Most of these directly correspond to
//    routines declared in ELIBFUNC.H.  Exceptions are noted below
//
//    Enumeration values up to 1000 are reserved by E86Mon.  Others
//    may be used by extensions.
//
//      PLEASE DO NOT ALTER THE ORDER IN THIS ENUMERATION!!!
//
typedef enum {

          // The Install message is internally generated in the extension
          // (by EMONLIBC.C) and is sent ONE TIME ONLY, when RAM for
          // the data segment is first allocated.  This message is
          // sent WITH INTERRUPTS DISABLED.  No I/O is allowed!

          Dispatch_Install,

          // The Restart message is sent whenever E86MON reinitializes
          // itself.  EmonData->RestartCode can be examined to determine
          // why the reinitialization is occurring.  Dispatch routines
          // should all return Dispatch_NotExclusive.

          Dispatch_Restart,

          // ExecCmd messages are sent to give each extension an
          // opportunity to process the command.  A module which
          // processes the command line should return either
          // Dispatch_IDidIt or Dispatch_ReturnToDebuggee.  Other
          // modules should return Dispatch_NotExclusive.

          Dispatch_ExecCmd,

          // If nobody processes the command, a GetError message is
          // sent to let the correct module report an error.  An error
          // may be reported by outputting a string, or using PrintError.
          // A module wishing to report an error should return Dispatch_
          // IDidIt.

          Dispatch_GetError,

          // The Information message is used to let external
          // extensions attach information at the END of the 'i'
          // command information dump.

          Dispatch_Information,

          // The LoadingFromROM message is used to let an external
          // debugger module know where to load symbols from.  The external
          // module can determine the linear address of the symbol table
          // (if any) from information passed.  MAKEHEX does not yet
          // store symbol information -- the external debugger will
          // require an upgrade to MAKEHEX.

          Dispatch_LoadingFromROM,


          // The rest of these messages correspond to functions
          // listed in ELIBFUNC.H

          Dispatch_InitPSP = 20,
          Dispatch_RAMSafe,
          Dispatch_GetTime,
          Dispatch_PostLEDs,
          Dispatch_InquireLEDs,
          Dispatch_FindItem,

          Dispatch_GetInputLine = 40,
          Dispatch_GetYN,
          Dispatch_Printf = 50,
          Dispatch_PrintError,

          Dispatch_ScanDelimiters = 60,
          Dispatch_ScanOption,
          Dispatch_ScanEOL,
          Dispatch_ScanString,
          Dispatch_ScanHexDigits,
          Dispatch_ScanByte,
          Dispatch_ScanWord,
          Dispatch_ScanDWord,
          Dispatch_ScanDecimal,
          Dispatch_ScanAddress,
          Dispatch_ScanRange,
          Dispatch_ScanList,
          Dispatch_ScanError,
             } DispatchOp;

//////////////////////////////////////////////////////////////////////////////
//    DispatchStruct contains information passed to the dispatch
//    routine for each kind of dispatch function.  Note that this
//    very simply corresponds to the parameter lists in ELIBFUNC.C
//
typedef struct {
        DispatchOp op;
        union {
            struct {
                LPCSTR     Prompt;
                BOOL       LeadingCRLF;
                    } GetYN;
            struct {
                LPCSTR     Prompt;
                BOOL       echo;
                    } GetInputLine;
            struct {
                LPVOID     FormatVector;
                   } Printf;

            struct {
                WORD Which;
                   } ScanDelimiters;

            struct {
                WORD Delimiters;
                BYTE name;
                   } ScanOption;
            struct {
                BOOL ErrorIfNot;
                   } ScanEOL;
            struct {
                LPSTR  s;
                WORD len;
                   } ScanString;
            struct {
                LPDWORD result;
                WORD base;
                   } ScanHexDigits;
            struct {
                LPBYTE b;
                   } ScanByte;
            struct {
                LPWORD w;
                   } ScanWord;
            struct {
                LPDWORD r;
                   } ScanDWord;
            struct {
                LPDWORD r;
                   } ScanDecimal;
            struct {
                WORD Flags;
                FPANYADDRESS clientAddress;
                   } ScanAddress;
            struct {
                WORD Flags;
                FPANYADDRESS a;
                LPDWORD l;
                   } ScanRange;
            struct {
                LPWORD NumBytes;
                BOOL EmptyOK;
                   } ScanList;
            struct {
                LPCSTR Cause;
                   } ScanError;
            struct {
                LPDWORD Milliseconds;
                   } GetTime;
            struct {
                BYTE b;
                   } PostLEDs;
            struct {
                WORD MinProgramParagraphs;
                   } InitPSP;
            struct {
                DWORD LinSourceAddress;
                   } LoadingFromROM;
            struct {
                LPCSTR Module;
                LPCSTR Item;
                LPVOID far * ItemLoc;
                   } FindItem;
               };
               } DispatchStruct, far * LPDispatchStruct;
            
//////////////////////////////////////////////////////////////////////////////
//    DispatchFunc is an actual dispatch function:
//
typedef DispatchResult (cdecl far * DispatchFunc)(LPDispatchStruct s);

//////////////////////////////////////////////////////////////////////////////
//    DispatchLink is what is used to loop through all the
//    attached dispatch routines:
//
typedef struct DispatchLinkS {
             LPDispatchLink   next;
             DispatchFunc far*func;
             LPCSTR           name;
             WORD             priority;
             WORD             codelength;
             WORD             codeseg;
             WORD             datalength;
             WORD             dataseg;
               } DispatchLink;

//////////////////////////////////////////////////////////////////////////////
//    RamOp are the operations passed to RamFuncs:
//
typedef enum {
          RAM_INSTALL,           // One-time only initialization
          RAM_START,             // Start running
          RAM_STOP               // Stop running
              } RamOp;

//////////////////////////////////////////////////////////////////////////////
//    RamFuncs MUST reside in RAM (they are called when E86Mon is programming
//    the flash.  Each RamFunc takes a single parameter:
//
typedef void (cdecl far * RamFunc)(RamOp);

//////////////////////////////////////////////////////////////////////////////
//    RamLink is what is used to loop through all the
//    attached dispatch routines:
//
typedef struct RamLinkS {
             LPRamLink   next;
             RamFunc     func;
             WORD        priority;
               } RamLink;

//////////////////////////////////////////////////////////////////////////////
//    dispatch() -- Tries to call an external routine to perform a function
//
#define dispatch CAT(CODE_LOC,dispatch)
#define dispatch_hdr(h) \
DispatchResult cdecl PDEF(h,dispatch) (DispatchOp op,...)
HDR(dispatch)

//////////////////////////////////////////////////////////////////////////////
//    Extern definitions.  These _MUST_ be provided by extension
//    programs which link with the library.  The LibName should be
//    < 10 characters, which uniquely identifies the library.  (It is
//    printed by the 'i' command.  The LibPriority should be a number
//    < 0xFFFF, which defines where in the dispatch chain the library
//    should be linked in.  The usual number is 0x8000.  Use a larger
//    number if you want to get control last, a smaller number if you
//    want to get control first.

extern char near pascal LibName[];
extern WORD near pascal LibPriority;


/////////////////////////////////////////////////////////////////////////////
//  This data segment is needed by extension functions
//  which set up dispatchers.  See below.
//
#pragma data_seg("DISPATCH_FUNCTIONS","DATA")
#pragma data_seg()
#define DispatchSeg _segname("DISPATCH_FUNCTIONS")

/////////////////////////////////////////////////////////////////////////////
//  This macro allows an extension which uses EMONLINK.LIB to easily declare
//  a dispatch function to handle messages.  See EXTCMD.C or DOSEMU.C
//  for examples of this.
//
#define DISPATCH(name)  DispatchResult far cdecl    \
Dispatch##name(LPDispatchStruct dstruc) { \
static DispatchFunc _based(DispatchSeg) LinkMeIn = Dispatch##name; \
switch (dstruc->op)

#define END_DISPATCH     return Dispatch_NotExclusive; }

/////////////////////////////////////////////////////////////////////////////
//  This data segment is needed by extension functions which hook into
//  the RamLink chain.  See below.
//
#pragma data_seg("RAM_LINKS","DATA")
#pragma data_seg()
#define RamSeg _segname("RAM_LINKS")

/////////////////////////////////////////////////////////////////////////////
//  This macro allows a RAM-based extension which uses EMONLINK.LIB
//  to easily declare a dispatch function to handle messages.
//  See DUALCRT.C for an example of this.
//  for examples of this.
//
#define RAMLINK(name,priority)  \
void cdecl far RAMLINK##name(RamOp request) {\
static RamLink _based(RamSeg) LinkMeIn = {0, RAMLINK##name, priority}; \
switch (request)

#define END_RAMLINK     }

#endif
