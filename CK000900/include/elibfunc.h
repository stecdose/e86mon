/******************************************************************************
 *                                                                            *
 *     ELIBFUNC.H                                                             *
 *                                                                            *
 *     This file contains definitions for standard e86mon library functions.  *
 *     These functions can be called from within the monitor, or from         *
 *     within monitor extensions or DOS-style application programs.           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1997 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#ifndef _elibfunc_h_
#define _elibfunc_h_

/* Get the base types which are used by functions in this module */

#include "types.h"

// This module is used by both E86Mon and its extension libraries.
// Most of the defined functions are prefixed as follows:
//
//  Base_   Base functions are the final dispatch functions within E86Mon
//  Near_   Near functions are called from ROM-based library code, or 
//          DOS-style programs
//  Far_    Far functions are called from DOS-style programs only.
//  Ram_    Ram functions can be called from within RAM-based library code
//
//  #defines are used to make function names default to Near_, Far_ or Ram_
//

#define BASE Base_

// define CODE_LOC appropriately

#ifndef CODE_LOC
#ifdef M_I86LM
#define CODE_LOC Far_
#else
#define CODE_LOC Near_
#endif
#endif

// define the default code segment

#define Near_ 0
#define Ram_ 1
#define Far_ 2

#if (CODE_LOC == Ram_)
#pragma code_seg("RELOCATE_TO_RAM","CODE")
#define CSEG near _based( _segname ("RELOCATE_TO_RAM" ))
#else
#if (CODE_LOC == Far_)
#define CSEG far
#else
#if (CODE_LOC != Near_)
Some error occurred here!
#endif
#define CSEG near _based( _segname ("_CODE" ))
#endif
#endif

#undef Near_
#undef Ram_
#undef Far_

// String helper macros, to concatenate strings passed to other macros

#define SCAT(a,b)    a##b              // Sub-concat
#define CAT(a,b)     SCAT(a,b)         // Concat
#define PDEF(a,b)    CSEG  a##b        // put CSEG in front of name

// The HDR macro helps to build prototype function declarations
// for the Base routine and the ROM or RAM routine.

#define HDR(h)  h##_hdr(BASE); h##_hdr(CODE_LOC);

//
// Flags for scan functions
//
#define  SCAN_SPACES  1              // Use space as a delimiter
#define  SCAN_COMMAS  2              // Use comma as a delimiter
#define  SCAN_EQUALS  4              // Use equals as a delimiter

#define  SCAN_DEFADDR   0x8000       // Default address (segment) allowed
#define  SCAN_1MEG      0x4000       // Accept lower meg addresses only
#define  SCAN_DEFLEN    0x2000       // Default length for range allowed
#define  SCAN_SILENTERR 0x1000       // Silent error (ScanAddress only)
#define  SCAN_RANGELEN  0x0800

//////////////////////////////////////////////////////////////////////////////
//    printf() -- See printf.c for full function definition
//
#define printf CAT(CODE_LOC,printf)
#define printf_hdr(h) \
void cdecl PDEF(h,printf) (LPCSTR format,...)
HDR(printf)

///////////////////////////////////////////////////////////////////////////
// GetCurrentTime() gets the current time in milliseconds
// It is more accurate than simply reading the time value because
// it polls the counter.
//
#define GetCurrentTime CAT(CODE_LOC,GetCurrentTime)
#define GetCurrentTime_hdr(h) \
void pascal PDEF(h,GetCurrentTime) (LPDWORD Milliseconds)
HDR(GetCurrentTime)

///////////////////////////////////////////////////////////////////////////
//    PostLEDs() -- light the LEDs with the supplied values
//
#define PostLEDs CAT(CODE_LOC,PostLEDs)
#define PostLEDs_hdr(h) \
void pascal PDEF(h,PostLEDs) ( BYTE LEDMask )
HDR(PostLEDs)

///////////////////////////////////////////////////////////////////////////
//    InquireLEDs() -- Get the current LED values
//
#define InquireLEDs CAT(CODE_LOC,InquireLEDs)
#define InquireLEDs_hdr(h) \
BYTE pascal PDEF(h,InquireLEDs) ( void )
HDR(InquireLEDs)

//////////////////////////////////////////////////////////////////////////
//   GetYNResponse() - Gets a Y or N response from the user.
//
#define GetYNResponse CAT(CODE_LOC,GetYNResponse)
#define GetYNResponse_hdr(h) \
BOOL pascal PDEF(h,GetYNResponse) (LPCSTR Prompt, BOOL LeadingCRLF)
HDR(GetYNResponse)

//////////////////////////////////////////////////////////////////////////
//    GetInputLine() -- Scan in a line from the user and place it
//                      into InputLine.
//
#define GetInputLine CAT(CODE_LOC,GetInputLine)
#define GetInputLine_hdr(h) \
BOOL pascal PDEF(h,GetInputLine) (LPCSTR Prompt, BOOL echo )
HDR(GetInputLine)

////////////////////////////////////////////////////////////////////////////
//    ScanDelimiters() -- munch on spaces and commas
//
#define ScanDelimiters CAT(CODE_LOC,ScanDelimiters)
#define ScanDelimiters_hdr(h) \
void pascal PDEF(h,ScanDelimiters) (WORD Which)
HDR(ScanDelimiters)

/////////////////////////////////////////////////////////////////////////
// ScanError() indicates to the user an error occurred in scanning
// the input.  It places a caret at the offending line location.
//
#define ScanError CAT(CODE_LOC,ScanError)
#define ScanError_hdr(h) \
BOOL pascal PDEF(h,ScanError) ( LPCSTR CauseMsg )
HDR(ScanError)

////////////////////////////////////////////////////////////////////////////
//    ScanOption() searches for an option character.
//    Pass it flags which define which delimiters to skip over.
//
#define ScanOption CAT(CODE_LOC,ScanOption)
#define ScanOption_hdr(h) \
BOOL pascal PDEF(h,ScanOption) (WORD Delimiters, BYTE name)
HDR(ScanOption)

////////////////////////////////////////////////////////////////////////////
//    ScanEOL() searches for the end of the line
//
#define ScanEOL CAT(CODE_LOC,ScanEOL)
#define ScanEOL_hdr(h) \
BOOL pascal PDEF(h,ScanEOL) (BOOL ErrorIfNot)
HDR(ScanEOL)

////////////////////////////////////////////////////////////////////////////
//    ScanString() assembles a string
//
#define ScanString CAT(CODE_LOC,ScanString)
#define ScanString_hdr(h) \
BOOL pascal PDEF(h,ScanString) (LPSTR  s,WORD len)
HDR(ScanString)

////////////////////////////////////////////////////////////////////////////
//    ScanHexDigits() assembles an arbitrary number of digits into a number
//
#define ScanHexDigits CAT(CODE_LOC,ScanHexDigits)
#define ScanHexDigits_hdr(h) \
WORD pascal PDEF(h,ScanHexDigits) (LPDWORD result,WORD base)
HDR(ScanHexDigits)

////////////////////////////////////////////////////////////////////////////
//    ScanByte() uses ScanHexDigits to retrieve a single byte
//
#define ScanByte CAT(CODE_LOC,ScanByte)
#define ScanByte_hdr(h) \
BOOL pascal PDEF(h,ScanByte) (LPBYTE b)
HDR(ScanByte)

////////////////////////////////////////////////////////////////////////////
//    ScanWord() uses ScanHexDigits to retrieve a word
//
#define ScanWord CAT(CODE_LOC,ScanWord)
#define ScanWord_hdr(h) \
BOOL pascal PDEF(h,ScanWord) (LPWORD w)
HDR(ScanWord)

////////////////////////////////////////////////////////////////////////////
//    ScanDWord() uses ScanHexDigits to retrieve a double word
//
#define ScanDWord CAT(CODE_LOC,ScanDWord)
#define ScanDWord_hdr(h) \
BOOL pascal PDEF(h,ScanDWord) (LPDWORD r)
HDR(ScanDWord)

////////////////////////////////////////////////////////////////////////////
//    ScanDecimal() uses ScanHexDigits to retrieve a decimal number
//
#define ScanDecimal CAT(CODE_LOC,ScanDecimal)
#define ScanDecimal_hdr(h) \
BOOL pascal PDEF(h,ScanDecimal) (LPDWORD r)
HDR(ScanDecimal)

////////////////////////////////////////////////////////////////////////////
//    ScanAddress() scans for an address.
//
#define ScanAddress CAT(CODE_LOC,ScanAddress)
#define ScanAddress_hdr(h) \
BOOL pascal PDEF(h,ScanAddress) (WORD Flags, FPANYADDRESS clientAddress)
HDR(ScanAddress)

////////////////////////////////////////////////////////////////////////////
//    ScanRange() looks for a range in either "address address"
//    or "address L length" format.
//
#define ScanRange CAT(CODE_LOC,ScanRange)
#define ScanRange_hdr(h) \
BOOL pascal PDEF(h,ScanRange) (WORD Flags, FPANYADDRESS a, LPDWORD l)
HDR(ScanRange)

////////////////////////////////////////////////////////////////////////////
//    ScanList() assembles a list consisting of hex numbers and/or
//      quoted strings.
//
#define ScanList CAT(CODE_LOC,ScanList)
#define ScanList_hdr(h) \
BOOL pascal PDEF(h,ScanList) (LPWORD NumBytes,BOOL EmptyOK)
HDR(ScanList)

////////////////////////////////////////////////////////////////////////////
//    ScanSegOffset scans for an address in the lower megabyte and
//    returns it in seg:offset format.
//
#define ScanSegOffset CAT(CODE_LOC,ScanSegOffset)
#define ScanSegOffset_hdr(h) \
BOOL pascal PDEF(h,ScanSegOffset) (LPVOID far * sa)
HDR(ScanSegOffset)

////////////////////////////////////////////////////////////////////////////
//    ScanRangeAddr acquires a range and another address.
//
#define ScanRangeAddress CAT(CODE_LOC,ScanRangeAddress)
#define ScanRangeAddress_hdr(h) \
BOOL pascal PDEF(h,ScanRangeAddress) \
         (WORD Flags, FPANYADDRESS a1, LPDWORD l, FPANYADDRESS a2)
HDR(ScanRangeAddress)

////////////////////////////////////////////////////////////////////////////
//    ScanRangeList acquires a range and a list
//
#define ScanRangeList CAT(CODE_LOC,ScanRangeList)
#define ScanRangeList_hdr(h) \
BOOL pascal PDEF(h,ScanRangeList) \
          (WORD Flags, FPANYADDRESS a1, LPDWORD l, LPWORD NumBytes)
HDR(ScanRangeList)

////////////////////////////////////////////////////////////////////////////
//    InitPSP() initializes a loaded program's program segment prefix
//    and/or simply reformats the DOS memory chain (if MinProgramParagraphs
//    is 0).
//
#define InitPSP CAT(CODE_LOC,InitPSP)
#define InitPSP_hdr(h) \
void pascal PDEF(h,InitPSP) (WORD MinProgramParagraphs)
HDR(InitPSP)

///////////////////////////////////////////////////////////////////////
// RAMSafe() returns the size of "safe" RAM.  If the RAM appears
// to have been trashed by the monitor, this is 0.
//
#define RAMSafe CAT(CODE_LOC,RAMSafe)
#define RAMSafe_hdr(h) \
WORD pascal PDEF(h,RAMSafe) (void)
HDR(RAMSafe)

//////////////////////////////////////////////////////////////////////////
//    SampleInputLine() -- just return the next non-delimiter character
//
#define SampleInputLine CAT(CODE_LOC,SampleInputLine)
#define SampleInputLine_hdr(h) \
BYTE pascal PDEF(h,SampleInputLine) ( BOOL lowercase )
HDR(SampleInputLine)

//////////////////////////////////////////////////////////////////////////
//    ReadInputLine() -- read the next character from input line
//
#define ReadInputLine CAT(CODE_LOC,ReadInputLine)
#define ReadInputLine_hdr(h) \
BYTE pascal PDEF(h,ReadInputLine) ( BOOL lowercase )
HDR(ReadInputLine)

////////////////////////////////////////////////////////////////////////
// tolower() Lowercases a character
//
#define tolower CAT(CODE_LOC,tolower)
#define tolower_hdr(h) \
BYTE pascal PDEF(h,tolower) (BYTE ch)
HDR(tolower)

////////////////////////////////////////////////////////////////////////
// toupper() Uppercases a character
//
#define toupper CAT(CODE_LOC,toupper)
#define toupper_hdr(h) \
BYTE pascal PDEF(h,toupper) (BYTE ch)
HDR(toupper)

////////////////////////////////////////////////////////////////////////
// tonum() Converts a character to a number
//
#define tonum CAT(CODE_LOC,tonum)
#define tonum_hdr(h) \
WORD pascal PDEF(h,tonum) (BYTE ch)
HDR(tonum)

/////////////////////////////////////////////////////////////////////////
// PrintCRLF() prints a CRLF, and then returns
// TRUE  to show no error occurred.
//
#define PrintCRLF CAT(CODE_LOC,PrintCRLF)
#define PrintCRLF_hdr(h) \
BOOL pascal PDEF(h,PrintCRLF) (void)
HDR(PrintCRLF)

/////////////////////////////////////////////////////////////////////////
// PrintError() prints a string to the internal error buffer, if
// we have not already stored one.
//
#define PrintError CAT(CODE_LOC,PrintError)
#define PrintError_hdr(h) \
BOOL cdecl PDEF(h,PrintError) (LPCSTR format, ...)
HDR(PrintError)

/////////////////////////////////////////////////////////////////////////
// assert() saves a string to the internal error buffer,
// if and only if it is an error, and is the first error.
//
#define assert CAT(CODE_LOC,assert)
#define assert_hdr(h) \
BOOL cdecl PDEF(h,assert) (BOOL ok, LPCSTR format, ...)
HDR(assert)

/////////////////////////////////////////////////////////////////////////
// putch() outputs one character to the console
//
#define putch CAT(CODE_LOC,putch)
#define putch_hdr(h) \
void        pascal PDEF(h,putch) (BYTE ch)
HDR(putch)

/////////////////////////////////////////////////////////////////////////
// getch() inputs one character from the keyboard
//
#define getch CAT(CODE_LOC,getch)
#define getch_hdr(h) \
BYTE pascal PDEF(h,getch) (void)
HDR(getch)

/////////////////////////////////////////////////////////////////////////
// FlushTransmitter() waits until all the characters have been output
//
#define FlushTransmitter CAT(CODE_LOC,FlushTransmitter)
#define FlushTransmitter_hdr(h) \
void pascal PDEF(h,FlushTransmitter) (void)
HDR(FlushTransmitter)

/////////////////////////////////////////////////////////////////////////
// FlushReceiver() removes all the characters waiting in the input buffer
//
#define FlushReceiver CAT(CODE_LOC,FlushReceiver)
#define FlushReceiver_hdr(h) \
void pascal PDEF(h,FlushReceiver) (void)
HDR(FlushReceiver)

/////////////////////////////////////////////////////////////////////////
// kbhit() returns TRUE if there is at least one character waiting
//
#define kbhit CAT(CODE_LOC,kbhit)
#define kbhit_hdr(h) \
BOOL pascal PDEF(h,kbhit) (void)
HDR(kbhit)

/////////////////////////////////////////////////////////////////////////
// CurrentCS() returns the value of the CS register.
//
#define CurrentCS CAT(CODE_LOC,CurrentCS)
#define CurrentCS_hdr(h) \
WORD pascal PDEF(h,CurrentCS) (void)
HDR(CurrentCS)

/////////////////////////////////////////////////////////////////////////
// FindItem() is passed two strings:  a unique (e.g. LONG) module
// identifier, and an item identifier.  It returns the address
// of the item if a module "claims" the find, or 0 if no such
// item.
//
#define FindItem CAT(CODE_LOC,FindItem)
#define FindItem_hdr(h) \
LPVOID pascal PDEF(h,FindItem) (LPCSTR Module, LPCSTR Item)
HDR(FindItem)

/////////////////////////////////////////////////////////////////////////
// BreakVector() returns the address of the monitor break handler
//
#define BreakVector CAT(CODE_LOC,BreakVector)
#define BreakVector_hdr(h) \
LPVOID pascal PDEF(h,BreakVector) (void)
HDR(BreakVector)

/////////////////////////////////////////////////////////////////////////////
//  InitEmonData() initializes the EmonData pointer to point to
//  the monitor's data segment.  It returns TRUE if the version
//  and size match the information the library was linked with.
//
#define InitEmonData CAT(CODE_LOC,InitEmonData)
#define InitEmonData_hdr(h) \
BOOL pascal PDEF(h,InitEmonData) ( void )
HDR(InitEmonData)

////////////////////////////////////////////////////////////////////////////
//    EMON has special versions of string routines for these reasons:
//       (1) intrinsics should be turned on so EI/DI and I/O work fine.
//       (2) String intrinsics (inlining) is wasteful
//       (3) There are bugs associated with string intrinsics
//           and optimization, especially with based pointers, in the
//           1.52 C compiler.
//
#define emon_strlen CAT(CODE_LOC,emon_strlen)
#define emon_strlen_hdr(h) \
int pascal PDEF(h,emon_strlen) (const char __far * str)
HDR(emon_strlen)

#define emon_strcmp CAT(CODE_LOC,emon_strcmp)
#define emon_strcmp_hdr(h) \
int pascal PDEF(h,emon_strcmp) (const char __far * s1, const char __far * s2)
HDR(emon_strcmp)

#define emon_strcpy CAT(CODE_LOC,emon_strcpy)
#define emon_strcpy_hdr(h) \
void pascal PDEF(h,emon_strcpy) (char __far * dst, const char __far *src)
HDR(emon_strcpy)

#define emon_memcmp CAT(CODE_LOC,emon_memcmp)
#define emon_memcmp_hdr(h) \
int pascal PDEF(h,emon_memcmp) (const void __far * s1, const void __far * s2, int len)
HDR(emon_memcmp)

#define emon_memcpy CAT(CODE_LOC,emon_memcpy)
#define emon_memcpy_hdr(h) \
void pascal PDEF(h,emon_memcpy) (void __far * dst, const void __far * src, int len)
HDR(emon_memcpy)

#endif
