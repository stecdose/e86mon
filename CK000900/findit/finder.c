
// This module is linked as a regular DOS-style .EXE, and shows
// how the E86Mon library functions can be used to find another
// module (the "findee") and link to it.

// It also demonstrates use of E86Mon's printf function.

#include "findme.h"

// First, make variables for all the functions

#define OneFunc(Name)  Name##Func Name
FuncList
#undef OneFunc

// Now go and fill in all the variable names

BOOL LinktoFindee(void)
{
    if ((DWORD)FindItem(MYMODULE,"version") != MYVERSION)
    {
        printf("Module not found\n");
        return FALSE;
    }

    #define OneFunc(Name) \
      if ((Name = (Name##Func)FindItem(MYMODULE, #Name)) == 0) return FALSE;
    FuncList
    #undef OneFunc

    return TRUE;
}

// Demonstrate linking to the findee and then executing its routines:

int main(void)
{
    if (!LinktoFindee())
    {
        printf("\nSorry -- couldn't find other module\n");
        printf("Values: %a %a %a\n",Add,Sub,BiteMe);

        // FlushTransmitter is required after printf because
        // emon assumes everything has been trashed when a 
        // DOS program returns, and will not attempt to output
        // any more characters.

        FlushTransmitter();
        return 1;
    }

    printf("\n%d + %d = %d\n",1,4,Add(1,4));
    printf("\n%d - %d = %d\n",1,4,Sub(1,4));
    BiteMe("Linked to Findee!");
    FlushTransmitter();

    return 0;
}
