/******************************************************************************
 *                                                                            *
 *     EMONDATA.H                                                             *
 *                                                                            *
 *     This file contains the structure definitions for most of E86Mon's      *
 *     global data. This structure is available to programs and extension     *
 *     libraries via the InitEmonData function.                               *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#ifndef _emondata_h_
#define _emondata_h_

#include "elibfunc.h"


//
// Values for RestartCode:  (do NOT change the order!!!)
//
#define RESTART_NONE                0xFFFF
#define RESTART_TOTAL               0x000
#define RESTART_RE_AUTOBAUD         0x100
#define RESTART_LOAD_LIBS           0x200
#define RESTART_ERASED_FLASH        0x300
#define RESTART_LOCK_IN_RAM         0x400
#define RESTART_EXIT_PROG           0x500
#define RESTART_RETURN_FROM_RAM     0x600

//
// Some of the pointers in the global data segment are based on the
// global data segment itself.  This macro makes this relatively painless.

#define SELFP(what) what __based((__segment) __self)*

//////////////////////////////////////////////////////////////////////////////
//    DispatchLink is what is used to loop through all the
//    attached dispatch routines.  Its full definition is in INTERLINK.C.
//    EmonGlobals contains one of these, but does not need to know what
//    it is.
//
struct DispatchLinkS;
typedef struct DispatchLinkS far * LPDispatchLink;

//////////////////////////////////////////////////////////////////////////////
//    RamLink is what is used to loop through all the attached
//    RAM-based dispatch routines.  Its full definition is in INTERLINK.C.
//    EmonGlobals contains one of these, but does not need to know what
//    it is.
//
struct RamLinkS;
typedef struct RamLinkS far * LPRamLink;

//////////////////////////////////////////////////////////////////////////////
//    LibEntryS is a structure which is used to store information about
//    a loaded library:
//    
typedef struct {
             WORD             codelength;      // Length of library in ROM
             WORD             codeseg;         // ROM segment of library
             WORD             datalength;      // Length of library's data
             WORD             dataseg;         // Where data stored in RAM
               } LibEntryS, far * FPLibEntry;

//////////////////////////////////////////////////////////////////////////////
//    SerialActions are passed to the SerialIO routine:
//
typedef enum {
        SerialPutch           = 0x000,        // Character in low order 8 bits
        SerialTryToGetch      = 0x100,        // Returns 0FFFF if none avail
        SerialFlushOutput     = 0x200,        // Wait until all chars sent
        SerialFlushInput      = 0x300,        // Clear input buffer
        SerialKbhit           = 0x400         // Return TRUE if data avail
              } SerialAction;


//////////////////////////////////////////////////////////////////////////////
//    SerialIO is the function used for low-level console I/O
//    Currently, the only handle defined is 0, for the console.
//
typedef WORD (far pascal * SerialIOFunc)(SerialAction action, WORD handle);


//////////////////////////////////////////////////////////////////////////////
//    EmonInternals exposes our internal variables to other modules.
//
//    PLEASE DO NOT ALTER THE ORDER OF THIS STRUCTURE!!!  (Add to the
//    end of it)
//
typedef struct {
    WORD           StrucSize;          // Size of overall structure
    WORD           Version;            // Version of E86MON in structure (hex)
    WORD           BetaCode;           // beta number (also hex)

    // The input line is where all line-oriented console I/O takes place

    SELFP(BYTE)    InputLine;          // Characters which have been input
    WORD           InputLineSize;      // Size of input line in bytes

    // The small buffer is a scratch buffer for use by code which processes
    // the commands.  Also, error strings are stored in this buffer.

    SELFP(BYTE)    SmallBuffer;        // Scratch buffer
    WORD           SmallBufSize;       // Size of small buffer in bytes

    // The BigBuffer is basically all of system memory which is not
    // being used by E86MON.  It is formatted as a DOS-style memory
    // chain.  The paragraph _before_ *BigBuffer is the first arena
    // header in the chain, and the paragraph _at_
    //  *(BigBuffer+BigBufParagraphs) is the last arena header in the
    // chain.  Libraries can allocate space by reducing BigBufParagraphs,
    // being careful to remember that the paragraph _at_ BigBufParagraphs
    // cannot be used.

    LPBYTE         BigBuffer;
    WORD           BigBufParagraphs;   // Size of big buffer in paragraphs

    // Libraries link themselves into the monitor (EMONLIBC.C in EMONLIB
    // does this for the libraries) by linking into the LibHead chain.

    LPDispatchLink LibHead;

    // RAM-based libraries which need notification of interrupt vector
    // destruction and creation also link themselves into the RamHead
    // chain.  ROM-based libraries should not do this, because these
    // callouts are made even when programming flash (ROM not available).
    // EMONLIBC.C handles this for RAM-based libraries.

    LPRamLink      RamHead;

    // Libraries should find an empty slot in LibEntries (which is
    // an array of length MaxLibEntry) to store their CS/DS information,
    // so they can find their data segment when E86MON reinitializes the
    // chain.  EMONLIBC.C does this automatically, as well.

    SELFP(LibEntryS) LibEntries;
    WORD           MaxLibEntry;

    // All communication with the CRT and keyboard takes place through
    // the SerialIO function.  RAM-based libraries ONLY can link this
    // vector in order to perform console functions.

    SerialIOFunc   SerialIO;

    // RawIO is a vector of bits (one for each file).  Currently
    // only bit 0 is examined.  If this bit is 0, (not raw I/O),
    // then putch outputs of '\n' will also output a '\r'.

    BOOL           RawIO;

    // LineIndex is the current parse location on InputLine.  It
    // is maintained by the scanXXX routines.

    WORD           LineIndex;

    // ErrorIndex is the location of the error in InputLine, and
    // PromptLength is the length of the command prompt.  Together,
    // these variables allow ScanError to point to the location of
    // an error for the user.

    WORD           ErrorIndex;
    WORD           PromptLength;

    // IgnoreError may be set by one Scan routine if it calls another
    // one.  This tells ScanError not to print an error message, because
    // the original Scan routine may try the parse in a different fashion.

    WORD           IgnoreError;

    // ScanError stores the given error in SmallBuffer, and sets ErrorStored
    // to the length of the error.  The fact that ErrorStored is non-zero
    // is used to indicate that there is an error pending.  Only the first
    // error encountered is stored in the buffer.

    WORD           ErrorStored;

    // If FileLineCount is non-zero when ScanError is called, it
    // will be output along with the error message, so that the user
    // knows on which line of a hex file an error occurred.

    WORD           FileLineCount;

    // LineUnfinished is maintained by putch.  It is set FALSE whenever
    // a '\n' is output, TRUE otherwise.  The console input routines
    // (GetInputLine and GetYN) use this to decide whether to output
    // a preceding '\n'.
    
    WORD           LineUnfinished;

    // RestartCode contains the type of restart which is currently
    // occuring (#defines at the top of this file)

    WORD           RestartCode;

    // ExitCode is set with the value of the exit code from an MS-DOS
    // program.  It will get printed out after the next restart.  (It
    // is not printed immediately, because the MS-DOS program could
    // have taken over the serial I/O.)

    WORD           ExitCode;

    // Milliseconds is the current number of ms since E86Mon started
    // running.  If they run E86Mon for over 49.7 days, it rolls over.
    // Oh well.

    DWORD          Milliseconds;

    // Stack points to the user program's stack

    FPSTACKREC     stack;

    // TrashedInputLine is used in error processing.  It is set by
    // CMDFILE.C, which processes the error line in-situ.  If it
    // is set, external modules know they should not attempt to report
    // errors on the command line.

    WORD           TrashedInputLine;

    // MonitorExited is set by the monitor whenever it returns to
    // a program.  This information is used by the console processing
    // code to know that E86Mon broke into execution (possibly of itself)
    // and then returned.  NestLevel is used internally by the monitor
    // to keep from nesting breaks too far.

    WORD           MonitorExited;
    WORD           NestLevel;

    // SpinLEDsWhileProgramming is used internally by the monitor to
    // control whether or not LEDs get updated by the programming action.
    // When downloading a file to ROM, this is set false so that the
    // LEDs get updated by file line number information.

    BOOL           SpinLEDsWhileProgramming;

    // MonitorMoved is used internally by the monitor to cause a special
    // restart when the monitor gets "locked" into RAM.

    WORD           MonitorMoved;

    // UnhandledIntVector is initialized by the monitor to point to
    // its int 3 which is used by the UnhandledInt code.  Console
    // replacement modules need this value to be able to send a
    // break to the monitor.

    DWORD          UnhandledIntVector;
                } EmonInternals, far * FPEmonInternals;


//////////////////////////////////////////////////////////////////////////////
//    Used to attach dispatcher to monitor
//
extern FPEmonInternals pascal EmonData;

#endif
