/******************************************************************************
 *                                                                            *
 *     TYPES.H                                                                *
 *                                                                            *
 *     This file contains common type declarations, compatible                *
 *     with standard Microsoft/Windows declarations, and with                 *
 *     the format of EMON's stack records.                                    *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#ifndef _types_h_
#define _types_h_

/* Allow single line comments and nameless structs/unions */

#pragma warning (disable : 4001)   /* Allow // Single line comments */
#pragma warning (disable : 4201)   /* Allow nameless structs and unions */
#pragma warning (disable : 4152)   /* Allow data/func pointer conversions */

//
//    Data types are defined to be reasonably compatible with
//    Microsoft's standard Windows definitions.
//

typedef unsigned long DWORD;
typedef unsigned short WORD;
typedef unsigned char  BYTE;

typedef int BOOL;

typedef void far *    LPVOID;
typedef DWORD far *   LPDWORD;
typedef WORD far *    LPWORD;
typedef BYTE far *    LPBYTE;
typedef BOOL far *    LPBOOL;
typedef char far *    LPSTR;
typedef const char far *    LPCSTR;

typedef void near *    PVOID;
typedef DWORD near *   PDWORD;
typedef WORD near *    PWORD;
typedef BYTE near *    PBYTE;
typedef BOOL near *    PBOOL;
typedef char near *    PSTR;
typedef const char *    PCSTR;

// Static variables which don't change (primarily strings) are stored
// in the code segment so that they do not occupy RAM during operation.

#ifndef ROM
#define ROM _based( _segname ("_CODE" ))
#endif

typedef const char ROM ROMCHAR;
typedef const BYTE ROM ROMBYTE;
typedef const WORD ROM ROMWORD;
typedef const DWORD ROM ROMDWORD;
typedef const char ROM * PROMCHAR;       // Pointer to rom based string

#ifndef NULL
#define  NULL  ((void*)0)
#endif

#if defined(DEBUG) || defined(_DEBUG) || defined(ALL_PUBLICS)
  #define STATIC
#else
  #define STATIC static
#endif

//
// Structure defining registers on the stack in the order they
// are passed by EMSTART.ASM.
//

typedef struct {
            WORD _ES;           // Microsoft treats "ES" as a reserved word.
            WORD DS;
            WORD DI;
            WORD SI;
            WORD BP;
            WORD SP_DUMMY;        // Pushed by "PUSHA", ignored by "POPA"
    union {
        struct {
            WORD BX;
            WORD DX;
            WORD CX;
            WORD AX;
        };
        struct {
            BYTE BL;
            BYTE BH;
            BYTE DL;
            BYTE DH;
            BYTE CL;
            BYTE CH;
            BYTE AL;
            BYTE AH;
        };
    };
            WORD IP;
            WORD CS;
            WORD Flags;
            WORD IntType;
            WORD SP;
            WORD SS;
} STACKREC,  near * NPSTACKREC, far * FPSTACKREC;

//
// Address structure for printf %la and ScanAddress:
//
typedef struct {
    WORD     Flags;
#define ANY_DISP_SEG 0x8000      // Set if should be displayed as segmented
    WORD     MemSpace;
    DWORD    LinearAddress;
    LPVOID   SegAddress;
} AnyAddress, far * FPANYADDRESS;


//
// Macros to split and recombine pointers
//
#define _FP_SEG(fp) ((WORD)*((unsigned __far *)&(fp)+1))
#define _FP_OFF(fp) ((WORD)*((unsigned __far *)&(fp)))
#define _MK_FP(seg, offset) (void __far *) \
    (((unsigned long)seg << 16) + (unsigned long)(unsigned)offset)

#define FP_SEG   _FP_SEG
#define FP_OFF   _FP_OFF
#define MK_FP    _MK_FP

//
//    constants
//
#define TRUE   1
#define FALSE  0

#define  ASCII_CR    0x0d
#define  ASCII_LF    0x0a
#define  ASCII_BS    0x08
#define  ASCII_BELL  0x07
#define  ASCII_TAB   0x09
#define  ASCII_XON   0x11
#define  ASCII_XOFF  0x13
#define  ASCII_ESC   0x1B
#define  ASCII_DEL   0x7F

#endif
