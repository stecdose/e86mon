
#include "findme.h"
#include "interlib.h"

// Disable optimization warning about assembly language
#pragma warning(disable : 4704)

// Declare a priority for linking into EMON's chain
// Use the default 0x8000.
WORD near pascal LibPriority = 0x8000;

// Declare a LibName for EMON's 'i' function to report our module.

char near pascal LibName[]   = "FINDEE";

#pragma pack (1)    // So thunks come out OK

// Declare functions which match the desired definition.
// We will call them through a "thunk", so be sure and
// save DS and set it up from AX, and then restore it.

WORD far pascal Add(WORD p1,WORD p2)
{
    WORD result;

    // Set up our DS -- thunk stored DS in AX

    _asm push  ds
    _asm mov   ds,ax


    result = p1+p2;

    // Restore caller's DS and return
    _asm pop ds
    return result;
}

WORD far pascal Sub(WORD p1,WORD p2)
{
    WORD result;

    // Set up our DS -- thunk stored DS in AX

    _asm push  ds
    _asm mov   ds,ax


    result = p1-p2;

    // Restore caller's DS and return
    _asm pop ds
    return result;
}

void far cdecl BiteMe(LPCSTR where)
{
    // Set up our DS -- thunk stored DS in AX

    _asm push  ds
    _asm mov   ds,ax

    printf("\nResult - (%s)\n",
            where);


    // Restore caller's DS and return
    _asm pop ds
}

///////////////////////////////////////////////////////////////////
//  This macro is what causes EMONLINK.LIB to link us into the
//  dispatch chain.  We only care about FindItem messages:
//
DISPATCH(ExtEmon)
{
    case Dispatch_Install:
        // DO something if required for installation
        break;

    case Dispatch_FindItem:
        if (emon_strcmp(dstruc->FindItem.Module, MYMODULE) != 0)
            break;

        if (emon_strcmp(dstruc->FindItem.Item,"version") == 0)
        {
            *(dstruc->FindItem.ItemLoc) = (LPDWORD)MYVERSION;
            return Dispatch_IDidIt;
        }

        #define OneFunc(Name) \
        {                                                         \
            static struct {                                       \
                BYTE       MovAxCs[2];                            \
                BYTE       FarJmp;                                \
                Name##Func JumpTarget;                            \
                   } Thunk = {{0x8C, 0xC8}, 0xEA, Name};          \
            if (emon_strcmp(dstruc->FindItem.Item, #Name) == 0)   \
            {                                                     \
                *(dstruc->FindItem.ItemLoc) = &Thunk;             \
                return Dispatch_IDidIt;                           \
            }                                                     \
        }
        FuncList
        #undef OneFunc
        break;

  default:
        break;
}
END_DISPATCH
