set cl=
call ..\setenv

rem Normal DOS-style link, but include EMONLINK.LIB

%amd_lpd_cl%cl /Fm /I ..\include finder.c /link ..\emonlib\emonlink
..\out\makehex finder

rem FINDEE is special -- must use /GS and /Zl to keep it from
rem dragging in CRT.LIB, and must link EMONLIBA.OBJ *first* as well
rem as linking with EMONLINK.LIB

%amd_lpd_cl%cl /I ..\include /Gs /Zl /c findee.c
echo /M:full ..\emonlib\emonliba findee  > link.lnk
echo findee                              >> link.lnk
echo findee                              >> link.lnk
echo %amd_lpd_lib%slibce ..\emonlib\emonlink; >>link.lnk
%amd_lpd_cl%link @link.lnk
del link.lnk
..\out\makehex findee EC00

copy *.hex ..\out
copy *.map ..\out

del *.exe
del *.obj
del *.hex
del *.map
