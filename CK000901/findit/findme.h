
#include "elibfunc.h"

#define MYMODULE "Some Really unique module name nobody else thought of"

#define MYVERSION  100    // Some non-zero number

// Define the types of functions here (for type-checking).
// Because the "finder" and the "findee" may not even be the
// same memory model, be sure to explicitly specify "far"
// and either "pascal" or "cdecl" for the func type, and
// also be sure to explicitly specify the size of data to
// be passed and return values.

// The type of the function should be function name + "Func":

typedef WORD (far pascal * AddFunc)(WORD a1, WORD a2);
typedef WORD (far pascal * SubFunc)(WORD a1, WORD a2);
typedef void (far cdecl * BiteMeFunc)(LPCSTR where);


// Now define a list of funcs.  We will use the list multiple
// times, according to whether this has been included by the
// findee or the finder.

#define FuncList      \
      OneFunc(Add);   \
      OneFunc(Sub);   \
      OneFunc(BiteMe);
