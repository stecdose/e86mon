comment ~
/******************************************************************************
 *                                                                            *
 *     EMON_186.ASM                                                           *
 *                                                                            *
 *     This file contains 186-specific EMON Startup code.                     *
 *     It MUST be linked first in the link map.                               *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
 end comment ~

	title   emon_186.asm - 186 Startup Module for EMON

	.186

.xlist
	include am186msr.inc
.list


_TEXT   segment para public 'CODE'

	assume ds:nothing,es:nothing,ss:nothing

extern  CPUIndependentSetup :NEAR       ; Next level of initialization
extern  OriginalCodeSeg: WORD
extern  PermArray:NEAR                  ; Permanent variable array

; FakeCall and FakeRet implement a single level call stack using
; BP before we have memory set up.

FakeCall MACRO ToWhere
    Local ReturnAddress
	mov     bp,OFFSET ReturnAddress
	jmp     ToWhere
ReturnAddress:
ENDM

FakeRet  MACRO
	jmp     bp
ENDM

;***********************************************************************
;
;    STARTUP()   -- Main program entry point, must be at file offset 0
;
STARTUP proc near public

; Point to our permanent variable record so the binary may be edited

	jmp     short @F
	db      "AMD LPD 01"
	dw      PermArray
@@:
; In case we jumped here, instead of a real reset, make sure we
; initialize with interrupts off and direction forward.
;
	cli
	cld
;
; Setup ROM UMCS register for 512K at 0 wait states,
; unless we are running from UCS DRAM.
;
; Note for 186CC: if we are running in x8 mode we do not clear 
; the USIZ bit (set to 1 in the CS_UMCS_RESERVED)
;

	mov     dx, CS_UMCS
	in      ax, dx
	test    ax, CS_DRAM_ENABLE
	jnz     @F
	
	mov     ax, CS_UMCS_512K+CS_WAIT3+CS_IGXRDY+CS_UMCS_RESERVED   ;dp
	out     dx,al

  ;	mov 	dx, RESCON_CC ; leave upper memory chip select wait state at 3
  ;	in 		ax, dx		  ; if a TIP is attached else set to 0 
  ;  and 	ax, 0080h
  ; 	jnz		doug

  ;	mov     dx, CS_UMCS
  ;	mov     ax, CS_UMCS_512K+CS_WAIT0+CS_IGXRDY+CS_UMCS_RESERVED
  ;	out     dx,al
	
;doug:
@@:
;
; HWInitEnd is used by the flash update routine.  It stores the
; code from zero to here as part of the recovery vector.  The only
; real thing we have to do is make sure we can address all the way
; down to the replacement monitor.
;
	HWInitEnd LABEL BYTE

;
; If this is a copy of the monitor which was accidentally left in RAM,
; don't execute it, as not all the variables will be set up properly.

	cmp     OriginalCodeSeg,0
	jz      @F
	retf
@@:

; Get the CPU type into AL

	mov     dx,PRL_REG
	in      ax,dx
	shr     ax,12
;
; If we are running out of lower RAM,  then assume the chip selects 
; have already been configured and contain the correct chip select size.   
;
	mov     bx, cs
	cmp     bx, 8000h
	jnbe    ContinueConfiguration
	jmp     RunningFromLowRAM
		
;
; Not running out of lower RAM.  Let's configure RAM, watch dog timers,
; etc. in a processor-specific fashion.
;
ContinueConfiguration:
	dec     ax

IFNDEF  ER

	js      GotEM
	jz      GotES
ENDIF

	dec     ax
	jz      GotER
	dec     ax

IFNDEF  ER

	jz      GotED
	dec     ax
	jz      GotCC

ENDIF

;(Your favorite processor goes here)

ASeriousProblemOccurredAndIDontKnowWhatToDoAboutItSoImGivingUp:
	hlt



IFNDEF ER

;****************************************************************
; The EM is pretty basic.  Set up LMCS, and go try to figure
; out how much RAM is available.

GotEM:
;
; setup LMCS register to cover entire RAM area at 0 wait states
;
	mov     dx, CS_LMCS
	mov     ax, CS_LMCS_512K+CS_WAIT0+CS_IGXRDY
	out     dx,al
;
; SizeRAM only returns if it encountered a problem...
;
	FakeCall    SizeRAM
	jmp     ASeriousProblemOccurredAndIDontKnowWhatToDoAboutItSoImGivingUp

;****************************************************************
; The ES is just like the EM (as far as first level initialization
; goes), except it has a watchdog timer we have to disable.
;
GotES:
	FakeCall  DisableES_EDorERWatchdogTimer
	jmp       GotEM

ENDIF

;****************************************************************
; The ER has its own RAM, and a watchdog timer.

GotER:
;
; Enable the ER's 32K internal SRAM, and report the size in BX
;
	FakeCall  DisableES_EDorERWatchdogTimer
	mov     dx, CS_IMCS
	mov     ax, CS_IMCS_SHOW_READ+CS_IMCS_RAM_ENABLE+CS_IMCS_RESERVED
	out     dx, al
	mov     bx,8000h/10h                ; ER has 32K RAM onboard
	jmp     CPUSpecificSetupFinished


IFNDEF  ER

;****************************************************************
; The ED is like an ES (watchdog timer) but has DRAM support.

GotED:
	FakeCall  DisableES_EDorERWatchdogTimer

; Set MCS1, MCS2, and MCS3 to normal mode for DRAM RAS and CAS support
	mov     dx, PIO_MODE0
	in      al, dx
	and     ax, NOT PIO0_MCS1
	out     dx, al

	mov     dx, PIO_DIR0
	in      al, dx
	and     ax, NOT PIO0_MCS1
	out     dx, al

	mov     dx, PIO_MODE1
	in      al, dx
	and     ax, NOT (PIO1_MCS2+PIO1_MCS3)
	out     dx, al

	mov     dx, PIO_DIR1
	in      al, dx
	and     ax, NOT (PIO1_MCS2+PIO1_MCS3)
	out     dx, al

; Refresh the DRAM often          

	mov     dx, DRAM_CLK
	mov     ax, 4Eh                        ; 15.6 us refresh at 5 Mhz        
	out     dx, al
 
	mov     dx, DRAM_ENAB
	mov     ax, DRAM_ENABLE
	out     dx, al

; Enable DRAM with 3 wait states - this can be modified in sd186.c 
; depending on the board configuration.

	mov     dx, CS_LMCS
	mov     ax, CS_LMCS_512K+CS_WAIT0+CS_IGXRDY+CS_DRAM_ENABLE
	out     dx,al
	FakeCall    SizeRAM

; If we got here, there was an error.  Perhaps the system has
; SRAM, not DRAM.  Disable the refresh counter, and try to set
; it up the same way as an EM.

	mov     dx, DRAM_ENAB
	mov     ax, NOT DRAM_ENABLE                ; disable refresh counter 
	out     dx, al
	jmp     GotEM


ENDIF

IFNDEF ER

;****************************************************************
; The 186CC is like an ED but the watchdog timer is in a different location
GotCC:
; Disable the watchdog timer
	FakeCall  DisableCCWatchdogTimer

;
; in this test version of EMON, I set serrano for SRAM rather
; than DRAM.  If it doesn't work, then I try DRAM.
; 
	mov     dx, CS_LMCS
	mov     ax, CS_LMCS_512K+CS_WAIT0+CS_IGXRDY
	out     dx,ax
;; SizeRAM only returns if it encountered a problem...
	FakeCall    SizeRAM
;
; try 8 bit mode SRAM if that didn't work               
	mov     dx, CS_LMCS
	mov     ax, CS_LMCS_512K+CS_WAIT0+CS_IGXRDY+020h
	out     dx,ax
;; SizeRAM only returns if it encountered a problem...
	FakeCall    SizeRAM
		
; Set MCS0 to normal mode MCS0 chip select // dtp 11-10-98
	mov     dx, PIOMODE0_CC
	in      al, dx
	and     ax, NOT PIO0_MCS0_CC
	out     dx, al

	mov     dx, PIODIR0_CC
	in      al, dx
	and     ax, NOT PIO0_MCS0_CC
	out     dx, al

; Set MCS3 to normal mode for DRAM RAS support
	mov     dx, PIOMODE0_CC
	in      al, dx
	and     ax, NOT PIO0_MCS3_CC
	out     dx, al

	mov     dx, PIODIR0_CC
	in      al, dx
	and     ax, NOT PIO0_MCS3_CC
	out     dx, al

; Refresh the DRAM often          

	mov     dx, CDRAM_CC
	mov     ax, 4Eh                        ; 15.6 us refresh at 5 Mhz        
	out     dx, al
 
	mov     dx, EDRAM_CC
	mov     ax, DRAM_ENABLE
	out     dx, al

; Enable DRAM with 3 wait states - this can be modified in sd186.c 
; depending on the board configuration.

	mov     dx, CS_LMCS
	mov     ax, CS_LMCS_512K+CS_WAIT3+CS_IGXRDY+CS_DRAM_ENABLE
	out     dx,al
	FakeCall    SizeRAM

; If we got here, there was an error.  Perhaps the system has
; SRAM, not DRAM.  Disable the refresh counter, and try to set
; it up the same way as an EM.

	mov     dx, EDRAM_CC
	mov     ax, NOT DRAM_ENABLE                ; disable refresh counter 
	out     dx, al
	jmp     GotEM

ENDIF

;****************************************************************
;
; Figure out how much memory we have using the existing configuration
; information.  Do not read LCS on the ER, as that enables the decode.
; (Ain't legacy support wonderful!)
;
; AL = Processor type when we get here.
;
RunningFromLowRam:

	mov     bx,8000h/10h   ; ER has 32K RAM onboard
	cmp     ax, 2h         ; Just bug out if ER
	jz      CPUSpecificSetupFinished

; It's not an ER, and we're running out of lower RAM.  Figure out
; exactly how much RAM that is by looking at how the LMCS register was
; previously programmed.  Assume it's one of 64K, 128K, 256K, or 512K.

	mov     dx, CS_LMCS
	in      ax, dx
	or      ax, 0800h       ; Assume at least 64K
	add     ax, ax
@@:
	mov     bx,ax           ; Assume we have the right number
	dec     ax              ; Should be only 1 bit left when we
	and     ax,bx           ; finish loop
	jnz     @B
	jmp     CPUSpecificSetupFinished


STARTUP ENDP


; Disable the watchdog timer on the ES or the ED.

DisableES_EDorERWatchdogTimer PROC NEAR
	mov     dx, WDOG_REG_ES
	mov     ax, WDOG_WR1_ES
	out     dx, al
	mov     ax, WDOG_WR2_ES
	out     dx, al
	mov     ax, 0
	out     dx, al
	FakeRet
DisableES_EDorERWatchdogTimer ENDP


IFNDEF  ER

; Disable the watchdog timer on the CC.

DisableCCWatchdogTimer PROC NEAR
	mov     dx, WDTCON_CC
	mov     ax, WDOG_WR1_ES
	out     dx, al
	mov     ax, WDOG_WR2_ES
	out     dx, al
	mov     ax, 0
	out     dx, al
	
		in              ax, dx
		mov             dx, 080h
		out             dx, ax

	FakeRet
DisableCCWatchdogTimer ENDP


ENDIF


;
; Attempt to determine the size of the RAM which starts at 00000h
; Returns only if an error occurs.
; Otherwise (no error), jumps to CPUSpecificSetupFinished
;    with BX = Detected RAM size in paragraphs

SizeRAM PROC NEAR
	xor     si,si
	mov     ds,si
	mov     ax,si
	mov     cx,si
	not     cx
	mov     ds:[si],ax
	mov     ds:[si+2],cx
	cmp     ax,ds:[si]
	jnz     MemoryFailure
	cmp     cx,ds:[si+2]
	jnz     MemoryFailure
;
; There seems to be something writeable there.  Is it at least 64K?
;
	mov     bx,8000h/10h                ; Does it wrap at 32K?
	mov     es,bx
	mov     es:[si],cx
	cmp     ax,ds:[si]
	jz      @F
MemoryFailure:
	FakeRet
;
; Now go try to size it
;
@@:
	cmp     bx,8000h                ; 512K/10h
	jae     @F
	add     bx,bx
	mov     es,bx
	mov     es:[si],cx
	cmp     ax,ds:[si]
	jz      @B
@@:
	mov     ax,bx
	dec     ax              ; Develop RAM address mask
	mov     cx, ax          ; save mask in cx
	in      ax,dx           ; Read current chip select settings        
	mov     ah,ch           ; Retain wait state, DA and Dram enable bits
	out     dx,ax

	.errnz  $ - CPUSpecificSetupFinished

SizeRAM ENDP

CPUSpecificSetupFinished:
	jmp     CPUIndependentSetup

;***********************************************************************
; HWInitSize returns the size of the initialization block which
; must be installed at monitor update.

HWInitSize     PROC NEAR PUBLIC
	lea     ax,HWInitEnd
	ret
HWInitSize     ENDP

;***********************************************************************
; SegToLin converts a segmented address into a linear address
; It assumes a wrap at 1MB, which is perfectly valid for the 186

SegToLin PROC NEAR PUBLIC
	pop     bx              ; return address
	pop     ax              ; offset
	pop     dx              ; segment
	rol     dx,4
	mov     cx,dx
	and     dx,0Fh
	xor     cx,dx
	add     ax,cx
	adc     dx,0
	and     dx,0Fh          ; Wrap at 1MB
	jmp     bx
SegToLin ENDP

;***********************************************************************
; LinToSeg converts a linear address into a segmented address.
; It assumes a wrap at 1MB, which is perfectly valid for the 186

LinToSeg PROC NEAR PUBLIC
	pop     bx              ; return address
	pop     ax              ; low part
	pop     dx              ; high part
	mov     cx,ax
	and     ax,0Fh
	xor     cx,ax
	and     dx,0Fh
	or      dx,cx
	ror     dx,4
	jmp     bx
LinToSeg ENDP

;***********************************************************************
; RunningFromRAM returns TRUE if we are running out of RAM,
; FALSE otherwise.

RunningFromRAM PROC NEAR PUBLIC
	mov     ax,cs
	shl     ax,1             ; Top bit to carry
	sbb     ax,ax            ; 0 for RAM, -1 for ROM
	inc     ax               ; 1 for RAM, 0 for ROM
	ret
RunningFromRAM  ENDP


_TEXT   ends

end        STARTUP
