/******************************************************************************
 *                                                                            *
 *     186MEM.C                                                               *
 *                                                                            *
 *     This file contains memory routines for 186 processors.                 *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "emon.h"


/////////////////////////////////////////////////////////////////////////
// AnyMem is the root routine for GetAnyMem/SetAnyMem
//
BOOL AnyMem(FPANYADDRESS a,LPVOID dest,LPVOID source,WORD TransferSize,
            BOOL Programming)
{
    DWORD Max = 0x100000;
    DWORD Min = 0;
    DWORD MonLocation;
    DWORD LinAddr = a->LinearAddress;

    Programming = Programming && (a->MemSpace>0);

    if (Programming)
    {
        MonLocation = ((DWORD)OriginalCS()) << 4;
        Max = GetFlashBootBase();
        Min = FreeFlash(FALSE);
        if (LinAddr < Min)
        {
            MonLocation = ((DWORD)CurrentCS()) << 4;
            Programming = FALSE;
            Min = SegToLin(EGD.BigBuffer);
            Max = Min + (((DWORD)EGD.BigBufParagraphs) << 4);
        }
        if (LinAddr < MonLocation)
        {
            if (MonLocation < Max)
                Max = MonLocation;
        }
        else if (MonLocation > Min)
            Min = MonLocation + 0x8000;
    }

    Max -= TransferSize;

    if ((a->MemSpace > 1) || (LinAddr > Max) || (LinAddr < Min))
        return PrintError(MainMsgMemLAddrRange,a,TransferSize);

    if (Programming)
        return ProgramFlash(LinAddr,source,TransferSize);

    emon_memcpy(dest,source,TransferSize);
    return TRUE;
}

/////////////////////////////////////////////////////////////////////////
// GetAnyMem() handles transfers from any generic address space.
//
BOOL    GetAnyMem(FPANYADDRESS a, LPVOID Buffer, WORD TransferSize)
{
    return AnyMem(a,Buffer,LinToSeg(a->LinearAddress),TransferSize,FALSE);
}

/////////////////////////////////////////////////////////////////////////
// SetAnyMem() handles transfers to any generic address space.
//
BOOL    SetAnyMem(FPANYADDRESS a, LPVOID Buffer, WORD TransferSize)
{
    return AnyMem(a,LinToSeg(a->LinearAddress),Buffer,TransferSize,TRUE);
}
