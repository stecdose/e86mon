/******************************************************************************
 *                                                                            *
 *     LIBLINK.C                                                              *
 *                                                                            *
 *     This file contains routines which allow external library               *
 *     routines to link into some of E86MON's functions to extend             *
 *     the capability of the monitor.                                         *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "libinfo.h"


// Disable optimization warning
#pragma warning(disable : 4704)

DispatchResult far cdecl InternalDispatch(LPDispatchStruct s);

static DispatchFunc funclist[] = {InternalDispatch,0};
static DispatchLink myDispatch = {0,funclist,SystemMsgName};

LPDispatchLink   RAMhead, ROMhead;
FPEmonInternals EmonData;


//////////////////////////////////////////////////////////////////////////////
//    SystemInRAM()  Returns TRUE if entire system running from RAM
//                   NOTE:  Later could modify to allow extensions
//                          to be moved to RAM, too.
// 
BOOL SystemInRAM(void)
{
    return (RunningFromRAM() && (EGD.LibHead == RAMhead));
}

//////////////////////////////////////////////////////////////////////////////
//    RunSystemFromRAM()  Controls whether monitor is moved to RAM or not.
//                   NOTE:  Later could modify to allow extensions
//                          to be moved to RAM, too.
// 
BOOL RunSystemFromRAM(BOOL ToRam)
{
    if (ToRam && (EGD.NestLevel > 1))
    {
        PrintError(SystemMsgMonReentered);
        return FALSE;
    }

    ForceMonitorToRAM(ToRam);
    EGD.LibHead = ToRam ? RAMhead : ROMhead;
    return SystemInRAM();
}

//////////////////////////////////////////////////////////////////////////////
//    NextLoadedProg() -- given the address of the previous program,
//                    returns the address of the next one.
//                    This routine is used by CmdFile as well as
//                    internally.
//
DWORD NextLoadedProg(DWORD Prev, BOOL Validate)
{
    DWORD Next;

    Next = GetFlashBase();
    if (Prev != 0)
        Next =  ((LPLibInfo)(LinToSeg(Prev)))->NextStruct;

    if (Validate)
        if ( (emon_memcmp(((LPLibInfo)(LinToSeg(Next)))->Signature,
           LibSig.Signature, sizeof(((LPLibInfo)(0))->Signature)) != 0) ||
              (Next <= Prev) )
        Next = 0;
    return Next;
}

//////////////////////////////////////////////////////////////////////////////
//    FreeFlash() -- returns address of free flash memory.
//
DWORD FreeFlash(BOOL recalculate)
{
    DWORD test;
    static DWORD value = 0;

    if (recalculate || (value==0))
    {
        test = 0;

        do {
            value = NextLoadedProg(test,FALSE);
            test  = NextLoadedProg(test,TRUE);
        } while (value == test);
    }

    return value;
}

//////////////////////////////////////////////////////////////////////////////
//    LinkLibraries()  -- gives all libraries a chance to link themselves
//      into the library chain.
//
void LinkLibraries(void)
{
    DWORD      LinAddress = 0;
    WORD       increment  = 0x4000;
    DispatchInstall LibFunc;
    WORD       i;

    for (
            LinAddress = (FreeFlash(TRUE) + 0x3FFF) & 0xFC000L;
            LinAddress < GetFlashBootBase();
            LinAddress += increment )
    {
        increment = (WORD)(((LinAddress & 0xFFFF) == 0) ? 0x4000 : 0x800);
        LibFunc = LinToSeg(LinAddress);
        if (emon_memcmp(LibFunc,&LibSig,sizeof(LibSig)) == 0)
        {
            _asm push ds;
            LibFunc(&EmonGlobalData);
            _asm pop ds;
            increment = 0x800;
        }
    }
    ROMhead = EGD.LibHead;

    /* If code was erased from the flash, mark its data segment so
     * that it doesn't inadvertently get reused.  We mark the data
     * segment rather than delete it in case it is being used by
     * RAM-resident library extensions (which are still linked in
     * though their ROM has been erased).
     */
    if (EGD.RestartCode != RESTART_ERASED_FLASH)
        return;

    for (i=0; i < EmonData->MaxLibEntry; i++)
    {
        FPLibEntry lib = EmonData->LibEntries + i;
        if (*(LPBYTE)MK_FP(lib->codeseg,0) == 0xFF)
            lib->codeseg=0;
    }
}

//////////////////////////////////////////////////////////////////////////////
//    Last stop for dispatcher -- internal routine handles console I/O
//
DispatchResult far cdecl InternalDispatch(LPDispatchStruct s)
{
    switch (s->op)
    {
        case Dispatch_Printf:
            vpprintf((WRITECHAR)putch,s->Printf.FormatVector);
            break;

        case Dispatch_PrintError:
            return iprinterror(s->Printf.FormatVector);
            break;

        case Dispatch_GetYN:
            return Base_GetYNResponse(s->GetYN.Prompt, s->GetYN.LeadingCRLF);

        case Dispatch_GetInputLine:
            return Base_GetInputLine(s->GetInputLine.Prompt,
                                     s->GetInputLine.echo);

        case Dispatch_ScanDelimiters:
            Base_ScanDelimiters(s->ScanDelimiters.Which);
            break;

        case Dispatch_ScanError:
            return Base_ScanError(s->ScanError.Cause);

        case Dispatch_ScanOption:
            return Base_ScanOption(s->ScanOption.Delimiters,s->ScanOption.name);

        case Dispatch_ScanEOL:
            return Base_ScanEOL(s->ScanEOL.ErrorIfNot);

        case Dispatch_ScanString:
            return Base_ScanString(s->ScanString.s, s->ScanString.len);

        case Dispatch_ScanHexDigits:
            return Base_ScanHexDigits(s->ScanHexDigits.result,
                                  s->ScanHexDigits.base);

        case Dispatch_ScanByte:
            return Base_ScanByte(s->ScanByte.b);

        case Dispatch_ScanWord:
            return Base_ScanWord(s->ScanWord.w);

        case Dispatch_ScanDWord:
            return Base_ScanDWord(s->ScanDWord.r);

        case Dispatch_ScanDecimal:
            return Base_ScanDecimal(s->ScanDecimal.r);

        case Dispatch_ScanAddress:
            return Base_ScanAddress(s->ScanAddress.Flags,
                                s->ScanAddress.clientAddress);

        case Dispatch_ScanRange:
            return Base_ScanRange(s->ScanRange.Flags,
                              s->ScanRange.a,
                              s->ScanRange.l);

        case Dispatch_ScanList:
            return Base_ScanList(s->ScanList.NumBytes, s->ScanList.EmptyOK);

        case Dispatch_GetTime:
            Base_GetCurrentTime(s->GetTime.Milliseconds);
            break;

        case Dispatch_PostLEDs:
            Base_PostLEDs(s->PostLEDs.b);
            break;

        case Dispatch_InquireLEDs:
            return Base_InquireLEDs();

        case Dispatch_RAMSafe:
            return CurrentCS() == OriginalCS();

        default:
            return Dispatch_NotExclusive;
    }
    return Dispatch_IDidIt;
}


void ClearLibraries(void)
{
    EmonGlobalData.UnhandledIntVector = (DWORD)MK_FP(CurrentCS(),&MonitorInt3);

    ROMhead = RAMhead = EGD.LibHead = &myDispatch;

    myDispatch.next = 0;
    myDispatch.priority = 0xFFFF;
    myDispatch.dataseg  = CurrentDS();
    myDispatch.datalength = MonitorRAMSize();
    myDispatch.codelength = MonitorROMSize();
    myDispatch.codeseg  = OriginalCS();
}
