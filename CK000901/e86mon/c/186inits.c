/******************************************************************************
 *                                                                            *
 *     186INITS.C                                                             *
 *                                                                            *
 *     This file contains 186 serial port initialization code,                *
 *     including automatic baudrate and port detection (just type 'a').       *
 *                                                                            *
 *     All differences between serial ports on EM, ES, ED, and ER are         *
 *     encapsulated here -- the run-time serial code in 186SER.C is           *
 *     handed a pointer to a structure which defines ports and bit offsets.   *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#include "emon.h"
#include "am186msr.h"
#include "186init.h"

#define PATTERN_LEN 30  // Length of autobaud "cylon eye" pattern

// EM and ER serial port structure

STATIC SerPort ROM EM_Serial   =
      {SPRT0_CTL,SPRT0_STAT,SPRT0_TX,SPRT0_RX,
       SPRT0_BDV,INT_SPRT0,INT_EOI,ITYPE_SPRT0, IMASK_ON, IMASK_OFF,
       SPRT_CTL_8BITS_EM | SPRT_CTL_TX_EM,
       SPRT_CTL_8BITS_EM | SPRT_CTL_TX_EM | SPRT_CTL_RX_EM, 
       SPRT_CTL_8BITS_EM | SPRT_CTL_TX_EM | SPRT_CTL_RX_EM | 
               SPRT_CTL_RXIE_EM | SPRT_CTL_RSIE_EM,
       SPRT_CTL_TXIE_EM,
       SPRT_STAT_RDR_EM,
       SPRT_STAT_THRE_EM,
       SPRT_STAT_BRK_EM,
       SPRT_STAT_FRAME_EM | SPRT_STAT_PARITY_EM | SPRT_STAT_OVERFLOW_EM | SPRT_STAT_BRK_EM,
       32,1,
       (WORD)~(PIO1_TX | PIO1_RX),
       PIO1_RX
      };

#ifndef ER

// ES and ED serial port structure -- two copies, one for each port

STATIC SerPort ROM ES_Serial[2] = {
      {SPRT1_CTL,SPRT1_STAT,SPRT1_TX,SPRT1_RX,
       SPRT1_BDV,INT_SPRT1,INT_EOI,ITYPE_SPRT1, IMASK_ON, IMASK_OFF,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES | SPRT_CTL_RMODE_ES,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES | SPRT_CTL_RMODE_ES |
           SPRT_CTL_RXIE_ES,
       SPRT_CTL_TXIE_ES,
       SPRT_STAT_RDR_ES,
       SPRT_STAT_THRE_ES,
       SPRT_STAT_BRK1_ES,
       SPRT_STAT_FRAME_ES | SPRT_STAT_PARITY_ES | SPRT_STAT_OVERFLOW_ES | SPRT_STAT_BRK1_ES,
       16,0,
       (WORD)~(PIO1_TX0 | PIO1_RX0 | PIO1_TX1 | PIO1_RX1),
       PIO1_RX0 | PIO1_RX1
      },
      {SPRT0_CTL,SPRT0_STAT,SPRT0_TX,SPRT0_RX,
       SPRT0_BDV,INT_SPRT0,INT_EOI,ITYPE_SPRT0, IMASK_ON, IMASK_OFF,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES | SPRT_CTL_RMODE_ES,
       SPRT_CTL_MODE1_ES | SPRT_CTL_TMODE_ES | SPRT_CTL_RMODE_ES |
           SPRT_CTL_RXIE_ES,
       SPRT_CTL_TXIE_ES,
       SPRT_STAT_RDR_ES,
       SPRT_STAT_THRE_ES,
       SPRT_STAT_BRK1_ES,
       SPRT_STAT_FRAME_ES | SPRT_STAT_PARITY_ES | SPRT_STAT_OVERFLOW_ES | SPRT_STAT_BRK1_ES,
       16,0,
       (WORD)~(PIO1_TX0 | PIO1_RX0 | PIO1_TX1 | PIO1_RX1),
       PIO1_RX0
      }
      };


// CC serial port structure -- one ES Style UART and a High-Speed UART

STATIC SerPort ROM CC_Serial[2] = {
      {HSPCON0_CC,HSPSTAT_CC,HSPTXD_CC,HSPRXD_CC,
       HSPBDV_CC,CH3CON_CC,EOI_CC,EOI_HSUART_CC, CHXCON_SRC_INT_CC,
	   CHXCON_SRC_INT_CC+CHXCON_MSK,
       0,
       HSPCON0_TMODE_CC | HSPCON0_RMODE_CC,
       HSPCON0_TMODE_CC | HSPCON0_RMODE_CC | HSPCON0_RXIE_CC,
       HSPCON0_TXIE_CC,
       HSPSTAT_RDR_CC,
       HSPSTAT_THRE_CC,
       HSPSTAT_BRK_CC,
       HSPSTAT_FER_CC | HSPSTAT_PER_CC | HSPSTAT_OER_CC | HSPSTAT_BRK_CC,
       16,0,
       (WORD)~(PIO1_UTX_CC | PIO1_URX_CC | PIO1_HSURX_CC),
       (WORD)(PIO1_URX_CC | PIO1_HSURX_CC)
      },
      {SPCON_CC,SPSTAT_CC,SPTXD_CC,SPRXD_CC,
       SPBDV_CC,CH11CON_CC,EOI_CC,EOI_UART_CC, CHXCON_SRC_INT_CC,
	   CHXCON_SRC_INT_CC+CHXCON_MSK,
       0,
       SPCON0_TMODE_CC | SPCON0_RMODE_CC,
       SPCON0_TMODE_CC | SPCON0_RMODE_CC | SPCON0_RXIE_CC,
       SPCON0_TXIE_CC,
       SPSTAT_RDR_CC,
       SPSTAT_THRE_CC,
       SPSTAT_BRK_CC,
       SPSTAT_FER_CC | SPSTAT_PER_CC | SPSTAT_OER_CC | SPSTAT_BRK_CC,
       16,0,
       (WORD)~(PIO1_UTX_CC | PIO1_URX_CC | PIO1_HSURX_CC),
       (WORD)(PIO1_URX_CC)
      }

      };

#endif

STATIC DWORD AssumedCPUSpeed;

//
//  Data for initial display of "Cylon Eye" pattern on the LEDs
//
STATIC ROMBYTE rbLedMap[PATTERN_LEN] = {
   0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80,
   0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01,
         0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80,
   0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01, 0x00
};

STATIC SerPort ROM * Sprt = &EM_Serial;   // Serial port pointer
STATIC WORD SprtMax= 1;                   // Total number of serial ports
STATIC WORD SprtIndex = 0;                // Index into current port

/////////////////////////////////////////////////////////////////////////
// GetDefSerial() returns a pointer to the default serial port structure.
//
SerPortPtr GetDefSerial(void)
{
    return Sprt+SprtIndex;
}

/////////////////////////////////////////////////////////////////////////
// SetDefSerial() sets up default serial port values.
//
void SetDefSerial(BOOL FirstTime, WORD PortType, WORD DefPort,
                     DWORD BaudRate, DWORD CpuSpeed)
{
    WORD Index;

    if (FirstTime)
    {

#ifndef	ER

        switch (PortType)
        {
            case ES_SERIAL:
                Sprt = ES_Serial;
                SprtMax = 2;
                SprtIndex = DefPort & 1;
                break;
                
            case CC_SERIAL:
                Sprt = CC_Serial;
                SprtMax = 2;
                SprtIndex = DefPort & 1;
                break;

        }

#endif

        BaudDivisor = (WORD)((CpuSpeed + (Sprt->BaudMConst/2) * BaudRate)
                  / (Sprt->BaudMConst * BaudRate))  - Sprt->BaudSConst;
    }

    // Initialize all serial ports at default baud rate with interrupts off

    for (Index = 0; Index < SprtMax; Index++)
        InitSPRT(Sprt+Index, FALSE);

    // Autobaud calculations are performed on the CPU speed.
    // Validate the speed somewhat, so the user can't easily
    // get completely lost.

    AssumedCPUSpeed = CpuSpeed;
    if (AssumedCPUSpeed <   1000000L)
        AssumedCPUSpeed =   1000000L;
    if (AssumedCPUSpeed > 160000000L)
        AssumedCPUSpeed = 160000000L;

} // SetDefSerial()


/////////////////////////////////////////////////////////////////////////
// SetBaudFromData() is passed a buffer of WORDs of times when
// bit transitions were noticed by the AutoBaud() routine.  It
// analyzes the data, and sets up our baudrate divisor and returns
// TRUE if the data is good.
//
STATIC BOOL SetBaudFromData(SerPortPtr Sprt, LPWORD SrcData)
{
    DWORD Counts[4];
    WORD   i;
    DWORD  CountsPerBit;
    WORD   BitValue = 1;
    WORD   CurBit   = 0;
    BYTE   ch = 0;

    for (i=0;i<4;i++)
    {
        Counts[i] = (SrcData[i+1] -SrcData[0]) & 0xFFFF;
        if ((i>0) && (Counts[i] < Counts[i-1]))
            Counts[i] += 0x10000;
    }

    CountsPerBit = (Counts[3] + 4) / 8;
    if (CountsPerBit == 0)
        return FALSE;            // Disallow divide by 0.

    for (i=0;i<3;i++)
    {
        WORD Test = (WORD)((Counts[i]*8 + CountsPerBit/2)/CountsPerBit);
        WORD NextBit = Test>>3;
        WORD Error   = Test&7;

        // Make sure bit transition is centered within 12.5% (Allow
        // jitter in receiver timing)

        if ((Error > 1) && (Error < 7))
            return FALSE;

        while (CurBit < NextBit)
            ch |= (BYTE)(BitValue << (CurBit++));

        BitValue = 1-BitValue;
    }

    if ((ch != 'a') && (ch != 'A'))
        return FALSE;

    // Counts[3] contains the number of timer ticks in 8 bit
    // times.  Since there are 4 CPU clocks per timer tick,
    // this is 8/4 = 2 times the number of CPU clocks/bit.
    // Use rounding when developing the baud rate divisor.

    BaudDivisor = (WORD) ((Counts[3] + Sprt->BaudMConst)
                   / (Sprt->BaudMConst * 2)
                       - Sprt->BaudSConst);

    return TRUE;
}

// Disable optimization warning
#pragma warning(disable : 4704)

///////////////////////////////////////////////////////////////////////////
// AutoBaud() is always called at boot to determine the preferred
// serial port and baud rate to use.  It may also be called later
// with the ReInitialize parameter set TRUE.
//
BOOL AutoBaud(BOOL ReInitialize)
{
    WORD SavedMaxA,SavedCtrl ,SavedMPCS;
    WORD TicksPerSecond, TicksPerLight;
    BOOL GotData;
    WORD RxPIOBits = Sprt->RxPIOBits;
    WORD ChangedBit;
    WORD Index;
    BOOL LEDtoggle;
    WORD BitCounts[5];
    WORD wBoardLEDPattern[PATTERN_LEN];

    // Disable serial port if we are reinitializing

    if (ReInitialize)
    {
        _disable();
        InitSPRT(Sprt+SprtIndex, FALSE);
    }

#ifndef	ER

    if (IS186CC)
    {
        SavedMaxA      = (WORD)_inpw(T2CMPA_CC);
        SavedCtrl      = (WORD)_inpw(T2CON_CC);
		SavedMPCS 	   = (WORD)_inpw(CS_MPCS);
        TicksPerSecond = (WORD)(AssumedCPUSpeed >> 18); // timer overflows/sec
        TicksPerLight  = (2*TicksPerSecond) / (sizeof rbLedMap);

        GotData = FALSE;

        OutToPCBReg(T2CON_CC, TMR_INH);
        OutToPCBReg(T2CMPA_CC,0);           // Max timer count value
        OutToPCBReg(T2CNT_CC,0);            // Current data
        OutToPCBReg(T2CON_CC, TMR_ENABLE | TMR_INH | TMR_CONT );

        if ((_inpw( RESCON_CC ) & 0x0080)) 
        {
            // Test Interface Port Detected
			_outp(TIP_HEX, 0xAA);
			// Configure IO Space to 16 bits wide
			OutToPCBReg(CS_MPCS, (SavedMPCS & ~MPCS_IOSIZ_CC));

			// While we are performing the baudrate detection, we wish to
			// output a pattern on the LEDs, but cannot afford to take the
			// time to call the display routine.  Instead, we very quickly
			// display the entire pattern, and capture the data values
			// associated with the pattern output.  Later we'll use the
			// captured data to re-output the pattern.

//			for (Index=0; Index < sizeof(wBoardLEDPattern); Index++)
//			{
//			    PostLEDs(rbLedMap[Index]);
//			    wBoardLEDPattern[Index] =(WORD)_inp(TIP_LEDS);
//			}
        }     
    }
    else
    
#endif    
    
    {
        SavedMaxA      = (WORD)_inpw(TMR2_MAXA);
        SavedCtrl      = (WORD)_inpw(TMR2_CTL);
        TicksPerSecond = (WORD)(AssumedCPUSpeed >> 18); // timer overflows/sec
        TicksPerLight  = (2*TicksPerSecond) / (sizeof rbLedMap);

        GotData = FALSE;

        OutToPCBReg(TMR2_CTL, TMR_INH);
        OutToPCBReg(TMR2_MAXA,0);           // Max timer count value
        OutToPCBReg(TMR2_CNT,0);            // Current data
        OutToPCBReg(TMR2_CTL, TMR_ENABLE | TMR_INH | TMR_CONT );

		// While we are performing the baudrate detection, we wish to
		// output a pattern on the LEDs, but cannot afford to take the
		// time to call the display routine.  Instead, we very quickly
		// display the entire pattern, and capture the data values
		// associated with the pattern output.  Later we'll use the
		// captured data to re-output the pattern.

		for (Index=0; Index < sizeof(wBoardLEDPattern); Index++)
		{
		    PostLEDs(rbLedMap[Index]);
		    wBoardLEDPattern[Index] =(WORD)_inpw(PIO_DIR0);
		}
    }

    // Only output to the LEDs if the pattern actually changes.

    LEDtoggle = (wBoardLEDPattern[0] != wBoardLEDPattern[1]);

#ifndef ER

    if (IS186CC)
    {
        do {
        _asm {
CC_WaitForStartBit:
            cld
            lea     si, wBoardLEDPattern 
            mov     cx,1 + size rbLedMap
            mov     bx,RxPIOBits
            mov     di,0                  ; Ticks left
            push    ss
            pop     es                    ; For INSW instruction later

    ; The code sequence that checks for a start bit is replicated throughout
    ; the loop so that on a slow processor we don't wait too long between
    ; checks.  Also, we do things like only load DL for I/O instructions,
    ; since we know that all registers are in the same page.

    ; (Once a start bit occurs, we want to get setup to locate the
    ; next high transition as accurately as possible.)

            mov     dh,PIODATA1_CC SHR 8

CC_WaitStartBitLoop:
            mov     dl,PIODATA1_CC AND 0FFh
            in      ax,dx
            and     ax,bx                   ; Wait until one of the bits
            cmp     ax,bx                   ; we are looking at goes low
            jnz     CC_GotStartBit

            mov     dl,T2CON_CC AND 0FFh
            in      ax,dx
            test    al,TMR_MC
            jz      CC_WaitStartBitLoop
            and     ax, NOT (TMR_INH + TMR_MC)
            out     dx,al

            mov     dl,PIODATA1_CC AND 0FFh
            in      ax,dx
            and     ax,bx                   ; Wait until one of the bits
            cmp     ax,bx                   ; we are looking at goes low
            jnz     CC_GotStartBit

            dec     di
            jns     CC_WaitStartBitLoop
            mov     di,TicksPerLight
            dec     cx
            jz      CC_NoCharPressed

            in      ax,dx
            and     ax,bx                   ; Wait until one of the bits
            cmp     ax,bx                   ; we are looking at goes low
            jnz     CC_GotStartBit

            cmp     LEDtoggle,0
            jz      CC_WaitStartBitLoop

            in      ax,dx
            and     ax,bx                   ; Wait until one of the bits
            cmp     ax,bx                   ; we are looking at goes low
            jnz     CC_GotStartBit

            mov     dx, TIP_LEDS_H			; TIP_LEDS
            mov     ax,ss:[si]
            add     si, 2
            out     dx,al

            mov     dx,PIODATA1_CC
            in      ax,dx
            and     ax,bx                   ; Wait until one of the bits
            cmp     ax,bx                   ; we are looking at goes low
            jz      CC_WaitStartBitLoop

CC_GotStartBit:
            xor     ax,bx                   ; Set high the one which went low
            mov     si,ax                   ; Store changed bit mask in SI

    /*
        The maximum time required to wait depends on CPU speed and
        baud rate.  The max CPU speed is 80MHz, and the minimum
        supported baud rate is 2400.

        Wait Time =  80000000 cycles/sec * 1 sec/2400 bits * 8 bits/char
                  =  266666.67 CPU cycles

        To compensate for jitter, add 25%: 333333 cycles

        A loop with 3+4+10+3+4+13 cycles = 37 cycles must be executed
                333333/37 = 9000 times to satisfy this requirement.

        If the CPU speed is slowed to 1MHz, this loop could occupy
        1/3 second.
    */
            mov     cx,9000
            lea     di,BitCounts
            xor     bx,bx
CC_EdgeLoop:
            dec     cx
            jz      CC_WaitForStartBit             ; Loop until we get an edge
            in      ax,dx
            xor     ax,bx
            and     ax,si
            jz      CC_EdgeLoop
CC_GotEdge:
            mov     dl,T2CNT_CC AND 0FFh
            insw
            not     bx
            lea     ax,BitCounts + (size BitCounts)
            cmp     di,ax
            jae     CC_ExitEdgeLoop

            mov     dl,PIODATA1_CC AND 0FFh
            in      ax,dx
            xor     ax,bx
            and     ax,si
            jnz     CC_GotEdge
            jmp     CC_EdgeLoop

CC_ExitEdgeLoop:
            mov     GotData,TRUE
            mov     ChangedBit,si
CC_NoCharPressed:
            }
            if (!GotData)
                break;

            GotData = SetBaudFromData(Sprt,BitCounts);
        } while (!GotData);


        if (GotData && (SprtMax > 1))
            SprtIndex = (WORD)((ChangedBit == Sprt[1].RxPIOBits) ? 1 : 0);
        
        OutToPCBReg(T2CON_CC,TMR_INH);
        OutToPCBReg(T2CON_CC,TMR_INH | SavedCtrl);  // Max timer count value
        OutToPCBReg(T2CNT_CC,0);                    // Current data
        OutToPCBReg(T2CMPA_CC,SavedMaxA);           // Max timer count value
        OutToPCBReg(CS_MPCS,SavedMPCS);           	// MPCS Chip select config

        _inpw( Sprt[SprtIndex].RxAddr);
        OutToPCBReg(Sprt[SprtIndex].StatAddr, 0);

        if (IS_TIP_ATTACHED) 
            _outpw(TIP_HEX, 0xcc86);
    }
    else  // NOT 186CC
    
#endif

    {
    do {
    _asm {
WaitForStartBit:
        cld
        lea     si, wBoardLEDPattern 
        mov     cx,1 + size rbLedMap
        mov     bx,RxPIOBits
        mov     di,0                  ; Ticks left
        push    ss
        pop     es                    ; For INSW instruction later

; The code sequence that checks for a start bit is replicated throughout
; the loop so that on a slow processor we don't wait too long between
; checks.  Also, we do things like only load DL for I/O instructions,
; since we know that all registers are in the same page.

; (Once a start bit occurs, we want to get setup to locate the
; next high transition as accurately as possible.)

        mov     dh,PIO_DATA1 SHR 8

WaitStartBitLoop:
        mov     dl,PIO_DATA1 AND 0FFh
        in      ax,dx
        and     ax,bx                   ; Wait until one of the bits
        cmp     ax,bx                   ; we are looking at goes low
        jnz     GotStartBit

        mov     dl,TMR2_CTL AND 0FFh
        in      ax,dx
        test    al,TMR_MC
        jz      WaitStartBitLoop
        and     ax, NOT (TMR_INH + TMR_MC)
        out     dx,al

        mov     dl,PIO_DATA1 AND 0FFh
        in      ax,dx
        and     ax,bx                   ; Wait until one of the bits
        cmp     ax,bx                   ; we are looking at goes low
        jnz     GotStartBit

        dec     di
        jns     WaitStartBitLoop
        mov     di,TicksPerLight
        dec     cx
        jz      NoCharPressed

        in      ax,dx
        and     ax,bx                   ; Wait until one of the bits
        cmp     ax,bx                   ; we are looking at goes low
        jnz     GotStartBit

        cmp     LEDtoggle,0
        jz      WaitStartBitLoop

        in      ax,dx
        and     ax,bx                   ; Wait until one of the bits
        cmp     ax,bx                   ; we are looking at goes low
        jnz     GotStartBit

        mov     dl,PIO_DIR0 AND 0FFh    ; Update LEDs from the pattern
        mov     ax,ss:[si]
        add     si, 2
        out     dx,al

        mov     dl,PIO_DATA1 AND 0FFh
        in      ax,dx
        and     ax,bx                   ; Wait until one of the bits
        cmp     ax,bx                   ; we are looking at goes low
        jz      WaitStartBitLoop

GotStartBit:
        xor     ax,bx                   ; Set high the one which went low
        mov     si,ax                   ; Store changed bit mask in SI

/*
    The maximum time required to wait depends on CPU speed and
    baud rate.  The max CPU speed is 80MHz, and the minimum
    supported baud rate is 2400.

    Wait Time =  80000000 cycles/sec * 1 sec/2400 bits * 8 bits/char
              =  266666.67 CPU cycles

    To compensate for jitter, add 25%: 333333 cycles

    A loop with 3+4+10+3+4+13 cycles = 37 cycles must be executed
            333333/37 = 9000 times to satisfy this requirement.

    If the CPU speed is slowed to 1MHz, this loop could occupy
    1/3 second.
*/
        mov     cx,9000
        lea     di,BitCounts
        xor     bx,bx
EdgeLoop:
        dec     cx
        jz      WaitForStartBit             ; Loop until we get an edge
        in      ax,dx
        xor     ax,bx
        and     ax,si
        jz      EdgeLoop
GotEdge:
        mov     dl,TMR2_CNT AND 0FFh
        insw
        not     bx
        lea     ax,BitCounts + (size BitCounts)
        cmp     di,ax
        jae     ExitEdgeLoop

        mov     dl,PIO_DATA1 AND 0FFh
        in      ax,dx
        xor     ax,bx
        and     ax,si
        jnz     GotEdge
        jmp     EdgeLoop

ExitEdgeLoop:
        mov     GotData,TRUE
        mov     ChangedBit,si
NoCharPressed:
        }
        if (!GotData)
            break;

        GotData = SetBaudFromData(Sprt,BitCounts);
    } while (!GotData);

    if (GotData && (SprtMax > 1))
        SprtIndex = (WORD)((ChangedBit == Sprt[1].RxPIOBits) ? 1 : 0);

    OutToPCBReg( TMR2_CTL, TMR_INH);
    OutToPCBReg(TMR2_CTL,TMR_INH | SavedCtrl);  // Max timer count value
    OutToPCBReg(TMR2_CNT,0);                    // Current data
    OutToPCBReg(TMR2_MAXA,SavedMaxA);           // Max timer count value

    _inpw( Sprt[SprtIndex].RxAddr);
    OutToPCBReg(Sprt[SprtIndex].StatAddr, 0);
    }
    if (ReInitialize)
    {
        if (GotData)
            InitSPRT(Sprt+SprtIndex, TRUE);
        _enable();
    }

    return GotData;
}
