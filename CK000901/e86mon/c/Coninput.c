/******************************************************************************
 *                                                                            *
 *     CONINPUT.C                                                             *
 *                                                                            *
 *     This file contains console input routines, both for line-oriented      *
 *     input, and for getting a yes/no response to a query.                   *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "emon.h"

//
//    static data storage
//
static ROMCHAR beep[]   = {ASCII_BELL,0};
static ROMCHAR backup[] = {ASCII_BS,' ',ASCII_BS,0};

//////////////////////////////////////////////////////////////////////////
//   InputLEDPattern() -- shows a unique pattern on LEDs when waiting
//
void InputLEDPattern(BOOL Stop)
{
    static BOOL Running  = FALSE;
    static BYTE LEDCode;

    if (!Running)
    {
        Running = TRUE;
        LEDCode = InquireLEDs();
    }

    if (Stop)
    {
        PostLEDs(LEDCode);
        Running = FALSE;
        return;
    }

    PostLEDs( (BYTE)(1 << (( (EGD.Milliseconds) >> 8) & 7)));
}

//////////////////////////////////////////////////////////////////////////
//   GetYNResponse() - Gets a Y or N response from the user.
//
BOOL Base_GetYNResponse(LPCSTR Prompt, BOOL LeadingCRLF)
{
    WORD response;
    BOOL Value;

    for ( ;; )
    {
        EGD.MonitorExited  = FALSE;
        if (EGD.LineUnfinished || LeadingCRLF)
            PrintCRLF();

        printf(ConsoleMsgYN,Prompt);
        FlushReceiver();

        while (!EGD.MonitorExited && !kbhit())
            InputLEDPattern(FALSE);
        if (EGD.MonitorExited)
        {
            printf(ConsoleMsgReturnedFromBreak);
            continue;
        }

        response = tolower(getch());
        Value = (response == 'y');
        if (!Value && (response != 'n'))
        {
            printf(beep);
            continue;
        }
        printf(SystemMsgCharCRLF,response);
        InputLEDPattern(TRUE);
        FlushTransmitter();
        return Value;
    };
} // GetYNResponse()


//////////////////////////////////////////////////////////////////////////
//    GetInputLine() -- Scan in a line from the user and place it
//                      into InputLine.
//
BOOL Base_GetInputLine(LPCSTR Prompt, BOOL echo )
{

    BOOL gotline;
    BOOL FirstTime;
    WORD MaxLine;
    BYTE newchar;
    BOOL done;

    MaxLine = 79;
    if (echo)
    {
        EGD.PromptLength = (WORD)emon_strlen(Prompt);
        MaxLine     -= EGD.PromptLength;
    }

    gotline           = FALSE;
    FirstTime         = TRUE;
    EGD.LineIndex     = 0;
    EGD.MonitorExited = TRUE;
    done              = FALSE;

    while (!done) {
        if (echo)
        {
            while (!EGD.MonitorExited && !kbhit())
                InputLEDPattern(FALSE);

            if (EGD.MonitorExited)
            {
                EGD.MonitorExited = FALSE;

                if (EGD.LineUnfinished)
                    PrintCRLF();

                if (!FirstTime)
                    printf(ConsoleMsgReturnedFromBreak);
                FirstTime = FALSE;
                printf(Prompt);
                EGD.LineIndex = 0;
                continue;
            }
        }
        // Get a character
        newchar = (BYTE)(getch() & 0x7F);
        switch (newchar) {
            case ASCII_CR:
            case ASCII_LF:
                EGD.InputLine[EGD.LineIndex] = 0;
                if (echo)
                    PrintCRLF();
                done = TRUE;
                gotline = TRUE;
                break;
            case ASCII_BS:
                if (EGD.LineIndex > 0) {
                    // erase the previous character
                    EGD.LineIndex--;
                    if (echo)
                        printf(backup);
                } // if
                break;
            default:
                if (newchar == ASCII_TAB)
                    newchar = ' ';
                if (newchar >= ' ') {
                    if (EGD.LineIndex < MaxLine)
                    {
                        EGD.InputLine[EGD.LineIndex++] = newchar;
                        if (echo)
                            printf("%c",newchar);
                    }
                    else
                        printf(beep);
                } else {
                        printf(SystemMsgCancel,newchar);
                    EGD.InputLine[0] = 0;
                    done = TRUE;
                    gotline = FALSE;
                } // else
                break;
        } // switch
    } // while

    EGD.LineIndex = EGD.ErrorIndex = 0;
    if (echo)
        InputLEDPattern(TRUE);
    return gotline;
} // GetInputLine()
