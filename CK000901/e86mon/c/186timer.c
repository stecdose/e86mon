/******************************************************************************
 *                                                                            *
 *     186TIMER.C                                                             *
 *                                                                            *
 *     This file contains platform-dependent timer code.                      *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996-1997 Advanced Micro Devices, Inc.                           *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#include "emon.h"
#include "am186msr.h"
#include "186init.h"
#include "isrthunk.h"

// The DOS timer interrupts 18.2 times per second.  We get better
// precision if we use an even divisor of the clock frequency,
// so we program it for 20 times/second.

#define  TICKS_PER_SECOND      20

STATIC BOOL TimerIntFlag;

static DWORD CpuSpeedDiv4000;
static WORD timer2Div = 0;
static WORD timer1Div = 0;
static DWORD msFrac;
static DWORD totalInc = 0;
static DWORD timer1Inc = 0;

///////////////////////////////////////////////////////////////////////////
//    Irq12Entry() -- service timer1 interrupt EM ES ER ED
//
void __far cdecl Irq12Entry(void)
{
    // Allow user programs to change the ticks/second, and
    // keep up with the time if possible.

    if ((timer2Div != (WORD)_inpw(TMR2_MAXA) ) ||
        (timer1Div != (WORD)_inpw(TMR1_MAXA)) )
    {
        timer2Div = (WORD)_inpw(TMR2_MAXA);
        timer1Div = (WORD)_inpw(TMR1_MAXA);

        timer1Inc = 1L + (WORD)(timer2Div-1);
        timer1Inc = ( (timer1Inc << 15) / CpuSpeedDiv4000) * 2;
        totalInc = 1L + (WORD)(timer1Div-1);
        totalInc*= timer1Inc;
    }
    msFrac += totalInc;
    EGD.Milliseconds += (msFrac >> 16);
    msFrac &= 0xFFFF;

    TimerIntFlag = TRUE;
    OutToPCBReg( TMR1_CTL, (WORD)_inpw( TMR1_CTL ) & ~(TMR_INH | TMR_MC) );
    OutToPCBReg( INT_EOI, EOITYPE_TMR1 );
}

#ifndef ER

///////////////////////////////////////////////////////////////////////////
//    Irq9Entry() -- service CC timer1 interrupt
//
void __far cdecl Irq9Entry(void)
{
    // Allow user programs to change the ticks/second, and
    // keep up with the time if possible.

    if ((timer2Div != (WORD)_inpw(T2CMPA_CC) ) ||
        (timer1Div != (WORD)_inpw(T1CMPA_CC)) )
    {
        timer2Div = (WORD)_inpw(T2CMPA_CC);
        timer1Div = (WORD)_inpw(T1CMPA_CC);

        timer1Inc = 1L + (WORD)(timer2Div-1);
        timer1Inc = ( (timer1Inc << 15) / CpuSpeedDiv4000) * 2;
        totalInc = 1L + (WORD)(timer1Div-1);
        totalInc*= timer1Inc;
    }
    msFrac += totalInc;
    EGD.Milliseconds += (msFrac >> 16);
    msFrac &= 0xFFFF;

    TimerIntFlag = TRUE;
    OutToPCBReg( T1CON_CC, (WORD)_inpw( T1CON_CC ) & ~(TMR_INH | TMR_MC) );
    OutToPCBReg( EOI_CC, EOI_T1_CC );
}

#endif

///////////////////////////////////////////////////////////////////////////
// TimerInterrupted() returns TRUE if the timer interrupted since the
// last time it was called.  It is used to help gain greater precision
// in getting the time.
//
BOOL TimerInterrupted(void)
{
    BOOL temp = TimerIntFlag;
    TimerIntFlag = FALSE;
    return temp;
}

///////////////////////////////////////////////////////////////////////////
// EnableTimerInterrupt() turns timer interrupts on.
//
void EnableTimerInterrupt(void)
{

#ifndef	ER

    if (IS186CC)
    {
        ISRTHUNK(Irq9Entry);

        SetISRVector(0x9,&Thunk_Irq9Entry);

        OutToPCBReg( EOI_CC, EOI_T1_CC );
        OutToPCBReg( CH0CON_CC, CHXCON_PR_7_CC);   // enable timer, low priority
        OutToPCBReg( T1CON_CC,
                   (WORD)_inpw( T1CON_CC ) & ~(TMR_INH | TMR_MC) | TMR_INT);

    }
    else

#endif

    {
        ISRTHUNK(Irq12Entry);

        SetISRVector(0x12,&Thunk_Irq12Entry);

        OutToPCBReg( INT_EOI, EOITYPE_TMR1 );
        OutToPCBReg( INT_TMR, IMASK_ON | 7);   // enable timer, low priority
        OutToPCBReg( TMR1_CTL,
                   (WORD)_inpw( TMR1_CTL ) & ~(TMR_INH | TMR_MC) | TMR_INT);
    }
}

///////////////////////////////////////////////////////////////////////////
// InitTimer sets up the timers for the correct mode and period
//
void InitTimer(BOOL FirstTime, DWORD CpuSpeed)
{
    CpuSpeedDiv4000 = CpuSpeed/4000;

    if (FirstTime)
    {
        EGD.Milliseconds = 0L;
        msFrac = 0;
    }

    // program timer for 20Hz, t2 := 1000Hz scalar

    timer1Div = 0;   // Force interrupt proc to recalculate

    // Always program the count after max, in case the user program
    // left count > max.  Especially for timer 1, we could count for
    // a long time before we come back around.

#ifndef	ER

    if (IS186CC)
    {
        OutToPCBReg( T2CMPA_CC, (WORD)(CpuSpeedDiv4000) );
        OutToPCBReg( T2CNT_CC,  0 );
        OutToPCBReg( T2CON_CC, TMR_ENABLE | TMR_INH | TMR_CONT );
        OutToPCBReg( T1CMPA_CC, 1000 / TICKS_PER_SECOND );
        OutToPCBReg( T1CNT_CC,  0 );
        OutToPCBReg( T1CON_CC, TMR_ENABLE | TMR_INH | TMR_2PRES | TMR_CONT);
        OutToPCBReg( CH0CON_CC, CHXCON_MSK_OFF_CC);  // disable timer ints
    }
    else

#endif

    {
        OutToPCBReg( TMR2_MAXA, (WORD)(CpuSpeedDiv4000) );
        OutToPCBReg( TMR2_CNT,  0 );
        OutToPCBReg( TMR2_CTL, TMR_ENABLE | TMR_INH | TMR_CONT );
        OutToPCBReg( TMR1_MAXA, 1000 / TICKS_PER_SECOND );
        OutToPCBReg( TMR1_CNT,  0 );
        OutToPCBReg( TMR1_CTL, TMR_ENABLE | TMR_INH | TMR_2PRES | TMR_CONT);
        OutToPCBReg( INT_TMR, IMASK_OFF);  // disable timer ints
    }



}

///////////////////////////////////////////////////////////////////////////
// GetCurrentTime() gets the current time in milliseconds
//
void Base_GetCurrentTime(LPDWORD Milliseconds)
{
    DWORD basetime;
    DWORD basefrac;
    WORD  increment;

    // Make sure we get concurrent values from both timers by
    // getting them again if timer 1 overflows and causes an interrupt.
    // This is more secure than disabling interrupts, because the
    // timer will keep going even if interrupts are disabled.

#ifndef	ER

    if (IS186CC)
    {
        do {
            basetime   = EGD.Milliseconds;
            basefrac   = msFrac;
            increment  = (WORD)_inpw( T1CNT_CC );
        } while (TimerInterrupted());

        if (increment >=  (WORD)_inpw(T1CMPA_CC))
        {
            increment = 0;
            OutToPCBReg( T1CNT_CC,  0);
        }
    }
    else

#endif

    {
        do {
            basetime   = EGD.Milliseconds;
            basefrac   = msFrac;
            increment  = (WORD)_inpw( TMR1_CNT );
        } while (TimerInterrupted());

        if (increment >=  (WORD)_inpw(TMR1_MAXA))
        {
            increment = 0;
            OutToPCBReg( TMR1_CNT,  0);
        }
    }
    *Milliseconds = (((increment * timer1Inc) + basefrac) >> 16) + basetime;
}
