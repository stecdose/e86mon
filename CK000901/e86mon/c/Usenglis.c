/******************************************************************************
 *                                                                            *
 *     USENGLIS.C                                                             *
 *                                                                            *
 *     This file instantiates  U.S. English language messages for             *
 *     EMON, simply by #defining _usenglish_c.                                *
 *                                                                            *
 *     To instantiate a different language, copy MESSAGE.H into               *
 *     YOURLANGUAGE.C, modify the macros and the messages,                    *
 *     and then modify the makefile to use the new language file.             *
 *                                                                            *
 *     This approach was taken so that the rest of the sources do             *
 *     not need to be modified to change the language (simply link            *
 *     with a different language file), while making MESSAGE.H                *
 *     self-documenting as far as what the actual message values are.         *                                                        
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#define  _usenglish_c_

#include "emon.h"
