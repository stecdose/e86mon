/******************************************************************************
 *                                                                            *
 *     PERMVARS.C                                                             *
 *                                                                            *
 *     This file contains code which allows the user to set and               *
 *     retrieve "permanent" variables from the flash.  These are stored       *
 *     downward from FFF00, until we run out of room.                         *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "emon.h"
#include "permvars.h"

#define SEGMENT 0xF000
#define FIRST_ADDRESS 0xFF00

static WORD MaxVar = 0;
static WORD NextAddress = FIRST_ADDRESS;
static WORD MinAddress = FIRST_ADDRESS;

static LPBYTE FlashArray = MK_FP(SEGMENT,0x0000);


///////////////////////////////////////////////////////////////////////////
// GetInitialValue() is called to get the stored value of a permanent
// variable.  It retrieves the initial ROM value of a variable, and
// overrides it with the 'Upgrade', if the Upgrade has been burned
// to other than FFFFFFFF.  It does not go rummaging through 'FlashArray'
// to find if it has been overridden more than once.  Use GetVarValue
// to do that.
//
STATIC DWORD GetInitialValue(LPPERM Var)
{
    // Get the initial ROM copy, not the RAM copy made from it.

    DWORD Value = *(LPDWORD)MK_FP(_FP_SEG(Var),Var->Ptr);

    // If Upgrade was burned in, get it

    if (Var->Upgrade != 0xFFFFFFFF)
        Value = Var->Upgrade;

    return Value;
}

///////////////////////////////////////////////////////////////////////////
// GetVarValue() is called to get the stored value of a permanent
// variable.  Since we may be running from an updating monitor
// as opposed to the boot monitor, it can do a lot of searching before
// it settles on the "correct" value.
//
STATIC DWORD GetVarValue(WORD index)
{
    WORD   Address;
    WORD   OldCS             = (WORD)(GetFlashBootBase() >> 4);
    LPWORD OldSignature      = MK_FP(OldCS,0);
    LPPERM OldArray          = MK_FP(OldCS,OldSignature[6]);
    LPWORD UpdatingSignature = MK_FP(OriginalCS(),0);
    LPPERM UpdatingArray     = PermArray+index;
    DWORD  Value             = GetInitialValue(UpdatingArray);

    // If someone screwed up our pointer mechanism, just return

    if (((WORD)&PermArray != UpdatingSignature[6]))
        return Value;


    // If we are an updating monitor, try to get the value
    // out of the boot monitor.  Also, get the index to
    // search with, because the boot monitor's variables
    // may be in a different arrangement.

    if (_FP_SEG(OldSignature) != _FP_SEG(UpdatingSignature))
    {
        // If the old monitor doesn't use the same scheme,
        // just return.

        if (emon_memcmp(OldSignature,UpdatingSignature,12) != 0)
                return Value;

        // Try to find the old monitor index which matches ours.
        // If we don't find it, we can just return, because there
        // must not be any matching value in the boot monitor.

        for(index = 0 ; ; index++,OldArray++)
            if (OldArray->Name == 0)
                if (index == 0)
                    continue;
                else
                    return Value;
            else if (emon_strcmp(MK_FP(OldCS,OldArray->Name),
                                    UpdatingArray->Name) == 0)
                break;
        
        // Now we have a good array and a good pointer

        Value = GetInitialValue(OldArray);
    }

    // See if the user modified the variable value.  Pick up the
    // latest version if so.

    Address = FIRST_ADDRESS;
    while ((Address -= 5) > NextAddress)
        if (FlashArray[Address] == index)
            Value = *(LPDWORD)(FlashArray+Address+1);

    return Value;
}
    
///////////////////////////////////////////////////////////////////////////
// InitPermVars() is called once, at system startup time.
// It initializes permanent vars from the user's settings,
// and also determines where we can store subsequently changed vars.
//
void InitPermVars(void)
{
    for(MinAddress = NextAddress = FIRST_ADDRESS-5;
               FlashArray[NextAddress] != 0xFF;
                        NextAddress -= 5)
        MinAddress = NextAddress;

    while (FlashArray[--MinAddress] == 0xFF);

    MinAddress += 64;

    // Update the variable with the user values.

    for (MaxVar = 1; PermArray[MaxVar].Name; MaxVar++)
        PermArray[MaxVar].Ptr->RamCopy->RamValue = GetVarValue(MaxVar);
 }


///////////////////////////////////////////////////////////////////////////
// ShowPermVar() displays a given permanent variable in the correct format.
//
void ShowPermVar(WORD index)
{
    LPPERM Perm = PermArray+index;

    switch (Perm->Ptr->RamCopy->DisplayType)
    {
        case DISPLAY_DECIMAL:
            printf(PermMsgVarDecimal,
                    (LPCSTR)Perm->Name, Perm->Ptr->RamCopy->RamValue);
            break;

        case DISPLAY_DWORD:
            printf(PermMsgVarHexDword,
                    (LPCSTR)Perm->Name, Perm->Ptr->RamCopy->RamValue);
            break;

        case DISPLAY_WORD:
            printf(PermMsgVarHexWord,
                    (LPCSTR)Perm->Name, Perm->Ptr->RamCopy->RamValue);
            break;
    }
}

///////////////////////////////////////////////////////////////////////////
// GetNewValue() gets a new value from the user for a permanent variable
//
BOOL GetNewValue(WORD index, LPDWORD Value)
{
    switch (PermArray[index].Ptr->RamCopy->DisplayType)
    {
        case DISPLAY_DECIMAL:
            if (ScanDecimal(Value))
                return TRUE;
            break;

        case DISPLAY_DWORD:
            if (ScanDWord(Value))
                return TRUE;
            break;

        case DISPLAY_WORD:
            *Value = 0;
            if (ScanWord((LPWORD)Value))
                return TRUE;
            break;

        default:
            PermArray[index].Ptr->RamCopy->AllowChange = FALSE;            
    }
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////
// GetPermByName() retrieves an index of a permanent variable, given
// its name.
//
WORD GetPermByName(LPSTR String)
{
    WORD index;

    for (index=1 ; ; index++)
        if (PermArray[index].Name == 0)
            return 0;
        else if (emon_strcmp(PermArray[index].Name,String) == 0)
            return index;
}

///////////////////////////////////////////////////////////////////////////
// CommandPermVar() is invoked via the 'P' command.  It shows the user
// the current permanent variable setting, and allows setting of
// new values.
//
// PSTR coercion is used to show the compiler we know we are really
// on the right stack (SS=SP) when this routine is called.
//
void CommandPermVar(void)
{
    char String[20];
    DWORD Value;
    WORD i;
    BYTE ProgramValue[5];

    // If 'P' command was given with no parameters, just print
    // the list of variables and current values.

    if (ScanEOL(FALSE))
    {
        for (i=1;i<MaxVar;i++)
            ShowPermVar(i);
        return;
    }

    // If we're not the boot monitor, we can't update because the
    // index value for the variable may have changed between monitor
    // revisions.

    if (CurrentCS() != GetFlashBootBase() >> 4)
    {
        PrintError(PermMsgMustBeBootMonitor);
        return;
    }

    // If we don't get the exact syntax we want, just return

    if (!ScanString(String,sizeof(String)))
        return;

    // Look for the variable.  Just print an error and return if
    // we don't find it.

    i = GetPermByName(String);

    if (i == 0)
    {
            PrintError(PermMsgVarNotFound,(LPCSTR)String);
            return;
    }

    if (ScanEOL(FALSE))
    {
        ShowPermVar(i);
        return;
    }

    if (!GetNewValue(i,&Value) || !ScanEOL(TRUE))
        return;

    if (!PermArray[i].Ptr->RamCopy->AllowChange)
    {
        PrintError(PermMsgCantChange);
        return;
    }

    ShowPermVar(i);

    // See if they just want to test the change.  If so, reboot
    // with the new variable.

    if (GetYNResponse(PermMsgTestFirst,TRUE))
    {
        PermArray[i].Ptr->RamCopy->RamValue = Value;
        printf(PermMsgRebooting);
        FlushAndRestart(RESTART_RE_AUTOBAUD);
    }

    // See if they want to make it permanent.  Return if not.

    printf(PermMsgAreYouSure,(LPCSTR)String,Value);
    if (!GetYNResponse(SystemMsgNull,FALSE))
        return;

    // Store the new value.

    PermArray[i].Ptr->RamCopy->RamValue = Value;

    // If we're out of room, just return

    if (MinAddress > NextAddress)
    {
        PrintError(PermMsgOutOfRoom);
        return;
    }

    // If it's already what we want, just return

    if (PermArray[i].Ptr->RamCopy->RamValue == GetVarValue(i))
        return;

    // Make sure we're running out of RAM, and then program it.

    RunSystemFromRAM(TRUE);

    // For now, we simply store the new value, occupying a new
    // 5 bytes of storage, no matter what.  In the future, we
    // can burn the index down to 0 in some cases.  For simple
    // booleans, this will allow us to halve the number of
    // writes we do to the permanent variable storage area.

    ProgramValue[0] = (BYTE)i;
    *((LPDWORD)(ProgramValue + 1)) = Value;

    if (!ProgramFlash(SegToLin(FlashArray+NextAddress),
                      (PBYTE)(WORD)(DWORD)&ProgramValue,5))
        return;

    NextAddress = NextAddress - 5;
    printf(PermMsgVarUpdated);
}

///////////////////////////////////////////////////////////////////////////
// MigratePermVars() is called from the flash update routine to
// migrate permanent variable values from the old monitor to the
// new one.  It is called twice:  first with TestOnly TRUE, and
// then with TestOnly FALSE to actually do the work.
//
BOOL MigratePermVars(BOOL TestOnly, BOOL Reflashulate)
{
    WORD Index;
    LPPERM Perm;

    for (Index = 1, Perm = PermArray+1; Index < MaxVar; Index++, Perm++)
        if (Perm->Ptr->RamCopy->RamValue != GetVarValue(Index))
            return PrintError(PermMsgCurrentMismatch,(LPCSTR)Perm->Name);

        else if (Perm->Ptr->RamCopy->RamValue == GetInitialValue(Perm))
            continue;

        else if (TestOnly)
            if (Reflashulate || (Perm->Upgrade == 0xFFFFFFFF))
                continue;
            else
                return PrintError(PermMsgCannotFixDefault,(LPCSTR)Perm->Name);

        else if (Reflashulate)
            Perm->Upgrade = Perm->Ptr->RamCopy->RamValue;
        else
        {
            DWORD ProgAddr = SegToLin(MK_FP(OriginalCS(),
                                        (WORD)(DWORD)&(Perm->Upgrade)));
            LPDWORD SrcAddr  = &Perm->Ptr->RamCopy->RamValue;

            if (!ProgramFlash(ProgAddr,(LPBYTE)SrcAddr,4))
                return FALSE;
        }

    return TRUE;
}
