/******************************************************************************
 *                                                                            *
 *     SCANF.C                                                                *
 *                                                                            *
 *     This file contains discrete input line scanning routines to            *
 *     scan for the kinds of data most frequently required by the             *
 *     monitor from the user.                                                 *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "emon.h"

////////////////////////////////////////////////////////////////////////////
//    ScanDelimiters -- munch on spaces and commas
//
void Base_ScanDelimiters(WORD Which)
{
    for (;;EGD.LineIndex++)
    {

        switch (EGD.InputLine[EGD.LineIndex])
        {
            case ' ':
                if ((Which&SCAN_SPACES) != 0)
                    continue;
                break;
            case '=':
                if ((Which&SCAN_EQUALS) != 0)
                    continue;
                break;
            case ',':
                if ((Which&SCAN_COMMAS) != 0)
                    continue;
                break;
        }
        break;
    }
}

////////////////////////////////////////////////////////////////////////////
//    ScanOption() searches for an option character
//
BOOL Base_ScanOption(WORD Delimiters, BYTE name)
{
    ScanDelimiters(Delimiters);

    if (SampleInputLine(TRUE) == name)
    {
        EGD.LineIndex++;
        return TRUE;
    }
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////
//    ScanEOL() searches for the end of the line
//
BOOL Base_ScanEOL(BOOL ErrorIfNot)
{
    ScanDelimiters(SCAN_SPACES);
    EGD.ErrorIndex = EGD.LineIndex;
    if (SampleInputLine(FALSE) == 0)
        return TRUE;
    if (ErrorIfNot)
        ScanError(ConsoleMsgEndOfLine);
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////
//    ScanString() assembles a string
//
BOOL Base_ScanString(LPSTR  s,WORD len)
{
    WORD i;
    char ch;

    EGD.ErrorIndex = EGD.LineIndex;

    ScanDelimiters(SCAN_SPACES|SCAN_COMMAS);

    i = 0;

    for (i=0; i< len-1; i++)
    {
        ch = SampleInputLine(TRUE);
        if ((ch == 0) || (ch == ' '))
            break;
        s[i] = ch;
        EGD.LineIndex++;
    }
    s[i] = 0;

    if (i != 0)
        return TRUE;

    return ScanError(ConsoleMsgString);
}

////////////////////////////////////////////////////////////////////////////
//    ScanHexDigits() assembles an arbitrary number of digits into a number
//
WORD Base_ScanHexDigits(LPDWORD result,WORD base)
{
    char  CurOp  = '+';
    WORD  DigitsRead = 0;
    WORD  MaxDigitsRead = 0;
    DWORD Value;
    WORD  NumOps = 0;
    BYTE  HadOp = FALSE;

    ScanDelimiters(SCAN_SPACES|SCAN_COMMAS);

    *result = 0;

    for(;;)
    {
        if ((DigitsRead = GetRegValue(&Value)) == 0)
        {
            Value = 0;

            EGD.ErrorIndex = EGD.LineIndex;

            for(;;)
            {
                WORD New = tonum(SampleInputLine(FALSE));
                if (New > base)
                    break;
                DigitsRead++;
                EGD.LineIndex++;
                Value = Value * base + New;
            }
        }
        switch (CurOp)
        {
            case '*':  *result *= Value;
                       break;
            case '+':  *result += Value;
                       break;
            case '-':  *result -= Value;
                       break;
        }
        CurOp = SampleInputLine(FALSE);

        if (HadOp)
        {
            if (DigitsRead == 0)
            {
                EGD.IgnoreError = FALSE;
                return (WORD)ScanError(ConsoleMsgNumReg);
            }
            if (CurOp == '*')
            {
                EGD.ErrorIndex++;
                EGD.IgnoreError = FALSE;
                return (WORD)ScanError(ConsoleMsgPlusMinus);
            }
        }

        if (DigitsRead > MaxDigitsRead)
            MaxDigitsRead=DigitsRead;

        if ((CurOp != '*') && (CurOp != '+') && (CurOp != '-'))
            return MaxDigitsRead;

        HadOp = TRUE;
        EGD.LineIndex++;
    }
}

////////////////////////////////////////////////////////////////////////////
//    ScanByte() uses ScanHexDigits() to retrieve a single byte
//
BOOL Base_ScanByte(LPBYTE b)
{
    DWORD d;
    switch (ScanHexDigits(&d,16))
    {
        case 1:
        case 2:
            *b = (BYTE)d;
            return TRUE;
    }
    return ScanError(ConsoleMsgByte);
}

////////////////////////////////////////////////////////////////////////////
//    ScanWord() uses ScanHexDigits() to retrieve a word
//
BOOL Base_ScanWord(LPWORD w)
{
    DWORD d;
    switch (ScanHexDigits(&d,16))
    {
        case 1:
        case 2:
        case 3:
        case 4:
            *w = (WORD)d;
            return TRUE;
    }
    return ScanError(ConsoleMsgWord);
}

////////////////////////////////////////////////////////////////////////////
//    ScanDWord() uses ScanHexDigits() to retrieve a double word
//
BOOL Base_ScanDWord(LPDWORD r)
{
    DWORD d;
    switch (ScanHexDigits(&d,16))
    {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
            *r = d;
            return TRUE;
    }
    return ScanError(ConsoleMsgDWord);
}

////////////////////////////////////////////////////////////////////////////
//    ScanDecimal() uses ScanHexDigits() to retrieve a decimal number
//
BOOL Base_ScanDecimal(LPDWORD r)
{
    DWORD d;
    switch (ScanHexDigits(&d,10))
    {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            *r = d;
            return TRUE;
    }
    return ScanError(ConsoleMsgDecimal);
}

////////////////////////////////////////////////////////////////////////////
//    ScanAddress() scans for an address.  Addresses may take on the
//    following forms:
//        xxxx:xxxx      -- Segmented addresses have four or fewer hex
//                          digits, a colon, and then four or fewer hex
//                          digits.
//        x/xxxxxxx      -- Partitioned addresses have 1 or 2 hex digit
//                          denoting the address space, a slash,
//                          and 1 to 8 hex digits.
//
//        Addresses are usually stored in a linear format in a DWORD.
//        The upper digit of the DWORD specifies the address partition,
//        and the lower 7 digits specify the offset within the partition.
//        Segmented addresses occupy partition 0xF, and have a separate
//        word which specifies a segment base.
//
//        The caller may specify a default segment or partition
//        using the SCAN_DEFADDR flag.
//
BOOL Base_ScanAddress(WORD Flags, FPANYADDRESS clientAddress)
{
    BOOL       ok           = FALSE;
    WORD       SaveIndex    = EGD.LineIndex;
    WORD       SaveIgnore   = EGD.IgnoreError;
    AnyAddress a;

    a = *clientAddress;
    a.Flags = 0;

    if ((Flags & SCAN_RANGELEN) == 0)
    {
        a.MemSpace = 0;
        _FP_SEG(a.SegAddress) = EGD.stack->DS;
    }

    EGD.IgnoreError = TRUE;

    switch (ScanHexDigits(&a.LinearAddress,16))
    {
        case 1:
        case 2:
        case 3:
            if (ScanOption(0,'/'))
            {
                a.MemSpace = (WORD)a.LinearAddress;
                if (!ScanDWord(&a.LinearAddress))
                    break;
                ok = TRUE;
                break;
            }
            // Drop through to look for a colon if no slash

        case 4:
            a.MemSpace = 0;
            if (ScanOption(0,':'))
            {
                _FP_SEG(a.SegAddress) = (WORD)a.LinearAddress;
                EGD.IgnoreError = TRUE;
                if (!ScanDWord(&a.LinearAddress) ||
                   (a.LinearAddress >= 0x10000))
                      break;
                EGD.IgnoreError = FALSE;
            }
            else if ((Flags&(SCAN_DEFADDR|SCAN_RANGELEN)) == 0)
                break;
            _FP_OFF(a.SegAddress) = (WORD)a.LinearAddress;
            a.LinearAddress += (DWORD)(_FP_SEG(a.SegAddress)) << 4;
            ok = TRUE;
            a.Flags=ANY_DISP_SEG;
            break;

        case 5:
        case 6:
        case 7:
        case 8:
            ok = TRUE;
            break;
    }

    EGD.ErrorIndex  = SaveIndex;

    if (ok && ((a.Flags & ANY_DISP_SEG) == 0))
    {
        a.SegAddress = LinToSeg(a.LinearAddress);

        ok = ((Flags & SCAN_1MEG) == 0) ||
               ((a.LinearAddress < 0x100000) && (a.MemSpace == 0));
    }

    EGD.IgnoreError = SaveIgnore;

    if (ok)
    {
        *clientAddress = a;
        return TRUE;
    }

    EGD.LineIndex   = SaveIndex;
    if ((Flags&SCAN_SILENTERR) == 0)
        ScanError(ConsoleMsgAddress);
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////
//    ScanRange() looks for a range in either "address address"
//    or "address L length" format.
//
BOOL Base_ScanRange(WORD Flags, FPANYADDRESS a, LPDWORD l)
{
    AnyAddress endAddress;

    if (!ScanAddress(Flags, a))
        return FALSE;

    if (ScanOption(SCAN_SPACES|SCAN_COMMAS,'l'))
        return ScanDWord(l);

    if ( ((Flags&SCAN_DEFLEN) != 0) && ScanEOL(FALSE))
        return TRUE;

    endAddress = *a;

    EGD.ErrorIndex = EGD.LineIndex;

    EGD.IgnoreError = TRUE;

    if (!ScanAddress(Flags|SCAN_RANGELEN, &endAddress))
    {
        EGD.IgnoreError = FALSE;
        return ScanError(ConsoleMsgRangeEnd);
    }
    EGD.IgnoreError = FALSE;

    if ((endAddress.MemSpace != a->MemSpace) ||
        (endAddress.LinearAddress <= a->LinearAddress))
        return ScanError(ConsoleMsgRangeErr);

    *l = endAddress.LinearAddress - a->LinearAddress;
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////
//    ScanList() assembles a list consisting of hex numbers and/or
//      quoted strings.
//
BOOL Base_ScanList(LPWORD NumBytes,BOOL EmptyOK)
{
    BYTE StringDelimiter;
    WORD Count = 0;
    DWORD d;
    BOOL Error = FALSE;

    while (!Error && !ScanEOL(FALSE))
        switch (ScanHexDigits(&d,16))
        {
            case 0:
                StringDelimiter = ReadInputLine(FALSE);
                if ((StringDelimiter != '"') && (StringDelimiter != '\''))
                    Error = TRUE;
                while (!Error)
                {
                    BYTE ch = ReadInputLine(FALSE);
                    if ((ch == StringDelimiter) && !ScanOption(0,ch))
                        break;
                    EGD.InputLine[Count++] = ch;
                    Error = (ch == 0);
                }
                break;

            case 1:
            case 2:
                EGD.InputLine[Count++] = (BYTE)d;
                break;

            default:
                Error = TRUE;
                break;
        }

    EGD.TrashedInputLine = (WORD)(Count!= 0);

    if (!Error && (EGD.TrashedInputLine || EmptyOK))
    {
        *NumBytes = Count;
        return TRUE;
    }

    EGD.ErrorIndex = EGD.LineIndex;
    return ScanError(ConsoleMsgList);
}
