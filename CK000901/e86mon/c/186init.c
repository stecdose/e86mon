/******************************************************************************
 *                                                                            *
 *     186INIT.C                                                              *
 *                                                                            *
 *     This file contains hardware-dependent setup code.                      *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#include "emon.h"
#include "am186msr.h"
#include "186init.h"
#include "permvars.h"

char _near MainMsgPrompt[] = "em86mon: ";

RamPermVar CpuSpeed    = {40000000L, DISPLAY_DECIMAL, TRUE};
RamPermVar BaudRate    = {19200,     DISPLAY_DECIMAL, TRUE};
RamPermVar HHKeychain  = {0,         DISPLAY_NONE,    FALSE};
RamPermVar LEDtoggle   = {1,         DISPLAY_DECIMAL, TRUE};
RamPermVar RefreshHz   = {64000L,    DISPLAY_NONE,    TRUE};
//RamPermVar WaitStates  = {0,         DISPLAY_NONE,    TRUE};
RamPermVar AutoRun     = {0,         DISPLAY_DECIMAL, TRUE};
RamPermVar MonitorPort = {1,         DISPLAY_DECIMAL, TRUE};
RamPermVar ProtectFlash= {0xE0000,   DISPLAY_DWORD,   TRUE};
RamPermVar LMCS_WS		= {0x0000,	 DISPLAY_NONE, TRUE};
RamPermVar UMCS_WS		= {0x0003,	 DISPLAY_NONE, TRUE};

//RamPermVar MacAddressH = {0,         DISPLAY_NONE,    FALSE};
//RamPermVar MacAddressL = {0,         DISPLAY_NONE,    TRUE};
//RamPermVar IPAddress   = {0,         DISPLAY_NONE,    TRUE};

RamPermVar PlaceHolder = {0xFFFFFFFFL,DISPLAY_NONE, FALSE};

RomPermVar PCpuSpeed    = {40000000L, &CpuSpeed};
RomPermVar PBaudRate    = {19200,     &BaudRate};
RomPermVar PHHKeychain  = {0,         &HHKeychain};
RomPermVar PLEDtoggle   = {1,         &LEDtoggle};
RomPermVar PRefreshHz   = {64000L,    &RefreshHz};
//RomPermVar PWaitStates  = {0,         &WaitStates};
RomPermVar PAutoRun     = {0,         &AutoRun};
RomPermVar PMonitorPort = {1,         &MonitorPort};
RomPermVar PProtectFlash= {0xE0000,   &ProtectFlash};
RomPermVar PLMCS_WS		= {0x0000,	  &LMCS_WS};
RomPermVar PUMCS_WS		= {0x0003,	  &UMCS_WS};
//RomPermVar PMacAddressH = {0,         &MacAddressH};
//RomPermVar PMacAddressL = {0,         &MacAddressL};
//RomPermVar PIPAddress   = {0,         &IPAddress};

RomPermVar PPlaceHolder = {0xFFFFFFFFL,&PlaceHolder};

// Do NOT change these names!  They are used for 'inheritance',
// as well as for displaying the actual value.  If they are
// changed, a new monitor will not be able to inherit the
// correct value from an old monitor.

STATIC ROMCHAR NullMsg[]        = "null";
STATIC ROMCHAR CpuSpeedMsg[]    = "cpuspeed";
STATIC ROMCHAR BaudRateMsg[]    = "baudrate";
STATIC ROMCHAR LEDtoggleMsg[]   = "led";
STATIC ROMCHAR RefreshHzMsg[]   = "refresh_hz";
//STATIC ROMCHAR WaitStatesMsg[]  = "waitstates";
STATIC ROMCHAR HHKeychainMsg[]  = "hhkey";
STATIC ROMCHAR AutoRunMsg[]     = "autorun";
STATIC ROMCHAR MonitorPortMsg[] = "monitorport";
STATIC ROMCHAR ProtectFlashMsg[]= "protectflash";
STATIC ROMCHAR LMCS_WSMsg[] 	= "lmcs_ws";
STATIC ROMCHAR UMCS_WSMsg[] 	= "umcs_ws";
//STATIC ROMCHAR MacAddressHMsg[] = "mac_high";
//STATIC ROMCHAR MacAddressLMsg[] = "mac_low";
//STATIC ROMCHAR IPAddressMsg[]   = "ipaddress";

// The PlaceHolder must be first in this array, but the order of
// the rest of the entries is immaterial.

PermVarArray ROM PermArray[]    = {
	  {NullMsg,&PPlaceHolder,0xFFFFFFFFL},  // PlaceHolder MUST be first
	  {BaudRateMsg,&PBaudRate,0xFFFFFFFFL},
	  {CpuSpeedMsg,&PCpuSpeed,0xFFFFFFFFL},
	  {HHKeychainMsg,&PHHKeychain,0xFFFFFFFFL},
	  {LEDtoggleMsg,&PLEDtoggle,0xFFFFFFFFL},
	  {RefreshHzMsg,&PRefreshHz,0xFFFFFFFFL},
   //	  {WaitStatesMsg,&PWaitStates,0xFFFFFFFFL},
	  {AutoRunMsg,&PAutoRun,0xFFFFFFFFL},
	  {MonitorPortMsg,&PMonitorPort,0xFFFFFFFFL},
	  {ProtectFlashMsg,&PProtectFlash,0xFFFFFFFFL},
	  {LMCS_WSMsg,&PLMCS_WS,0xFFFFFFFFL},
	  {UMCS_WSMsg,&PUMCS_WS,0xFFFFFFFFL},
//          {MacAddressHMsg,&PMacAddressH,0xFFFFFFFFL},
//          {MacAddressLMsg,&PMacAddressL,0xFFFFFFFFL},
//          {IPAddressMsg,&PIPAddress,0xFFFFFFFFL},
	  {0,0,0}
			     };

///////////////////////////////////////////////////////////////////////////
// GetProtectFlash() returns the value of the ProtectFlash permanent variable
//
DWORD GetProtectFlash(void)
{
    return ProtectFlash.RamValue;
}

///////////////////////////////////////////////////////////////////////////
// GetAutoRun() returns the value of the AutoRun permanent variable
//
DWORD GetAutoRun(void)
{
    return AutoRun.RamValue;
}

///////////////////////////////////////////////////////////////////////////
// DisableAllInterrupts() shuts off all async interrupt sources
//
void DisableAllInterrupts(void)
{


#ifndef ER

    if (IS186CC)
    {
	OutToPCBReg(IMASK_CC,0xFFFF);
    }
    else

#endif

    {
	OutToPCBReg(INT_MASK,0xFFFF);
    }
}

///////////////////////////////////////////////////////////////////////////
// EnableAllInterrupts() brings the system up.
//
void EnableAllInterrupts(void)
{
    EnableTimerInterrupt();
    InitSPRT(GetDefSerial(), TRUE);
}

///////////////////////////////////////////////////////////////////////////
// IsNet186Board() returns TRUE if the current board is a Net186 board.
//
// Assumes that a check for processor type (ES) has been done before the
// call.
//
// The RESCON ID of the Net186 is 4000h.  Unfortunately, early ES demo
// boards had no default RESCON values (so they might inadvertently
// match 4000h).  Fortunately, the early boards did not have 512K of
// Flash or RAM, so this routine looks at the size of Flash and RAM.
//

#ifndef ER

BOOL IsNet186Board(void)
{
    return (  ((_inpw( RCFG_REG ) & 0xFF00) == 0x4000)  &&
		  (_inpw(CS_LMCS) == 0x7f3c));
}

#endif

///////////////////////////////////////////////////////////////////////////
// InitSystem() is the first C routine called for hardware setup.
// It sets up system-specific ports.
//
void InitSystem(BOOL FirstTime)
{
    WORD        PortType;
    WORD        PortIndex;
    WORD        LedType;
    WORD        LedMask;
    WORD        SerMask;
    BOOL        EnablePCS;

    WORD        Mode[3],Dir[3],Data[3];

    // Set defaults to assume an EM, then change them as needed.

    PortType  = EM_SERIAL;
    PortIndex = (WORD)MonitorPort.RamValue ^ 1;
    LedType   = LED_EM_ES_ER;
    EnablePCS = 0xFFFF;

    // Perform different actions depending on processor type.

    switch (_inpw(PRL_REG) & 0xF000)
    {

#ifndef ER
	case 0x0000: //EM
   		_outpw(CS_UMCS, (_inpw(CS_UMCS) & 0xFFFC));	   
		break;

	case 0x1000:  //ES
	    MainMsgPrompt[1] = 's';
	    PortType = ES_SERIAL;

   		_outpw(CS_UMCS, (_inpw(CS_UMCS) & 0xFFFC));		

	    if (IsNet186Board())
	    {
		// Set up the PCS2 and 3 chip selects for PCNet ISA
		// locate at i/o space 0x200, and set LEDs correctly

//                MacAddressH.DisplayType = DISPLAY_DWORD;
//                MacAddressL.DisplayType = DISPLAY_WORD;
//                IPAddress.DisplayType   = DISPLAY_DWORD;
		EnablePCS &= ~(PIO1_PCS2 | PIO1_PCS3);
		LedType = LED_VER;
	    }
	    break;

#endif

	case 0x2000:  //ER
	    MainMsgPrompt[1] = 'r';
   		_outpw(CS_UMCS, (_inpw(CS_UMCS) & 0xFFFC));
	    break;

#ifndef ER

	case 0x3000:  //ED
	    MainMsgPrompt[1] = 'd';
	    PortType = ES_SERIAL;
	    RefreshHz.DisplayType = DISPLAY_DECIMAL;
 	    LMCS_WS.DisplayType = DISPLAY_DECIMAL;
 	    UMCS_WS.DisplayType = DISPLAY_DECIMAL;
	    // Set DRAM wait states and refresh parameters,  unless the
	    // 'panic' bit is set in the board configuration register.
	    // This bit gives people a way to set to very conservative
	    // values in order to resuscitate a monitor afer the
	    // CPU speed, refresh rate, or wait states have been
	    // incorrectly programmed.

	    if ((_inpw( RCFG_REG ) & 0x0100) == 0)
	    {
		_outpw(CS_LMCS, (_inpw(CS_LMCS) & 0xFFFC) |
		     ((WORD)LMCS_WS.RamValue & 0x3));
		_outpw(CS_UMCS, (_inpw(CS_UMCS) & 0xFFFC) |
		     ((WORD)UMCS_WS.RamValue & 0x3));

		_outpw(DRAM_CLK,
		     (WORD)(CpuSpeed.RamValue / RefreshHz.RamValue) & 0x7FF);
	    }
	    
	    // ED board and verification board are both new enough
	    // to have unique reset configuration (RESCON) patterns.
	    // This is good, because the LEDs are connected differently,
	    // and the ED demo board's serial ports are wired backward.

	    switch((_inpw( RCFG_REG ) & 0xF000))     
	    {
		case 0x8000:          // Verification board
		    LedType = LED_VER;
		    break;

		default:              // ED demo board
		    LedType = LED_ED;
		    PortIndex ^= 1;   // Swap serial ports on ED demo board
		    break;
	    }
	    break;

	case 0x4000:  //CC
	    MainMsgPrompt[0] = 'c';
	    MainMsgPrompt[1] = 'c';
	    PortType = CC_SERIAL;
	    RefreshHz.DisplayType = DISPLAY_DECIMAL;
	    LMCS_WS.DisplayType = DISPLAY_DECIMAL;
	    UMCS_WS.DisplayType = DISPLAY_DECIMAL;		
			/* Configure Interface 4 to for UART Use        (No HDLC D) */
			_outpw(SYSCON_CC, (
			(_inpw(SYSCON_CC) & ~SYSCON_ITF4_MASK_CC) |  
			SYSCON_ITF4_UART_CC));

	    // Set DRAM wait states and refresh parameters,  unless the
	    // 'panic' bit is set in the board configuration register.
	    // This bit gives people a way to set to very conservative
	    // values in order to resuscitate a monitor afer the
	    // CPU speed, refresh rate, or wait states have been
	    // incorrectly programmed.

	    if ((_inpw( RESCON_CC ) & 0x0100) == 0)
	    {
		_outpw(CS_LMCS, (_inpw(CS_LMCS) & 0xFFFC) |
		     ((WORD)LMCS_WS.RamValue & 0x3));
	   	_outpw(CS_UMCS, (_inpw(CS_UMCS) & 0xFFFC) |
		     ((WORD)UMCS_WS.RamValue & 0x3));

		_outpw(CDRAM_CC,
		     (WORD)(CpuSpeed.RamValue / RefreshHz.RamValue) & 0x7FF);
	    }
	    
	    // CC boards are new enough to have unique reset configuration 
	    // patterns.
       
	    switch((_inpw( RESCON_CC ) & 0xF000))     
	    {
		case 0x8000:          // Customer Evaluation Platform
		    LedType = LED_CEP;
		    break;

		case 0x4000:          // Terminal Adapter Reference
					MainMsgPrompt[5] = 't';
					MainMsgPrompt[6] = 'a';
		    LedType = LED_CC_REF;
		    break;

		case 0x1000:          // Router Reference Design
		default:              
		    LedType = LED_CC_REF;
		     break;
	    }

	    if (IS_TIP_ATTACHED)                       
	    {
#define SMALL_DELAY { volatile int i; for(i=0; i<0x500; i++); }

		// Test Interface Port Detected
		LedType = LED_TIP;

				// Set IO Space to 8 bits wide
				_outpw(CS_MPCS, (_inpw(CS_MPCS) | MPCS_IOSIZ_CC));
				_outp(TIP_LEDS,0x3c);
		_outpw(TIP_HEX,0xcc86);
				
				// Clear TIP LCD Display
				_outp(0x30c,0x38); SMALL_DELAY SMALL_DELAY
				_outp(0x30c,0x0e); SMALL_DELAY SMALL_DELAY
				_outp(0x30c,0x06); SMALL_DELAY SMALL_DELAY
				_outp(0x30c, 1);   SMALL_DELAY SMALL_DELAY
				
				// Write out message
		_outp(0x30e, 'A'); SMALL_DELAY
				_outp(0x30e, 'M'); SMALL_DELAY
				_outp(0x30e, 'D'); SMALL_DELAY
			_outp(0x30e, ' '); SMALL_DELAY
				_outp(0x30e, '3'); SMALL_DELAY
				_outp(0x30e, '.'); SMALL_DELAY
				_outp(0x30e, '4'); SMALL_DELAY
				_outp(0x30e, '2'); SMALL_DELAY
			_outp(0x30e, ' ');
		
	    }     

	break;

#endif            

    }

    // Initialize all serial ports at default baud rate with interrupts off,
    // set LED information, and initialize timer.

    InitTimer(FirstTime, CpuSpeed.RamValue);
    SetDefSerial(FirstTime,PortType,PortIndex,
	     BaudRate.RamValue,CpuSpeed.RamValue);
    SerMask = GetDefSerial()->PIOEnable;
    LedMask = SetLedType(LedType, (WORD)LEDtoggle.RamValue);

    // Get initialization values for PIO pins
    // The Data registers wake up to undefined states
    // -- set them to 0 by default

#ifndef ER

    if (IS186CC)
    {
	Mode[0] = (WORD)_inpw(PIOMODE0_CC);
	Dir [0] = (WORD)_inpw(PIODIR0_CC);
	Data[0] = 0;

	Mode[1] = (WORD)_inpw(PIOMODE1_CC);
	Dir [1] = (WORD)_inpw(PIODIR1_CC);
	Data[1] = 0;

	Mode[2] = (WORD)_inpw(PIOMODE2_CC);
	Dir [2] = (WORD)_inpw(PIODIR2_CC);
	Data[2] = 0;

	// Set LED pins to input w/o pulldowns

	Mode[0] |= LedMask;
	Dir [0] |= LedMask;
    }
    else

#endif

    {
	Mode[0] = (WORD)_inpw(PIO_MODE0);
	Dir [0] = (WORD)_inpw(PIO_DIR0);
	Data[0] = 0;

	Mode[1] = (WORD)_inpw(PIO_MODE1);
	Dir [1] = (WORD)_inpw(PIO_DIR1);
	Data[1] = 0;

	// Set LED pins to input w/o pulldowns

	Mode[0] |= LedMask;
	Dir [0] |= LedMask;

    }
    
#ifndef ER

    // If using PCS0-3, set up the registers to do so.

    if (EnablePCS != 0xFFFF)
    {
	OutToPCBReg(CS_MPCS, CS_MPCS_RESERVED + CS_MPCS_8K);
	OutToPCBReg(CS_PACS, CS_PACS_RESERVED + CS_WAIT3 + CS_IGXRDY);

	Mode[1] &= EnablePCS;
	Dir [1] &= EnablePCS;
    }

#endif

    // Set serial pins to normal function

    Mode[1] &= SerMask;
    Dir [1] &= SerMask;

    // Lots of setup for the Hamilton-Hallmark keychain.  Most
    // of this probably belongs in the application, not here,
    // but somehow it wound up here, so we have a legacy issue.

    if ((WORD)HHKeychain.RamValue)
    {
	// Enable IRDA_SD, PWRON, and RS232_SD as outputs

	Mode[1] |= (PIO1_PCS0 | PIO1_PCS1 | PIO1_PCS3
			      | PIO1_SDEN0 | PIO1_SDEN1);

	// enable PIO20 (SCLK) and PIO21 (SDATA) as inputs

	Dir[1] |= PIO1_SCLK | PIO1_SDATA;

	// Enable PIO16 (PCS0/RS232EN), PIO17 (PCS1/IRDA_SD),
	// PIO19 (PCS3/PWRON), PIO22 (SDEN0/RS232_SD)
	// and PIO23 (SDEN1/IRDA_EN) as outputs

	Dir[1] &= ~(PIO1_PCS0 | PIO1_PCS1 | PIO1_PCS3
			      | PIO1_SDEN0 | PIO1_SDEN1);

	// Turn on IRDA_SD (PIO17/PCS1), PWRON (PIO19/PCS3),
	//               RS232_SD (PIO22/SDEN0)

	Data[1] |= (PIO1_PCS1 | PIO1_PCS3 | PIO1_SDEN0);
    }

#ifndef ER

    if (IS186CC)
    {
		// force the PIOs to good settings for specific 
		// platforms to avoid conflicts.  For instance,
		// the TA reference ties many PIOs to grounds, ones that
		// default to outputs with high data outputs.  This
		// causes the TA to run very hot with default settings.
		switch((_inpw( RESCON_CC ) & 0xF000))     
		{
		case 0x4000:          // Terminal Adapter Reference
			Dir [0] = 0x06ff;       // these settings determined by expirementation
			Dir [1] = 0x9ffe;
			Dir [2] = 0xfff1;
			break;
		default:
			break;
		}
// set the RTR and RTS pios to normal operation on the
// the Serrano boards, to allow possible use of flow
// control.  This will cause these boards to assert
// CTS to the PC, which will allow emon to work if
// the PC has hardware flow control turned on.
			Dir [2] &= 0x3fff;      // enable CTS and RTS
			Mode [2] |= 0x8000;     // enable CTS


	OutToPCBReg( PIOMODE2_CC, Mode[2] );
	OutToPCBReg( PIODIR2_CC,  Dir[2] );
	OutToPCBReg( PIODATA2_CC, Data[2] );

	OutToPCBReg( PIOMODE1_CC, Mode[1] );
	OutToPCBReg( PIODIR1_CC,  Dir[1] );
	OutToPCBReg( PIODATA1_CC, Data[1] );

	OutToPCBReg( PIOMODE0_CC, Mode[0] );
	OutToPCBReg( PIODIR0_CC,  Dir[0] );
	OutToPCBReg( PIODATA0_CC, Data[0] );      
    }
    else

#endif

    {
	OutToPCBReg( PIO_MODE1, Mode[1] );
	OutToPCBReg( PIO_DIR1,  Dir[1] );
	OutToPCBReg( PIO_DATA1, Data[1] );

	OutToPCBReg( PIO_MODE0, Mode[0] );
	OutToPCBReg( PIO_DIR0,  Dir[0] );
	OutToPCBReg( PIO_DATA0, Data[0] );
    }

} // InitSystem()
