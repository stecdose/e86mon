/******************************************************************************
 *                                                                            *
 *     EMMAIN.C                                                               *
 *                                                                            *
 *     This file contains the interrupt 1 service routine, which is the       *
 *     main debug loop of the monitor.  (The initial C routine is in          *
 *     SYSTEM.C, then this is the next C routine executed.)                   *                                                                            *
 *     IrqEntry() is where most of the work of the monitor is performed.      *
 *     The main parse loop is contained here, and Trace and Go commands       *
 *     are executed simply by returning from this handler.  When a            *
 *     trace or breakpoint interrupt occurs, this handler will be             *
 *     reentered, and will prompt for the next command.                       *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996, 1997 Advanced Micro Devices, Inc.                          *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#include "interlib.h"
#include "emon.h"
//
//    8086 constants
//
#define  FLAGS_TF    0x100
#define  OP_INT3     0xcc

BOOL       DoAutoRun = FALSE;
LPBYTE     BPAddress;

//    static variables

STATIC WORD   TraceCount;            // number of steps to trace
STATIC BYTE   bOldOpCode;            // breakpoint memory

///////////////////////////////////////////////////////////////////////
//    RestartIfMoved() -- Used to set default SS, SP, CS, IP if
//    we had to move to RAM.  (We move the entire monitor to RAM
//    if we ever perform a flash operation, so we do not attempt
//    to execute out of the flash while we are programming it.)
//
void RestartIfMoved(void)
{
    BOOL SystemOK;

    if (!SystemInRAM())
        return;
    
    SystemOK = !EGD.MonitorMoved && RAMSafe();

    RunSystemFromRAM(FALSE);

    if (!SystemOK)
    {
        BOOL m = EGD.MonitorMoved;
        EGD.MonitorMoved = FALSE;
        FlushAndRestart((WORD)(m ? RESTART_LOCK_IN_RAM
                                 : RESTART_RETURN_FROM_RAM));
    }
}

///////////////////////////////////////////////////////////////////////
//    DisableBP() -- clear any outstanding breakpoint
//
STATIC void DisableBP( void )
{
   if( BPAddress != 0L )
      *BPAddress  = bOldOpCode;                 // reset old op
   BPAddress = 0L;
} // CommandDisableBP()


///////////////////////////////////////////////////////////////////////
//    CommandHelpOrHex()  [num num]  -- Posts help screen, or
//                                      performs math.
//
STATIC void CommandHelpOrHex( void )
{
    DWORD Num1, Num2;

    if (!ScanEOL(FALSE) && ScanDWord(&Num1)
                        && ScanDWord(&Num2) && ScanEOL(FALSE))
    {
        printf(MainMsgHexMath1,
                  Num1,Num2,Num1+Num2,
                  Num1,Num2,Num1-Num2,
                  Num1,Num2,Num1*Num2);
        if (Num2 != 0)
            printf(MainMsgHexMath2,
                  Num1,Num2,Num1/Num2,
                  Num1,Num2,Num1%Num2);
    }
    else
        printf(MainMsgHelp);
} // CommandHelpOrHex()

///////////////////////////////////////////////////////////////////////
//        CommandTrace(); [=address] [count]
//   Traces execution for the user.
//
BOOL CommandTrace(void)
{
        LPVOID a;
        BOOL GotAddress = FALSE;
        WORD   TempTraceCount;

        if (ScanOption(SCAN_SPACES|SCAN_COMMAS,'='))
            if (!ScanSegOffset(&a))
                return FALSE;
            else
                GotAddress = TRUE;

        if (ScanEOL(FALSE))
            TempTraceCount = 1;
        else if (!ScanWord(&TempTraceCount) || !ScanEOL(TRUE))
            return FALSE;

        TraceCount = TempTraceCount;

        if (GotAddress)
        {
            EGD.stack->CS = _FP_SEG(a);
            EGD.stack->IP = _FP_OFF(a);
        }
        EGD.stack->Flags |= FLAGS_TF;              // start tracing
        return TRUE;
}

///////////////////////////////////////////////////////////////////////
//   ErrDispatch is the internal error reporter
//
DispatchResult ErrDispatch(void)
{
    OutputErrorString();
    return Dispatch_IDidIt;
}

///////////////////////////////////////////////////////////////////////
//   CmdDispatch is the internal command processor
//
DispatchResult CmdDispatch(void)
{
    char   FileName[16];     // Matches size in CMDFILE.C
    LPSTR  FPtr;

    LPVOID UserAddress;
    DWORD  UserDWord;
    WORD   UserWord, UserWord2;
    BYTE   UserByte;
    
    EGD.LineIndex = 0;
    EGD.FileLineCount = 0;
    EGD.ErrorStored   = 0;
    EGD.SpinLEDsWhileProgramming = FALSE;
    FPtr = 0;
    FreeFlash(TRUE);

    ScanDelimiters(SCAN_SPACES);
    switch (ReadInputLine(TRUE))
    {
    case 'b':
       DisableBP();                          // clear any existing breaker
       if (!ScanSegOffset(&BPAddress))
           break;
       bOldOpCode = *BPAddress;              // remember old op
       *BPAddress = OP_INT3;                 // and set irq3
       break;

    case 'c':
        CommandCompare();
        break;

    case 'd':
        CommandDump();
        break;

    case 'e':
        CommandExamine();
        break;

    case 'f':
        CommandFill();
        break;

    case 'g':
        ScanDelimiters(SCAN_SPACES|SCAN_EQUALS|SCAN_COMMAS);
        if (!ScanEOL(FALSE))
            if (!ScanSegOffset(&UserAddress) || !ScanEOL(TRUE))
                break;
            else
            {
                EGD.stack->CS = _FP_SEG(UserAddress);
                EGD.stack->IP = _FP_OFF(UserAddress);
            }
        return Dispatch_ReturnToDebuggee;

    case 'h':
    case '?':
        CommandHelpOrHex();
        break;

    case 'i':
        if(ScanEOL(FALSE))
        {
            LPDispatchLink   library = EmonData->LibHead;
            printf(MainMsgShow1);
            while (library)
            {
                printf(MainMsgLibrary,
                          library->name,
                          library->codeseg,
                          library->codelength,
                          library->dataseg,
                          library->datalength);
                library = library->next;
            }
            GetCurrentTime(&UserDWord);
            UserWord = (WORD)(UserDWord % 1000);
            UserDWord = (UserDWord / 1000);
            printf(MainMsgShow2,
                EGD.BigBufParagraphs,
                UserDWord,UserWord);
            ShowFlashParameters();
            dispatch(Dispatch_Information);
            break;
        }
        if (ScanOption(0,'w'))
        {
            if (ScanWord(&UserWord) && ScanEOL(TRUE))
                printf(MainMsgHexWordCRLF,_inpw(UserWord));
            break;
        }
        if (ScanWord(&UserWord) && ScanEOL(TRUE))
            printf(MainMsgHexByteCRLF,_inp(UserWord));
        break;

    case 'j':
        if (!ScanEOL(TRUE))
            break;
        printf(MainMsgTestingAutobaud);
        FlushTransmitter();
        while (!AutoBaud(TRUE));
        break;

    case 'l':
        if (ScanOption(SCAN_SPACES|SCAN_COMMAS,'l') && ScanEOL(TRUE))
            FlushAndRestart(RESTART_LOAD_LIBS);

        {
            BOOL GoAfterLoad = ScanOption(SCAN_SPACES|SCAN_COMMAS,'g');
            if (CommandLoad(-1) && GoAfterLoad)
            {
                return Dispatch_ReturnToDebuggee;
            }
            break;
        }

    case 'm':
        CommandMove();
        break;

    case 'o':
        if (ScanOption(0,'w'))
        {
            if (ScanWord(&UserWord) && ScanWord(&UserWord2) && ScanEOL(TRUE))
                _outpw(UserWord,UserWord2);
            break;
        }
        if (ScanWord(&UserWord) && ScanByte(&UserByte) && ScanEOL(TRUE))
            _outp(UserWord,UserByte);
        break;

    case 'p':
        CommandPermVar();
        break;

    case 'r':
        CommandRegister(FALSE);
        break;

    case 's':
        CommandSearch();
        break;

    case 't':
        if (CommandTrace())
        {
            return Dispatch_ReturnToDebuggee;
        }
        break;

    case 'x':
        if (!ScanByte(&UserByte) || !ScanEOL(TRUE))
            break;
        if (!RunSystemFromRAM(TRUE))
            break;

        EraseFlash(UserByte);
        OutputErrorString();
        FlushAndRestart(RESTART_ERASED_FLASH);   // May have blown away a library
        break;

    case 'z':
        if( !ScanEOL(TRUE) )
                break;

        UpdateMonitor(FALSE);
        break;

    case 'w':
        if (ScanEOL(FALSE))
            FileName[0] = 0;
        else if (!ScanString(FileName,sizeof(FileName)))
            break;

        if (!ScanEOL(TRUE))
                break;
        FPtr = FileName;

        // Drop through to file colon case

    case ':':
        if (ScanEOL(FALSE))
        {
            // the printf is not included in the getinput line to prevent overrun
            printf(FileMsgDummyAlert);
            if (!GetInputLine(SystemMsgNull,FALSE))
                break;
        }
        if (CommandHexFile(FPtr) &&
             (OriginalCS() != CurrentCS()) )
        {
            //If we get a good load, don't allow a restart to blow away
            // our CS:IP.

            RunSystemFromRAM(FALSE);
            printf(MainMsgRamOverwritten);
        }
        break;

    case ';':  // Comment character
    case 0:
        break;

    default:
        ScanError(MainMsgInvalid);
        break;

   } // switch

    return EGD.ErrorStored ? Dispatch_NotExclusive : Dispatch_IDidIt;

} // CmdDispatch


///////////////////////////////////////////////////////////////////////
//    IrqEntry() -- main process after int entry
//
void cdecl IrqEntry( STACKREC stack)
{
    static WORD highestStack;

    BOOL   ShowRegs;
    FPSTACKREC OldStack;

    // If we traced into an interrupt routine, just return
    // without tracing.

    if ((stack.IntType == 1) && ((stack.Flags & FLAGS_TF) == 0)
        && (TraceCount != 0))
        return;

    _enable();

    ShowRegs = TRUE;

    switch (stack.IntType)
    {
        case 0xFFFF:    // Entry through standard monitor
            ShowRegs = FALSE;
            break;      // int 1 location -- no program loaded

        case 257:
            printf(MainMsgBreakReceived);
            FlushReceiver();
            break;

        case 3:
            if ( (BPAddress == 0) ||
                 (SegToLin(BPAddress) != SegToLin(MK_FP(stack.CS,stack.IP))-1) )
            {
                printf(MainMsgEmbeddedInt3);
                break;
            }
            printf(MainMsgAtBP);
            DisableBP();
            --stack.IP;                    // back up over int3 op-code
            break;

        case 1:
            if (TraceCount != 0)
            {
                if (--TraceCount == 0)
                    break;

                printf("%a\n",stack.IP,stack.CS);
                return;                              // keep tracing
            }

           // Drop through to default case if we were not tracing before

        default:
             printf(MainMsgUnhandledInt,stack.IntType);
             break;
     }

    stack.Flags &= ~FLAGS_TF;      // Quit tracing
    TraceCount = 0;
  
    OldStack = EGD.stack;
    EGD.stack = &stack;

    if (ShowRegs)
        CommandRegister(TRUE);

    if (EGD.RestartCode != RESTART_NONE)
        dispatch(Dispatch_Restart);

    if (DoAutoRun)
        DoAutoRun = (GetAutoRun() && CommandLoad((WORD)GetAutoRun()));

    switch (EGD.RestartCode)
    {
        case RESTART_TOTAL:
        case RESTART_RE_AUTOBAUD:
            if (UpdateInProgress())
                UpdateMonitor(TRUE);
    
            if (!DoAutoRun)
                printf(MainMsgWelcome);
            FlushReceiver();
            break;

        case RESTART_LOCK_IN_RAM:
            printf(MainMsgMonitorMoved);
            break;
        case RESTART_LOAD_LIBS:
            break;
        case RESTART_EXIT_PROG:
            printf(MainMsgProgramTerminated,EGD.ExitCode);
            EGD.ExitCode = 0;
            break;
        case RESTART_ERASED_FLASH:
        case RESTART_RETURN_FROM_RAM:
            printf(MainMsgRamOverwritten);
            break;
        default:
            if (stack.IntType == 0xFFFF)
                printf(MainMsgNoProgramLoaded);
    }

    EGD.RestartCode = RESTART_NONE;

    if (DoAutoRun)
    {
        DoAutoRun = FALSE;
        EGD.stack = OldStack;
        return;
    }

    if ((WORD)(DWORD)&stack >= highestStack)
    {
        highestStack = (WORD)(DWORD)&stack;
        EGD.NestLevel = 1;
    }
    else if (EGD.NestLevel > 3)
    {
        printf(MainMsgTooDeep);
        EGD.stack = OldStack;
        EGD.MonitorExited = TRUE;
        return;
    }
    else
        EGD.NestLevel++;


    for( ;; )
    {
        DispatchResult r;

        RestartIfMoved();

        if( !GetInputLine(MainMsgPrompt, TRUE) )
           continue;

        EGD.TrashedInputLine = FALSE;

    // CmdDispatch is not on the dispatch stack.  It is called separately.
    // The reason for this is that the monitor's code segment could have
    // moved (monitor relocated to RAM), and this would not be reflected
    // properly in the far call structure on the dispatch stack.

        r = dispatch(Dispatch_ExecCmd);
        if (r == Dispatch_NotExclusive)
            r = CmdDispatch();

        switch (r)
        {
            case Dispatch_ReturnToDebuggee:
                EGD.stack = OldStack;
                EGD.NestLevel--;
                EGD.MonitorExited = TRUE;
                return;

            case Dispatch_NotExclusive:
                {
                    WORD SaveError = EGD.ErrorStored;
                    EGD.ErrorStored = 0;
                    if (!EGD.TrashedInputLine &&
                        (dispatch(Dispatch_GetError) != Dispatch_NotExclusive))
                        break;
                    if (EGD.ErrorStored == 0)
                        EGD.ErrorStored = SaveError;
                    break;
                }
            default:
                break;
         }
         OutputErrorString();
    } // for
} // IrqEntry()
