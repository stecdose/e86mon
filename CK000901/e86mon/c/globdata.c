/******************************************************************************
 *                                                                            *
 *     GLOBDATA.C                                                             *
 *                                                                            *
 *     This file contains global data which E86Mon shares with its            *                                                                            *
 *     library functions, as well as a routine which is called a single       *
 *     time (at monitor startup) to initialize the data.                      *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "emon.h"

#define SMALL_SIZE 128
#define INPUT_SIZE 84    // Must be at least 80, for assumption in CONINPUT
#define LIBLINK_SIZE 8   // Room for 8 libraries

extern WORD far SerialThunk(SerialAction action,WORD handle);

EmonInternals    EGD;
FPEmonInternals  EmonData;

static BYTE      SmallBuffer  [SMALL_SIZE];
static BYTE      InputLine    [INPUT_SIZE];
static LibEntryS LibLinkArray [LIBLINK_SIZE];

// These offsets are defined so that EMONSTRT.ASM can know where
// these values are stored.

LPBYTE near * BigBufOffset      = &(EGD.BigBuffer);
WORD   near * BigBufParaOffset  = &(EGD.BigBufParagraphs);
WORD   near * RestartCodeOffset = &(EGD.RestartCode);

void InitGlobals(void)
{
    EmonData = &EGD;

    EGD.StrucSize = sizeof(EGD);
    EGD.Version = VER_NUM;
    EGD.InputLineSize = INPUT_SIZE;
    EGD.SmallBufSize  = SMALL_SIZE;
    EGD.MaxLibEntry   = LIBLINK_SIZE;
    EGD.InputLine     = InputLine;
    EGD.SmallBuffer   = SmallBuffer;
    EGD.LibEntries    = LibLinkArray;
    EGD.SerialIO      = MK_FP(CurrentDS(),(WORD)(DWORD)&SerialThunk);
}
