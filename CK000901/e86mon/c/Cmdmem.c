/******************************************************************************
 *                                                                            *
 *     CMDMEM.C                                                               *
 *                                                                            *
 *     This file contains all of the monitor's commands which pertain         *
 *     to memory, e.g. move, examine, search, fill, compare, dump             *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "emon.h"

///////////////////////////////////////////////////////////////////////
//        CommandCompare();  range addr
//        Compares two memory blocks
//
void CommandCompare(void)
{
    AnyAddress a1,a2;
    DWORD Length;
    WORD  i;

    if (!ScanRangeAddress(SCAN_DEFADDR, &a1, &Length, &a2))
        return;

    while ((Length != 0) && !kbhit())
    {
        WORD chunk = EGD.SmallBufSize/2;
        if (chunk > Length)
            chunk = (WORD)Length;

        if (!GetAnyMem(&a1,EGD.SmallBuffer,chunk) ||
            !GetAnyMem(&a2,EGD.SmallBuffer + EGD.SmallBufSize/2,chunk))
            return;

        for (i=0;i<chunk;i++)
        {
            if (EGD.SmallBuffer[i] != EGD.SmallBuffer[i+EGD.SmallBufSize/2])
                if (kbhit())
                    break;
                else
                    printf(MainMsgLAddrByteByteLAddr,&a1,
                           EGD.SmallBuffer[i],EGD.SmallBuffer[EGD.SmallBufSize/2+i],
                                &a2);
            a1.LinearAddress++;
            a2.LinearAddress++;
            Length--;
        }
    }
}

///////////////////////////////////////////////////////////////////////
//        CommandDump();     [range]
//        Dumps memory contents
//
void CommandDump( void )
{
    static AnyAddress Address;

    DWORD  Length = 128;

    if (!ScanEOL(FALSE))
    {
        if (!ScanRange(SCAN_DEFADDR|SCAN_DEFLEN,&Address, &Length) ||
            !ScanEOL(TRUE))
            return;
    }

    while (Length && !kbhit())
    {
        WORD i;
        WORD Offset = (WORD)Address.LinearAddress & 0xF;
        WORD chunk  = 16 - Offset;

        if (chunk > Length)
            chunk = (WORD)Length;

        if (!GetAnyMem(&Address,EGD.SmallBuffer+Offset,chunk))
            return;

        Address.LinearAddress -= Offset;
        printf(MainMsgLAddress3Space,&Address);
        Address.LinearAddress += chunk+Offset;
        Length -= chunk;

        for(i=0;i<16;i++)
        {
            if ((i<Offset) || (i >=chunk+Offset))
            {
                printf(SystemMsg3Spaces);
                EGD.SmallBuffer[i] = ' ';
            }
            else
            {
                BYTE ch = EGD.SmallBuffer[i];
                printf(MainMsgHexByteSpace,ch);
                EGD.SmallBuffer[i] = (BYTE)
                            (((ch >= ' ') && (ch < 0x7F)) ? ch : '.');
            }
            if ((i&7)==7)
                printf(SystemMsgSpace);
        }
        EGD.SmallBuffer[16] = 0;
        printf(SystemMsgStringCRLF,(LPCSTR)EGD.SmallBuffer);
    }
}

///////////////////////////////////////////////////////////////////////
//        CommandExamine();  addr  list
//        Show/alter memory bytes
//
void CommandExamine( void )
{
    WORD NumBytes;
    BYTE Value;
    AnyAddress Address;

    if (!ScanAddress(SCAN_DEFADDR,&Address))
        return;
               
    if (!ScanList(&NumBytes,TRUE))
        return;

    if (NumBytes != 0)
    {
        SetAnyMem(&Address,EGD.InputLine,NumBytes);
        return;
    }

    for( ;; )
    {
        if (!GetAnyMem(&Address,&Value,1))
            return;
        printf(MainMsgLAddressHexByte,&Address,Value);
        if( !GetInputLine(SystemMsgNull,TRUE) || (EGD.InputLine[0] == '.') )
            break;

        if (!ScanList(&NumBytes,TRUE))
            continue;

        if (!SetAnyMem(&Address,EGD.InputLine,NumBytes))
            return;
        if (NumBytes == 0)
            NumBytes = 1;
        Address.LinearAddress += NumBytes;
    }
}

///////////////////////////////////////////////////////////////////////
//        CommandFill();     range list
//
void CommandFill(void)
{
    AnyAddress Address;
    DWORD  Length;
    WORD   NumBytes;

    EGD.SpinLEDsWhileProgramming = TRUE;

    if (!ScanRangeList(SCAN_DEFADDR, &Address, &Length,&NumBytes))
        return;

    // Collect a reasonable number of bytes together for the fill

    while (NumBytes*2 < EGD.InputLineSize)
    {
        emon_memcpy(EGD.InputLine+NumBytes,EGD.InputLine,NumBytes);
        NumBytes *= 2;
    }

    while (Length)
    {
        if (NumBytes > Length)
            NumBytes = (WORD) Length;

        if (!SetAnyMem(&Address,EGD.InputLine,NumBytes))
            return;

        Length  -= NumBytes;
        Address.LinearAddress += NumBytes;
    }
}

///////////////////////////////////////////////////////////////////////
//        CommandMove();     range address
//
void CommandMove(void)
{
    AnyAddress a1,a2;

    DWORD  Length;
    BOOL   Backward;

    EGD.SpinLEDsWhileProgramming = TRUE;

    if (!ScanRangeAddress(SCAN_DEFADDR,&a1,&Length,&a2))
        return;

    Backward = a2.LinearAddress > a1.LinearAddress;

    if (Backward)
    {
        a1.LinearAddress += Length;
        a2.LinearAddress += Length;
    }

    while (Length)
    {
        WORD chunk = EGD.SmallBufSize;

        if (Length < chunk)
            chunk = (WORD) Length;

        if (Backward)
        {
            a1.LinearAddress -= chunk;
            a2.LinearAddress -= chunk;
        }

        if (!GetAnyMem(&a1,EGD.SmallBuffer,chunk)
            || !SetAnyMem(&a2,EGD.SmallBuffer,chunk))
            return;

        if (!Backward)
        {
            a1.LinearAddress += chunk;
            a2.LinearAddress += chunk;
        }

        Length -= chunk;
    }
}

///////////////////////////////////////////////////////////////////////
//        CommandSearch();   range list
//
void CommandSearch( void )
{
    AnyAddress  Address;

    DWORD  Length;
    WORD   NumBytes;

    if (!ScanRangeList(SCAN_DEFADDR, &Address, &Length, &NumBytes))
        return;

    if (NumBytes > Length)
        return;

    NumBytes--;

    Length -= NumBytes;

    while (Length && !kbhit())
    {
        WORD CurCmp = 0;
        WORD chunk = EGD.SmallBufSize - NumBytes;
        if (chunk > Length)
            chunk = (WORD)Length;

        if (!GetAnyMem(&Address,EGD.SmallBuffer,chunk+NumBytes))
            return;

        while ( (CurCmp < chunk ) &&
                (emon_memcmp(EGD.SmallBuffer+CurCmp,EGD.InputLine, NumBytes+1)!=0) )
            CurCmp++;

        Address.LinearAddress += CurCmp;
        Length -= CurCmp;

        if (CurCmp < chunk)
        {
            printf(MainMsgLAddressCRLF,&Address);
            Address.LinearAddress++;
            Length--;
        }
    }
}
