/******************************************************************************
 *                                                                            *
 *     186LEDS.C                                                              *
 *                                                                            *
 *     This file contains hardware-dependent LED display code.                *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#include "emon.h"
#include "am186msr.h"
#include "186init.h"

STATIC WORD BoardLEDMask;
STATIC WORD BoardLEDShift;
STATIC WORD LEDtoggle = 0;

///////////////////////////////////////////////////////////////////////////
//    PostLEDs() -- light the LEDs with the supplied values
//
void Base_PostLEDs( BYTE bLEDMask )
{
   bLEDMask = (BYTE)(~bLEDMask);   // Turn them "on" by setting them "low"

   if (LEDtoggle)
   {

#ifndef ER

        if (IS186CC)
        {
            // Currently only the TIP has LEDs
            if (IS_TIP_ATTACHED)
            {
                _outp(TIP_LEDS, ~bLEDMask);
        	}
        }
        else


#endif

        {   OutToPCBReg(PIO_DIR0,  ((((((WORD)bLEDMask << BoardLEDShift) & 0xff00)
                | (WORD)bLEDMask ) & BoardLEDMask)
                | (_inpw(PIO_DIR0) & ~BoardLEDMask) ) ); 
        }
   }
}


///////////////////////////////////////////////////////////////////////////
//    InquireLEDs() -- Get the current LED values
//
BYTE Base_InquireLEDs( void )
{

#ifndef	er

    if (IS186CC)    
    {
        // Currently only the TIP has LEDs
        if (IS_TIP_ATTACHED)
        {
 		    return (BYTE)_inp(TIP_LEDS);
        }
        return 0;
    }
    else

#endif

    {
        WORD Value = (~ (WORD)_inpw(PIO_DIR0)) & BoardLEDMask;
        return (BYTE)(Value + (Value >> BoardLEDShift));
    }
}


///////////////////////////////////////////////////////////////////////////
//    SetLedType() -- set LED shift and mask for specific board
//
WORD SetLedType(WORD System, WORD UseLEDs)
{
    LEDtoggle = UseLEDs;

    if (LEDtoggle == 0)
        return 0;

    // Since the configuration register on older demonstration boards 
    // wakes up to random default values, there is no guaranteed way
    // to distinguish a verification board from a demo board.  Because
    // of this, we have a special code to force the monitor to understand
    // it's a verification board.  This is done by setting LEDtoggle to 54321.

    if (LEDtoggle == 54321)
        System = LED_VER;

    switch (System)
    {

#ifndef ER

        case LED_ED:
            BoardLEDMask = 0x603f;
            BoardLEDShift = 7;
            break;
        case LED_VER:
            BoardLEDMask = 0x047f;
            BoardLEDShift = 3;
            break;
        case LED_CEP:
            BoardLEDMask = 0x0000;      // Need to define these further 
            BoardLEDShift = 0;
            break;
        case LED_CC_REF:
            BoardLEDMask = 0x0000;      // Need to define these further 
            BoardLEDShift = 0;
            break;
        case LED_TIP:
            BoardLEDMask = 0x0000;      // Need to define these further 
            BoardLEDShift = 0;
            break;

#endif

        default:
            BoardLEDMask = 0xc03f;
            BoardLEDShift = 8;
            break;
    }

    return BoardLEDMask;
}
