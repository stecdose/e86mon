#include "interlib.h"
#include "emon.h"

//////////////////////////////////////////////////////////////////////////////
//    DispatchInstall is used to install a library's DispatchFunc:
//
typedef void (far cdecl * DispatchInstall)(FPEmonInternals internals);

//////////////////////////////////////////////////////////////////////////////
//    LibInfo is the header stored in front of libraries and .EXE files
//    which have been stored to flash.
//
typedef struct {
    BYTE    Signature[22];
    WORD    AllocLength;    //  Required memory allocation in paragraphs
                            //  (includes PSP and extra memory)
    DWORD   ProgStart;      //  Stored location of program
    DWORD   RelocStart;     //  Stored location of relocations
    DWORD   ExtraStart;     //  Extra information (relocation ends here)
    DWORD   NextStruct;     //  Structure ends here.
    DWORD   CSIP;
    DWORD   SSSP;
    char    Name[16];       //  Name of program
} LibInfo, far * LPLibInfo;

//////////////////////////////////////////////////////////////////////////////
//    LibHdr is the header stored in linked libraries.
//
typedef struct {
    WORD    ShortJmp;          // Jump around the rest of this
    BYTE    Signature[22];
} LibHdr, far * LPLibHdr;

//////////////////////////////////////////////////////////////////////////////
//    Functions in dispatch called by other monitor functions
//
DWORD NextLoadedProg(DWORD Prev, BOOL Validate);
void ClearLibraries(void);
void LinkLibraries(void);

//////////////////////////////////////////////////////////////////////////////
//    Used to make sure routines are our libraries
//
extern LibHdr ROM LibSig;

#ifdef DEF_SIGNATURE

LibHdr ROM LibSig = {0x16EB,"E86Mon Lib Extension 1"};
#endif
