/******************************************************************************
 *                                                                            *
 *     186INIT.H                                                              *
 *                                                                            *
 *     This file contains structure and function declarations for             *
 *     communication between the 186-specific initialization routines.        *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#define EM_SERIAL 0
#define ES_SERIAL 1
#define CC_SERIAL 2

#define IMASK_ON     0x10        // 0 = highest priority
#define IMASK_OFF    0x18        // Mask off

#define LED_EM_ES_ER   0         // LED signals for SD 186 EM, ES, ER
#define LED_ED         1         // LED signals for SD 186 ED board
#define LED_VER        2         // LED signals for Verification board
#define LED_CEP        3         // LED signals for the 186 CC Customer 
                                 //     Evaluation Platform
#define LED_CC_REF     4         // LED signals for the 186 CC Reference Design
#define LED_TIP		   5	     // LED on Test Interface Port
//
// All IO ports and masks associated with serial communication
//
typedef struct {
    WORD CtlAddr;
    WORD StatAddr;
    WORD TxAddr;
    WORD RxAddr;
    WORD BaudAddr;
    WORD IMaskAddr;
    WORD EOIAddr;
    WORD IntType;
    WORD IntsEnabled;
    WORD IntsDisabled;
    WORD DisabledCtlMask;
    WORD NoIntCtlMask;
    WORD RxCtlMask;
    WORD TxCtlMask;
    WORD RxStatMask;
    WORD TxStatMask;
    WORD BreakStatMask;
    WORD ErrStatMask;
    WORD BaudMConst;
    WORD BaudSConst;
    WORD PIOEnable;
    WORD RxPIOBits;
} SerPort, far * SerPortPtr;

extern WORD BaudDivisor;

//
// Routine in 186SER.C called from 186INIT.C and 186INITS.C:
//     Initialize a single serial port
//
void InitSPRT(SerPortPtr Sprt, BOOL EnableInterrupts);

// Routines in 186INITS.C called from 186INIT.C:
//     Set default serial port and baudrate, get default serial port
//
void SetDefSerial(BOOL FirstTime, WORD PortType, WORD DefPort,
           DWORD BaudRate, DWORD CpuSpeed);
SerPortPtr GetDefSerial(void);

// Routine in 186TIMER.C called from 186INIT.C
//
void EnableTimerInterrupt(void);
void InitTimer(BOOL FirstTime, DWORD CpuSpeed);

// Routine in 186LEDS.C called from 186INIT.C
//
WORD SetLedType(WORD System, WORD UseLEDs);
