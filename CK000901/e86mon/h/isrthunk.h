/******************************************************************************
 *                                                                            *
 *     ISRTHUNK.H                                                             *
 *                                                                            *
 *     This file contains definitions for E86MON thunking structures.         *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#ifndef _isrthunk_h_
#define _isrthunk_h_

#pragma pack (1)
typedef struct {
    BYTE PushA;
    BYTE PushDS;
    BYTE PushES;
    BYTE PushCS;
    BYTE PopDS1;
    BYTE FarCall;
    LPVOID CallTarget;
    BYTE PopES;
    BYTE PopDS2;
    BYTE PopA;
    BYTE IRet;
               } ISRThunkData;
#pragma pack ()

#define ISRTHUNK(Proc)  \
    static ISRThunkData Thunk_##Proc = \
        {0x60,0x1E,0x06,0x0E,0x1F,0x9A, Proc, 0x07,0x1F,0x61,0xCF}

void SetISRVector(WORD iNum, ISRThunkData _far * CodeProc);

#endif
