
#include "interlib.h"

// The priority number determines where in the E86Mon chain we get
// linked in.  Lower numbers are linked earlier in the dispatch chain.

WORD near pascal LibPriority = 0x8000;

// The LibName is displayed for our module name by the "i" command.

char near pascal LibName[]   = "EXTEMON";

