/******************************************************************************
 *                                                                            *
 *     LINEEDIT.C                                                             *
 *                                                                            *
 *     This file contains a line editor which replaces the line-oriented      *
 *     input function in the base monitor's CONINPUT.C file.                  *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996-1997 Advanced Micro Devices, Inc.                           *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/
#include "interlib.h"

// Disable optimization warning
#pragma warning(disable : 4704)

#define SCREEN_COLUMNS    79      // Max width supported

//
//    static data storage
//
static ROMCHAR backupstr[] = {ASCII_BS,0};
static ROMCHAR space[] = " ";

static ROMCHAR CancelMsg[] = " %2X ##\n";
static ROMCHAR BreakMsg[]  = "\n   (returning from monitor BREAK)\n";
static ROMCHAR PrintChar[] = "%c";

BYTE CmdLineStorage[160];


#define MAX_STR ((WORD)(sizeof(CmdLineStorage)-1))

static WORD StrIndex;
static WORD CharIndex;
static BOOL InsMode;
static WORD CurrentLength;

//////////////////////////////////////////////////////////////////////////
//   GetTimeState() -- gets a designated state, based on time
//
WORD GetTimeState(WORD msPerCycle, WORD NumStates)
{
    DWORD  mSec;

    GetCurrentTime(&mSec);

    return (WORD)((mSec % msPerCycle)/(msPerCycle/NumStates));
}

//////////////////////////////////////////////////////////////////////////
//   InputLEDPattern() -- shows a unique pattern on LEDs when waiting
//
void InputLEDPattern(BOOL Stop)
{
    static BOOL Running  = FALSE;
    static BYTE LEDCode;
    static WORD Pattern = 0;
    static WORD State = 0;

    WORD   Output;

    if (!Running)
    {
        Running = TRUE;
        LEDCode = InquireLEDs();
        Pattern = (Pattern+1) % 8;
        State = 0;
    }

    if (Stop)
    {
        PostLEDs(LEDCode);
        Running = FALSE;
        return;
    }

    switch ((Pattern < 7) ? Pattern : GetTimeState(300,7))
    {
        // Alternating 55/AA in 1.5 second cycle
        case 0:
            Output = (WORD)(0x55 << GetTimeState(1500,2));
            break;

        // Pass-through oscillations, 1.5 second cycle
        case 1:
            Output = GetTimeState(1500,4) * 2;
            Output = (WORD)((1 << Output) | (0x80 >> Output));
            break;
                     
        // Alternating quick up/down, 3 second cycle
        case 2:
            if (GetTimeState(3000,2))
                Output = (WORD)(1 << GetTimeState(1500,13));
            else
                Output = (WORD)(0x80 >> GetTimeState(1500,13));
            break;

        // Alternating C3, 3C, .5 second cycle
        case 3:
            Output = (WORD)(GetTimeState(500,2) ? 0x3C : 0xC3);
            break;

        // Collision pattern, 2 second cycle
        case 4:
            Output = GetTimeState(2000,14);
            Output = (WORD)((1 << Output) | (0x80 >> Output));
            break;

        // Fast skipping oscillation, .6 second cycle
        case 5:
            Output = GetTimeState(600,8) * 2;
            Output = (WORD)((1 << Output) | (2 << (14-Output)));
            break;

        // VU meter, 48 second cycle
        case 6:
            Output = GetTimeState(48000,480);
            Output = ((Output % 37) ^ Output ^ (Output%91) ^ (Output%19)) % 9;
            if (Output < State)
                State--;
            else
                State++;
            Output = (WORD)(1 << State) -1;
            break;

        default:
            Output = 0;
    }
    PostLEDs((BYTE)Output);
}

//////////////////////////////////////////////////////////////////////////
//   Backup() outputs control-H's and moves current position back.
//
void Backup(WORD howfar, BOOL update)
{

    if (((int)howfar) < 0)
        return;

    if (update)
    {
        if (howfar > CharIndex)
            howfar = CharIndex;
        CharIndex -= howfar;
    }

    while (howfar--)
        printf(backupstr);
}
    
//////////////////////////////////////////////////////////////////////////
//   FollowingString() points to the next string in CmdLineStorage
//
WORD FollowingString(WORD index)
{
    return index + CmdLineStorage[index];
}

//////////////////////////////////////////////////////////////////////////
//   GoodString() returns TRUE if a string in CmdLineStorage appears undamaged
//
BOOL GoodString(WORD index)
{
    WORD next;
    WORD i;

    next = FollowingString(index);
    i = next - index;

    if ((index >= MAX_STR) || (next > MAX_STR) || (i < 2)
        || (i - 2 > SCREEN_COLUMNS - EmonData->PromptLength) )
        return FALSE;

    for (i=index+1;i < next-1; i++)
        if ((CmdLineStorage[i] < ' ') || (CmdLineStorage[i] > 0x7E))
            return FALSE;

    return CmdLineStorage[next-1] == 0;
}

//////////////////////////////////////////////////////////////////////////
//   FirstBad() returns a pointer to the first bad string in CmdLineStorage
//
WORD FirstBad(void)
{
    WORD index = 0;
    while (GoodString(index))
        index = FollowingString(index);
    return index;
}

//////////////////////////////////////////////////////////////////////////
//   CopyString() copies a string out of CmdLineStorage into the command
//   line buffer.  It also removes all vestiges of previous string from
//   screen and sets the cursor to the end of the new string.
//
void CopyString(WORD index)
{
    WORD OldLength = CurrentLength;

    Backup(CharIndex,TRUE);

    if (!GoodString(index))
    {
        EmonData->InputLine[0] = 0;
        CurrentLength = 0;
    }
    else
    {
        CurrentLength = (WORD)CmdLineStorage[index]-2;
        emon_memcpy(EmonData->InputLine,CmdLineStorage+index+1,CurrentLength+1);
    }

    printf((LPCSTR)EmonData->InputLine);

    CharIndex = CurrentLength;

    while (CharIndex < OldLength)
    {
        printf(space);
        CharIndex++;
    }

    Backup(CharIndex - CurrentLength,TRUE);
}

//////////////////////////////////////////////////////////////////////////
//   UpString() is called when the user presses the up-arrow.  It
//   finds the "previous" string in CmdLineStorage, and uses CopyString
//   to blast it to the display.
//
void UpString(void)
{
    WORD sentinel = StrIndex;

    StrIndex = 0;

    if (sentinel == 0)
        sentinel = 0xFFFF;

    while (GoodString(StrIndex) &&
          (FollowingString(StrIndex) < sentinel) &&
          GoodString(FollowingString(StrIndex)) )
        StrIndex = FollowingString(StrIndex);
}

//////////////////////////////////////////////////////////////////////////
//   DownString() is called when the user presses the down-arrow.  It
//   finds the "next" string in CmdLineStorage, and uses CopyString
//   to blast it to the display.
//
void DownString(void)
{
    if (GoodString(StrIndex))
        StrIndex = FollowingString(StrIndex);

    if (!GoodString(StrIndex))
        StrIndex = 0;
}

//////////////////////////////////////////////////////////////////////////
//   InsertString() inserts the current command into CmdLineStorage,
//   removing as many previous commands as necessary to make it fit.
//
void InsertString(void)
{
    
    WORD newlen   = CurrentLength+2;
    WORD maxidx   = MAX_STR - newlen;
    WORD storeloc = FirstBad();

    if (EmonData->InputLine[0] == 0)
        return;

    StrIndex = 0xFFFF;
    UpString();

    if (GoodString(StrIndex) && 
         (emon_strcmp ((LPCSTR)CmdLineStorage+StrIndex+1,
                       (LPCSTR)EmonData->InputLine) == 0))
        return;

    while (storeloc > maxidx)
    {
        WORD index = FollowingString(0);
        memcpy(CmdLineStorage,CmdLineStorage+index,storeloc-index);
        storeloc -= index;
        CmdLineStorage[storeloc] = 0;
    }
    CmdLineStorage[storeloc] = (BYTE)newlen;
    emon_memcpy(CmdLineStorage+storeloc+1,EmonData->InputLine,newlen-1);
    CmdLineStorage[storeloc+newlen] = 0;
}

//////////////////////////////////////////////////////////////////////////
//   InitInput() is called every time the line editing package is entered,
//   and every time it is reentered after a break.
//
void InitInput(LPCSTR Prompt, BOOL wasBreak)
{
    EmonData->MonitorExited = FALSE;
    CharIndex = 0;
    CurrentLength = 0;
    InsMode = FALSE;
    StrIndex = 0xFFFF;

    if (EmonData->LineUnfinished)
        PrintCRLF();

    if (wasBreak)
        printf(BreakMsg);

    printf(Prompt);
    EmonData->PromptLength = (WORD)emon_strlen(Prompt);
}
 
//////////////////////////////////////////////////////////////////////////
//    LineEdit() -- Scan in a line from the user and place it
//                  into InputLine.
//
BOOL LineEdit(LPCSTR Prompt)
{
    BOOL done;
    BOOL gotline;
    LPSTR InputLine;
    char newchar;
    WORD MaxIndex;
    WORD i;

    done    = FALSE;
    gotline = TRUE;
    InputLine = (LPSTR)EmonData->InputLine;

    InitInput(Prompt, FALSE);

    MaxIndex =  SCREEN_COLUMNS - EmonData->PromptLength;

    while (!done) {
        while (!EmonData->MonitorExited && !kbhit())
            InputLEDPattern(FALSE);

        if (EmonData->MonitorExited)
            InitInput(Prompt, TRUE);

        // Get a character
        newchar = (BYTE)(getch() & 0x7F);
        switch (newchar) {
            case ASCII_CR:
            case ASCII_LF:
                InputLine[CurrentLength] = 0;
                PrintCRLF();
                InsertString();
                done = TRUE;
                break;
            case ASCII_BS:
                if (CharIndex == 0)
                    break;
                Backup(1,TRUE);
                // Drop through to delete char
            case ASCII_DEL:
                if (CurrentLength == 0)
                    break;
                CurrentLength-=1;
                emon_memcpy(InputLine+CharIndex,
                         InputLine+CharIndex+1,
                         CurrentLength-CharIndex);
                InputLine[CurrentLength] = ' ';
                InputLine[CurrentLength+1] = 0;
                printf(InputLine+CharIndex);
                Backup(CurrentLength+1-CharIndex,FALSE);
                break;
            case ASCII_TAB:
                InsMode = !InsMode;
                break;
            case ASCII_ESC:
                i = (WORD)(EmonData->Milliseconds);
                while (!kbhit() && ((WORD)(EmonData->Milliseconds) - i < 200) )
                {
                    ; // Do nothing
                }
                if (!kbhit() && (CurrentLength > 0))
                {
                    CopyString(0xFFFF);
                    break;
                }
                if (!kbhit() || (getch() != '['))
                {
                    gotline = FALSE;
                    break;
                }
                switch (getch())
                {
                  case 'A':
                      UpString();
                      CopyString(StrIndex);
                      break;
                  case 'B':
                      DownString();
                      CopyString(StrIndex);
                      break;
                  case 'C':
                      if (CharIndex >= CurrentLength)
                          break;
                      printf(PrintChar,InputLine[CharIndex++]);
                      break;
                  case 'D':
                      Backup(1,TRUE);
                      break;
                  default:
                      gotline = FALSE;
                      break;
                  }
                  break;

            default:
                if (newchar >= ' ')
                {
                    if (!InsMode)
                    {
                        if (CharIndex < MaxIndex)
                        {
                            InputLine[CharIndex++] = newchar;
                            printf(PrintChar,newchar);
                            if (CharIndex > CurrentLength)
                                CurrentLength = CharIndex;
                        }
                    }
                    else if (CurrentLength < MaxIndex)
                    {
                        for (i=CurrentLength; i > CharIndex; i--)
                            InputLine[i] = InputLine[i-1];
                        InputLine[CharIndex] = newchar;
                        CurrentLength++;
                        InputLine[CurrentLength] = 0;
                        printf(InputLine+CharIndex);
                        CharIndex++;
                        Backup(CurrentLength-CharIndex,FALSE);
                    }
                }
                else
                    gotline = FALSE;
                break;
        } // switch

        if (!gotline)
        {
            printf(CancelMsg,newchar);
            InputLine[0] = 0;
            done = TRUE;
        }
    } // while

    EmonData->LineIndex = EmonData->ErrorIndex = 0;
    InputLEDPattern(TRUE);
    return gotline;
}


//////////////////////////////////////////////////////////////////////////
//  This DISPATCH macro links us into E86Mons\'s dispatch chain so that
//  we can call LineEdit to process GetInputLine messages.
//  Just let the base monitor handle the ones with echo turned off --
//  those are for hex file downloads.
//
DISPATCH(LineEditor)
{
    case Dispatch_GetInputLine:
        if (dstruc->GetInputLine.echo)
            return LineEdit(dstruc->GetInputLine.Prompt);
        break;
    default:
        break;
}
END_DISPATCH
