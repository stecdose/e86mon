/******************************************************************************
 *                                                                            *
 *     DOSEMU.C                                                               *
 *                                                                            *
 *     This file contains a minimal DOS emulator, primarily an                *
 *     int 21h handler with a few supporting routines.                        *
 *                                                                            *
 *     Console, time, and memory allocation calls are supported.              *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1996-1997 Advanced Micro Devices, Inc.                           *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

#include "interlib.h"
#include "message.h"

#define  DOS_VERSION    210

#define  DOSSVC_TERM    0x00
#define  DOSSVC_CHRIN   0x01
#define  DOSSVC_CHROUT  0x02
#define  DOSSVC_STROUT  0x09
#define  DOSSVC_KBDSTS  0x0b
#define  DOSSVC_SETISRV 0x25
#define  DOSSVC_GETDATE 0x2a
#define  DOSSVC_GETTIME 0x2c
#define  DOSSVC_SETTIME 0x2d
#define  DOSSVC_GETVER  0x30
#define  DOSSVC_GETISRV 0x35
#define  DOSSVC_READ    0x3f
#define  DOSSVC_WRITE   0x40
#define  DOSSVC_GETDVI  0x44
#define  GETDVI_GETDVD  0x00
#define  DOSSVC_ALLOCM  0x48
#define  DOSSVC_FREEM   0x49
#define  DOSSVC_RESIZM  0x4a
#define  DOSSVC_TERMP   0x4c

#define  HANDLE_STDIN   0x00
#define  HANDLE_STDOUT  0x01
#define  HANDLE_STDERR  0x02
#define  HANDLE_AUX     0x03
#define  HANDLE_PRN     0x04

//
//    Microsoft-compatible memory "arena" types
//
#define ARENA_MIDDLE            'M'
#define ARENA_END               'Z'
//
//    MS-DOS memory error definitions
//
#define ERROR_ARENA_TRASHED      7
#define ERROR_NOT_ENOUGH_MEMORY  8
#define ERROR_INVALID_BLOCK      9

//
// Memory control records are compatible with MS-DOS's.  Each
// memory block is paragraph-aligned.  The preceding paragraph
// is an "arena header" with the following format:
//
#pragma pack (1)
typedef struct {
    BYTE arenaSignature;           // ARENA_MIDDLE or ARENA_END
    WORD arenaOwner;               // 0 for free, non-zero for owned
    WORD arenaSize;                // size of arena in paragraphs
} far * LPARENA;
#pragma pack ( )

static WORD ArenaError;            // Most recent arena error code

//
//    psp - program segment prefix structure
//
typedef struct {
   BYTE rbTermVect[2];      // PSP:0000 - Terminate vector
   WORD usEndAllocation;    // PSP:0002 - Address of last segment allocated
   BYTE rbReserved1[1];
   BYTE rbDosCalVect[5];    // PSP:0005 - DOS Function call
   BYTE rbVectorSave[12];   // PSP:000A - Parent's saved IRQ vectors
   BYTE rbReserved2[22];
   WORD usEnvSeg;           // PSP:002C - Environment Segment        
   BYTE rbReserved3[34];
   BYTE rbINT21[3];         // PSP:0050 - New Dos call vector
   BYTE rbReserved4[9];
   BYTE rbPrimaryFCB[16];   // PSP:005C - default FCB
   BYTE rbSecondaryFCB[16]; // PSP:006C - secondary FCB
   BYTE rbReserved5[4];
   BYTE rbCommandTail[128]; // PSP:0080 - command line tail
} PSP, far * LPPSP;

// Disable optimization warning
#pragma warning(disable : 4704)

///////////////////////////////////////////////////////////////////////
// FirstArena() returns the address of the first arena, which
// immediately precedes EmonData->BigBuffer.
//
LPARENA FirstArena(void)
{
   LPARENA a;
   a = (LPARENA)EmonData->BigBuffer;
   _FP_SEG(a)--;
   return a;
}

///////////////////////////////////////////////////////////////////////
// NextArena() returns the address of the next arena, given a starting
// one.  It performs no error checking, because it is used to insert
// arenas.
//
LPARENA NextArena(LPARENA a)
{
    _FP_SEG(a) += 1 + a->arenaSize;
    return a;
}

///////////////////////////////////////////////////////////////////////
// PrevArena() returns the address of the previous arena, given an
// arena.  It performs validation on the arena chain as it goes
// along.  If there is a problem, it stores the error code in 
// ArenaError and returns NULL.
//
LPARENA PrevArena(LPARENA a, BOOL NoError)
{
    LPARENA a1,a2;

    a2 = FirstArena();
 
    for ( ;; )
    {
        a1 = a2;
        a2 = NextArena(a1);
        if (a1->arenaSignature == ARENA_END)
        {
            ArenaError = ERROR_INVALID_BLOCK;
            break;
        }
        if (a1->arenaSignature != ARENA_MIDDLE)
        {
            ArenaError = ERROR_ARENA_TRASHED;
            break;
        }
        if ((DWORD)a2 > (DWORD)a)
        {
            ArenaError = ERROR_INVALID_BLOCK;
            break;
        }
        if ((DWORD)a2 == (DWORD)a)
        {
            return a1;
        }
        if ((DWORD)a2 <= (DWORD)a1)
        {
            ArenaError = ERROR_ARENA_TRASHED;
            break;
        }
    }
    if (NoError)
        return a1;
    return 0;
}

///////////////////////////////////////////////////////////////////////
// RAMSafe() returns the size of "safe" RAM.  If the RAM appears
// to have been trashed by the monitor, this is 0.
//
WORD Base_RAMSafe(void)
{
    LPARENA prev;

    ArenaError = 0;
    
    // If the monitor is RAM-based, all of EmonData->BigBuffer is safe because
    // the monitor never moves, so it never destroys it.

    if (_FP_SEG(EmonData->BigBuffer) > CurrentCS())
        return EmonData->BigBufParagraphs;

    prev = PrevArena(MK_FP(CurrentCS(),0),TRUE);
    if (ArenaError == ERROR_ARENA_TRASHED)
        return 0;

    if (prev->arenaSignature != ARENA_END)
        if ((prev->arenaOwner != 0) ||
            (NextArena(prev)->arenaSignature != ARENA_END))
            return 0;

    return _FP_SEG(prev);
}

///////////////////////////////////////////////////////////////////////
// ValidArena() returns TRUE if an arena is valid.  If it returns
// FALSE, ArenaError will contain the cause of the problem.
//
BOOL ValidArena(LPARENA a)
{
    ArenaError = ERROR_ARENA_TRASHED;
    return (((DWORD)a == (DWORD)FirstArena()) || (PrevArena(a,FALSE) != 0))
                && (a->arenaSignature == ARENA_MIDDLE);
}

///////////////////////////////////////////////////////////////////////
// CombineBlocks combines an arena with the next one, if the next
// one is unowned, and returns a pointer to the combined arena.
//
LPARENA CombineBlocks(LPARENA a)
{
    LPARENA b = NextArena(a);
    if ((b->arenaOwner == 0) && (b->arenaSignature == ARENA_MIDDLE))
        a->arenaSize += b->arenaSize+1;

    return a;
}

///////////////////////////////////////////////////////////////////////
// InitArenas() -- setup the memory areas
//
void InitArenas(void)
{
   LPARENA a = FirstArena();

   a->arenaSignature = ARENA_MIDDLE;
   a->arenaSize      = EmonData->BigBufParagraphs;
   a->arenaOwner     = _FP_SEG(EmonData->BigBuffer);

   a = NextArena(a);
   a->arenaSignature = ARENA_END;
   a->arenaSize      = 0;
   a->arenaOwner     = 0;
}

///////////////////////////////////////////////////////////////////////
// InitPSP() performs a minimal program-segment-prefix and memory block
// initialization, when a program is loaded.  The command parameters
// are not touched, because the Name command sets them.
//
void Base_InitPSP(WORD MinProgramParagraphs)
{
    LPPSP psp;
    WORD  delta;

    InitArenas();

    if (MinProgramParagraphs == 0)
        return;

    if (MinProgramParagraphs > EmonData->BigBufParagraphs)
    {
        PrintError(DosMsgInsufficientRAM);
        return;
    }

    delta = EmonData->BigBufParagraphs - MinProgramParagraphs;

    // Always allocate at least 1K local heap if available.
    // Never allocate more than 16K local heap.
    // Split the difference between local and far heap.

    // The borland compiler seems to die if it doesn't get enough
    // heap (its MinAllocation is set incorrectly)


    if (delta < 0x80)
        delta = 0x40;
    else
        delta /= 2;

    if (delta > 0x400)
        delta = 0x400;

    psp = (LPPSP)EmonData->BigBuffer;

    MinProgramParagraphs += delta;           // Add local heap

    psp->rbTermVect[0] = 0xcd;              // fill in PSP
    psp->rbTermVect[1] = 0x20;
    psp->usEndAllocation = _FP_SEG(psp) + MinProgramParagraphs;
    psp->usEnvSeg = _FP_SEG( psp ) + 3;

    _asm {
        mov     bx,MinProgramParagraphs
        mov     es,word ptr psp[2]
        mov     ah,DOSSVC_RESIZM
        int     21h
    }
}

///////////////////////////////////////////////////////////////////////
//  DosSvcStrOut() displays a string terminated by a dollar sign.
//
void DosSvcStrOut(FPSTACKREC stack)
{
    LPSTR ptr = MK_FP(stack->DS,stack->DX);
    while( *ptr != '$' )
        putch( *(ptr++) );
    FlushTransmitter();
}

///////////////////////////////////////////////////////////////////////
//    DosSvcGetTime() -- return current time-of-day
//
//    Note:  this is solely based on the number of elapsed ticks since reset
//
void DosSvcGetTime(FPSTACKREC stack)
{
    DWORD ms;

    GetCurrentTime(&ms);

    stack->DL = (BYTE)((ms/10)%100);        // 1/100 sec
    stack->DH = (BYTE)((ms/1000) % 60);     // Seconds
    stack->CL = (BYTE)((ms/60000L) % 60);   // minutes
    stack->CH = (BYTE)((ms/3600000L) % 24); // Hours

}

///////////////////////////////////////////////////////////////////////
// DosSvcGetDate() returns a bogus date.
//
void DosSvcGetDate(FPSTACKREC stack)
{
    stack->DL = (BYTE)(30);
    stack->CX = 1997;
    stack->DH = 10;
    stack->AL = (BYTE)(4);
}

///////////////////////////////////////////////////////////////////////
// DosSvcSetTime() -- set the current time
//
void DosSvcSetTime(FPSTACKREC stack)
{
    DWORD NewTick;

    if ((stack->CH > 23) ||
        (stack->CL > 59) ||
        (stack->DH > 59) ||
        (stack->DL > 99))
    {
        stack->Flags |= 0x0001;       // set carry flag (assume failure)
        return;
    }
    stack->Flags &= ~0x0001;

    NewTick = stack->CH;                      // hours
    NewTick = NewTick * 60  + stack->CL;      // minutes
    NewTick = NewTick * 60  + stack->DH;      // seconds
    NewTick = NewTick * 100 + stack->DL;
    NewTick *= 10;

    _disable();
    EmonData->Milliseconds = NewTick;
    _enable();
    stack->AL = 0;                            // signal success
}

///////////////////////////////////////////////////////////////////////
// DosSvcGetVer() -- return DOS version number
//
void DosSvcGetVer(FPSTACKREC stack)
{
   stack->AX = (DOS_VERSION / 100) | ((DOS_VERSION % 100) << 8);
   stack->BX = 0;
   stack->CX = 0;
}

///////////////////////////////////////////////////////////////////////
//    GetDVIGetDeviceData() -- return device data word
//
void GetDVIGetDeviceData(FPSTACKREC stack)
{
   stack->Flags &= ~0x0001;                   // clear carry flag
   switch( stack->BX ) {
      case HANDLE_STDIN:
     stack->DX = 0x80d3;
     break;

      case HANDLE_STDOUT:
     stack->DX = 0x00c2;
     break;

      case HANDLE_STDERR:
     stack->DX = 0x80d3;
     break;

      case HANDLE_AUX:
     stack->DX = 0x80c0;
     break;

      case HANDLE_PRN:
     stack->DX = 0xa0c0;
     break;
   }
}

///////////////////////////////////////////////////////////////////////
//    GetDVI() -- return device data word
//
BOOL GetDVI(FPSTACKREC stack)
{
    switch( stack->AL )
    {
        case GETDVI_GETDVD:
            GetDVIGetDeviceData(stack);
            return TRUE;

        case 'A':
            if (stack->BX == 'M'*256 + 'D')
              ;  // Add stuff here later
    }
    return FALSE;
}

///////////////////////////////////////////////////////////////////////
//    DosSvcRead() -- characters from device
//
//    Note: we don't check the handle at this point
//
void DosSvcRead(FPSTACKREC stack)
{
    LPBYTE pbyte = MK_FP(stack->DS, stack->DX);
    WORD   index = 0;

    stack->Flags &= ~0x0001;                       // clear carry flag

    GetInputLine(SystemMsgNull,TRUE);

    EmonData->TrashedInputLine = TRUE;

    index = (WORD)emon_strlen((LPCSTR)EmonData->InputLine);
    EmonData->InputLine[index++] = ASCII_CR;
    EmonData->InputLine[index++] = ASCII_LF;

    if (index > stack->CX)
        index = stack->CX;

    emon_memcpy(pbyte,EmonData->InputLine,index);

    stack->AX = index;
}

///////////////////////////////////////////////////////////////////////
//    DosSvcWrite() -- characters to device
//
//    Note: we don't check the handle at this point
//
void DosSvcWrite(FPSTACKREC stack)
{
    LPBYTE pbyte = MK_FP(stack->DS, stack->DX);

    stack->Flags &= ~0x0001;                       // clear carry flag
    stack->AX = 0;

    while(stack->AX++ < stack->CX)
        putch( *pbyte++ );
    FlushTransmitter();
}

///////////////////////////////////////////////////////////////////////
//    DosSvcAllocMem() -- try to allocate a memory block
//
void DosSvcAllocMem(FPSTACKREC stack)
{
    LPARENA a = FirstArena();
    WORD    LargestFree = 0;
    WORD    RequestedSize = stack->BX;
    WORD    ThisBlock;

    stack->Flags |= 0x0001;                      // set carry flag (assume failure)

    for ( ;; )
    {
        if (a->arenaSignature != ARENA_MIDDLE)
        {
            stack->AX = ERROR_ARENA_TRASHED;
            if (a->arenaSignature == ARENA_END)
            {
                stack->AX = ERROR_NOT_ENOUGH_MEMORY;
                stack->BX = LargestFree;
            }
            return;
        }
        if ((a->arenaOwner == 0) &&
            (a->arenaSize > LargestFree) &&
            ((LargestFree = a->arenaSize) > RequestedSize))
        {
            // Allocate it and then resize it

            stack->AX     = ThisBlock = _FP_SEG(a)+1;
            a->arenaOwner = _FP_SEG(EmonData->BigBuffer);
            _asm {
                mov     bx,RequestedSize
                mov     es,ThisBlock
                mov     ah,DOSSVC_RESIZM
                int     21h
            }
            stack->Flags     &= ~0x0001;            // clear carry flag
            return;
        }
        a = NextArena(a);
    }
}

///////////////////////////////////////////////////////////////////////
//    DosSvcFreeMem() -- return passed block to free list
//
void DosSvcFreeMem(FPSTACKREC stack)
{
    LPARENA prev,free;

    free = MK_FP(stack->_ES-1,0);

    if ((prev=PrevArena(free,FALSE)) == 0)
    {
        stack->AX = ArenaError;
        stack->Flags |= 0x0001;       // set carry flag (assume failure)
        return;
    }

    free->arenaOwner = 0;

    // Combine with previous block if it's free

    if (prev->arenaOwner == 0)
        free = CombineBlocks(prev);

    // Combine with the next block

    CombineBlocks(free);

    stack->Flags &= ~0x0001;                       // clear carry flag
}

///////////////////////////////////////////////////////////////////////
//    DosSvcResizeMem() -- re-size passed block
//
void DosSvcResizeMem(FPSTACKREC stack)
{
    LPARENA a,b;
    WORD    OldSize;

    a = MK_FP(stack->_ES - 1,0);
    stack->Flags |= 0x0001;       // set carry flag (assume failure)

    if (!ValidArena(a))
    {
        stack->AX = ArenaError;
        return;
    }

    OldSize = a->arenaSize;
    if (stack->BX > OldSize)
    {
        b = NextArena(a);
        if (b->arenaSignature != ARENA_MIDDLE)
        {
            if (b->arenaSignature == ARENA_END)
                stack->AX = ERROR_NOT_ENOUGH_MEMORY;
            else
                stack->AX = ERROR_ARENA_TRASHED;
            return;
        }
        if ((b->arenaOwner != 0) || (b->arenaSize < stack->BX - OldSize - 1))
        {
            stack->AX = ERROR_NOT_ENOUGH_MEMORY;
            return;
        }

        // Grab the next block

        CombineBlocks(a);
        OldSize = a->arenaSize;
    }
                
    if (stack->BX < OldSize)
    {
        a->arenaSize = stack->BX;
        a = NextArena(a);
        a->arenaSignature = ARENA_MIDDLE;
        a->arenaSize = OldSize - stack->BX -1;
        a->arenaOwner = 0;

        CombineBlocks(a);           // Add in next free block
    }

    stack->Flags &= ~0x0001;

}

///////////////////////////////////////////////////////////////////////
// ShowDosMemChain() prints out the memory chain for inspection
//
void ShowDosMemChain(void)
{
    LPARENA a = FirstArena();

    while (a->arenaSignature == ARENA_MIDDLE)
    {
        printf(DosMsgMemBlock,_FP_OFF(a),_FP_SEG(a)+1,a->arenaSize,a->arenaOwner);
        a = NextArena(a);
    }
    if (a->arenaSignature != ARENA_END)
        printf(DosMsgMemTrashed);
}

 
///////////////////////////////////////////////////////////////////////
// ConsoleInput() performs console input functions.  It is broken
// out as a separate procedure so that the flush function can call
// the same function with the code in AL instead of AH.
//
void ConsoleInput(FPSTACKREC stack, BYTE code)
{
    switch (code)
    {
        case DOSSVC_CHRIN:
            putch(stack->AL = getch());
            FlushTransmitter();
            break;

        case 6:    // Direct console I/O
            if (stack->DL == 0xFF)
            {
                stack->Flags |= 0x40;             // Set zero flag
                if (kbhit())
                {
                    stack->Flags &= ~0x40;             // Clear zero flag
                    stack->AL = getch();
                }
            }
            else
                putch(stack->DL);
            break;

        case 7:
        case 8:
            stack->AL = getch();
            break;

        case 0x0A:
        {
            LPBYTE pbyte = MK_FP(stack->DS, stack->DX);
            WORD   len;

            if (pbyte[0] == 0)
                break;

            GetInputLine(SystemMsgNull,TRUE);

            EmonData->TrashedInputLine = TRUE;

            len = (WORD)emon_strlen((LPCSTR)EmonData->InputLine);
            if (len >= pbyte[0])
                len = (WORD)(pbyte[0] - 1);
            emon_memcpy(pbyte+2,EmonData->InputLine,len);

            pbyte[1] = (BYTE)len;
            pbyte[2+len] = ASCII_CR;
        }
    }
}

///////////////////////////////////////////////////////////////////////
//    Irq21Entry() -- perform DOS service request
//
BOOL __far cdecl Irq21Entry( STACKREC stack )
{
    typedef void (*ServiceRoutine)(FPSTACKREC);

    ServiceRoutine SR = 0;

    _enable();                   // re-enable interrupts within service routine

    switch (stack.AH)
    {
        case DOSSVC_CHROUT:
            putch(stack.DL);
            FlushTransmitter();
            break;

        case 0x03:  // Aux Input
            stack.AL = InquireLEDs();
            break;

        case 0x04:  // Aux Output
            PostLEDs(stack.DL);
            break;

        case 0x0C:
            FlushReceiver();
            ConsoleInput(&stack,stack.AL);
            break;

        case DOSSVC_CHRIN:
        case 6:    // Direct console I/O
        case 7:
        case 8:
        case 0x0A:
            ConsoleInput(&stack,stack.AH);
            break;
 
        case DOSSVC_STROUT:
            SR = DosSvcStrOut;
            break;

        case DOSSVC_KBDSTS:
            stack.AL = (BYTE)(kbhit() ? 0xff : 0x00);
            break;

        case DOSSVC_SETISRV:
            ((LPWORD)(DWORD)(stack.AL * 4))[0] = stack.DX;
            ((LPWORD)(DWORD)(stack.AL * 4))[1] = stack.DS;
            break;

        case DOSSVC_GETISRV:
            stack.BX = ((LPWORD)(DWORD)(stack.AL * 4))[0];
            stack._ES = ((LPWORD)(DWORD)(stack.AL * 4))[1];
            break;

        case DOSSVC_GETDATE:
            SR = DosSvcGetDate;
            break;

        case DOSSVC_GETTIME:
            SR = DosSvcGetTime;
            break;

        case DOSSVC_SETTIME:
            SR = DosSvcSetTime;
            break;
   
        case DOSSVC_GETVER:
            SR = DosSvcGetVer;
            break;

        case DOSSVC_READ:
            SR = DosSvcRead;
            break;

        case DOSSVC_WRITE:
            SR = DosSvcWrite;
            break;

        case DOSSVC_GETDVI:
            if (!GetDVI(&stack))
                return FALSE;
            break;

        case DOSSVC_ALLOCM:
            SR = DosSvcAllocMem;
            break;

        case DOSSVC_FREEM:
            SR = DosSvcFreeMem;
            break;

        case DOSSVC_RESIZM:
            SR = DosSvcResizeMem;
            break;

        case 0:     // Original terminate process
            stack.AX = 0;
        case DOSSVC_TERMP:
            EmonData->ExitCode = stack.AL;
            _asm int 20h
            break;

        default:
            return FALSE;
    }
    if ((WORD)SR !=0)
        (SR)(&stack);
    return TRUE;
}


///////////////////////////////////////////////////////////////////////
//    The dispatcher is what causes EMONLINK.LIB to link us into E86Mon
//
//    This particular dispatcher sets up a thunk for the int 21h entry
//    point at restart time, and also handles calling out to
//    InitPSP(), RAMSafe(), and ShowDOSMemChain().
//
DISPATCH(DosEmu)
{
#pragma pack(1)
static struct {
    BYTE PushA;
    BYTE PushDS;
    BYTE PushES;
    BYTE PushCS;
    BYTE PopDS1;
    BYTE FarCall;
    LPVOID CallTarget;
    BYTE OrAxAx[2];

    BYTE PopES;
    BYTE PopDS2;
    BYTE PopA;
    BYTE Jz[2];
    BYTE IRet;
    BYTE FarJmp;
    LPVOID JmpTarget;
               } Thunk_Int21 = 
        {0x60,0x1E,0x06,0x0E,0x1F,0x9A, Irq21Entry, {0x09,0xC0},
          0x07,0x1F,0x61,{0x74,0x01}, 0xCF, 0xEA, 0};
#pragma pack()

    case Dispatch_Restart:
        if (EmonData->RestartCode <= RESTART_EXIT_PROG)
        {
            Thunk_Int21.JmpTarget = *(LPVOID far *)(DWORD)(0x21*4);
            *(LPVOID far *)(DWORD)(0x21*4) = &Thunk_Int21;
        }
        break;

    case Dispatch_InitPSP:
        Base_InitPSP(dstruc->InitPSP.MinProgramParagraphs);
        return Dispatch_IDidIt;

    case Dispatch_RAMSafe:
        return Base_RAMSafe();

    case Dispatch_Information:
        ShowDosMemChain();
        break;

  default:
        break;
}
END_DISPATCH
