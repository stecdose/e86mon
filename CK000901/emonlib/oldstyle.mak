!include ..\version.mak

all:
    @del emonlink.lib
    @$(AMD_LPD_CL)cl /nologo /c @<< 
@ /I..\include /f- /ACw /W4 /Oiscglo
@ /Gs /G1 /Zd /Zl /GA /Gc
@ /D VER_NUM=$(VER_NUM_C) /D VER_STRING=$(VER_STRING)
@ emonlibc.c
<<
    @$(AMD_LPD_CL)lib /NOLOGO /BATCH emonlink +emonlibc;
    @del emonlibc.obj
    @oldhelp1  $(VER_NUM_C) $(BETA_NUM) Near_
    @oldhelp1  $(VER_NUM_C) $(BETA_NUM) Far_
    @oldhelp1  $(VER_NUM_C) $(BETA_NUM) Ram_
    @del emonlink.bak
    @set ml= /c /Sa /DSTKSIZE=1024 /W3 /Zi /Iinc
	@$(AMD_LPD_ML)ml  /nologo emonliba.asm

clean:
    del *.lib
    del *.obj
