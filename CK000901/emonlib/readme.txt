
Making the library:

There are two ways to make the library:

NEWSTYLE.BAT makes a library using the features that MS C++ uses to
discard unused member functions.  This builds very quickly.

OLDSTYLE.BAT makes an old-fashioned library the hard way.  If your
linker requires this, do it; otherwise it is way too slow.

