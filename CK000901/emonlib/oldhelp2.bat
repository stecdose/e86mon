@copy elibfunc.c %4%3.c > nul
@%amd_lpd_cl%\cl /D MAKE_%3 /D CODE_LOC=%4 /D VER_NUM=%1 /D BETA_NUM=%2 %4%3.c
@%amd_lpd_cl%\lib /NOLOGO /BATCH emonlink +%4%3;
@del %4%3.obj
@del %4%3.c