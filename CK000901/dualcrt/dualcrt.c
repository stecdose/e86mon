/******************************************************************************
 *                                                                            *
 *     DUALCRT.C                                                              *
 *                                                                            *
 *     This file contains an example of a RAM-based E86Mon extension          *
 *     which enhances console I/O functionality to go to both ports           *
 *     on an ES or ED board                                                   *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * Copyright 1997 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

/*
 * Tell libraries we want RAM versions of all functions and we
 * want our code to be allocated in the DGROUP segment
 */
#define CODE_LOC Ram_

#include "interlib.h"

// Disable optimization warning
#pragma warning(disable : 4704)

WORD near LibPriority = 0x8000;       // Unique priority number, < 0xFFFF
char near LibName[]   = "DUALCRT";

// These routines are in OtherCRT.C

WORD CSEG OtherSerialIO(SerialAction action, WORD handle);
void CSEG OtherInitSPRT(void);

// This is used to chain to the main monitor's SerialIO function

SerialIOFunc MainSerialIO;

/////////////////////////////////////////////////////////////////////////
// SerialIO() is the runtime entry into this module.
//
WORD far SerialIO(SerialAction action, WORD handle)
{
    WORD result;

    // Since we're in RAM, it is easy to set up our DS.  Simply
    // save the caller's DS, and copy our CS into DS.

    _asm push ds
    _asm push cs
    _asm pop ds

    switch (action >> 8)
    {
        case SerialTryToGetch >> 8:

            result = MainSerialIO(action,handle);
            if (result == 0xFFFF)
               result = OtherSerialIO(action,handle);
            break;

        case SerialKbhit >> 8:

            result = (WORD)(MainSerialIO(action,handle) ||
                            OtherSerialIO(action,handle));
            break;

        default:
            MainSerialIO(action,handle);
            result = OtherSerialIO(action,handle);
            break;
    }

    // Restore caller's DS before returning

    _asm pop ds
    return result;
}

/////////////////////////////////////////////////////////////////////////
// The RAMLINK macro causes EMONLINK.LIB to insert this code into
// the RAM chain.  The macro takes a function name and a priority.
// Lower numbers will be inserted sooner in the chain.
//
// There are only 3 messages passed down this chain, and we only
// care about two of them.
//
// INTERRUPTS ARE ALWAYS OFF WHEN THIS FUNCTION IS INVOKED!!!
//
RAMLINK(DualCRT,0x8000)
{
    // One-time installation call

    case RAM_INSTALL:
        MainSerialIO = EmonData->SerialIO;
        EmonData->SerialIO = SerialIO;
        break;

    // Every time we are restarted, this is called

    case RAM_START:
        OtherInitSPRT();
        break;

    // Right before we restart, this is called

    case RAM_STOP:
        break;

    default:
        break;
}
END_RAMLINK
