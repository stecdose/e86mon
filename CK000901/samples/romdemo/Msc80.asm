	page	60, 132
	name	msc80
	title	Microsoft C/C++ 8.0 Startup Code

;
; Paradigm LOCATE Embedded System Startup Code for MSC/C++ 8.0, Version 5.11
; Copyright (C) 1995 Paradigm Systems.  All rights reserved.
;
; *********************************************************************
; Permission to modify and distribute object files based on this
; source code is granted to licensed users of Paradigm LOCATE.
;
; Under no circumstances is this source code to be re-distributed
; without the express permission of Paradigm Systems.
; *********************************************************************
;
; Make sure that this startup module is specified first when linking
; the application.  Requires MASM 6.1 or later to assemble.
;
; 	masm /mx /D__MDL__ [ /DPDREMOTE ] msc80.asm
; 	ml /c /Cx /D__MDL__ [ /DPDREMOTE ] msc80.asm
;
; where
; 	__MDL__ is one of the following:
;
; 	__S__	Small memory model
; 	__C__	Compact memory model
; 	__M__	Medium memory model
; 	__L__	Large memory model
; 	__H__	Huge memory model
;
; If no memory model is specified, the small memory model is
; assumed and a warning is issued.
;
; The symbol PDREMOTE should be defined if the application will be
; debugged with the Paradigm PDREMOTE kernel to permit exiting to the
; debugger or intercepting exceptions.
;
; FAR_DATA/FAR_BSS support is not included in this startup module.  If
; needed, include CINIT.ASM with FARDATA defined.
;
	
	INCLUDE	startup.inc		; Macros/assembly language definitions
	INCLUDE	msc80.inc		; Compiler-specific definitions

	SHOW	<Paradigm LOCATE Microsoft C/C++ 8.0 Startup Support>
IFDEF	PDREMOTE
	SHOW	<Building Paradigm PDREMOTE-compatible startup code>
ENDIF	;	PDREMOTE

	subttl	Segment ordering/alignment section
	page
;
; Segment and group declarations.  The order of these declarations is
; used to control the order of segments in the .ROM file since the
; Microsoft linker copies segments in the same order they are
; encountered in the object files.
;
; Make sure that this startup module is specified first when linking
; the application.
;

DefSeg	_text,    para,  public,  CODE,	       <>	; Default code


;
; The following segments form the Microsoft C/C++ DGROUP.  The startup code
; requires that the segments in DGROUP follow the Microsoft C/C++ standard.
; The order of these segments/classes must not be changed since the
; startup code assumes a specific relationship between classes.
;

DefSeg	_data,    para,  public,  DATA,        DGROUP	; Initialized data
DefSeg	xiheap,   word,  common,  DATA,        DGROUP	; Heap initialization
DefSeg	const,    word,  public,  CONST,       DGROUP	; Constant data
DefSeg	hdr,      word,  public,  MSG,         DGROUP	; Misc. data
DefSeg	_bss,     word,  public,  BSS,         DGROUP	; Uninitialized data
DefSeg	c_common, word,  public,  BSS,         DGROUP	; Uninitialized data
DefSeg	_stack,   para,  stack,   STACK,       DGROUP	; Program stack


;
; The following segments are used to mark the place for ROM copies
; of initialized data for use by the startup code.
;

DefSeg	_brd,     para,  public,  ROMDATA,       <>	; Copy of initialized data
DefSeg	_erd,     para,  public,  ENDROMDATA,    <>


;
; External references
;
ExtProc	main				; User application entry point
ExtProc	_cinit				; C/C++ initializers/constructors
		
public	__acrtused			; Object modules reference this symbol
	__acrtused = 9876h		; to pull in startup code
public	__aDBused			; Created when the /qc option is used
	__aDBused = 0d6d6h

	subttl	Startup/initialization code
	page
_text	segment
	assume	cs:_text

BegProc	_startup, far			; startup code entry point
IFDEF	PDREMOTE
;
; Check if the segment registers have been setup by DOS.  PDREMOTE always
; clears the DS/ES segment registers so it can be used to check if the
; application is accidentally running under MS-DOS instead of PDREMOTE.
;
	mov	ax, es
	or	ax, ax
	jz	embedded

	push	cs
	pop	ds
	assume	ds:_text
	mov	dx, offset errmsg
	mov	ah, 09h
	int	21h
	mov	ah, 04ch
	int	21h
errmsg	db	"Embedded application: DOS not supported", 0ah, 0dh, '$'
embedded:
ENDIF	; PDREMOTE

;
; Disable interrupts and force the forward direction
;
	cli
	cld

;
; Initialize the stack segment and pointer registers to point to the
; default stack.
;
	mov	ss, cs:_dataseg
	mov	sp, offset DGROUP:_tos
	assume	ss:DGROUP

;
; Prepare the segment registers for initialization.  The initialized
; data is assumed to have been located in the class ROMDATA which begins
; with the segment _BRD.
;
	mov	es, cs:_dataseg
	mov	ax, _brd
	mov	ds, ax

;
; Copy DGROUP initialized data from its position in ROM to the target
; address in RAM.  Because this is a group, there is never more than
; 64K of data to copy.
;
	mov	si, offset brdata
	mov	di, offset DGROUP:idata
	mov	cx, offset DGROUP:bdata
	sub	cx, di
	jcxz	@F
	shr	cx, 1
	rep	movsw
@@:

;
; Zero out the BSS area
;
	mov	es, cs:_dataseg
	assume	es:DGROUP

	xor	ax, ax
	mov	di, offset DGROUP:bdata
	mov	cx, offset DGROUP:_edata
	sub	cx, di
	jcxz	@F
	shr	cx, 1
	rep	stosw
@@:

;
; Setup the segment register conventions that will be used to call the
; Microsoft C/C++ main() function.
;
	mov	ds, cs:_dataseg
	mov	es, cs:_dataseg
	assume	ds:DGROUP, es:DGROUP

;
; Near/far heap initialization is performed here since the stack is
; modified to initialize the near heap.  This guarantees that only one
; level of function nesting is active when the initializer is called.
;
	mov	cx, [DGROUP:xinheap]
	jcxz	@F
	call	cx
@@:

;
; Execute C/C++ initializers, if any
;
	xor	bp, bp
	call	_cinit

;
; Call the C/C++ entry point main() - initialization is complete and user
; application can take control.
;
	sti
	call	main

;
; A return from main() is application dependent.  If PDREMOTE is being used,
; we return the contents of AX as the application exit code; otherwise,
; the user must decide how to handle this condition.
;
IFNDEF	PDREMOTE
	jmp	_startup		; *** Application dependent ***
ELSE
;
; Hand over control to the exit handler, passing any exit code returned
; by the application.
;
	and	ax, 7fh
	push	ax
	xor	bx, bx
	mov	es, bx
	pushf
	push	bx
	push	bx
	jmp	dptr es:[000ch]
ENDIF	;	PDREMOTE
EndProc	_startup

GlobalW	_dataseg, DGROUP		; Used to access DGROUP value

_text	ends

	subttl	Data declarations
	page
_data	segment
public	idata
idata	label	byte			; Mark the start of the DATA class
null	dw	055aah			; NULL address (within the segment)
_data	ends

xiheap	segment
xinheap	label	word			; Near heap initializer offset
	dw	0			; Override via linked in module
xiheap	ends

_bss	segment
public	bdata
bdata	label	byte			; Mark the start of the BSS class
_bss	ends

;
; The following segments are used to mark the place for ROM copies
; of initialized data for use by the startup code.  We need two sets of
; classes, a pair to hold the place of the near initialized data and
; another pair to hold the place of the for the far initialized data.
;
_brd	segment
brdata	label	byte			; Mark the start of the ROMDATA class
_brd	ends
_erd	segment
	db	16 dup (?)		; Force the next segment to a new frame
_erd	ends

;
; The stack segment differs from the Microsoft DOS startup code in that
; the stack is assigned a fixed length.
;
_stack	segment
LabelW	_edata, public
IFDEF	STKSIZE
	db	STKSIZE dup (?)
ELSE
	db	8192 dup (?)
	SHOW	<No stack size specified: stack size set to 8KB>
ENDIF	;	STKSIZE
LabelW	_tos, public			; Mark the top of stack
_stack	ends

	end	_startup
