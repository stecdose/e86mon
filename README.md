# AMD's e86mon          

A quite useful and user friendly monitor for Am186 and possibly other 80186/8086 family chips.

A huge drawback is it's horrible spaghetti style and the old toolchain used.

* **CK000900** -- v3.4.0  
* **CK000901** --  v3.4.2 
* **my-e86mon** -- v3.5.0 my working-copy, a lot of changes
* **hexfiles** -- various precompiled hexfiles, main purpose is to have em at hand

# Building it
All the tools are needed did eat up a lot of money back then, but luckily today we find ISOs and floppy images for everything on the internet all around.
You will need a DOS 5.0-6.22 system/<=Windows98, due to fucked up batch files, otherwise almost every DOS or Windows would do. Also that old MSVC makes trouble on newer systems.
A more or less "rock stable" dev box for this job I have is:
* Pentium 2/233MHz Laptop, 256MB RAM
* a real serial port
* Windows 98 SE + nusb for USB mass storage
* 8GB HDD (OK but short in space, a 20GB disk would be much better)

My recommended source for all the tools and operating systems is https://winworldpc.com/

You will need an operating system:
* Either MS-DOS 6.22 and Winows 3.11 disks 
* or Windows 95 CD (+ a bootdisk with CD drivers or DOS+CD drivers)
* or Windows 98 CD (this one is bootable)

And the tools:
* MASM 6.11 Microsoft Assembler disks
* MS Visual C++ 1.52 CD
* a serial Terminal (I use realterm on W98Se)

I also successfully tried  dosbox and windows 3.11, but building crashed dosbox regularily.

A virtual system using VMWare, VirtualBox, VirtualPC, etc also works fine.
Also I recommend using total commander and notepad++ for development, I do not know about any ready to use IDE for this job. So these tools are enough.

